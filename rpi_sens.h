/*  (c) Copyright:  2020..2024  Patrn, Confidential Data
 *
 *  Workfile:           rpi_sens.h
 *  Purpose:            Headerfile for rpi_sens.c
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    01 Nov 2020:      Ported from rpislim
 *    23 Apr 2024:      Split globals.h and locals.h
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _RPI_SENS_H_
#define _RPI_SENS_H_

typedef enum _sens_ids_
{
#define EXTRACT_SENS(a,b,c,d) a,
#include "sens.h"
   //
   SENS_TIMEDATE,
   MAX_NUM_SENS   
#undef   EXTRACT_SENS
}  SENSID;

//
// Smart Sensor data
//
typedef bool(*PFFUN)(char *, SENS *);
// 
// Smart Sensor record for RPi #6 (M=mandatory, C=Configurable)
//  
// "Dist=043e, Flags=01, Count=0040, Pump=1, X10=0, Temp=0000, Solar=0000, Time=00347e, Mode=00, Sensor=v1.04-064!3a\n\r"
//  M          M         M           M       C      C          C           M            M        M
//  
#define MANDATORY_DATA        7
//
typedef struct _smart_definition_
{
   char       *pcSensor;
   PFFUN       pfHandler;
   const char *pcInfo;
}  SMDEF;

//
// Prototypes
//
bool  SENS_LookupCode            (char *, SENS *);

#endif /* _RPI_SENS_H_ */
