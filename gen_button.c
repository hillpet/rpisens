/*  (c) Copyright:  2020..2024  Patrn, Confidential Data
 *
 *  Workfile:           gen_button.c
 *  Purpose:            Interface Simple Buttons functions
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    08 Feb 2018:      Created
 *    05 Oct 2020:      Add G_stLog
 *    29 Mar 2022:      Add Variadic macro's for PRINTF()
 *    24 Apr 2024:      Move to global/local startup 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
//
#include <common.h>
#include "config.h"
#include "locals.h"
#include "globals.h"
//
#include "gen_button.h"

//#define USE_PRINTF
#include <printx.h>


//
// Local prototypes/data
//
static void gen_SetupGpio        (void);

/*
 * Function    : GEN_Init
 * Description : Init Simple key handler
 *
 * Parameters  : 
 * Returns     : TRUE if OKee
 *
 */
bool GEN_Init(void)
{
   gen_SetupGpio();
   return(TRUE);
}

/*
 * Function    : GEN_Exit
 * Description : Exit Simple key handler
 *
 * Parameters  : 
 * Returns     : TRUE if OKee
 * Note        : Turn Red and Green LEDs OFF
 *
 */
bool GEN_Exit(void)
{
   LED(LED_Y, 0);
   LED(LED_G, 0);
   return(TRUE);
}


/*------  Local functions separator ------------------------------------
__LOCAL_FUNCTIONS_(){};
----------------------------------------------------------------------------*/

/*
 * Function    : gen_SetupGpio()
 * Description : Setup GPIO for various IO alternatives
 *
 * Parameters  : 
 * Returns     : 
 *
 */
static void gen_SetupGpio()
{
   int   iCc;

   PRINTF("gen_SetupGpio()" CRLF);
   //
   iCc = SETUP_GPIO();
   if(iCc == -1)
   {
      LOG_Report(0, "BTN", "gen_SetupGpio(): ERROR: GPIO Setup");
      LOG_printf("gen_SetupGpio(): GPIO Setup failed!" CRLF);
   }
   OUT_GPIO(LED_Y);
   OUT_GPIO(LED_G);
   //
   // Set initial values
   //
   LED(LED_Y, 1);
   LED(LED_G, 1);
   //
   PRINTF("gen_SetupGpio(): Ready" CRLF);
}


