/*  (c) Copyright:  2020..2024 Patrn, Confidential Data 
 *
 *  Workfile:           gen_http.h
 *  Purpose:            Generic HTTP functions
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    01 Nov 2020:      Ported from rpislim
 *    24 Apr 2024:      Move to global/local startup 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _GEN_HTTP_H_
#define _GEN_HTTP_H_

//
// Dynamic page registered URLs
//
typedef bool(*PFVOIDPI)(void *, int);
//
char    *GEN_FindDynamicPageName       (int);
int      GEN_GetDynamicPageUrl         (int);
char    *GEN_GetDynamicPageName        (int);
int      GEN_GetDynamicPagePid         (int);
int      GEN_GetDynamicPagePort        (int);
int      GEN_GetDynamicPageTimeout     (int);
FTYPE    GEN_GetDynamicPageType        (int);
PFVOIDPI GEN_GetDynamicPageCallback    (int);
//
int      GEN_RegisterDynamicPage       (int, int, FTYPE, int, int, const char *, PFVOIDPI);


#endif /* _GEN_HTTP_H_ */
