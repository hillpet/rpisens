/*  (c) Copyright:  2024  Patrn ESS, Confidential Data
 *
 *  Workfile:           cmd_debug.h
 *  Purpose:            Extract file for Debug commands
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    18 Apr 2024:      Ported from rpiocr
 *    24 Apr 2024:      Move to global/local startup 
 *    09 May 2024:      Add Email
 *    
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 **/

//          eDbCmd            iCmd  pfDbCmd                       pcHelp               
EXTRACT_DEB(DB_CMD_LF_0,      ' ',  dbg_Linefeed,                 ""                               )
EXTRACT_DEB(DB_CMD_QUEST,     '?',  dbg_SetDebugLevel,            "SYS: Set debug level"           )
EXTRACT_DEB(DB_CMD_LO_M,      'm',  dbg_MallocBalance,            "SYS: Malloc balance"            )
EXTRACT_DEB(DB_CMD_LO_Q,      'q',  dbg_Quit,                     "SYS: Quit debug thread"         )
EXTRACT_DEB(DB_CMD_UP_Q,      'Q',  dbg_QuitTerminate,            "SYS: Quit ALL threads"          )
EXTRACT_DEB(DB_CMD_LO_V,      'v',  dbg_SetVerboseLevel,          "SYS: Set verbose level"         )
EXTRACT_DEB(DB_CMD_LO_H,      'h',  dbg_Help,                     "SYS: Help"                      )
EXTRACT_DEB(DB_CMD_UP_L,      'L',  dbg_TruncateLogfile,          "SYS: Truncate LOG"              )
EXTRACT_DEB(DB_CMD_UP_E,      'E',  dbg_SendEmail,                "SYS: Sens Email"                )
//
EXTRACT_DEB(DB_CMD_LF_1,      ' ',  dbg_Linefeed,                 "===  uController ==============")
EXTRACT_DEB(DB_CMD_UC_1,      '>',  dbg_ControllerModeOn,         "RUN: uController mode ON"       )
EXTRACT_DEB(DB_CMD_UC_0,      '<',  dbg_ControllerModeOff,        "RUN: uController mode OFF"      )
EXTRACT_DEB(DB_CMD_UP_T,      'T',  dbg_ControllerHiWater,        "RUN: uController Hi Water"      )
EXTRACT_DEB(DB_CMD_LO_T,      't',  dbg_ControllerLoWater,        "RUN: uController Lo Water"      )
EXTRACT_DEB(DB_CMD_UP_X,      'X',  dbg_ControllerTransfer,       "RUN: uController transfer"      )
//
EXTRACT_DEB(DB_CMD_LF_2,      ' ',  dbg_Linefeed,                 "===  STDOUT data ==============")
EXTRACT_DEB(DB_CMD_PERD,      '.',  dbg_DebugNoPrint,             "OUT: All prints OFF"            )
EXTRACT_DEB(DB_CMD_UP_C,      'C',  dbg_DebugCommsPrint,          "OUT: on/off Comms print"        )
EXTRACT_DEB(DB_CMD_UP_G,      'G',  dbg_DebugGuardPrint,          "OUT: on/off Guard print"        )
EXTRACT_DEB(DB_CMD_UP_H,      'H',  dbg_DebugHostPrint,           "OUT: on/off Host print"         )
EXTRACT_DEB(DB_CMD_UP_M,      'M',  dbg_DebugMailPrint,           "OUT: on/off Mail print"         )
EXTRACT_DEB(DB_CMD_UP_S,      'S',  dbg_DebugSensorPrint,         "OUT: on/off Sensor print"       )
EXTRACT_DEB(DB_CMD_UP_P,      'P',  dbg_DebugGenericPrint,        "OUT: on/off Generic print"      )
EXTRACT_DEB(DB_CMD_UP_R,      'R',  dbg_DebugRawSerialPrint,      "OUT: on/off uCtl raw print"     )
//
EXTRACT_DEB(DB_CMD_LF_3,      ' ',  dbg_Linefeed,                 "===============================")
