/*  (c) Copyright:  2020..2024  Patrn, Confidential Data
 *
 *  Workfile:           par_pids.h
 *  Purpose:            Extract headerfile for the RPI thread PIDs
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    01 Nov 2020:      Ported from rpislim
 *    25 Jun 2022:      Add Guard thread
 *    18 Apr 2024:      Add Debug thread
 *    23 Apr 2024:      Split globals.h and locals.h
 *    09 May 2024:      Add MailX client
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 **/

//          ePid           pcPid          pcHelp
EXTRACT_PID(PID_HOST,      "Host   ",     "Host"         )
EXTRACT_PID(PID_SRVR,      "Server ",     "HTTP server"  )
EXTRACT_PID(PID_COMM,      "Comms  ",     "Serial Comms" )
EXTRACT_PID(PID_GUARD,     "Guard  ",     "Guard"        )
EXTRACT_PID(PID_DEBUG,     "Debug  ",     "Debug"        )
EXTRACT_PID(PID_MAILX,     "Mailx  ",     "Mailx client" )
EXTRACT_PID(PID_SPARE1,    "Spare1 ",     "Spare1"       )
EXTRACT_PID(PID_SPARE2,    "Spare2 ",     "Spare2"       )

