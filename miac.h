/*  (c) Copyright:  2020..2024  Patrn, Confidential Data
 *
 *  Workfile:           miac.h
 *  Purpose:            Macro def file smart sensor RAM/EEPROM Register memory
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    30 Nov 2020:      Created
 *    23 Apr 2024:      Split globals.h and locals.h
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

//
// Macro:
//       
//             Enum                       pcSymbol             pfHandler               pcInfo
EXTRACT_MIAC(  MIAC_R_TASK01,             "b0Task01",          miac_AddressRam,        "Pump Control"                               )
EXTRACT_MIAC(  MIAC_R_TASK02,             "b0Task02",          miac_AddressRam,        "Ultrasoon Sensor Control"                   )
EXTRACT_MIAC(  MIAC_R_TASK03,             "b0Task03",          miac_AddressRam,        "Input Switch 1&2 "                          )
EXTRACT_MIAC(  MIAC_R_TASK04,             "b0Task04",          miac_AddressRam,        "LED images"                                 )
EXTRACT_MIAC(  MIAC_R_TASK05,             "b0Task05",          miac_AddressRam,        "EEPROM store"                               )
EXTRACT_MIAC(  MIAC_R_TASK06,             "b0Task06",          miac_AddressRam,        "MIAC handler"                               )
//
EXTRACT_MIAC(  MIAC_E_EEHIWATERHI,        "EeHiWaterHi",       miac_AddressRom,        "Default value (hi byte)"                    )
EXTRACT_MIAC(  MIAC_E_EEHIWATERLO,        "EeHiWaterLo",       miac_AddressRom,        "Default value (lo byte)"                    )
EXTRACT_MIAC(  MIAC_E_EELOWATERHI,        "EeLoWaterHi",       miac_AddressRom,        "Default value (hi byte)"                    )
EXTRACT_MIAC(  MIAC_E_EELOWATERLO,        "EeLoWaterLo",       miac_AddressRom,        "Default value (lo byte)"                    )
EXTRACT_MIAC(  MIAC_E_EESYSFLAGS,         "EeSysFlags",        miac_AddressRom,        "Persistent flags (comSysFlags)"             )
EXTRACT_MIAC(  MIAC_E_EETHCOUNTS,         "EeThCounts",        miac_AddressRom,        "Default water max counts before pump ON"    )
EXTRACT_MIAC(  MIAC_E_EESAMNUM,           "EeSamNum",          miac_AddressRom,        "Samples (1,2,4,8,16,32,64 or 128)"          )
EXTRACT_MIAC(  MIAC_E_EESAMRATE,          "EeSamRate",         miac_AddressRom,        "Sample rate in 100 mSecs ticks"             )
EXTRACT_MIAC(  MIAC_E_EETMRPUMPON,        "EeTmrPumpOn",       miac_AddressRom,        "Timer duration Pump ON"                     )
EXTRACT_MIAC(  MIAC_E_EETMRPUMPOFF,       "EeTmrPumpOff",      miac_AddressRom,        "Timer duration Pump OFF"                    )
EXTRACT_MIAC(  MIAC_E_EEPOWCNTSHI,        "EePowCntsHi",       miac_AddressRom,        "Power cycles High"                          )
EXTRACT_MIAC(  MIAC_E_EEPOWCNTSLO,        "EePowCntsLo",       miac_AddressRom,        "Power cycles Low"                           )
EXTRACT_MIAC(  MIAC_E_EEUARTOERHI,        "EeUartOErHi",       miac_AddressRom,        "UART Overrun High"                          )
EXTRACT_MIAC(  MIAC_E_EEUARTOERLO,        "EeUartOErLo",       miac_AddressRom,        "UART Overrun Low"                           )
EXTRACT_MIAC(  MIAC_E_EEOFLCNTSHI,        "EeOflCntsHi",       miac_AddressRom,        "Overflow counts High"                       )
EXTRACT_MIAC(  MIAC_E_EEOFLCNTSLO,        "EeOflCntsLo",       miac_AddressRom,        "Overflow counts Low"                        )

