/*  (c) Copyright:  2020..2024  Patrn, Confidential Data
 *
 *  Workfile:           slimp.h
 *  Purpose:            Extract headerfile for Kinshi Ultrasoon sensor
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    01 Nov 2020:      Ported from rpislim
 *    23 Apr 2024:      Split globals.h and locals.h
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

EXTRACT_DYN(DYN_SME_NONE_LIVE,   HTTP_HTML,  0, DYN_FLAG_PORT, "meter",                slim_DynPageLiveData          )
EXTRACT_DYN(DYN_SME_HTML_LIVE,   HTTP_HTML,  0, DYN_FLAG_PORT, "meter.html",           slim_DynPageLiveData          )
EXTRACT_DYN(DYN_SME_JSON_LIVE,   HTTP_JSON,  0, DYN_FLAG_PORT, "meter.json",           slim_DynPageLiveData          )
EXTRACT_DYN(DYN_SME_HTML_DAY,    HTTP_HTML,  0, DYN_FLAG_PORT, "smartday.html",        slim_DynPageDay               )
EXTRACT_DYN(DYN_SME_NONE_DAYSLM, HTTP_HTML,  0, DYN_FLAG_PORT, "slim",                 slim_DynPageDaySmart          )
EXTRACT_DYN(DYN_SME_HTML_DAYSLM, HTTP_HTML,  0, DYN_FLAG_PORT, "slim.html",            slim_DynPageDaySmart          )
EXTRACT_DYN(DYN_SME_HTML_MON,    HTTP_HTML,  0, DYN_FLAG_PORT, "slimmaand.html",       slim_DynPageMonth             )
EXTRACT_DYN(DYN_SME_JSON_MON,    HTTP_JSON,  0, DYN_FLAG_PORT, "slimmaand.json",       slim_DynPageMonth             )
EXTRACT_DYN(DYN_SME_HTML_EXPORT, HTTP_HTML,  0, DYN_FLAG_PORT, "slimexport.html",      slim_DynPageSlimExport        )
EXTRACT_DYN(DYN_SME_NONE_MIACE,  HTTP_JSON,  0, DYN_FLAG_PORT, "rom",                  slim_DynPageMiacRom           )
EXTRACT_DYN(DYN_SME_JSON_MIACE,  HTTP_JSON,  0, DYN_FLAG_PORT, "rom.json",             slim_DynPageMiacRom           )
EXTRACT_DYN(DYN_SME_HTML_MIACE,  HTTP_HTML,  0, DYN_FLAG_PORT, "rom.html",             slim_DynPageMiacRom           )
EXTRACT_DYN(DYN_SME_NONE_MIACR,  HTTP_JSON,  0, DYN_FLAG_PORT, "ram",                  slim_DynPageMiacRam           )
EXTRACT_DYN(DYN_SME_JSON_MIACR,  HTTP_JSON,  0, DYN_FLAG_PORT, "ram.json",             slim_DynPageMiacRam           )
EXTRACT_DYN(DYN_SME_HTML_MIACR,  HTTP_HTML,  0, DYN_FLAG_PORT, "ram.html",             slim_DynPageMiacRam           )
EXTRACT_DYN(DYN_SME_NONE_MIAC,   HTTP_JSON,  0, DYN_FLAG_PORT, "miac",                 slim_DynPageMiac              )
EXTRACT_DYN(DYN_SME_JSON_MIAC,   HTTP_JSON,  0, DYN_FLAG_PORT, "miac.json",            slim_DynPageMiac              )
EXTRACT_DYN(DYN_SME_HTML_MIAC,   HTTP_HTML,  0, DYN_FLAG_PORT, "miac.html",            slim_DynPageMiac              )
EXTRACT_DYN(DYN_SME_NONE_SENS,   HTTP_JSON,  0, DYN_FLAG_PORT, "sensor",               slim_DynPageSensor            )
EXTRACT_DYN(DYN_SME_JSON_SENS,   HTTP_JSON,  0, DYN_FLAG_PORT, "sensor.json",          slim_DynPageSensor            )
EXTRACT_DYN(DYN_SME_HTML_SENS,   HTTP_HTML,  0, DYN_FLAG_PORT, "sensor.html",          slim_DynPageSensor            )
//
// LINK related HTML pages
//
EXTRACT_DYN(DYN_SME_HTML_MONUP,  HTTP_HTML,  0, DYN_FLAG_PORT, "smartmonthup.html",    slim_DynPageMonthScaleUpHtml  )
EXTRACT_DYN(DYN_SME_HTML_MONDN,  HTTP_HTML,  0, DYN_FLAG_PORT, "smartmonthdn.html",    slim_DynPageMonthScaleDnHtml  )
EXTRACT_DYN(DYN_SME_HTML_DAYPRV, HTTP_HTML,  0, DYN_FLAG_PORT, "smartprevday.html",    slim_DynPageDayPrevHtml       )
EXTRACT_DYN(DYN_SME_HTML_DAYNXT, HTTP_HTML,  0, DYN_FLAG_PORT, "smartnextday.html",    slim_DynPageDayNextHtml       )
EXTRACT_DYN(DYN_SME_HTML_DAYUP,  HTTP_HTML,  0, DYN_FLAG_PORT, "smartdayup.html",      slim_DynPageDayScaleUpHtml    )
EXTRACT_DYN(DYN_SME_HTML_DAYDN,  HTTP_HTML,  0, DYN_FLAG_PORT, "smartdaydn.html",      slim_DynPageDayScaleDnHtml    )
EXTRACT_DYN(DYN_SME_HTML_NXTSLM, HTTP_HTML,  0, DYN_FLAG_PORT, "slimnext.html",        slim_DynPageDaySmartNextHtml  )
EXTRACT_DYN(DYN_SME_HTML_PRVSLM, HTTP_HTML,  0, DYN_FLAG_PORT, "slimprev.html",        slim_DynPageDaySmartPrevHtml  )
EXTRACT_DYN(DYN_SME_HTML_PRVMON, HTTP_HTML,  0, DYN_FLAG_PORT, "smartprevmonth.html",  slim_DynPageMonthPrevHtml     )
EXTRACT_DYN(DYN_SME_HTML_NXTMON, HTTP_HTML,  0, DYN_FLAG_PORT, "smartnextmonth.html",  slim_DynPageMonthNextHtml     )
