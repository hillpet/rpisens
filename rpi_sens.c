/*  (c) Copyright:  2020..2024  Patrn, Confidential Data 
 *
 *  Workfile:           rpi_sens.c
 *  Purpose:            Smart Sensor support
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PIC 16f628A Ultrasoon sensor
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    01 Nov 2020:      Ported from rpislim
 *    11 Nov 2020:      Add Sensor specific functions
 *    24 Nov 2020:      Add Pump Mode
 *    04 May 2021:      Check for inlikely distance measurements
 *    30 May 2021:      Add System Flags and Dropped samples to record
 *    29 Mar 2022:      Add Variadic macro's for PRINTF()
 *    24 Apr 2024:      Move to global/local startup 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sched.h>
#include <time.h>
//
#include <common.h>
#include "config.h"
#include "locals.h"
#include "globals.h"
//
#include "rpi_sens.h"

//#define USE_PRINTF
#include <printx.h>

//
// Local generic prototypes
//
static bool    sens_ConvertToInt             (char *, char, int *);
static char   *sens_FindValueStart           (char *, char);
//
// Local prototypes
//
static bool    sens_AdmCounts                (char *, SENS *);
static bool    sens_AdmDistance              (char *, SENS *);
static bool    sens_AdmDropped               (char *, SENS *);
static bool    sens_AdmEquipment             (char *, SENS *);
static bool    sens_AdmPumpMode              (char *, SENS *);
static bool    sens_AdmPumpOn                (char *, SENS *);
static bool    sens_AdmX10Bypass             (char *, SENS *);
static bool    sens_AdmSolarStrength         (char *, SENS *);
static bool    sens_AdmSystemFlags           (char *, SENS *);
static bool    sens_AdmTemperature           (char *, SENS *);
static bool    sens_AdmTimeInSeconds         (char *, SENS *);

//
//    340 m     = 1 sec
//    340000 mm = 1000000 uSec
//
//                     1000000
//         1 mm = 2 * --------- = 5.88235 uSec
//                     340000
//
//     Distance = TimerTicks * 0.4 uSecs /5.88235 mm
//              = TimerTicks * 0.06800 mm
//
const double flTimerTickPerMm = 14.7059;
const double flMmPerTimerTick = 0.06800;
//
//
static const SMDEF stSensorDefinition[] =
{
#define EXTRACT_SENS(a,b,c,d) {b,c,d},
#include "sens.h"
#undef   EXTRACT_SENS
};
static int iSensorDefinitions = sizeof(stSensorDefinition)/sizeof(SMDEF);


/*------  Local functions separator ------------------------------------
__GLOBAL_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//  
// Function:   SENS_LookupCode
// Purpose:    Lookup the SENSOR code from the list and run the handler function
//
// Parameters: SENSOR code buffer ptr, SENS info struct ptr
// Returns:    TRUE if mandatory data found
// Note:       pcCode contains Smart Sensor record for RPi #6 (M=mandatory, C=Configurable)
//  
//             "Dist=043e, Flags=01, Count=0040, Pump=1, X10=0, Temp=0000, Solar=0000, Time=00347e, Mode=00, Sensor=v1.04-064!3a23"
//              M          M         M           M       C      C          C           M            M        M
//  
bool SENS_LookupCode(char *pcCode, SENS *pstSens)
{
   bool        fCc=FALSE;
   int         iNr=0, tSensor;
   const char *pcRes;
   char       *pcVar;
   PFFUN       pfFun;

   //
   // Init NON-essential data in case they are not supplied
   //
   pstSens->iDropped   = 0;
   pstSens->iX10Active = 0; 
   pstSens->iX10Bypass = 1;
   pstSens->iSysFlags  = 0;
   //
   for(tSensor=0; tSensor<iSensorDefinitions; tSensor++)
   {
      pcRes = stSensorDefinition[tSensor].pcSensor;
      PRINTF("SENS-LookupCode():Id=%2d: Find [%s]" CRLF, tSensor, pcRes);
      if( (pcVar = GEN_STRSTRI(pcCode, pcRes)) )
      {
         //
         // SENS code found
         //
         PRINTF("SENS-LookupCode():Found: Id=%2d" CRLF, tSensor);
         pfFun = stSensorDefinition[tSensor].pfHandler;
         if(pfFun)
         {
            pstSens->tSensor = tSensor;
            if(pfFun(pcVar, pstSens))
            {
               // Keep track we have all the mandatory data
               iNr++;
               PRINTF("SENS-LookupCode():Found %d of %d" CRLF, iNr, MANDATORY_DATA);
            }
         }
      }
   }
   if(iNr >= MANDATORY_DATA) fCc = TRUE;
   return(fCc);
}

/*------  Local functions separator ------------------------------------
__LOCAL_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//  
// Function:   sens_ConvertToInt
// Purpose:    Convert a hex ASCII string to integer
// 
// Parameters: SENS code, value delimiter, integer ptr
// Returns:    TRUE if OKee
// Note:       
//  
static bool sens_ConvertToInt(char *pcCode, char cDelim, int *piResult)
{
   bool        fCc=FALSE;
   const char *pcInt;

   pcInt = sens_FindValueStart(pcCode, cDelim);
   if(pcInt)
   {
      *piResult = (int) strtol(pcInt, NULL, 16);
      fCc = TRUE;
   }
   return(fCc);
}

//  
// Function:   sens_FindValueStart
// Purpose:    Find the starting point of a value in the SENS record
// 
// Parameters: Record ptr, delimiter char
// Returns:    Value ptr
// Note:       
//  
static char *sens_FindValueStart(char *pcCode, char cDelim)
{
   char *pcValue=NULL;

   while(*pcCode)
   {
      if(*pcCode++ == cDelim)
      {
         pcValue = pcCode;
         break;
      }
   }
   return(pcValue);
}


/*------  Local functions separator ------------------------------------
__SENSOR_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//  
// Function:   sens_AdmCounts
// Purpose:    Handle this SENSOR code
//             "Count=xx"
// Parameters: Sensor code "xxxx", SENS struct ptr
// Returns:    TRUE if Mandatory data
// Note:       The Smart Sensor (Pic-16f628A) will increment this counter
//             on each UNINTERRUPTED ultrasoon distance measurement. If each 
//             and every sample is interrupted (by any IRQ) this counter shows 
//             that no new data has been acquired if it reflects the same value
//             as in the previous record.
//  
static bool sens_AdmCounts(char *pcCode, SENS *pstSens)
{
   bool  fCc;
   int   iVal;

   fCc = sens_ConvertToInt(pcCode, '=', &iVal);
   if(fCc)  
   {
      pstSens->iCounts = iVal;
      PRINTF("sens-AdmCounts():%d" CRLF, iVal);
   }
   else PRINTF("sens-AdmCounts():BAD counts" CRLF);
   //
   return(fCc);
}

//  
// Function:   sens_AdmDistance
// Purpose:    Handle this SENSOR code
//             "Dist=2a4f"
// Parameters: Sensor code "xxxx", SENS struct ptr
// Returns:    TRUE if Mandatory data
// Note:       Returned Smart Sensor distance is in HEX value
//             Rework the sensor value into distance from sensor to waterlevel:
//             
//             HC-SR04 Ultrasoon Sensor                                    
//             ----------------------------------------                            
//             Speed of sound:   340    m/s     
//             Factor:           2 * (1000000/340000) = 
//                               5,8824 uSec/mm  (to and from)
//             Timer Clock:      0,4    uSec    
//             Distance:         TimerTicks * 0.4 / 5,8824 mm
//                                         
//             Distance  Pulse Width   Timer Value
//               (cm)     (uSec)       (Dec Ticks)
//                0,0         0             0
//               10,0       588          1471
//               20,0      1176          2941
//               30,0      1765          4412
//               40,0      2353          5882
//               50,0      2941          7353
//               60,0      3529          8824
//               70,0      4118         10294
//               80,0      4706         11765
//               90,0      5294         13235
//               100,0     5882         14706
//  
static bool sens_AdmDistance(char *pcCode, SENS *pstSens)
{
   bool     fCc;
   int      iRes;
   double   flRes=0.0;

   fCc = sens_ConvertToInt(pcCode, '=', &iRes);
   if(fCc)
   {
      //
      // We have the HEX timer value: rework to distance
      //
      flRes = (double) iRes * flMmPerTimerTick;
      pstSens->flDistance = flRes;
      PRINTF("sens-AdmDistance():%5.3f" CRLF, flRes);
   }
   else PRINTF("sens-AdmDistance():BAD Distance" CRLF);
   return(fCc);
}

//  
// Function:   sens_AdmDropped
// Purpose:    Handle this SENSOR code
// 
// Parameters: Sensor code "xxxx", SENS struct ptr
// Returns:    TRUE if Mandatory data
// Note:       This field is not mandatory: 
//  
static bool sens_AdmDropped(char *pcCode, SENS *pstSens)
{
   bool  fCc;
   int   iVal;

   fCc = sens_ConvertToInt(pcCode, '=', &iVal);
   if(fCc)  
   {
      pstSens->iDropped = iVal;
      PRINTF("sens-AdmDropped():%d" CRLF, iVal);
   }
   else PRINTF("sens-AdmDropped():BAD counts" CRLF);
   //
   return(FALSE);
}

//  
// Function:   sens_AdmEquipment
// Purpose:    Handle this SENSOR code
// 
// Parameters: Sensor code "xxxx", SENS struct ptr
// Returns:    TRUE if Mandatory data
// Note:       Record-->"..., Sensor=02.0A!xxxx"
//             Version is in hex!
//
static bool sens_AdmEquipment(char *pcCode, SENS *pstSens)
{
   bool  fCc=FALSE;
   int   iLen=MAX_SENS;
   char *pcDest=pstSens->cEquipment;
   char *pcSrc;

   pcSrc = sens_FindValueStart(pcCode, '=');
   if(pcSrc)
   {
      while(iLen)
      {
         switch(*pcSrc)
         {
            case ',':
            case ' ':
            case '!':
               iLen = 0;
               break;

            default:
               *pcDest++ = *pcSrc++;
               iLen--;
               break;
         }
      }
      *pcDest = 0;
      fCc = TRUE;
   }
   return(fCc);
}

//  
// Function:   sens_AdmPumpMode
// Purpose:    Handle this SENSOR code
//             "Mode=xxx"
// Parameters: Sensor code "xxxx", SENS struct ptr
// Returns:    TRUE if Mandatory data
// Note:       
//  
static bool sens_AdmPumpMode(char *pcCode, SENS *pstSens)
{
   bool  fCc;
   int   iVal;

   fCc = sens_ConvertToInt(pcCode, '=', &iVal);
   if(fCc)
   {
      pstSens->iPumpMode = iVal;
   }
   else PRINTF("sens-AdmPumpMode():BAD Pump mode" CRLF);
   return(fCc);
}
//  
// Function:   sens_AdmPumpOn
// Purpose:    Handle this SENSOR code
//             "Pump=x"
// Parameters: Sensor code "xxxx", SENS struct ptr
// Returns:    TRUE if Mandatory data
// Note:       
//  
static bool sens_AdmPumpOn(char *pcCode, SENS *pstSens)
{
   bool  fCc;
   int   iOn;

   fCc = sens_ConvertToInt(pcCode, '=', &iOn);
   if(fCc)
   {
      if(iOn) 
      { 
         pstSens->iPumpOn  = 1; 
         pstSens->iPumpOff = 0;
         PRINTF("sens-AdmPumpOn():On" CRLF);
      }
      else    
      { 
         pstSens->iPumpOn  = 0; 
         pstSens->iPumpOff = 1;
         PRINTF("sens-AdmPumpOn():Off" CRLF);
      }
   }
   else PRINTF("sens-AdmPumpOn():BAD Pump on/off" CRLF);
   return(fCc);
}

//  
// Function:   sens_AdmX10Bypass
// Purpose:    Handle this SENSOR code
//             "X10=x"
// Parameters: Sensor code "xxxx", SENS struct ptr
// Returns:    TRUE if Mandatory data
// Note:       This field is not mandatory: 
//  
static bool sens_AdmX10Bypass(char *pcCode, SENS *pstSens)
{
   bool  fCc;
   int   iOn;

   fCc = sens_ConvertToInt(pcCode, '=', &iOn);
   if(fCc)
   {
      if(iOn) 
      { 
         pstSens->iX10Active = 0; 
         pstSens->iX10Bypass = 1;
         PRINTF("sens-AdmX10Bypass():Bypass" CRLF);
      }
      else    
      { 
         pstSens->iX10Active = 1; 
         pstSens->iX10Bypass = 0;
         PRINTF("sens-AdmX10Bypass():Actusl" CRLF);
      }
   }
   else PRINTF("sens-AdmX10Bypass():BAD X10 actual/bypass" CRLF);
   return(FALSE);
}

//  
// Function:   sens_AdmSolarStrength
// Purpose:    Handle this SENSOR code
//             "Solar=xxxx"
// Parameters: Sensor code "xxxx", SENS struct ptr
// Returns:    TRUE if Mandatory data
// Note:       This field is not mandatory: 
//  
static bool sens_AdmSolarStrength(char *pcCode, SENS *pstSens)
{
   return(FALSE);
}

//  
// Function:   sens_AdmSystemFlags
// Purpose:    Handle this SENSOR code
// 
// Parameters: Sensor code "xxxx", SENS struct ptr
// Returns:    TRUE if Mandatory data
// Note:       
//  
static bool sens_AdmSystemFlags(char *pcCode, SENS *pstSens)
{
   bool  fCc;
   int   iVal;

   fCc = sens_ConvertToInt(pcCode, '=', &iVal);
   if(fCc)  
   {
      pstSens->iSysFlags = iVal;
      PRINTF("sens-AdmSystemFlags():%d" CRLF, iVal);
   }
   else PRINTF("sens-AdmSystemFlags():BAD flags" CRLF);
   //
   return(fCc);
}

//  
// Function:   sens_AdmTemperature
// Purpose:    Handle this SENSOR code
//             "Temp=xx"
// Parameters: Sensor code "xxxx", SENS struct ptr
// Returns:    TRUE if Mandatory data
// Note:       This field is not mandatory: 
//  
static bool sens_AdmTemperature(char *pcCode, SENS *pstSens)
{
   bool     fCc;
   int      iVal;
   double   flVal=0.0;

   fCc = sens_ConvertToInt(pcCode, '=', &iVal);
   if(fCc)
   {
      //
      // Rework Hex temp reading to double temp xx.x C
      //
      flVal = ((double) iVal)/10;
   }
   else PRINTF("sens-AdmTemperature():BAD Temperature" CRLF);
   pstSens->flTemperature = flVal;
   return(FALSE);
}

//  
// Function:   sens_AdmTimeInSeconds
// Purpose:    Handle this SENSOR code
//             "Time=xxx"
// Parameters: Sensor code "xxxx", SENS struct ptr
// Returns:    TRUE if Mandatory data
// Note:       
//  
static bool sens_AdmTimeInSeconds(char *pcCode, SENS *pstSens)
{
   bool  fCc;
   int   iVal;

   fCc = sens_ConvertToInt(pcCode, '=', &iVal);
   if(fCc)
   {
      pstSens->iTimeInSecs = iVal;
   }
   else PRINTF("sens-AdmTimeInSecs():BAD Timestamp" CRLF);
   return(fCc);
}


