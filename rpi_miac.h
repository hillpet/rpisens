/*  (c) Copyright:  2020..2024  Patrn, Confidential Data
 *
 *  Workfile:           rpi_miac.h
 *  Purpose:            Headerfile for rpi_miac.c
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    30 Nov 2020:      Created
 *    23 Apr 2024:      Split globals.h and locals.h
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _RPI_MIAC_H_
#define _RPI_MIAC_H_

#define EXTRACT_MIAC(a,b,c,d) a,
//
typedef enum _miac_ids_
{
#include "miac.h"
   //
   MAX_NUM_MIAC,
   MIAC_NONE
#undef   EXTRACT_MIAC
}  MIACID;

typedef enum _miac_mode_
{
   MIAC_MODE_BANK  = 0,
   MIAC_MODE_ADDR,
   MIAC_MODE_DATA,
   MIAC_MODE_CSUM,
   MIAC_MODE_TYPE,
   MIAC_MODE_EXIT
}  MIMODE;

//
// Smart Sensor memory
//
typedef struct SMMEM
{
   MIACID      tMiacId;
   char       *pcSymbol;
   bool       (*pfHandler)(char *, u_int8 *);
   const char *pcInfo;
}  SMMEM;
//
typedef bool(*PFMEM)(char *, u_int8 *);
//
// Prototypes
//
bool     MIAC_GetEepromMemory       (MIACID, u_int8 *);
int      MIAC_GetEepromMemoryAddress(MIACID);
bool     MIAC_GetRamMemory          (MIACID, u_int8 *);
int      MIAC_GetRamMemoryAddress   (MIACID);
int      MIAC_Init                  (const char *);
bool     MIAC_LookupSymbols         (char *);
bool     MIAC_ParseRamMemory        (void);
bool     MIAC_ParseEepromMemory     (void);
bool     MIAC_SetEepromMemory       (MIACID, u_int8);
bool     MIAC_SetRamMemory          (MIACID, u_int8);

#endif /* _RPI_MIAC_H_ */
