/*  (c) Copyright:  2020..2024 Patrn, Confidential Data
 *
 *  Workfile:           rpi_guard.c
 *  Purpose:            This thread monitors all other rpisens threads
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PIC 16f628A Ultrasoon sensor
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    25 Jun 2022:      Created
 *    24 Apr 2024:      Move to global/local startup 
 *    25 Apr 2024:      Move guard duty and watchdog heartbeat to Guard thread
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <string.h>
#include <semaphore.h>
#include <stddef.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <resolv.h>
#include <sys/types.h>
#include <sys/signal.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <ctype.h>
#include <errno.h>
//
#include <common.h>
#include "config.h"
#include "locals.h"
#include "globals.h"
//
#include "rpi_main.h"
#include "rpi_func.h"
#include "rpi_guard.h"
//
//#define USE_PRINTF
#include <printx.h>

//
// Local functions
//
static int     grd_Execute                (void);
static bool    grd_CheckGuard             (u_int32);
static pid_t   grd_CheckSendHeartbeat     (u_int32, pid_t);
static int     grd_CheckThread            (PIDT);
static bool    grd_Restart                (void);
static bool    grd_WaitSignal             (int, int);
//
static bool    grd_SignalRegister         (sigset_t *);
static void    grd_ReceiveSignalSegmnt    (int);
static void    grd_ReceiveSignalInt       (int);
static void    grd_ReceiveSignalTerm      (int);
static void    grd_ReceiveSignalUser1     (int);
static void    grd_ReceiveSignalUser2     (int);

//
// Local arguments
//
static int         fThreadRunning   = TRUE;
static char        cDateTime[32];
static const char *pcRebootDelayed  = "shutdown -r 2 \"Reboot in 2 mins\" ";

/*----------------------------------------------------------------------
_______________GLOBAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/

//
// Function:   GRD_Init
// Purpose:    Handle running state monitor for rpisens
//
// Parms:      
// Returns:    Exit codes
// Note:       Called from main() to split off the guard thread
//
int GRD_Init()
{
   int      iCc;
   pid_t    tPid;

   tPid = fork();
   //
   switch(tPid)
   {
      default:
         // Parent
         GLOBAL_PidPut(PID_GUARD, tPid);
         break;

      case 0:
         // Child:
         GLOBAL_PidSetInit(PID_GUARD, NULL);
         GEN_Sleep(500);
         iCc = grd_Execute();
         LOG_Report(0, "GRD", "GRD-Init():Exit normally, cc=%d", iCc);
         exit(iCc);
         break;

      case -1:
         // Error
         GEN_Printf("GRD-Init(): Error!" CRLF);
         LOG_Report(errno, "GRD", "GRD-Init():Exit fork ERROR:");
         exit(EXIT_CC_GEN_ERROR);
         break;
   }
   return(GLOBAL_GRD_ALL_INI);
}

/*----------------------------------------------------------------------
________________LOCAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/

//
// Function:   grd_Execute
// Purpose:    Handle threads running state monitor
//
// Parms:      
// Returns:    Exit codes
// Note:       
//
static int grd_Execute()
{
   pid_t    tDogPid=0;
   pid_t    tBadPid;
   int      iOpt, iCc=EXIT_CC_OKEE;
   int      iTimeout=GUARD_SECS;
   int      iBeat=0;
   u_int32  ulSecsNow; 
   sigset_t tBlockset;

   if(grd_SignalRegister(&tBlockset) == FALSE)
   {
      LOG_Report(errno, "GRD", "grd-Execute():Exit Register ERROR:");
      return(EXIT_CC_GEN_ERROR);
   }
   GLOBAL_SemaphoreInit(PID_GUARD);
   GLOBAL_PidTimestamp(PID_GUARD, 0);
   GLOBAL_PidSaveGuard(PID_GUARD, GLOBAL_GRD_ALL_INI);
   //
   // Init ready
   // Wait for Host Run/Execute
   //
   GLOBAL_SetSignalNotification(PID_HOST, GLOBAL_GRD_ALL_INI);
   grd_WaitSignal(GLOBAL_HST_ALL_RUN, 0);
   PRINTF("grd-Execute():Received Host run permission" CRLF);
   //
   // Run/Execute
   // Main loop of the Guard: wait for the semaphore posted by the parent 
   //
   while(fThreadRunning)
   {
      iOpt = GLOBAL_SemaphoreWait(PID_GUARD, GUARD_TIMEOUT);
      switch(iOpt)
      {
         case 0:
            GLOBAL_PidSetGuard(PID_GUARD);
            if(GLOBAL_GetSignalNotification(PID_GUARD, GLOBAL_HST_GRD_MID)) 
            {
               // Midnight
               PRINTF("grd-Execute():Midnight" CRLF);
               LOG_Report(0, "GRD", "grd-Execute():Midnight");
            }
            break;

         case 1:
            //
            // Timeout GUARD_TIMEOUT mSecs 
            // Show heartbeat
            //
            iBeat ^= 1;
            LED(LED_Y, iBeat);
            GLOBAL_PidSetGuard(PID_GUARD);
            ulSecsNow = RTC_GetSystemSecs();
            //
            // Check if it is time to kick the dog (rpidog), if it's there
            //
            tDogPid = grd_CheckSendHeartbeat(ulSecsNow, tDogPid);
            //
            // Check all threads every GUARD_SECS secs (default 5 mins)
            //
            if(iTimeout) iTimeout--;
            else
            {
               //
               // The Guard will check periodically if all threads are still running.
               // If not, it will signal the Watchdog (if any) to restart the App.
               // If the Watchdog is OTL, reboot instead.
               //
               iTimeout = GUARD_SECS;
               tBadPid  = -1;
               //
               if( grd_CheckThread(PID_HOST)  != 1) tBadPid = PID_HOST;
               if( grd_CheckThread(PID_SRVR)  != 1) tBadPid = PID_SRVR;
               if( grd_CheckThread(PID_COMM)  != 1) tBadPid = PID_COMM;
               if( grd_CheckThread(PID_DEBUG) != 1) tBadPid = PID_DEBUG;
               //
               // If one of the threads is gone, restart
               //
               if(tBadPid != -1)
               {
                  if(GLOBAL_DebugPrint(MM_DEBUG_GUARD, 1)) GEN_PRINTF("grd-CheckGuard():ERROR, %s is OTL: restart" CRLF, GLOBAL_PidGetName(tBadPid));
                  LOG_Report(0, "GRD", "grd-Execute():ERROR, %s is OTL: restart", GLOBAL_PidGetName(tBadPid));
                  fThreadRunning = grd_Restart();
               }
               else
               {
                  //
                  // The Guard will reset all thread check-flags periodically. 
                  // Check if all threads responded within the time allowed by 
                  // signalling the flag again.
                  //
                  if(fThreadRunning) fThreadRunning = grd_CheckGuard(ulSecsNow);
               }
            }
            break;

         default:
         case -1:
            // Error
            LOG_Report(errno, "GRD", "grd-Execute():ERROR GLOBAL_SemaphoreWait");
            PRINTF("grd-Execute():ERROR GLOBAL_SemaphoreWait(errno=%d)" CRLF, errno);
            break;
      }
   }
   GLOBAL_SemaphoreDelete(PID_GUARD);
   return(iCc);
}

//
// Function:   grd_CheckGuard
// Purpose:    Check guard at the end of the watch period (each SET_GUARD_SECS secs)
//
// Parms:      Secs now
// Returns:    FALSE to abort main loop and exit
// Note:       
//             
static bool grd_CheckGuard(u_int32 ulSecsNow)
{
   static u_int32 ulSecsGuard=0;
   static int     iGuardCount=0;
   //
   bool           fCc=TRUE;

   if(ulSecsGuard == 0)
   {
      ulSecsGuard = ulSecsNow + SET_GUARD_SECS;
      iGuardCount = GUARD_COUNT;
   }
   //
   if(ulSecsNow > ulSecsGuard)
   {
      //
      // All active threads need to report back within SET_GUARD_SECS secs.
      // If they didn't the service will be restarted by the WatchDog, 
      // who is triggered by PID_HOST through a SIGUSR1 (Restart) or 
      // SIGUSR2 (Reboot).
      //
      // G_stPidList[i]->pid_t       tPid   : The PID
      //                 int         iFlag  : The thread's guard marker
      //                 const char *pcName : The PID short-name
      //                 const char *pcHelp : The PID long-name
      //
      //
      // Check all threads if it has set its guard marker again
      // to signal it is still alive and kicking
      //
      if(GLOBAL_PidCheckGuards())
      {
         //
         // All threads reported back: Restart guards duty
         //
         if(GLOBAL_DebugPrint(MM_DEBUG_GUARD, 1)) GEN_PRINTF("grd-CheckGuard():All threads OK" CRLF);
         iGuardCount  = GUARD_COUNT;
         ulSecsGuard += SET_GUARD_SECS;
      }
      else
      {
         //
         // Not all threads reported back: if multiple times in a row, take action
         //
         if(iGuardCount-- == 0)
         {
            //
            // Problems: restart theApp
            //
            if(GLOBAL_DebugPrint(MM_DEBUG_GUARD, 1)) GEN_PRINTF("grd-CheckGuard():Check Guard Error! Restart theApp !" CRLF);
            LOG_Report(0, "GRD", "grd-CheckGuard():Check Guard Error! Restart theApp !");
            GLOBAL_Notify(PID_HOST, GLOBAL_ALL_HST_RST, SIGUSR1);
            fCc = FALSE;
         }
         else
         {
            //
            // Not all threads reported back
            //
            RTC_ConvertDateTime(TIME_FORMAT_WW_DD_MM_YYYY_HH_MM_SS, cDateTime, ulSecsNow);
            if(GLOBAL_DebugPrint(MM_DEBUG_GUARD, 1)) GEN_PRINTF("grd-CheckGuard():Problem %d at %s" CRLF, GUARD_COUNT-iGuardCount, cDateTime);
            LOG_Report(0, "GRD", "grd-CheckGuard():Problem %d at %s", GUARD_COUNT-iGuardCount, cDateTime);
            ulSecsGuard += GUARD_SECS_RETRY;
         }
      }
      GLOBAL_PidClearGuards();
   }
   return(fCc);
}

//
// Function:   grd_CheckSendHeartbeat
// Purpose:    Check if Send heartbeat as SIGHUP to RPIDOG
//
// Parms:      Secs now, WatchDog PID or 0
// Returns:    WatchDog PID
// Note:       Send HB every 15 mins
//             
static pid_t grd_CheckSendHeartbeat(u_int32 ulSecsNow, pid_t tPid)
{
   static u_int32 ulSecsHeartbeat=0;
   
   if(ulSecsHeartbeat == 0) ulSecsHeartbeat = ulSecsNow + 60;
   else if(ulSecsNow > ulSecsHeartbeat)
   {
      if(tPid == 0) 
      {
         tPid = GEN_GetPidByName(RPI_WATCHDOG);
         LOG_Report(0, "HST", "grd-CheckSendHeartbeat():Watchdog (" RPI_WATCHDOG ") Pid=%d", tPid);
         if(GLOBAL_DebugPrint(MM_DEBUG_GUARD, 2)) GEN_PRINTF("grd-CheckSendHeartbeat():Watchdog (" RPI_WATCHDOG ") Pid=%d" CRLF, tPid);
      }
      if(tPid > 0)
      {
         if(GLOBAL_DebugPrint(MM_DEBUG_GUARD, 2)) 
         {
            LOG_Report(0, "HST", "grd-CheckSendHeartbeat():Kick the dog " RPI_WATCHDOG " (SIGHUP to %d)", tPid);
            GEN_PRINTF("grd-CheckSendHeartbeat():Kick the dog " RPI_WATCHDOG " (SIGHUP to %d)" CRLF, tPid);
         }
         kill(tPid, SIGHUP);
      }
      else tPid = 0;
      ulSecsHeartbeat = 0;
   }
   return(tPid);
}

//  
//  Function    : grd_CheckThread
//  Description : Check essential background thread
//  
//  Parameters  : ePid
//  Returns     :  0 Not found
//                +1 Running
//                -1 Zombie
//  
static int grd_CheckThread(PIDT ePid)
{
   int         iCc=0;
   PIDINFO     stInfo;
   pid_t       tPid;
   const char *pcName;

   tPid = GLOBAL_PidGet(ePid);
   if(tPid)
   {
      pcName = GLOBAL_PidGetName(ePid);
      switch(GEN_GetPidInfo(&stInfo, tPid, PIDST_STATE))
      {
         case 0:
            //
            // PID not found
            //
            if(GLOBAL_DebugPrint(MM_DEBUG_GUARD, 1)) GEN_PRINTF("grd-CheckThread():Pid-%d [%s] NOT FOUND [%X]" CRLF, tPid, pcName, kill(tPid, 0));
            LOG_Report(0, "GRD", "grd-CheckThread():Pid-%d [%s] NOT FOUND [%X]", tPid, pcName, kill(tPid, 0));
            break;

         case 1:
            if(GLOBAL_DebugPrint(MM_DEBUG_GUARD, 5))
            { 
               if( GEN_GetPidInfo(&stInfo, tPid, PIDST_NAME)  > 0)   PRINTF("grd-CheckThread():%s is %s"       CRLF, pcName, stInfo.cName);
               if( GEN_GetPidInfo(&stInfo, tPid, PIDST_STATE) > 0)   PRINTF("grd-CheckThread():%s state is %d" CRLF, pcName, stInfo.tState);
               if( GEN_GetPidInfo(&stInfo, tPid, PIDST_PPID)  > 0)   PRINTF("grd-CheckThread():%s ppid is %d"  CRLF, pcName, stInfo.tPPid);
               if( GEN_GetPidInfo(&stInfo, tPid, PIDST_OPID)  > 0)   PRINTF("grd-CheckThread():%s opid is %d"  CRLF, pcName, stInfo.tOPid);
            }
            switch(stInfo.tState)
            {
               case PIDST_IS_SLEEPING:
                  if(GLOBAL_DebugPrint(MM_DEBUG_GUARD, 4)) GEN_PRINTF("grd-CheckThread():Pid-%d [%s]:Sleeping [%X]" CRLF, tPid, pcName, kill(tPid, 0));
                  iCc = 1;
                  break;

               case PIDST_IS_RUNNING:
                  if(GLOBAL_DebugPrint(MM_DEBUG_GUARD, 4)) GEN_PRINTF("grd-CheckThread():Pid-%d [%s]:Running [%X]" CRLF, tPid, pcName, kill(tPid, 0));
                  iCc = 1;
                  break;

               case PIDST_IS_ZOMBIE:
                  LOG_Report(0, "GRD", "grd-CheckThread():Pid-%d [%s]:Zombie [%X]", tPid, pcName, kill(tPid, 0));
                  if(GLOBAL_DebugPrint(MM_DEBUG_GUARD, 4)) GEN_PRINTF("grd-CheckThread():Pid-%d [%s]:Zombie [%X]" CRLF, tPid, pcName, kill(tPid, 0));
                  iCc = -1;
                  break;

               default:
                  if(GLOBAL_DebugPrint(MM_DEBUG_GUARD, 4)) GEN_PRINTF("grd-CheckThread():Pid-%d [%s]:Other [%X]" CRLF, tPid, pcName, kill(tPid, 0));
                  iCc = -1;
                  break;
            }
            break;

         default:
         case -1:
            //
            // Error 
            //
            if(GLOBAL_DebugPrint(MM_DEBUG_GUARD, 4)) GEN_PRINTF("grd_CheckThread():Pid-%d [%s] ERROR [%X]" CRLF, tPid, pcName, kill(tPid, 0));
            LOG_Report(0, "GRD", "grd_CheckThread():Pid-%d [%s] ERROR [%X]", tPid, pcName, kill(tPid, 0));
            break;
      }
   }
   else
   {
      if(GLOBAL_DebugPrint(MM_DEBUG_GUARD, 4)) GEN_PRINTF("grd-CheckThread():Pid-%d NOT FOUND" CRLF, tPid);
      LOG_Report(0, "GRD", "grd-CheckThread():Pid-%d NOT FOUND", tPid);
   }
   return(iCc);
}


// 
// Function:   grd_Restart
// Purpose:    Restart theAPP
// 
// Parameters:
// Returns:    FALSE if to proceed restart 
// 
static bool grd_Restart()
{
   bool  fCc=FALSE;
   pid_t tDogPid;

   //
   // We need to restart the App: SIGUSR1 RpiDog
   //
   tDogPid = GEN_GetPidByName(RPI_WATCHDOG);
   if(tDogPid > 0) 
   {
      PRINTF("grd-restart():RESTART theApp through RpiDog on Pid=%d" CRLF, tDogPid);
      LOG_Report(0, "GRD", "grd-restart():RESTART theApp through RpiDog on Pid=%d", tDogPid);
      kill(tDogPid, SIGUSR1);
   }
   else         
   {
      PRINTF("grd-restart():RESTART theApp by RpiDog NOT FOUND !:: REBOOT here instead." CRLF);
      LOG_Report(0, "GRD", "grd-restart():RESTART theApp by RpiDog NOT FOUND !:: REBOOT here instead.");
      system(pcRebootDelayed);
      fCc = TRUE;
   }
   return(fCc);
}

//
// Function:   grd_WaitSignal
// Purpose:    Wait for signal
//
// Parms:      Signal(s) to wait for, max time in mSecs(0=indef)
// Returns:    TRUE if OK
// Note:       
//
static bool grd_WaitSignal(int iSignal, int iMsecs)
{
   bool     fCc=TRUE;
   bool     fWaiting=TRUE;
   int      iOpt;

   while(fThreadRunning && fWaiting)
   {
      iOpt = GLOBAL_SignalWait(PID_GUARD, iSignal, iMsecs);
      switch(iOpt)
      {
         case 0:
            // Run debugger
            fWaiting = FALSE;
            break;

         case 1:
            // Timeout 
            fWaiting = FALSE;
            fCc = FALSE;
            break;

         default:
         case -1:
            // Error
            LOG_Report(errno, "GRD", "grd-WaitSignal():ERROR:");
            PRINTF("grd-WaitSignal():ERROR(%s)" CRLF, strerror(errno));
            fWaiting = FALSE;
            fCc = FALSE;
            break;
      }
   }
   return(fCc);
}

/*------  Local functions separator ------------------------------------
__SIGNAL_FUNCTIONS(){};
----------------------------------------------------------------------------*/

// 
// Function:   grd_SignalRegister
// Purpose:    Register all SIGxxx
// 
// Parameters: Blockset
// Returns:    TRUE if delivered
// 
static bool grd_SignalRegister(sigset_t *ptBlockset)
{
  bool fCC = TRUE;

  // SIGSEGV:
  if( signal(SIGSEGV, &grd_ReceiveSignalSegmnt) == SIG_ERR)
  {
     LOG_Report(errno, "GRD", "grd-SignalRegister(): SIGSEGV ERROR");
     fCC = FALSE;
  }
  // SIGUSR1:
  if( signal(SIGUSR1, &grd_ReceiveSignalUser1) == SIG_ERR)
  {
     LOG_Report(errno, "GRD", "grd-SignalRegister(): SIGUSR1 ERROR");
     fCC = FALSE;
  }
  // SIGUSR2:
  if( signal(SIGUSR2, &grd_ReceiveSignalUser2) == SIG_ERR)
  {
     LOG_Report(errno, "GRD", "grd-SignalRegister(): SIGUSR2 ERROR");
     fCC = FALSE;
  }
  // CTL-X handler
  if( signal(SIGTERM, &grd_ReceiveSignalTerm) == SIG_ERR)
  {
     LOG_Report(errno, "GRD", "grd-SignalRegister(): SIGTERM ERROR");
     fCC = FALSE;
  }
  // Ctl-C handler
  if( signal(SIGINT, &grd_ReceiveSignalInt) == SIG_ERR)
  {
     LOG_Report(errno, "GRD", "grd-SignalRegister(): SIGINT ERROR");
     fCC = FALSE;
  }

  if(fCC)
  {
     //
     // Setup the SIGxxx events
     //
     sigemptyset(ptBlockset);
     sigaddset(ptBlockset, SIGSEGV);
     sigaddset(ptBlockset, SIGTERM);
     sigaddset(ptBlockset, SIGINT);
     sigaddset(ptBlockset, SIGUSR1);
     sigaddset(ptBlockset, SIGUSR2);
  }
  return(fCC);
}

//
// Function:   grd_ReceiveSignalSegmnt
// Purpose:    Handle the SIGSEGV
//
// Parms:
// Returns:    TRUE if delivered
// Note:       Do NOT use LOG_xxx() here
//
static void grd_ReceiveSignalSegmnt(int iSignal)
{
   if(fThreadRunning)
   {
      GLOBAL_SegmentationFault(__FILE__, __LINE__);
      LOG_SegmentationFault(__FILE__, __LINE__);
      //
      // (TRY TO) Cleanup background threads:
      //
      GLOBAL_Notify(PID_HOST, GLOBAL_ALL_HST_FLT, SIGTERM);
      LOG_Report(errno, "SMA", "grd-ReceiveSignalSegmnt():Pid=%d", getpid());
      LOG_printf("grd-ReceiveSignalSegmnt():Pid=%d", getpid());
      fThreadRunning = FALSE;
      GLOBAL_SemaphorePost(PID_GUARD);
   }
   else exit(EXIT_CC_GEN_ERROR);
}

// 
// Function:   grd_ReceiveSignalTerm
// Purpose:    Add the SIGTERM command in the buffer (Ctl-X)
// 
// Parameters: 
// Returns:    TRUE if delivered
// 
static void grd_ReceiveSignalTerm(int iSignal)
{
   LOG_Report(0, "GRD", "grd-ReceiveSignalTerm()");
   fThreadRunning = FALSE;
   GLOBAL_SemaphorePost(PID_GUARD);
}

//
// Function:   grd_ReceiveSignalInt
// Purpose:    System callback for SIGINT (Ctl-C)
//
// Parms:
// Returns:    TRUE if delivered
//
static void grd_ReceiveSignalInt(int iSignal)
{
   LOG_Report(0, "GRD", "grd-ReceiveSignalInt()");
   fThreadRunning = FALSE;
   GLOBAL_SemaphorePost(PID_GUARD);
}

//
// Function:   grd_ReceiveSignalUser1
// Purpose:    System callback for SIGUSR1 signal
//
// Parms:
// Returns:    
// Note:
//
static void grd_ReceiveSignalUser1(int iSignal)
{
   GLOBAL_SemaphorePost(PID_GUARD);
}

//
// Function:   grd_ReceiveSignalUser2
// Purpose:    System callback for SIGUSR2 signal
//
// Parms:
// Returns:    
// Note:
//
static void grd_ReceiveSignalUser2(int iSignal)
{
   GLOBAL_SemaphorePost(PID_GUARD);
}


/*----------------------------------------------------------------------
______________COMMENT_FUNCTIONS(){}
------------------------------x----------------------------------------*/
#ifdef COMMENT

#endif   //COMMENT
