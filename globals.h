/*  (c) Copyright:  2020..2024  Patrn, Confidential Data
 *
 *  Workfile:           globals.h
 *  Purpose:            Define the mmap variables, common over all threads. If
 *                      the structure needs to change, roll the revision number to make sure a new mmap file is created !
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PIC 16f628A Ultrasoon sensor
 *  Note:               
 * 
 *  Author:             Peter Hillen
 *  Changes:
 *    01 Nov 2020:      Ported from rpislim
 *    11 Nov 2020:      Add SENS struct
 *    20 Nov 2020:      Modify G_stCmd struct
 *    01 Dec 2020:      Add raw sensor distance
 *    01 May 2021:      Add Total-sensor data
 *    05 May 2021:      Make drop/rise G_parameter
 *    21 May 2021:      Add HTTP Delete (for csv files)
 *    31 May 2021:      Add SysFlags and Dropped Samples
 *    12 Aug 2021:      Add Pump Mode command
 *    22 Mar 2022:      Fix PARARGS rename
 *    25 Jun 2022:      Add Guard thread
 *    11 Jan 2023:      Add G_iDelete as extension to G_pcDelete
 *    18 Apr 2024:      Add debug thread
 *    23 Apr 2024:      Split globals.h and locals.h
 *    01 May 2024:      Move uCtl RAM/EEPROM content to persistent storage
 *    04 May 2024:      Add GLOBAL_GetStatusText
 *    09 May 2024:      Add midnight mail
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _GLOBALS_H_
#define _GLOBALS_H_

#include <semaphore.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <netinet/in.h>
//
#ifdef DEFINE_GLOBALS
   #define  EXTERN
#else
   #define  EXTERN extern
#endif
//
#ifdef      FUN_CHECK_ON
   #define  FUNCHECK(x)       pstMap->G_iFunCheck=x
#else
   #define  FUNCHECK(x)
#endif
//
// Motion Debug 
//
#ifdef      FEATURE_DEBUG_MOTION_DETECT
   #define  TRIGGER(x,y)  if(pstMap->G_iDetectDebug & x) y
   #define  ONESHOT(x,y)  if(pstMap->G_iDetectDebug & x) {y;pstMap->G_iDetectDebug &= ~x;}
#else
   #define  TRIGGER(x,y)
   #define  ONESHOT(x,y)
#endif
//
// We MUST define these sizes here as well (if no pre-processor)
//
#define  SIZEOF_INT                       4
#define  SIZEOF_LONG                      4
#define  SIZEOF_FLOAT                     4
#define  SIZEOF_LONGLONG                  8
#define  SIZEOF_DOUBLE                    8
#define  SIZEOF_SEMT                      16
//
#define  MIN_LOG_DISK_FREE                10.0
//
// HTTP Functions (callback through HTML or JSON)
//
// Local over this project
// are in locals.h (and start at 0x0000.0010)
//
// Global over all project
#define  PAR____                          0x00000000     // None
#define  PAR_PRM                          0x00000001     // Generic
#define  PAR_EML                          0x00000002     // Mailx
#define  PAR_ERR                          0x00000004     // Error
//                                        0x00000008     // Spare
#define  PAR_ALL                          0x0000FFFF     // All HTTP Parameters
//
// JSON markers for text-style-vars ("value" = "123456")
//             or number-style-vars ("value" =  123456 )
//
#define  JSN_TXT                          0x10000000     // JSON Variable is text
#define  JSN_INT                          0x20000000     // JSON Variable is integer
#define  WB                               0x80000000     // Warm boot var
#define  PAR_A                            0x08000000     // Parm is ASCII
#define  PAR_B                            0x04000000     // Parm is BCD
#define  PAR_F                            0x02000000     // Parm is FLOAT
#define  PAR_H                            0x01000000     // Parm is HEX
#define  PAR_TYPE_MASK                    0x0F000000     // Parm is HEX
//
#define  ALWAYS_CHANGED                   -1
#define  NEVER_CHANGED                    -2
//
#define  MAX_DAYS                         31
#define  MAX_DAYSZ                        (31+1)
#define  MAX_QRTS                         (24*4)
#define  MAX_QRTSZ                        ((24*4)+1)
//
#define  SECS_PER_YEAR                    (365*24*3600)  // Secs in 1 year of 365 days
#define  SECS_PER_MONTH                   (31*24*3600)   // Secs in 1 month of 31 days
#define  SECS_PER_QTR                     (60*15)        // Secs in 1 quarter hour
#define  SECS_PER_DAY                     (60*60*24)     // Secs in 1 day of 24 hours
//
#define  ONEMEGABYTE                      (1024*1024)
#define  ONEGIGABYTE                      (1024*ONEMEGABYTE)
#define  ONEMILLION                       1000000L
#define  ONEBILLION                       1000000000L
//
#define  ARCHIVE_USAGE_SECS               120         // Check Archiving necessity in delta secs
#define  THREADS_CHECK_SECS               300         // Thread min respond secs
#define  MAX_NUM_REPORT_LINES             1000        // Default max LOG file lines
#define  MAX_DYN_PAGES                    64
#define  HTTP_REPLY_LEN                   4095        // HTTP Get motion.json reply max length
#define  HTTP_REPLY_LENZ                  HTTP_REPLY_LEN + 1
//
// Global area array sizes
//
#define  MAX_REC_LEN                      255
#define  MAX_REC_LENZ                     (MAX_REC_LEN+1)
#define  MAX_PATH_LEN                     255
#define  MAX_PATH_LENZ                    MAX_PATH_LEN+1
#define  MAX_URL_LEN                      31
#define  MAX_URL_LENZ                     MAX_URL_LEN+1
#define  MAX_MAC_LEN                      31
#define  MAX_MAC_LENZ                     MAX_MAC_LEN+1
#define  MAX_ADDR_LEN                     INET_ADDRSTRLEN
#define  MAX_ADDR_LENZ                    INET_ADDRSTRLEN+1
#define  MAX_PARM_LEN                     15
#define  MAX_PARM_LENZ                    MAX_PARM_LEN+1
#define  MAX_ARG_LEN                      255
#define  MAX_ARG_LENZ                     MAX_ARG_LEN+1
#define  MAX_XFR_LEN                      1023
#define  MAX_XFR_LENZ                     MAX_XFR_LEN+1
#define  MAX_CMD_LEN                      256
#define  MAX_CMDLINE_LEN                  (MAX_CMD_LEN + MAX_ARG_LEN)
#define  MAX_MAIL_LEN                     127
#define  MAX_MAIL_LENZ                    MAX_MAIL_LEN+1
#define  MAX_PASS_LEN                     127
#define  MAX_PASS_LENZ                    MAX_PASS_LEN+1
//
typedef bool (*HTTPFUN)(int);
//
typedef struct httpargs
{
   int            iId;
   int            iFunction;
   const char    *pcJson;
   const char    *pcHtml;
   int            iValueOffset;
   int            iValueSize;
   int            iChangedOffset;
   HTTPFUN        pfHttpFun;
}  HTTPARGS;
//
typedef enum _cl_action_
{
   CLACT_NONE = 0,
   CLACT_EXPAND_MAP,
   CLACT_EXIT
}  CLACT;
//
typedef enum _global_parameters_
{
#define  EXTRACT_PAR(a,b,c,d,e,f,g,h,i,j,k)    a,
#include "par_defs.h"
#undef EXTRACT_PAR
   NUM_GLOBAL_DEFS
}  GLOPAR;
//
// Define argument structure to streamer apps
//
typedef struct sarg
{
   int         iLenArgs;
   const char *pcArg;
   char       *(*pfGetArg)(const struct sarg *);
}  SARG;
//
typedef struct _rpidata_
{
   char    *pcObject;
   FTYPE    tType;
   int      iIdx;
   int      iSize;
   int      iFree;
   int      iLastComma;
}  RPIDATA;
//
// Enums PIDs
//
typedef enum _proc_pids_
{
   #define  EXTRACT_PID(a,b,c)   a,
   #include "par_pids.h"
   #undef EXTRACT_PID
   //
   NUM_PIDT
}  PIDT;
//
typedef int (*PFUNIN)();
//
typedef struct _proc_pidlist_
{
   pid_t       tPid;
   sem_t      *ptSem;
   int         iFlag;
   int         iNotify;
   u_int32     ulTs;
   int         iCount;
   PFUNIN      pfnInit;
   const char *pcName;
   const char *pcHelp;
}  PIDL;
//
typedef struct _globals_
{
   int         iFunction;
   const char *pcOption;
   const char *pcDefault;
   int         iChanged;
   int         iChangedOffset;
   int         iValueOffset;
   int         iValueSize;
   HTTPFUN     pfHttpFun;
}  GLOBALS;
//
typedef enum _varreset_
{
   VAR_COLD    = 0,                       // Cold reset
   VAR_WARM,                              // Warm reset
   VAR_UPDATE,                            // Update

   NUM_VARRESETS
}  VARRESET;
//
typedef enum _system_vars_
{
   SVAR_NONE         = 0,
   SVAR_TEMPERATURE, 
   //
   NUM_SVARS
}  SVAR;
//
typedef struct _system_data_
{
   char    *pcFile;
   void    *pvData;
}  SDATA;
//
typedef enum _rpi_mapstate_
{
   MAP_CLEAR    = 0,                      // Clear all the mapped memory
   MAP_SYNCHR,                            // Synchronize the map
   MAP_RESTART,                           // Restart the counters using the last stored data

   NUM_MAPSTATES
}  MAPSTATE;
//
//
// Dynamic page registered URLs
//
typedef bool(*PFVOIDPI)(void *, int);
typedef struct _dynr_
{
   int         tUrl;                      // DYN_HTML_... (from EXTRACT_DYN pages.h, ...)
   FTYPE       tType;                     // HTTP_JSON, HTTP_HTML
   int         ePid;                      // Owner pid enum PID_xxxx
   int         iTimeout;                  // Estimated call duration 
   int         iPort;                     // Port
   char        pcUrl[MAX_URL_LENZ];       // URL
   PFVOIDPI    pfCallback;                // Callback function
}  DYNR;
//
// Global notification struct
//
typedef struct NFY
{
   PIDT     ePid;                         // Pid enum
   int      iFlag;                        // Notification
   int    (*pfCb)(void *);                // Callback
}  NFY;
//
#define  LCD_VIRTUAL_ROWS  159                             // Lines
#define  LCD_VIRTUAL_COLS  120                             // Columns
#define  LCD_VIRTUAL_SIZE (LCD_VIRTUAL_COLS+1)*LCD_VIRTUAL_ROWS   // LCD_VERTUAL AsciiZ
//
typedef struct _vlcd_
{
   bool     fLcdCursor;
   int      iLcdCmd;
   int      iLcdMode;
   int      tLcdHandle;
   u_int8   ubVirtRows, ubVirtCols;
   u_int8   ubVirtRow,  ubVirtCol;
   u_int8   ubCursRow,  ubCursCol;
   //
   char     pcVirtLcd[LCD_VIRTUAL_SIZE];
}  VLCD;
//
// WAN changes
//
#define WAN_CHECK_SECS  (1*3600)       // 1 hour checks
//
#define WAN_NOT_FOUND   0x0000         // Not yet retrieved
#define WAN_SAME        0x0001         // WAN retrieved and no change
#define WAN_CHANGED     0x0002         // WAN retrieved and has changed
#define WAN_NO_URL      0x0004         // WAN URL not set in persistent memory
#define WAN_ERROR       0x8000         // ERROR retrieving

//=============================================================================
// Global parameters mapped to a shared MMAP file 
//=============================================================================
typedef struct _rpimap_
{
   int               G_iSignature1;
   int               G_iVersionMajor;
   int               G_iVersionMinor;
   int               G_iRunCounter;
   int               G_iVerbose;
   bool              G_fInit;
   u_int32           G_ulStartTimestamp;
   pthread_mutex_t   G_tMutex;
   pthread_mutex_t   G_tMutexSpare1;
   pthread_mutex_t   G_tMutexSpare2;
   //
   //=============== Start WB RESET/ZERO area =================================
   //                The parms below are initialized with their default values
   //                upon each startup if and when the WB marker has been set.
   //                If not, the parameter will be zero.
   int               G_iResetStart;
   //
   // PID list Threads Installed
   // PID List Threads Semaphores
   //
   PIDL              G_stPidList             [NUM_PIDT];
   sem_t             G_tSemList              [NUM_PIDT];
   //
   char              G_pcCommand             [MAX_PARM_LENZ];
   char              G_pcStatus              [MAX_PARM_LENZ];
   char              G_pcRcuKey              [MAX_PARM_LENZ];
   //
   // Global Delete marker: 
   //    http:\\192.168.178.xxxx/parms.jsom?Delete=Yes
   // Select file to download (*.csv)
   // File will download and delete afterwards.
   //
   char              G_pcDelete              [MAX_PARM_LENZ];
   char              G_pcSegFaultFile        [MAX_PATH_LENZ];
   char              G_pcLoopFile            [MAX_PATH_LENZ];
   char              G_pcHostIp              [MAX_ADDR_LENZ];
   char              G_pcWanAddr             [MAX_ADDR_LENZ];
   //
   int64             G_llLogFilePos;
   u_int32           G_ulDebugMask;
   u_int32           G_ulSecondsCounter;
   //
   int               G_iWanAddr;             // WAN retrieval
   int               G_iMallocs;
   int               G_iSegFaultLine;
   int               G_iSegFaultPid;
   int               G_iGuards;
   int               G_iTraceCode;
   int               G_iFunCheck;
   int               G_iDelete;
   //
   // Dynamic HTTP page list
   //
   int               G_iHttpStatus;
   int               G_iHttpArgs;
   int               G_iNumDynPages;
   int               G_iCurDynPage;
   int               G_iRcuKey;
   //
   DYNR              G_stDynPages            [MAX_DYN_PAGES];
   //
   VLCD              G_stVirtLcd;
   GLOG              G_stLog;
   GLOCMD            G_stCmd;
   //
   // Changed markers
   //
   u_int8            G_ubCommandChanged;
   u_int8            G_ubIpAddrChanged;
   //==========================================================================
   // Project specific data 
   //==========================================================================
   SMDATA            G_stSensorLife;
   SMDATA            G_stDispDay             [MAX_QRTS];
   SMDATA            G_stDispMonth           [MAX_DAYS];
   //
   char              G_pcPumpState           [MAX_PARM_LENZ];
   char              G_pcSysFlags            [MAX_PARM_LENZ];
   char              G_pcDropped             [MAX_PARM_LENZ];
   //
   char              G_pcMiacCmd             [MIAC_CIRC_SIZE];
   char              G_pcMiacRes             [MIAC_CIRC_SIZE];
   //
   int               G_iCommsTout;
   //
   double            G_flDm;
   double            G_flPondLevel;
   //--------------------------------------------------------------------------
   // Take additional power-on reset parms from this pool:
   //--------------------------------------------------------------------------
   #define SPARE_POOL_ZERO 1000
   #define EXTRA_POOL_ZERO (1*SIZEOF_INT)+(1*SIZEOF_DOUBLE)
   //
   #if     EXTRA_POOL_ZERO > SPARE_POOL_ZERO
   #error  Spare zero-init pool too small
   #endif
   //
   // Extra from zero-init pool
   //
   int               G_iSecsFlow;
   double            G_flFlowRate;
   // Above this line ---------------------------------------------------------
   char              G_pcSparePoolZero       [SPARE_POOL_ZERO-EXTRA_POOL_ZERO];
   //
   //--------------------------------------------------------------------------
   int               G_iResetEnd;
   //                The parms above were initialized with their default values
   //                upon each startup if and when the WB marker has been set.
   //                If not, the parameter has been set to zero.
   //=============== End WB reset/zero area ===================================


   //=============== Start Persistent storage==================================
   // Generic persistent data
   //==========================================================================
   int               G_iDebug;
   int               G_iDebugLevel;
   int               G_iSimulateTime;
   int               G_iDetectDebug;
   int               G_iDetectCounter;
   int               G_iLogLines;
   //
   u_int32           G_ulMailSecsNow;
   u_int32           G_ulMailSecsMail;
   //
   char              G_pcSwVersion           [MAX_PARM_LENZ];
   char              G_pcHostname            [MAX_URL_LENZ];
   char              G_pcWanUrl              [MAX_URL_LENZ];
   char              G_pcWwwDir              [MAX_PATH_LENZ];
   char              G_pcRamDir              [MAX_PATH_LENZ];
   //
   char              G_pcLogFile             [MAX_PATH_LENZ];
   char              G_pcDebugMask           [MAX_PARM_LENZ];
   char              G_pcLastFile            [MAX_PATH_LENZ];
   char              G_pcLastFileSize        [MAX_PARM_LENZ];
   char              G_pcFilter              [MAX_PARM_LENZ];
   char              G_pcDefault             [MAX_PATH_LENZ];
   char              G_pcWanIp               [MAX_ADDR_LENZ];
   char              G_pcGenericIp           [MAX_ADDR_LENZ];
   char              G_pcSrvrPort            [MAX_PARM_LENZ];
   char              G_pcNumFiles            [MAX_PARM_LENZ];
   //
   char              G_pcYear                [MAX_PARM_LENZ];
   char              G_pcMonth               [MAX_PARM_LENZ];
   char              G_pcDay                 [MAX_PARM_LENZ];
   char              G_pcRestart             [MAX_PARM_LENZ];
   //
   char              G_pcMailxSubj           [MAX_MAIL_LENZ];
   char              G_pcMailxBody           [MAX_MAIL_LENZ];
   char              G_pcMailxAttm           [MAX_MAIL_LENZ];
   char              G_pcMailxTo             [MAX_MAIL_LENZ];
   char              G_pcMailxCc             [MAX_MAIL_LENZ];

   //==========================================================================
   // Project specific persistent data 
   //==========================================================================
   //
   // Values stored in  G_stLast/ThisDay and
   //                   G_stLast/ThisMonth
   //
   SMDATA            G_stLastMonth           [MAX_DAYS];
   SMDATA            G_stThisMonth           [MAX_DAYS];
   SMDATA            G_stLastDay             [MAX_QRTS];
   SMDATA            G_stThisDay             [MAX_QRTS];
   //
   // Values stored in  G_stSensXxx are in mm
   //
   SENS              G_stSensNow;
   SENS              G_stSensQtr;
   SENS              G_stSensDay;
   SENS              G_stSensMon;
   SENS              G_stSensMin;
   //
   char              G_pcPing                [KINS_SIZE_LENZ];
   char              G_pcPong                [KINS_SIZE_LENZ];
   //
   // Sensor data
   //
   char              G_pcEquipment           [MAX_PARM_LENZ];
   char              G_pcWaterLevel          [MAX_PARM_LENZ];
   char              G_pcPumpMode            [MAX_PARM_LENZ];
   char              G_pcPumpOn              [MAX_PARM_LENZ];
   char              G_pcX10Bypass           [MAX_PARM_LENZ];
   char              G_pcTemperature         [MAX_PARM_LENZ];
   char              G_pcTimeInSecs          [MAX_PARM_LENZ];
   char              G_pcSmartSamples        [MAX_PARM_LENZ];
   //
   u_int32           G_ulSmartSecs;
   int               G_iSmartScaleMonth;
   int               G_iSmartScaleDay;
   //
   double            G_flFloater;
   double            G_flSensorBottom;       
   double            G_flMaxDrop;
   double            G_flMaxRise;
   double            G_flHiWater;
   double            G_flLoWater;
   //
   int               G_iMaxInval;
   int               G_iPumpMode;
   int               G_iMiacMemory;
   int               G_iPumpMinutes;
   int               G_iRamAddr;
   int               G_iRomAddr;
   int               G_iMiacData;
   //
   u_int8            G_ubRamImage            [KINS_RAM_LEN];
   u_int8            G_ubRomImage            [KINS_ROM_LEN];
   //--------------------------------------------------------------------------
   // Take additional persistent data from this pool:
   //--------------------------------------------------------------------------
   // Add:  
   //
   #define SPARE_POOL_PERS 1000
   #define EXTRA_POOL_PERS 0
   //
   #if     EXTRA_POOL_PERS > SPARE_POOL_PERS
   #error  Spare persistent pool too small
   #endif
   //
   // Extra from non-volatile pool
   //

   // Above this line ---------------------------------------------------------
   char              G_pcSparePoolPers       [SPARE_POOL_PERS-EXTRA_POOL_PERS];
   //=============== End Persistent storage ===================================
   int               G_iSignature2;          // Signature bottom
}  RPIMAP;

//=============================================================================
// GLOBAL MMAP structure pointer
//=============================================================================
EXTERN RPIMAP *pstMap;

//
// Global functions
//
bool           GLOBAL_CheckDelete            (char *);
bool           GLOBAL_Close                  (bool);
u_int32        GLOBAL_ConvertDebugMask       (bool);
bool           GLOBAL_DebugClr               (int);
void           GLOBAL_DebugCopy              (u_int8 *, u_int8 *, int);
int            GLOBAL_DebugGet               (int);
int            GLOBAL_DebugPrint             (int, int);
bool           GLOBAL_DebugSet               (int);
int            GLOBAL_ExecuteNotifications   (const NFY *, int, void *);
bool           GLOBAL_ExecuteParameter       (GLOPAR);
void           GLOBAL_ExpandMap              (int, int);
int            GLOBAL_FlushLog               (void);
bool           GLOBAL_GetFileInfo            (char *, char *);
bool           GLOBAL_Init                   (void);
bool           GLOBAL_GetParameterAsInt      (GLOPAR, int *);
u_int32        GLOBAL_GetDebugMask           (void);
void          *GLOBAL_GetGlobalParameter     (int);
char          *GLOBAL_GetHostName            (void);
GLOG          *GLOBAL_GetLog                 (void);
int            GLOBAL_GetMallocs             (void);
char          *GLOBAL_GetOption              (const char *);
const GLOBALS *GLOBAL_GetParameters          (void);
void          *GLOBAL_GetParameter           (GLOPAR);
int            GLOBAL_GetParameterSize       (GLOPAR);
int            GLOBAL_GetParameterType       (GLOPAR);
int            GLOBAL_GetSignal              (int);
bool           GLOBAL_GetSignalNotification  (int, int);
const char    *GLOBAL_GetStatusText          (int);
int            GLOBAL_GetTraceCode           (void);
int            GLOBAL_HostNotification       (int);
int            GLOBAL_Lock                   (void);
void          *GLOBAL_MemorySafeMalloc       (const char *, size_t);
void          *GLOBAL_MemorySafeFree         (const char *, void *);
u_int32        GLOBAL_MemoryGetTotalMalloc   (void);
u_int32        GLOBAL_MemoryGetMallocSize    (void *);
int            GLOBAL_Notify                 (int, int, int);
u_int32        GLOBAL_ReadSecs               (void);
void           GLOBAL_SegmentationFault      (const char *, int);
void           GLOBAL_SetDebugMask           (u_int32);
bool           GLOBAL_SetParameterAsFloat    (GLOPAR, double);
bool           GLOBAL_SetParameterAsInt      (GLOPAR, int);
bool           GLOBAL_SetParameterAsText     (GLOPAR, char *);
int            GLOBAL_SetSignalNotification  (int, int);
bool           GLOBAL_Share                  (void);
int            GLOBAL_Status                 (int);
bool           GLOBAL_Sync                   (void);
int            GLOBAL_Unlock                 (void);
void           GLOBAL_UpdateSecs             (void);
//
// Global PID functions
//
bool           GLOBAL_PidCheckGuards         (void);
bool           GLOBAL_PidCheckThreads        (u_int32);
bool           GLOBAL_PidGetGuard            (PIDT);
bool           GLOBAL_PidSetGuard            (PIDT);
void           GLOBAL_PidClearGuards         (void);
void           GLOBAL_PidSaveGuard           (PIDT, int);
pid_t          GLOBAL_PidGet                 (PIDT);
const char    *GLOBAL_PidGetName             (PIDT);
const char    *GLOBAL_PidGetHelp             (PIDT);
int            GLOBAL_PidInit                (PIDT);
bool           GLOBAL_PidPut                 (PIDT, pid_t);
int            GLOBAL_PidsLog                (void);
void           GLOBAL_PidSetInit             (PIDT, PFUNIN);
int            GLOBAL_PidTerminate           (int, int);
int            GLOBAL_PidsTerminate          (int);
void           GLOBAL_PidTimestamp           (PIDT, u_int32);
void           GLOBAL_PutMallocs             (int);
void           GLOBAL_Signal                 (PIDT, int);
void           GLOBAL_SignalPid              (pid_t, int);
int            GLOBAL_SignalWait             (int, int, int);
//
bool           GLOBAL_SemaphoreDelete        (int);
bool           GLOBAL_SemaphoreInit          (int);
bool           GLOBAL_SemaphorePost          (int);
int            GLOBAL_SemaphoreWait          (int, int);

#endif /* _GLOBALS_H_ */
