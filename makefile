#################################################################################
# 
# makefile:
#	$(TARGET) - Raspberry Pi RPi#11- Kinshi Smart Sensor Controller
#
#  add:
#	cp && echo copy_is_okee || echo copy_is_not_okee
#
#	18Apr2024: Skip strip for GDB debug info
#	09May2024: Add mailx, sample -S 15 secs
#
#	Copyright (c) 2020..2024 Peter Hillen
#################################################################################
BOARD=$(shell cat ../../RPIBOARD)

ifeq ($(BOARD),6)
#
# RPi#6 (Rev-1): Old Kinshi Server
#
	BOARD_OK=yes
	RPIDIR=rpisens
	TARGET=rpisens
	COMMON=../common
	OBJM=
	OBJX=rpi_comm.o rpi_slim.o rpi_sens.o gen_button.o rpi_miac.o
	DEFS=-D FEATURE_IO_PATRN -D IPADDR="\"108\""
	LIBSIO=-lwiringPi -lwiringPiDev
	BUPDIR=/usr/local/share/rpi
endif

ifeq ($(BOARD),8)
#
# RPi#8 (Rev-2): pikrellcam Pond
#
	BOARD_OK=yes
	RPIDIR=rpidog
	TARGET=rpidog
	COMMON=../common
	OBJM=
	OBJX=
	DEFS=-D FEATURE_IO_NONE -D IPADDR="\"208\""
	LIBSIO=
	BUPDIR=/usr/local/share/rpi
endif

ifeq ($(BOARD),9)
#
# RPi#9 (Rev-3): VPN Server (with Smart Sensor)
#
	BOARD_OK=yes
	RPIDIR=rpislim
	TARGET=rpislim
	COMMON=../common
	OBJM=
	OBJX=rpi_slim.o rpi_obis.o rpi_comm.o gen_button.o rpi_solis.o
	DEFS=-D FEATURE_IO_PATRN -D IPADDR="\"209\""
	LIBSIO=-lwiringPi -lwiringPiDev
	BUPDIR=/usr/local/share/rpi
endif

ifeq ($(BOARD),10)
#
# RPi#10 (Rev-3B+): Development RPI-3B+ (Buster Int-Wifi - PATRN GPIO)
#
	BOARD_OK=yes
	RPIDIR=rpisens
	TARGET=rpisens
	COMMON=../common
	OBJM=
	OBJX=rpi_comm.o rpi_slim.o rpi_sens.o gen_button.o rpi_miac.o
	DEFS=-D FEATURE_IO_PATRN -D IPADDR="\"210\""
	LIBSIO=-lwiringPi
	BUPDIR=/all/rpi
endif

ifeq ($(BOARD),11)
#
# RPi#11 (Rev-3B+): New KINSHI RPI-3B+ (Buster TP-Link LAN - PATRN GPIO)
#
	BOARD_OK=yes
	RPIDIR=rpisens
	TARGET=rpisens
	COMMON=../common
	OBJM=rpi_mail.o rpi_mailx.o
	OBJX=rpi_comm.o rpi_slim.o rpi_sens.o gen_button.o rpi_miac.o
	DEFS=-D FEATURE_IO_PATRN -D IPADDR="\"211\""
	LIBSIO=-lwiringPi
	BUPDIR=/all/rpi
endif

ifndef BOARD_OK
#
# All other RPi's: 
#
	BOARD_OK=yes
	RPIDIR=rpislim
	TARGET=rpislim
	COMMON=../common
	OBJM=
	OBJX=
	DEFS=-D FEATURE_IO_NONE -DIPADDR="\"108\""
	LIBSIO=
	BUPDIR=/usr/local/share/rpi
endif

CC			= gcc
DEFS		+= -D BOARD_RPI$(BOARD)
INCDIR	+= -I/opt/vc/include
INCDIR	+= -I/opt/vc/include/interface/vmcs_host/linux
INCDIR	+= -I/opt/vc/include/interface/vcos/pthreads -DRPI=1
INCDIR	+= -I$(COMMON)
LDFLGS	= -pthread
DESTDIR	= ./bin
LIBFLGS	= -L/usr/local/lib -L/usr/lib -L/opt/vc/lib -L$(COMMON)
LIBS	   = -lrt
LIBCOM	= -lcommon
DEPS	   = config.h
OBJ		= rpi_main.o \
			rpi_guard.o rpi_srvr.o rpi_page.o rpi_json.o rpi_func.o rpi_html.o \
			gen_http.o globals.o rpi_debug.o
OBJ		+= $(OBJX)
OBJ		+= $(OBJM)

debug:	LIBCOM = -lcommondebug
debug:	CFLAGS = -O0 -Wall -rdynamic -ggdb
debug:	DEFS += -D FEATURE_USE_PRINTF -D DEBUG_ASSERT
debug:	all

trace:	LIBCOM = -lcommon
trace:	CFLAGS = -O2 -Wall -ggdb
trace:	DEFS += -D FEATURE_TRACE_PRINTF
trace:	all

release: CFLAGS = -O2 -Wall -ggdb
release: all

all: modules $(DESTDIR)/$(TARGET)

modules:
$(DESTDIR)/$(TARGET): $(OBJ)
	$(CC) $(CFLAGS) $(DEFS) -o $@ $(OBJ) $(LIBFLGS) $(LIBS) $(LIBSIO) $(LIBCOM) $(LDFLGS)
################################################################################
# 14 Apr 2024: Do NOT strip, it removes the symbols for GDB and SEGM Fault logs.
################################################################################
#	strip $(DESTDIR)/$(TARGET)

sync:
	@echo "RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	cp -u /mnt/rpi/softw/$(RPIDIR)/*.c ./
	cp -u /mnt/rpi/softw/$(RPIDIR)/*.h ./
	cp -u /mnt/rpi/softw/$(RPIDIR)/makefile ./
	cp -u /mnt/rpi/softw/$(RPIDIR)/rebuild ./
	cp -u /mnt/rpi/softw/$(RPIDIR)/$(TARGET).service ./
	cp -u /mnt/rpi/softw/$(RPIDIR)/Filtersoon.lst ./
	cp -u /mnt/rpi/softw/$(RPIDIR)/runsense ./ 2>/dev/null || :
	cp -u /mnt/rpi/softw/$(RPIDIR)/mailxtest ./ 2>/dev/null || :

forcesync:
	@echo "RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	cp /mnt/rpi/softw/$(RPIDIR)/*.c ./
	cp /mnt/rpi/softw/$(RPIDIR)/*.h ./
	cp /mnt/rpi/softw/$(RPIDIR)/makefile ./
	cp /mnt/rpi/softw/$(RPIDIR)/rebuild ./
	cp /mnt/rpi/softw/$(RPIDIR)/$(TARGET).service ./
	cp /mnt/rpi/softw/$(RPIDIR)/Filtersoon.lst ./
	cp /mnt/rpi/softw/$(RPIDIR)/runsense ./ 2>/dev/null || :
	cp /mnt/rpi/softw/$(RPIDIR)/mailxtest ./ 2>/dev/null || :

html:
	@echo "RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	cp -u /mnt/rpi/softw/$(TARGET)/html/*.html /home/public/www/
	cp -u /mnt/rpi/softw/$(TARGET)/html/*.gif /home/public/www/
	cp -u /mnt/rpi/softw/$(TARGET)/html/*.ico /home/public/www/
	cp -u /mnt/rpi/softw/$(TARGET)/html/*.jpg /home/public/www/

ziplog:
	@echo "Zip logfile: RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	sudo gzip -c $(BUPDIR)/$(TARGET).log >>$(BUPDIR)/$(TARGET)logs.gz
	sudo rm -rf $(BUPDIR)/$(TARGET).log
	sudo rm -rf /mnt/rpicache/$(TARGET).log

copylog:
	@echo "Copy logfile to PC: RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	sudo cp /mnt/rpicache/$(TARGET).log /mnt/rpi/softw/$(RPIDIR)/$(TARGET).log 2>/dev/null || :

sortlog:
	@echo "Sort and copy logfile to PC: RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	sudo cat /mnt/rpicache/$(TARGET).log | sort -n >/mnt/rpicache/$(TARGET)sort.log
	sudo cp /mnt/rpicache/$(TARGET)sort.log /mnt/rpi/softw/$(RPIDIR)/$(TARGET).log
	sudo rm -rf /mnt/rpicache/$(TARGET)sort.log

dellog:
	@echo "Delete logfiles: RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	sudo rm -rf $(BUPDIR)/$(TARGET).log
	sudo rm -rf /mnt/rpicache/$(TARGET).log

delmap:
	@echo "Delete mapfiles: RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	sudo rm -rf /mnt/rpicache/$(TARGET).map
	sudo rm -rf $(BUPDIR)/$(TARGET).map

clean:
	@echo "RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	rm -rf *.o
	rm -rf $(DESTDIR)/$(TARGET)

cli:
	mv -f $(DESTDIR)/$(TARGET) $(DESTDIR)/clislim

install:
	@echo "Install: RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	sudo systemctl stop $(TARGET).service
	cp $(DESTDIR)/$(TARGET) $(DESTDIR)/$(TARGET).latest 2>/dev/null || :
	@echo "Zip logfile: RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	@echo `date`: New Install RPi-$(TARGET) Board=RPi#$(BOARD) >>$(BUPDIR)/$(TARGET).log
	sudo gzip -c $(BUPDIR)/$(TARGET).log 2>/dev/null >>$(BUPDIR)/$(TARGET)logs.gz
	sudo rm -rf $(BUPDIR)/$(TARGET).log
	sudo rm -rf /mnt/rpicache/$(TARGET).log
	sudo cp $(TARGET).service /lib/systemd/system/
	sudo cp $(DESTDIR)/$(TARGET) /usr/sbin/$(TARGET)
	sudo cp Filtersoon.* $(BUPDIR)/
	sudo systemctl restart $(TARGET).service
	sudo systemctl daemon-reload

run:
	sudo cp $(DESTDIR)/$(TARGET) /usr/sbin/$(TARGET)
	sudo systemctl restart $(TARGET).service

screen3:
	@echo "Screen-run($(TARGET)): Debug mode, Board=RPi#$(BOARD)"
	sudo screen -S $(TARGET) $(DESTDIR)/debug/$(TARGET) -S15 -v3 -d1

screen4:
	@echo "Screen-run($(TARGET)): Debug mode, Board=RPi#$(BOARD)"
	sudo screen -S $(TARGET) $(DESTDIR)/debug/$(TARGET) -S15 -v4 -d1

restart:
	sudo systemctl stop $(TARGET).service
	sudo cp $(DESTDIR)/$(TARGET) /usr/sbin/$(TARGET)
	sudo systemctl restart $(TARGET).service

reload:
	sudo cp $(DESTDIR)/$(TARGET) /usr/sbin/$(TARGET)
	sudo cp $(TARGET).service /lib/systemd/system/$(TARGET).service
	sudo systemctl enable $(TARGET).service
	sudo systemctl start $(TARGET).service

setup:
	sudo cp $(BUPDIR)/$(TARGET).map /mnt/rpicache/$(TARGET).map 2>/dev/null || :
	sudo cp $(BUPDIR)/$(TARGET).log /mnt/rpicache/$(TARGET).log	2>/dev/null || :
	sudo cp $(TARGET).service /lib/systemd/system/$(TARGET).service

.c.o: $(DEPS)
	$(CC) -c $(CFLAGS) $(DEFS) $(INCDIR) $< 
