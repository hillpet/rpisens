/*  (c) Copyright:  2024  Patrn ESS, Confidential Data
 *
 *  Workfile:           rpi_debug.h
 *  Purpose:            Headerfile for debug thread
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    18 Apr 2024:      Add Debug thread
 *    23 Apr 2024:      Split globals.h and locals.h
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _RPI_DEBUG_H_
#define _RPI_DEBUG_H_ 

typedef struct DBCMD
{
   int         iCmd;
   bool      (*pfDbCmd)(const struct DBCMD *);
   const char *pcHelp;
}  DBCMD;

typedef enum _en_db_command_
{
   #define  EXTRACT_DEB(a,b,c,d)   a,
   #include "cmd_debug.h"
   #undef EXTRACT_DEB
   //
   NUM_ENDB
}  ENDB;

//
// Global functions
//
int DBG_Init               (void);

#endif /* _RPI_DEBUG_H_ */
