/*  (c) Copyright:  2020..2024 Patrn, Confidential Data
 *
 *  Workfile:           rpi_main.c
 *  Purpose:            main()
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PIC 16f628A Ultrasoon sensor
 * 
 *  Author:             Peter Hillen
 *  Changes:
 *    01 Nov 2020:      Ported from rpislim
 *    24 Nov 2020:      Add SENSOR Pump Mode
 *    30 Nov 2020:      Add Xref file symbol read
 *    01 May 2021:      Add CVS records to day file; Add CLI startup mode
 *    02 May 2021:      Fix KINS_MODE_POWEROFF bug
 *    03 May 2021:      Add CLI -S xx for CSV sampling time
 *    20 May 2021:      Backup csv files to persistent filesystem
 *    07 Aug 2021:      Do NOT ignore too STEEP rise/fall samples
 *    12 Aug 2021:      Add PVC Floater offset to waterlevel measurement
 *    23 Mar 2022:      Add Verbose RAM/EEPROM logging
 *    29 Mar 2022:      Add Variadic macro's for PRINTF()
 *    21 May 2022:      Add Watchdog SIGHUP heartbeat
 *    25 Jun 2022:      Add Guard thread
 *    19 Apr 2024:      Add Debug thread and MM_DEBUG_ print; Remove X10 prints
 *    24 Apr 2024:      Move to global/local startup 
 *    25 Apr 2024:      Move guard duty and watchdog heartbeat to Guard thread
 *    04 May 2024:      Add global signal handling
 *    08 May 2024:      Adapt nighttime pump modes
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <string.h>
#include <semaphore.h>
#include <stddef.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <time.h>
#include <resolv.h>
#include <sys/types.h>
#include <sys/signal.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <pwd.h>
//
#include <common.h>
#include "config.h"
#include "locals.h"
#include "globals.h"
//
#include "rpi_srvr.h"
#include "rpi_func.h"
#include "rpi_comm.h"
#include "rpi_sens.h"
#include "rpi_guard.h"
#include "rpi_debug.h"
#include "rpi_miac.h"
#include "gen_button.h"
#include "rpi_mail.h"
#include "rpi_mailx.h"
#include "rpi_main.h"

#define USE_PRINTF
#include <printx.h>

#if defined(__DATE__) && defined(__TIME__)
   static const char RPI_BUILT[] = __DATE__ " " __TIME__;
#else
   static const char RPI_BUILT[] = "Unknown";
#endif

#ifdef      DEBUG_ASSERT                  // DEBUG BUILD 
//-----------------------------------------------------------------------------
//    DEBUG Mode
//-----------------------------------------------------------------------------
   static   void rpi_DisplaySmartSensor(const char *, SENS *);
   static   void rpi_DisplaySmartFloats(const char *);

   #define  RPI_DAEMON()                  LOG_printf("rpi-Deamon():Debug mode !" CRLF);
   #define  RPI_DISPLAY(a,b)
// #define  RPI_DISPLAY(a,b)              rpi_DisplaySmartSensor(a,b)
   #define  RPI_DAY(a)
// #define  RPI_DAY(a)                    rpi_DisplaySmartFloats(a)
//
// Dump serial port data
//
// #define  FEATURE_DUMP_DATA
   #ifdef   FEATURE_DUMP_DATA
   #define  LOG_DUMPDATA(x,y,z)           LOG_DumpData(x,y,z)
   #else    //FEATURE_DUMP_DATA
   #define  LOG_DUMPDATA(x,y,z)
   #endif   //FEATURE_DUMP_DATA
//

#else                                     //DEBUG_ASSERT
//-----------------------------------------------------------------------------
//    RELEASE Mode
//-----------------------------------------------------------------------------
   static   void rpi_Daemon               (void);
   #define  RPI_DAEMON()                  rpi_Daemon();
   #define  RPI_DISPLAY(a,b)
   #define  RPI_DAY(a)
   #define  LOG_DUMPDATA(x,y,z)
#endif      //DEBUG_ASSERT
//
static void    rpi_Execute                (void);
//
static int     rpi_BackupCsvFile          (void);
static int     rpi_BackupLogfile          (void);
static KPUMP   rpi_CheckKinshiFlow        (u_int32, KPUMP, SENS *);
static bool    rpi_CheckLogFull           (u_int32);
static bool    rpi_CheckMail              (u_int32);
static bool    rpi_CheckMidnight          (u_int32);
static bool    rpi_CheckSmartSensor       (u_int32);
static bool    rpi_CheckPowerLevel        (u_int32);
static bool    rpi_CheckPowerOff          (u_int32, SENS *);
static KPUMP   rpi_CheckPump              (u_int32, KPUMP);
static bool    rpi_CheckRestart           (u_int32);
static bool    rpi_CreateNewLog           (void);
static bool    rpi_CheckWan               (u_int32);
static bool    rpi_ExtractSystemVars      (SVAR);
static int     rpi_HandleCsvRecord        (u_int32, char *);
static bool    rpi_HandleSensorTransfer   (SMCMD);
static void    rpi_PowerOff               (void);
static void    rpi_Reboot                 (bool);
static int     rpi_RestoreCsvFile         (void);
static void    rpi_SetupGpio              (void);
static bool    rpi_SetupUser              (const char *);
//
static void    rpi_SensorAverage          (SMDATA *, SENS *);
static bool    rpi_SensorCheckCrc         (char *);
static void    rpi_SensorSum              (SENS *, SENS *);
static bool    rpi_SensorConvertData      (char *, SENS *);
static void    rpi_SensorSetTimers        (u_int32);
static void    rpi_SensSampleDay          (u_int32, SENS *, SENS *);
static void    rpi_SensSampleMin          (u_int32, SENS *, SENS *);
static void    rpi_SensSampleMon          (u_int32, SENS *, SENS *);
static void    rpi_SensSampleQtr          (u_int32, SENS *, SENS *);
static void    rpi_SensSampleTot          (u_int32, SENS *, SENS *);
static void    rpi_SensSampleCsv          (u_int32, SENS *, SENS *);
static void    rpi_SensUpdateGlobals      (KINSP, SENS *, SMDATA *);
//
static int     rpi_SignalMemoryChanged    (void *);
static int     rpi_SignalMemoryEeprom     (void *);
static int     rpi_SignalMemoryError      (void *);
static int     rpi_SignalMemoryMiac       (void *);
static int     rpi_SignalMemoryRam        (void *);
static int     rpi_SignalSensorPing       (void *);
static int     rpi_SignalSensorPong       (void *);
static int     rpi_SignalRequestPowerOff  (void *);
static int     rpi_SignalRequestReboot    (void *);
static int     rpi_SignalRequestTerminate (void *);
static int     rpi_SignalSegmentationFault(void *);
static int     rpi_SignalSendEmail        (void *);
static int     rpi_SignalSetTimers        (void *);
static int     rpi_SignalTransferMemory   (void *);
//
static bool    rpi_SignalRegister         (sigset_t *);
static void    rpi_ReceiveSignalInt       (int);
static void    rpi_ReceiveSignalTerm      (int);
static void    rpi_ReceiveSignalUser1     (int);
static void    rpi_ReceiveSignalUser2     (int);
static void    rpi_ReceiveSignalSegmnt    (int);
//
static void    rpi_CalculateWatermarks    (void);
static bool    rpi_CheckValidSample       (bool, char *, char *);
static int     rpi_Startup                (void);
static bool    rpi_ValidateSample         (u_int32, SENS *);
static bool    rpi_WaitStartup            (void);
//
// CLI functions
//
static void    rpi_Help                   (void);
static bool    rpi_RestoreActualData      (void);
static bool    rpi_RestoreMapData         (u_int32);
static void    rpi_RestoreMonthData       (int, int);
static void    rpi_ShowDay                (void);
static void    rpi_ShowMonth              (void);
static void    rpi_ShowSensorData         (int, int, int);
//
extern const double flTimerTickPerMm;
extern const double flMmPerTimerTick;
//
#ifdef   FEATURE_RUN_USERSPACE
const char             *pcNewUser   = "peter";
const char             *pcGroupWww  = "www-data";
#else    //FEATURE_RUN_USERSPACE
const char             *pcNewUser   = NULL;
const char             *pcGroupWww  = NULL;
#endif   //FEATURE_RUN_USERSPACE

//=================================================================================================
// Notification handlers
//=================================================================================================
static NFY stHostNotifications[] =
{
//    ePid        iFlag                pfCb(void *)       
   {  PID_HOST,   GLOBAL_SVR_HST_MEM,  rpi_SignalMemoryChanged          },
   {  PID_HOST,   GLOBAL_COM_HST_ROM,  rpi_SignalMemoryEeprom           },
   {  PID_HOST,   GLOBAL_COM_HST_ERR,  rpi_SignalMemoryError            },
   {  PID_HOST,   GLOBAL_COM_HST_MEM,  rpi_SignalMemoryMiac             },
   {  PID_HOST,   GLOBAL_COM_HST_RAM,  rpi_SignalMemoryRam              },
   {  PID_HOST,   GLOBAL_ALL_HST_END,  rpi_SignalRequestTerminate       },
   {  PID_HOST,   GLOBAL_ALL_HST_PWR,  rpi_SignalRequestPowerOff        },
   {  PID_HOST,   GLOBAL_ALL_HST_RBT,  rpi_SignalRequestReboot          },
   {  PID_HOST,   GLOBAL_ALL_HST_FLT,  rpi_SignalSegmentationFault      },
   {  PID_HOST,   GLOBAL_COM_HST_PPI,  rpi_SignalSensorPing             },
   {  PID_HOST,   GLOBAL_COM_HST_PPO,  rpi_SignalSensorPong             },
   {  PID_HOST,   GLOBAL_SVR_HST_TMR,  rpi_SignalSetTimers              },
   {  PID_HOST,   GLOBAL_DBG_HST_MEM,  rpi_SignalTransferMemory         },
   {  PID_HOST,   GLOBAL_ALL_HST_EML,  rpi_SignalSendEmail              }
};
static int iNumHostNotifications = sizeof(stHostNotifications) / sizeof(NFY);
//
//
const char *pcHelp                  =  "\n"
                                       "RpiSens Kinshi monitor Command line options:\n"
                                       "====================================================================\n"
                                       "  -Y 2017  : Set Year\n"
                                       "  -M 11    : Set Month\n"
                                       "  -D 25    : Show SENSOR data month & days (ex: -Y 2017 -M 11 -D 25)\n"
                                       "  -B 11    : Build month file from day files (ex: -Y 2017 -B 11)\n"
                                       "\n"
                                       "  -R       : Restore MAP G_stThis* data from file\n"
                                       "  -S x     : CSV Sample interval in secs\n"
                                       "  -a 1000  : Expand map file persistent area\n"
                                       "  -r       : Show RTC data and exit\n"
                                       "  -h       : Show help, check MMAP and exit\n"
                                       "  -z 1000  : Expand map file zero-reset area\n"
                                       "\n"
                                       "  SENSOR modes\n"
                                       "====================================================================\n"
                                       "  -G       : Show global map file expand area and exit\n"
                                       "  -d x     : Run command-line debugger on/off\n"
                                       "  -v x     : Sensor Mode Verbose bits\n"
                                       "               Start in Sensor-Idle mode: 0x01\n"
                                       "               Reset uCtlr:               0x02\n"
                                       "               Start in Sensor mode       0x04\n"
                                       "               Simulation mode:           0x80\n"
                                       "  -T 10000 : Simulation sample period in mSecs\n"
                                       "  -X       : Force RpiDog control (Heartbeat/restart/reboot)\n"
                                       "\n";
//
// Generic timestamp
//
static int         iTransferIndex      = 0;
static int         iTransferRetry      = 0;
static int         iTransferAgain      = 0;
static int         iLoopTimeout        = DEFAULT_TIMEOUT;
static int         iCsvSampleSecs      = 60;
static const char *pcMapFile           = RPI_MAP_PATH;
static const char *pcLogfile           = RPI_PUBLIC_LOG_PATH;
static const char *pcCsvFile           = "%s/SmartSensor_%s.csv";
static const char *pcShellBackupCsv    = "cp " RPI_WORK_DIR "SmartSensor*.csv "   RPI_BACKUP_DIR;
static const char *pcShellRestorCsv    = "cp " RPI_BACKUP_DIR "SmartSensor*.csv " RPI_WORK_DIR;
//
static int         iSensorResets       = MAX_RETRIES_NO_SENS;
static int         tCsvFile            = 0;
static int         iCsvDay             = 0;
static bool        fHostRunning        = TRUE;
static bool        fRpiPowerOff        = FALSE;
static bool        fRpiDogControl      = FALSE;
static bool        fRpiDaemonMode      = FALSE;
static u_int32     ulSecsRestart       = 0;
static u_int32     ulSecsPoweroff      = 0;
static u_int32     ulSecsMailTimeout   = 0;
static u_int32     ulEmailPending      = 0;
static int         iMaxEmailRetries    = 0;
static double      flCpuTemp           = 0.0;
static char       *pcSensorData        = NULL;
//
static const char *pcSwVersion         = VERSION;
static const char *pcCacheDir          = RPI_WORK_DIR;
static const char *pcDirBackup         = RPI_BACKUP_DIR;
static const char *pcDirWork           = RPI_WORK_DIR;
static const char *pcSymbols           = RPI_SYMBOL_FILE;
//
static const char *pcPowerOff          = "shutdown -h -t 1 \"Power OFF in 1 min\" ";
static const char *pcRebootDelayed     = "shutdown -r 1 \"Reboot in 1 min\" ";
static const char *pcRebootNow         = "reboot";
static const char *pcSeparator         = "=================================================================================================================";
//
static const SDATA stSystemVars[]      =
{
   { NULL,                                      NULL           },
   { "/sys/class/thermal/thermal_zone0/temp",   &flCpuTemp     },
   //
   { NULL,                                      NULL           }
};
//
// Sensor command strings
//
static const char *pcSensorCommands[] = 
{
#define  EXTRACT_SMC(a,b)   b,
#include "gen_cmds.h"
#undef   EXTRACT_SMC
};
//
static SMCMD ptTransferCommands[] =
{
   SMCMD_INIT,
   SMCMD_SET_RAM, SMCMD_DELAY, SMCMD_DLD_RAM, SMCMD_IDLE_MODE,
   SMCMD_SET_ROM, SMCMD_DELAY, SMCMD_DLD_ROM, SMCMD_IDLE_MODE,
   SMCMD_DELAY,   SMCMD_READY
};
static int iNumTransferCmds = sizeof(ptTransferCommands)/sizeof(SMCMD);

/*----------------------------------------------------------------------
_______________GLOBAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/

//
//  Function:   main
//  Purpose:    Main entry point for the Raspberry Pi Smart Sensor monitor
//
//  Parms:      Commandline options
//  Returns:    Exit codes
//
int main(int argc, char **argv)
{
   bool        fSaveMap=FALSE;
   u_int32     ulSecsNow;
   int         iNr, iOpt, iAddZero=0, iAddSpare=0, iCmdLineAction=CLACT_NONE;
   int         iDay=0, iMonth=0, iYear=0, iBuild=0;
   sigset_t    tBlockset;
   char       *pcBuffer=NULL;

   //
   // At this stage, we should be ROOT.
   // If we use IO, it probably has to init before switching to user mode !
   // Make sure we have the right mmap parameters setup for us !
   //
   rpi_SetupGpio();
   //
   // Pending FEATURE_RUN_USERSPACE, we run as root (pcNewUser=NULL) or normal user
   // Also initialize the GLOBAL MMAP persistent filesystem
   //
   if(!rpi_SetupUser(pcNewUser))
   {
      printf("main():Error setting up user [%s]" CRLF, pcNewUser);
      exit(EXIT_CC_GEN_ERROR);
   }
   //==============================================================================================
   // Truncate reports file
   //==============================================================================================
   if(pstMap->G_iLogLines == 0) pstMap->G_iLogLines = MAX_NUM_REPORT_LINES;
   if(GEN_FileTruncate((char *)pcLogfile, pstMap->G_iLogLines) != EXIT_CC_OKEE)
   {
      printf("main():Error truncating LOG file [%s]" CRLF, pcLogfile);
      exit(EXIT_CC_GEN_ERROR);
   }
   else printf("main():Truncating LOG file [%s] to %d lines" CRLF, pcLogfile, pstMap->G_iLogLines);
   //
   // Open logfile
   //
   if( LOG_ReportOpenFile((char *)pcLogfile) == FALSE)
   {
      printf("main():Error opening log file [%s]" CRLF, pcLogfile);
      exit(EXIT_CC_GEN_ERROR);
   }
   else 
   {
      //
      // Track the LOG fileposition, we must update the Global G_ variable when we have the MMAP 
      //
      pstMap->G_llLogFilePos = LOG_ReportFilePosition();
      printf("main():Logfile is %s (at pos %lld)" CRLF, pcLogfile, pstMap->G_llLogFilePos);
   }
   printf("main():" RPI_APP_TEXT " has %d args" CRLF, argc);
   //
   // Args:
   //    -Y 2017  : Set Year
   //    -M 11    : Set Month
   //    -D 25    : Show SENSOR data month & days (ex: -Y 2017 -M 11 -D 25)
   //    -B 11    : Build month file from day files (ex: -Y 2017 -B 11)
   //
   //    -R       : Restore MAP G_stThis* data from file
   //    -S x     : CSV Sample interval in secs
   //    -a 1000  : Expand map file persistent area
   //    -h       : Show help, check MMAP and exit
   //    -r       : Show RTC data and exit
   //    -z 1000  : Expand map file zero-reset area
   //    
   //    SENSOR modes
   //    ================================================
   //    -G       : Show global map file expand area and exit
   //    -d x     : Run command-line debugger on/off
   //    -v x     : Sensor Mode Verbose bits
   //                Start in Sensor-Idle mode: 0x01
   //                Reset uCtlr:               0x02
   //                Start in Sensor mode       0x04
   //                Simulation mode:           0x80
   //    -T 10000 : Simulation sample period in mSecs
   //    -X       : Force RpiDog control (Heartbeat/restart/reboot)
   //
   while ((iOpt = getopt(argc, argv, "B:D:GM:RS:T:XY:a:d:hrv:z:")) != -1)
   {
      switch (iOpt)
      {
         case 'B':
            printf("rpi-main():Option -B=%s" CRLF, optarg);
            iBuild = atoi(optarg);
            break;

         case 'D':
            printf("rpi-main():Option -D=%s" CRLF, optarg);
            iDay = atoi(optarg);
            break;

         case 'G':
            printf("rpi-main():Option -G" CRLF);
            iCmdLineAction = CLACT_EXPAND_MAP;
            break;

         case 'M':
            printf("rpi-main():Option -M=%s" CRLF, optarg);
            iMonth = atoi(optarg);
            break;

         case 'R':
            printf("rpi-main():Option -R=%s" CRLF, optarg);
            rpi_RestoreActualData();
            GLOBAL_Close(TRUE);
            exit(EXIT_CC_OKEE);
            break;

         case 'S':
            printf("rpi-main():Option -S=%s" CRLF, optarg);
            iCsvSampleSecs = atoi(optarg);
            break;

         case 'T':
            printf("rpi-main():Option -T=%s: ** Simulation mode **" CRLF, optarg);
            pstMap->G_iSimulateTime = atoi(optarg);
            break;

         case 'X':
            printf("rpi-main():Option -X" CRLF);
            fRpiDogControl = TRUE;
            break;

         case 'Y':
            printf("rpi-main():Option -Y=%s" CRLF, optarg);
            iYear = atoi(optarg);
            break;

         case 'a':
            printf("rpi-main():Option -a=%s" CRLF, optarg);
            iAddSpare = atoi(optarg);
            break;

         case 'd':
            printf("rpi-main():Option -d=%s" CRLF, optarg);
            if(strtol(optarg, NULL, 0)) GLOBAL_DebugSet(MM_DEBUG_ENABLED);
            else                        GLOBAL_DebugClr(MM_DEBUG_ENABLED);
            break;

         case 'h':
            rpi_Help();
            GLOBAL_Close(FALSE);
            exit(EXIT_CC_OKEE);
            break;

         case 'r':
            pcBuffer = safemalloc(128);
            for(int iYr=1970; iYr<2024; iYr++)
            {
               ulSecsNow = RTC_GetSecs(iYr, 1, 1);
               printf("rpi-main():RTC_GetSecs() Year=%4d  = %10ld Secs" CRLF, iYr, ulSecsNow);
            }
            //
            // Get SystemSecs and show if DST is correctly included
            //
            ulSecsNow = RTC_GetSystemSecs();
            printf("rpi-main():RTC_GetSystemSecs()      = %ld Secs" CRLF, ulSecsNow);
            printf("rpi-main():RTC_GetMidnight()        = %ld Secs" CRLF, RTC_GetMidnight(ulSecsNow));
            printf("rpi-main():RTC_GetSecs() Midnight   = %ld Secs" CRLF, RTC_GetSecs(RTC_GetYear(ulSecsNow), RTC_GetMonth(ulSecsNow), RTC_GetDay(ulSecsNow)));
            //
            printf("rpi-main():RTC_GetYear()            = %d" CRLF, RTC_GetYear(ulSecsNow));
            printf("rpi-main():RTC_GetMonth()           = %d" CRLF, RTC_GetMonth(ulSecsNow));
            printf("rpi-main():RTC_GetDay()             = %d" CRLF, RTC_GetDay(ulSecsNow));
            printf("rpi-main():RTC_GetHour()            = %d" CRLF, RTC_GetHour(ulSecsNow));
            printf("rpi-main():RTC_GetMinute()          = %d" CRLF, RTC_GetMinute(ulSecsNow));
            printf("rpi-main():RTC_GetSecond()          = %d" CRLF, RTC_GetSecond(ulSecsNow));
            //
            RTC_ConvertDateTime(TIME_FORMAT_WW_S_DD_MM_YYYY_HH_MM_SS, pcBuffer, ulSecsNow);
            printf("rpi-main():RTC_ConvertDateTime()    = %s" CRLF, pcBuffer);
            pcBuffer = safefree(pcBuffer);
            iCmdLineAction = CLACT_EXIT;
            break;

         case 'v':
            pstMap->G_iVerbose = (int)strtol(optarg, NULL, 0);
            printf("rpi-main():Option -v=%d (0x%x)" CRLF, pstMap->G_iVerbose, pstMap->G_iVerbose);
            break;

         case 'z':
            printf("rpi-main():Option -z=%s" CRLF, optarg);
            iAddZero = atoi(optarg);
            break;

         default:
            printf("rpi-main(): Invalid option %c" CRLF, (char) iOpt);
            break;
      }
   }
   switch(iCmdLineAction)
   {
      default:
         break;

      case CLACT_EXPAND_MAP:
         {
            //
            // Cli -G:
            // Show the MMAP signature locations. It should enable us to expand the MMAP
            // by allocating additional spare space in the Global MMAP file
            //    G_pcSparePoolZero       [SPARE_POOL_ZERO-EXTRA_POOL_ZERO];
            //    G_iResetEnd; 
            //
            int   iSize;
            void *pvTmp1;
            void *pvTmp2;

            pvTmp1  = &pstMap->G_iResetEnd;
            pvTmp2  = &pstMap->G_pcSparePoolZero;
            iSize   = (int) (pvTmp1 - pvTmp2 - SPARE_POOL_ZERO);
            printf("rpi-main():SpareZero Start=%p" CRLF, pstMap->G_pcSparePoolZero);
            printf("rpi-main():SpareZero End  =%p" CRLF, &pstMap->G_iResetEnd);
            printf("rpi-main():SpareZero Add  =%d" CRLF, iSize);
            //
            pvTmp1  = &pstMap->G_iSignature2;
            pvTmp2  = &pstMap->G_pcSparePoolPers;
            iSize   = (int) (pvTmp1 - pvTmp2 - SPARE_POOL_PERS);
            printf("rpi-main():SparePers Start=%p" CRLF, &pstMap->G_iSignature2);
            printf("rpi-main():SparePers End  =%p" CRLF, pstMap->G_pcSparePoolPers);
            printf("rpi-main():SparePers Add  =%d" CRLF, iSize);
            //
            // Update the Solis history:
            //    - SmartS_yyymm.bin
            //    - SmartS_yyymmdd.bin
            //
            GLOBAL_Close(FALSE);
            exit(EXIT_CC_OKEE);
         }
         break;

      case CLACT_EXIT:
         GLOBAL_Close(FALSE);
         exit(EXIT_CC_OKEE);
         break;
      
   }
   if(iDay && iMonth && iYear)
   {
      printf("rpi-main(): Show Smart Sensor Data %d:%d:%d" CRLF, iYear, iMonth, iDay);
      rpi_ShowSensorData(iYear, iMonth, iDay);
      GLOBAL_Close(TRUE);
      exit(EXIT_CC_OKEE);
   }

   if(iBuild && iYear)
   {
      printf("rpi-main(): Restore month %d:%d" CRLF, iYear, iBuild);
      rpi_RestoreMonthData(iYear, iBuild);
      GLOBAL_Close(TRUE);
      exit(EXIT_CC_OKEE);
   }

   if( (iAddZero>0) || (iAddSpare>0) )
   {
      printf("rpi-main(): Expand MAP zero:%d spare:%d" CRLF, iAddZero, iAddSpare);
      //
      // CL Option to expand the mmap file: do it and exit
      //
      GLOBAL_ExpandMap(iAddZero, iAddSpare);
      //
      printf("rpi-main(): Sizeof int       = %d" CRLF, sizeof(int));
      printf("rpi-main(): Sizeof long      = %d" CRLF, sizeof(long));
      printf("rpi-main(): Sizeof long long = %d" CRLF, sizeof(long long));
      printf("rpi-main(): Sizeof float     = %d" CRLF, sizeof(float));
      printf("rpi-main(): Sizeof double    = %d" CRLF, sizeof(double));
      printf("rpi-main(): Sizeof Day       = %d" CRLF, sizeof(pstMap->G_stThisDay));
      printf("rpi-main(): Sizeof Month     = %d" CRLF, sizeof(pstMap->G_stThisMonth));
      printf("rpi-main(): Sizeof SENS      = %d" CRLF, sizeof(SENS));
      //
      printf("rpi-main(): Buffer DispDay   = 0x%X" CRLF, offsetof(RPIMAP, G_stDispDay  ));
      printf("rpi-main(): Buffer DispMonth = 0x%X" CRLF, offsetof(RPIMAP, G_stDispMonth));
      printf("rpi-main(): Buffer LastMonth = 0x%X" CRLF, offsetof(RPIMAP, G_stLastMonth));
      printf("rpi-main(): Buffer ThisMonth = 0x%X" CRLF, offsetof(RPIMAP, G_stThisMonth));
      printf("rpi-main(): Buffer LastDay   = 0x%X" CRLF, offsetof(RPIMAP, G_stLastDay  ));
      printf("rpi-main(): Buffer ThisDay   = 0x%X" CRLF, offsetof(RPIMAP, G_stThisDay  ));
      //
      printf("rpi-main(): Done:" CRLF);
      //
      GLOBAL_Close(TRUE);
      exit(EXIT_CC_OKEE);
   }

   #ifdef FEATURE_PI_JUICE
   {
      //
      // {'data': {'battery': 'CHARGING_FROM_IN', 'powerInput5vIo': 'NOT_PRESENT', 'isFault': True, 'isButton': False, 'powerInput': 'PRESENT'}, 'error': 'NO_ERROR'}
      // {'data':78, 'error':'NO_ERROR'}
      //
      bool     fCc=TRUE;
      bool     fVal;
      int      iVal;
      NMAPH   *pstNmap;

      printf("rpi-main():%s v%s-%d.%d (%s)" CRLF, RPI_APP_TEXT, pcSwVersion, DATA_VERSION_MAJOR, DATA_VERSION_MINOR, RPI_BUILT);
      //
      // Putout PiJuice status
      //
      pcBuffer = safemalloc(1024);
      pstNmap  = safemalloc(sizeof(NMAPH));
      if(PJ_GetStatus(PJ_CHARGE,  &iVal,       1))    printf("rpi-main(): PiJuice Battery Charge Level = %d%%" CRLF, iVal);     else fCc = FALSE;
      if(PJ_GetStatus(PJ_BATTERY, pcBuffer, 1000))    printf("rpi-main(): PiJuice Battery status       = %s" CRLF, pcBuffer);   else fCc = FALSE;
      if(PJ_GetStatus(PJ_5V,      pcBuffer, 1000))    printf("rpi-main(): PiJuice IO 5V Input          = %s" CRLF, pcBuffer);   else fCc = FALSE;
      if(PJ_GetStatus(PJ_FAULT,   &fVal,       1))    printf("rpi-main(): PiJuice Fault status         = %d" CRLF, fVal);       else fCc = FALSE;
      if(PJ_GetStatus(PJ_BUTTON,  &fVal,       1))    printf("rpi-main(): PiJuice Button status        = %d" CRLF, fVal);       else fCc = FALSE;
      if(PJ_GetStatus(PJ_POWER,   pcBuffer, 1000))    printf("rpi-main(): PiJuice Power Input          = %s" CRLF, pcBuffer);   else fCc = FALSE;
      if(PJ_GetStatus(PJ_ERROR,   pcBuffer, 1000))    printf("rpi-main(): PiJuice Error status         = %s" CRLF, pcBuffer);   else fCc = FALSE;
      //
      if(fCc == FALSE)                                printf("rpi-main(): PiJuice ERROR !!" CRLF);
      //
      // Determine hosts on the network
      //
      NMAP_Init(pstNmap);
      if(NMAP_Get(pstNmap, NMAP_CMD_NR_HOSTS, &iVal, 1))    printf("rpi-main():NMAP: %d hosts found", iVal);
      NMAP_Exit(pstNmap);
      pstNmap  = safefree(pstNmap);
      pcBuffer = safefree(pcBuffer);
   }
   #endif
   //
   // Spin off threads
   //
   RPI_DAEMON();
   //
   GLOBAL_PidPut(PID_HOST, getpid());
   //
   if(fRpiDaemonMode || fRpiDogControl) LOG_Report(0, "HST", "Watchdog (" RPI_WATCHDOG ") is ACTIVE");
   else                                 LOG_Report(0, "HST", "Watchdog (" RPI_WATCHDOG ") is NOT ACTIVE");
   //
   // Daemon mode (if not in debug build):
   //    the HOST has exited already and we are now running in the background
   //
   if(rpi_SignalRegister(&tBlockset) == FALSE)
   {
      PRINTF("rpi-main():Exit Register ERROR:%s" CRLF, strerror(errno));
      GLOBAL_Close(FALSE);
      exit(EXIT_CC_GEN_ERROR);
   }
   //
   GLOBAL_SemaphoreInit(PID_HOST);
   //
   // Most system inits completed: open LOG file
   // Init the global LOG/Trace functions
   //
   if(!LOG_Init())
   {
      PRINTF("main(): LOG init ERROR");
      GLOBAL_SemaphoreDelete(PID_HOST);
      GLOBAL_Close(FALSE);
      exit(EXIT_CC_GEN_ERROR);
   }
   //
   // Check if the MMAP file was properly restored or not
   //
   if(pstMap->G_fInit) 
   {
      LOG_Report(0, "HST", "Main():MMAP file signature or DB version mismatch, created new Mapping:");
      PRINTF("Main():MMAP file signature or DB version mismatch, created new Mapping:" CRLF);
   }
   else 
   {
      LOG_Report(0, "HST", "Main():MMAP file OKee");
      PRINTF("Main():MMAP file OKee" CRLF);
   }
   LOG_Report(0, "HST", "Main():" RPI_APP_TEXT " v%s-%d.%d (%s)", pcSwVersion, pstMap->G_iVersionMajor, pstMap->G_iVersionMinor, RPI_BUILT);
   PRINTF("rpi-main():%s v%s-%d.%d (%s)" CRLF, RPI_APP_TEXT, pcSwVersion, pstMap->G_iVersionMajor, pstMap->G_iVersionMinor, RPI_BUILT);
   PRINTF("rpi-main():Logfile is %s" CRLF, pcLogfile);
   PRINTF("rpi-main():Master IP Addr=192.168.178.%s" CRLF, IPADDR);
   PRINTF("rpi-main():Verbose: 0x%04X (%d)" CRLF, pstMap->G_iVerbose, pstMap->G_iVerbose);
   //
   // Init GPIO and read Smart Sensor symbol definitions from file
   //
   GEN_Init();
   GEN_STRNCPY(pstMap->G_pcSwVersion, pcSwVersion, MAX_PARM_LEN);
   rpi_RestoreCsvFile();
   iNr = MIAC_Init(pcSymbols);
   if(iNr)
   {
      PRINTF("rpi-main():%d symbols read from %s" CRLF, iNr, pcSymbols);
      LOG_Report(0, "HST", "rpi-main():%d symbols read from %s", iNr, pcSymbols);
   }
   else
   {
      PRINTF("rpi-main():NO MIAC symbols available from %s" CRLF, pcSymbols);
      LOG_Report(0, "HST", "rpi-main():NO MIAC symbols available from %s", pcSymbols);
   }
   //
   // Start Smart Sensor thread
   // Pass pointer to ping_pong buffer
   //
   pstMap->G_iGuards = rpi_Startup();
   GLOBAL_SetSignalNotification(PID_HOST, GLOBAL_HST_ALL_INI);
   fHostRunning = rpi_WaitStartup();
   //
   // Display all pids in log file
   // Run permission for the threads
   //
   GLOBAL_PidsLog();
   GLOBAL_PidSetInit(PID_HOST, NULL);
   GLOBAL_PidTimestamp(PID_HOST, 0);
   GLOBAL_PidSaveGuard(PID_HOST, GLOBAL_HST_ALL_INI);
   //
   GLOBAL_SetSignalNotification(PID_GUARD, GLOBAL_HST_ALL_RUN);
   GLOBAL_SetSignalNotification(PID_SRVR,  GLOBAL_HST_ALL_RUN);
   GLOBAL_SetSignalNotification(PID_COMM,  GLOBAL_HST_ALL_RUN);
   GLOBAL_SetSignalNotification(PID_DEBUG, GLOBAL_HST_ALL_RUN);
   //
   PRINTF("rpi-main():All threads are Running" CRLF);
   //
   // Host main execute loop
   //
   rpi_Execute();
   //
   //==========================================================================
   // End of fHostRunning loop: Terminate all threads
   // If we made it this far, make sure to save all MMAP settings to file
   //==========================================================================
   fSaveMap = TRUE;
   LOG_Report(0, "HST", "EXIT %s v%s-%d.%d (%s)", RPI_APP_TEXT, pcSwVersion, pstMap->G_iVersionMajor, pstMap->G_iVersionMinor, RPI_BUILT);
   //
   GLOBAL_SemaphoreDelete(PID_HOST);
   LOG_Report(0, "HST", "Safe memory allocation=%d" CRLF, safemalloccount());
   LOG_ReportCloseFile();
   //
   GLOBAL_Sync();
   GLOBAL_Close(fSaveMap);
   //==============================================================================================
   // From here on :
   // CANNOT Use functions which use MMAP anymore:
   // CANNOT Use LOG_printf(), PRINTFx() or LOG_Report() due to the use of the G_tmutex !
   //==============================================================================================
   printf("rpi-main():EXIT %s v%s (%s)" CRLF, RPI_APP_TEXT, pcSwVersion, RPI_BUILT);
   //
   // Check if we need to power down
   //
   if(fRpiPowerOff) 
   {
      //
      // Powerdown: turn Yellow and Green LEDs on to see when the system is really down 
      // (the LEDs will no longer be driven actively and will dim.
      //
      LED(LED_Y, 1);
      LED(LED_G, 1);
      rpi_PowerOff();
   }
   else
   {
      //
      // Normal exit: turn OFF LEDs
      //
      GEN_Exit();
   }
   GEN_FFLUSH(stderr);
   GEN_FFLUSH(stdout);
   //
   exit(EXIT_CC_OKEE);
}

/*----------------------------------------------------------------------
________________LOCAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/

#ifndef  DEBUG_ASSERT
//
//  Function:   rpi_Daemon
//  Purpose:    Daemonize the watchdog
//
//  Parms:
//  Returns:
//
static void rpi_Daemon(void)
{
   pid_t    tPid;

   fRpiDaemonMode = TRUE;
   tPid           = fork();
   //
   switch(tPid)
   {
      default:
         // Parent (Commandline or service) 
         LOG_Report(0, "HST", "rpi-Daemon():Exit Parent, Rpi-Sense now daemonized.");
         exit(EXIT_CC_OKEE);
         break;

      case 0:
         // child
         PRINTF("rpi-Daemon(): RpiSense running background mode" CRLF);
         pstMap->G_iDebug = 0;
         break;

      case -1:
         // Error
         LOG_Report(errno, "HST", "rpi-Daemon():Exit fork ERROR:");
         exit(EXIT_CC_GEN_ERROR);
         break;
   }
}
#endif   //DEBUG_ASSERT

//
//  Function:   rpi_Execute
//  Purpose:    Run the host thread
//
//  Parms:
//  Returns:
//
static void rpi_Execute(void)
{
   bool        fCc;
   u_int32     ulSecsNow;
   int         iOpt, iRecordReceived=0;
   pid_t       tDogPid=0;
   KPUMP       tPumpState;
   KPUMP       tFlowState;

   char       *pcSample;
   SENS       *pstSensNow;
   SENS       *pstSensMin;
   SENS       *pstSensQtr;
   SENS       *pstSensDay;
   SENS       *pstSensMon;
   SENS       *pstSensTot;
   SENS       *pstSensCsv;

   RPI_DAY("Init");
   //
   // Init Sensor data
   //
   pstSensNow = &(pstMap->G_stSensNow);
   pstSensMin = &(pstMap->G_stSensMin);
   pstSensQtr = &(pstMap->G_stSensQtr);
   pstSensDay = &(pstMap->G_stSensDay);
   pstSensMon = &(pstMap->G_stSensMon);
   //
   // Total sensor data keeps track of running averages
   //
   pstSensTot = safemalloc(sizeof(SENS));       // The absolute totals
   pstSensCsv = safemalloc(sizeof(SENS));       // The CSV data for dayly storage
   pcSample   = safemalloc(KINS_SIZE_LENZ);     //
   //
   pstSensNow->iCrcErrors =  0;
   pstSensNow->iPeriod    =  0;
   pstSensMin->iPeriod    = -1;
   pstSensQtr->iPeriod    = -1;
   pstSensDay->iPeriod    = -1;
   pstSensMon->iPeriod    = -1;
   pstSensTot->iPeriod    = -1;
   pstSensCsv->iPeriod    = -1;
   //
   tPumpState = rpi_CheckPump(0, KINS_PUMP_INIT);
   tFlowState = rpi_CheckKinshiFlow(0, KINS_FLOW_INIT, pstSensNow);
   //
   /*==================================================================================================
   //
   // Temp set debug flags
   //
   pstMap->G_iDebugLevel = 1;
   GLOBAL_DebugSet(MM_DEBUG_HOST);
   GLOBAL_DebugClr(MM_DEBUG_SENSOR);
   GLOBAL_DebugClr(MM_DEBUG_GUARD);
   GLOBAL_DebugClr(MM_DEBUG_COMMS);
   GLOBAL_DebugClr(MM_DEBUG_PRINT);
   //
   // tPumpState = rpi_CheckPump(0, KINS_PUMP_TEST_OFF);
   // if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 5)) LOG_printf("rpi-Execute():TEST TEST TEST" CRLF);
   //
   =====================================================================================================*/

   if(RPI_GetFlag(VERBOSE_MODE_SENSOR))
   {
      //
      // Powerup is in SENSOR mode. This means the uCtl should be in SENSOR 
      // mode sending Sensor data records to the COMM thread on a regular basis.
      // The G_stCmd.iSecsTout holds the next system secs where a new record
      // must be transferred. If not, the correct SENSOR_MODE will be checked.
      // Disable the initial transfer sequence now.
      //
      PRINTF("rpi-main():Startup mode=SENSOR" CRLF);
      rpi_HandleSensorTransfer(SMCMD_READY);
   }
   else
   {
      //
      // Powerup is in IDLE mode. Request the COMM thread to start the transfer
      // RAM and EEPROM data from the uCtl
      //
      PRINTF("rpi-main():Startup mode=Transfer RAM/EEPROM" CRLF);
      iTransferIndex = 0;
      rpi_HandleSensorTransfer(SMCMD_NONE);
   }
   //
   while(fHostRunning)
   {
      //
      // The Smart Sensor can be in different modes:
      //
      //    o  Idle mode
      //    o  RPi Sensor mode, receiving records with sensor info.
      //       Sensor data should be coming in about every 10 secs
      //    o  MIAC mode, waiting for a command.
      //
      pcSensorData = NULL;
      iOpt         = GLOBAL_SemaphoreWait(PID_HOST, iLoopTimeout);
      //
      // Sem unlocked (Timeout or Event)
      //    o SIGINT:   Terminate normally
      //    o SIGTERM:  Terminate normally
      //    o SIGSEGV:  Segmentation fault
      //    o SIGUSR1:  COMMS buffer completed
      //    o SIGUSR2:  Power Off
      //
      ulSecsNow = RTC_GetSystemSecs();
      //
      switch(iOpt)
      {
         case 0:
            GLOBAL_PidSetGuard(PID_HOST);
            //
            // Sem unlocked
            // Notification handlers
            //
            if(!GLOBAL_ExecuteNotifications(stHostNotifications, iNumHostNotifications, NULL))
            {
               if(GLOBAL_DebugGet(MM_DEBUG_IDLE)) LOG_printf("rpi-Execute():WARNING:Sem unlocked without signal!" CRLF);
            }
            //
            // Check incoming Sensor signals
            // Reset the Sensor timeout to MAX_SECS_NO_SENS secs, if the COMM thread did not 
            // disable the sensor data timeout check.
            //
            if(pstMap->G_stCmd.iSecsTout > 0)
            {
               // Timeout enabled: move new timeout value
               // --------+-------------------------+------------------
               //      ulSecsNow                 iSecsTout
               //
               pstMap->G_stCmd.iSecsTout = ulSecsNow + MAX_SECS_NO_SENS;
               iSensorResets             = MAX_RETRIES_NO_SENS;
            }
            //
            if(pcSensorData)
            {
               //
               // Smart Sensor data received
               //
               if(GLOBAL_DebugPrint(MM_DEBUG_SENSOR, 5)) LOG_printf("rpi-main():Received Smart Sensor data..." CRLF);
               //
               // Keep activity LED ON as long as we have incoming Smart Sensor data
               //
               if(iRecordReceived == 0)
               {
                  // Display warning once
                  LOG_Report(0, "HST", "rpi-main():Smart Sensor data received again!");
                  if(GLOBAL_DebugPrint(MM_DEBUG_SENSOR, 5)) LOG_printf("rpi-main():Smart Sensor data received again!" CRLF);
                  iRecordReceived = 1;
               }
               //
               if( rpi_SensorCheckCrc(pcSensorData))
               {
                  GEN_RemoveChar(pcSensorData, CR);
                  GEN_RemoveChar(pcSensorData, LF);
                  GEN_TrimRight(pcSensorData);
                  if( (fCc = rpi_SensorConvertData(pcSensorData, pstSensNow)) )
                  {
                     //
                     // Make sure to pass only valid data to the world. 
                     // Samples beyond a certain threshold must be ignored !
                     // 21 May 2024: Ignore check result for now, only report
                     //
                     if( (fCc = rpi_ValidateSample(ulSecsNow, pstSensNow)) )
                     {
                        //===============================================================================
                        // SENSOR data has been acquired succesfully:
                        // pstSensNow has the latest values. 
                        // If we have NEW samples: add to sum
                        //===============================================================================
                        rpi_SensUpdateGlobals(KINS_NOW, pstSensNow, &pstMap->G_stSensorLife);
                        rpi_SensSampleMin(ulSecsNow, pstSensMin, pstSensNow);
                        rpi_SensSampleQtr(ulSecsNow, pstSensQtr, pstSensNow);
                        rpi_SensSampleDay(ulSecsNow, pstSensDay, pstSensNow);
                        rpi_SensSampleMon(ulSecsNow, pstSensMon, pstSensNow);
                        rpi_SensSampleTot(ulSecsNow, pstSensTot, pstSensNow);
                        rpi_SensSampleCsv(ulSecsNow, pstSensCsv, pstSensNow);
                        //
                        tFlowState = rpi_CheckKinshiFlow(ulSecsNow, tFlowState, pstSensNow);
                        //
                        RPI_DISPLAY("NOW", pstSensNow);
                        RPI_DISPLAY("MIN", pstSensMin);
                        RPI_DISPLAY("QTR", pstSensQtr);
                        RPI_DISPLAY("DAY", pstSensDay);
                        RPI_DISPLAY("MON", pstSensMon);
                        RPI_DISPLAY("TOT", pstSensTot);
                        RPI_DISPLAY("CSV", pstSensCsv);
                     }
                     else
                     {
                        LOG_Report(0, "HST", "rpi-main():Invalid Smart Sensor data:");
                        LOG_ListData("Main Loop():", pcSensorData, KINS_SIZE_LENZ);
                     }
                  }
                  if(!rpi_CheckValidSample(fCc, pcSample, pcSensorData))
                  {
                     LOG_Report(0, "HST", "rpi-main():Smart Sensor data stays BAD: Request uController Reset...");
                     LOG_ListData("Main Loop():", pcSensorData, KINS_SIZE_LENZ);
                     if(GLOBAL_DebugPrint(MM_DEBUG_SENSOR, 1)) LOG_printf("rpi-main():Smart Sensor data stays BAD: Request uController Reset..." CRLF);
                     pstMap->G_stCmd.iCommand = SMCMD_RESET;
                     GLOBAL_Signal(PID_COMM, SIGUSR2);
                     iRecordReceived = 0;
                  }
               }
               else
               {
                  LOG_Report(0, "HST", "rpi-main():Smart Sensor data CRC error!");
                  if(GLOBAL_DebugPrint(MM_DEBUG_SENSOR, 1)) LOG_printf("rpi-main():Smart Sensor data CRC Error" CRLF);
                  LOG_ListData("Main Loop():", pcSensorData, KINS_SIZE_LENZ);
                  pstSensNow->iCrcErrors++;
               }
            }
            break;

         case 1:
            GLOBAL_PidSetGuard(PID_HOST);
            //
            // Timeout: No Smart Sensor data received
            //
            if(iRecordReceived)
            {
               // Display warning once
               LOG_Report(0, "HST", "rpi-main():No Sensor data received!");
               if(GLOBAL_DebugPrint(MM_DEBUG_SENSOR, 1)) LOG_printf("rpi-main():No Sensor data received!" CRLF);
               iRecordReceived = 0;
            }
            break;

         default:
         case -1:
            LOG_Report(errno, "HST", "rpi-main():ERROR GEN_WaitSemaphore");
            PRINTF("rpi-main():ERROR GEN_WaitSemaphore(errno=%d)" CRLF, errno);
            break;
      }
      //
      // Send regular heartbeats to Watchdog
      // Reset Watchdog PID every day
      // Handle the waterpump OFF-At-Night mode
      //
      if(rpi_CheckMidnight(ulSecsNow)) 
      {
         LOG_Report(0, "HST", "rpi-Execute():Midnight");
         GLOBAL_Notify(PID_GUARD, GLOBAL_HST_GRD_MID, SIGUSR1);
         //
         // At the moment, the waterpump Night-Mode starts fixed at midnight and stays OFF
         // for G_iPumpMinutes mins.
         //
         tPumpState = rpi_CheckPump(ulSecsNow, KINS_PUMP_TURN_OFF);
         //
         if(GLOBAL_FlushLog() != EXIT_CC_OKEE) LOG_Report(errno, "HST", "rpi-CheckMidnight(): ERROR saving app files: ");
         rpi_BackupLogfile();
         
         #ifdef FEATURE_TRUNCATE_LOG_MIDNIGHT
         //
         // Truncate LOG file
         //
         if(GEN_FileTruncate((char *)pcLogfile, pstMap->G_iLogLines) != EXIT_CC_OKEE)
         {
            LOG_Report(errno, "HST", "rpi-Execute():Error truncating LOG file [%s] to %d lines " CRLF, pcLogfile, pstMap->G_iLogLines);
         }
         #endif   //TRUNCATE_LOG_MIDNIGHT

         tDogPid = 0;
      }
      else 
      {
         //
         // Check wether the waterpump should be turned ON again
         //
         tPumpState = rpi_CheckPump(ulSecsNow, tPumpState);
      }
      //
      // Check a few other non-critical situations on regular intervals:
      //    o WAN IP changes
      //    o Email pregress
      //    o Smart Sensor activity
      //    o LOG file too big
      //    o Battery level (If PiJuice HAT)
      //    o Restart request
      //    o Power OFF request
      //    o Guard activity
      //
      // Check if we should abort for reboot/poweroff
      //
      rpi_CheckWan(ulSecsNow);
      rpi_CheckMail(ulSecsNow);
      //
      if(fHostRunning) fHostRunning = rpi_CheckLogFull(ulSecsNow);
      if(fHostRunning) fHostRunning = rpi_CheckPowerLevel(ulSecsNow);
      if(fHostRunning) fHostRunning = rpi_CheckRestart(ulSecsNow);
      if(fHostRunning) fHostRunning = rpi_CheckPowerOff(ulSecsNow, pstSensNow);
      //
      //  Do regular checks only if startup sequence of Sensor memory download has been completed
      //
      if(!rpi_HandleSensorTransfer(SMCMD_NONE))
      {
         if(fHostRunning) fHostRunning = rpi_CheckSmartSensor(ulSecsNow);
      }
      LED(LED_G, iRecordReceived);
   }  
   //
   // End HostRunning
   //
   PRINTF("rpi-main():Exit Smart Sensor data collection" CRLF);
   //
   // Always save what we have so far of the current day to file
   //
   RPI_SaveDay(pstSensDay->ulSecsSample, (u_int8 *)pstMap->G_stThisDay, sizeof(pstMap->G_stThisDay));
   //
   // Always backup the existing SmartSensor_xxx.csv files, they will NOT survive reboot
   //
   if(tCsvFile) 
   {
      safeclose(tCsvFile);
      tCsvFile = 0;
   }
   rpi_BackupCsvFile();
   //
   // Terminate Smart Sensor thread
   //
   PRINTF("rpi-main():Terminate all threads..." CRLF);
   GLOBAL_PidsTerminate(SMARTS_TIMEOUT);
   PRINTF("rpi-main():...done" CRLF);
   //
   // In CMD-Line mode, we do not need the Watchdog thread RpiDog to check on us, unless overruled with CL option -X
   //
   if(fRpiDaemonMode || fRpiDogControl) 
   {
      //
      // Background mode
      // Check how to proceed from here:
      //    o Restart the App
      //    o Reboot the system
      //    o Just terminate the App   
      //
      if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_ALL_HST_RST))
      {
         //
         // We need to restart the App: SIGUSR1 RpiDog
         //
         tDogPid = GEN_GetPidByName(RPI_WATCHDOG);
         if(tDogPid > 0) 
         {
            PRINTF("rpi-main():RESTART theApp through RpiDog on Pid=%d" CRLF, tDogPid);
            LOG_Report(0, "HST", "rpi-main():RESTART theApp through RpiDog on Pid=%d", tDogPid);
            kill(tDogPid, SIGUSR1);
         }
         else         
         {
            PRINTF("rpi-main():RESTART theApp by RpiDog NOT FOUND !:: REBOOT here instead." CRLF);
            LOG_Report(0, "HST", "rpi-main():RESTART theApp by RpiDog NOT FOUND !:: REBOOT here instead.");
            rpi_Reboot(TRUE);
         }
      }
      else if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_ALL_HST_RBT))
      {
         //
         // We need to reboot the system: SIGUSR2 RpiDog
         //
         tDogPid = GEN_GetPidByName(RPI_WATCHDOG);
         if(tDogPid > 0) 
         {
            PRINTF("rpi-main():REBOOT theApp through RpiDog on Pid=%d" CRLF, tDogPid);
            LOG_Report(0, "HST", "rpi-main():REBOOT theApp through RpiDog on Pid=%d", tDogPid);
            kill(tDogPid, SIGUSR2);
         }
         else
         {
            PRINTF("rpi-main():REBOOT theApp by RpiDog NOT FOUND !:: REBOOT here instead." CRLF);
            LOG_Report(0, "HST", "rpi-main():REBOOT theApp by RpiDog NOT FOUND !:: REBOOT here instead.");
            rpi_Reboot(TRUE);
         }
      }
   }
   //
   // If the MMAP was initialized this run, now it's time to increment the run counter
   //
   pstMap->G_iRunCounter++;
   //
   safefree(pcSample);
   safefree(pstSensCsv);
   safefree(pstSensTot);
}

/*----------------------------------------------------------------------
________________LOCAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/

//
// Function:   rpi_BackupCsvFile
// Purpose:    Copy the cvs file from RAM disk to SD
//             
// Parms:     
// Returns:    cc
// Note:       
//
static int rpi_BackupCsvFile()
{
   int      iCc=1; 
   char     cSrc, cDst;

   GLOBAL_GetFileInfo((char *)pcDirBackup, &cSrc);
   GLOBAL_GetFileInfo((char *)pcDirWork, &cDst);
   //
   if( (cSrc == 'D') && (cDst == 'D') )
   {
      iCc = system(pcShellBackupCsv);
      switch(iCc)
      {
         case EXIT_CC_OKEE:
            PRINTF("rpi-BackupCsvFile():%s OK" CRLF, pcShellBackupCsv);
            LOG_Report(0, "HST", "rpi-BackupCsvFile():%s OK", pcShellBackupCsv);
            break;

         default:
         case EXIT_CC_GEN_ERROR:
            PRINTF("rpi-BackupCsvFile():%s ** ERROR: %s" CRLF, pcShellBackupCsv, strerror(errno));
            LOG_Report(errno, "HST", "rpi-BackupCsvFile():%s ** ERROR:", pcShellBackupCsv);
            break;
      }
   }
   else LOG_Report(0, "HST", "rpi-BackupCsvFile():ERROR: No such dirs !! Shell=%s", pcShellBackupCsv);
   return(iCc);
}

//
// Function:   rpi_BackupLogfile
// Purpose:    Copy the log file from RAM disk to SD with timestamped filename
//             
// Parms:     
// Returns:    cc
// Note:       
//
static int rpi_BackupLogfile()
{
   int      iCc=EXIT_CC_GEN_ERROR; 
   u_int32  ulSecs;
   char     cSrc, cDst;
   char    *pcFileName;
   char    *pcDateTime;
   char    *pcShellBup;

   GLOBAL_GetFileInfo((char *)pcDirBackup, &cSrc);
   GLOBAL_GetFileInfo((char *)pcDirWork, &cDst);
   //
   if( (cSrc == 'D') && (cDst == 'D') )
   {
      pcDateTime = (char *) safemalloc(TIME_FORMAT_MAX_SIZE);
      pcFileName = (char *) safemalloc(256);
      pcShellBup = (char *) safemalloc(256);
      //
      ulSecs = RTC_GetSystemSecs();
      RTC_ConvertDateTime(TIME_FORMAT_YYYY_MM_DD_HH_MM_SS, pcDateTime, ulSecs);
      //
      GEN_SNPRINTF(pcFileName, 255, "%s_" RPI_LOG_FILE, pcDateTime);
      GEN_SNPRINTF(pcShellBup, 255, "cp " RPI_PUBLIC_LOG_PATH " " RPI_BACKUP_DIR "%s", pcFileName);
      //
      iCc = system(pcShellBup);
      switch(iCc)
      {
         case EXIT_CC_OKEE:
            PRINTF("rpi-BackupLogfile():%s OK" CRLF, pcShellBup);
            LOG_Report(0, "HST", "rpi-BackupLogfile():%s OK", pcShellBup);
            break;

         default:
         case EXIT_CC_GEN_ERROR:
            PRINTF("rpi-BackupLogfile():%s ** ERROR:%s" CRLF, pcShellBup, strerror(errno));
            LOG_Report(errno, "HST", "rpi-BackupLogfile():%s ** ERROR: ", pcShellBup);
            break;
      }
      safefree(pcShellBup);
      safefree(pcFileName);
      safefree(pcDateTime);
   }
   return(iCc);
}

// 
// Function:   rpi_CheckKinshiFlow
// Purpose:    Check the Kishi waterflow 
// 
// Parameters: Current system secs, state, Kinshi struct
// Returns:    state
// Note:       When the Kinshi pump turns OFF:
//                o Start the flow timer (G_iSecsFlow)
//                o Sample all waterlevels till time is up
//                o Calculate flow while the pump is still OFF (G_flFlowRate)
//
static KPUMP rpi_CheckKinshiFlow(u_int32 ulSecsNow, KPUMP tState, SENS *pstSensor)
{
   static double  flStartLevel=0.0;
   static u_int32 ulSecsFlow=0;
   //
   u_int32  ulSecs;


   /*-----------------------------------------------------------------------------------------
    *         Waterlevel (mm)
    *            |      
    *            |                 Kinshi Hi-Water (Pump ON)
    *            |     ...                  /\
    *            |      \                  / |\
    *            |       \                /  | \
    *            |        \              /   |  \
    *            |         \            /    |   \
    *            |          \          /     |    \
    *            |           \        /      |     \
    *            |            \      /       |      \     ...
    *            |             \    /        |       \    /
    *            |              \  /         |        \  /
    *            |               \/          |         \/
    *            |      Kinsho Lo-Water (Pump OFF)
    *            |               |           |
    *            +---------------+-----------+---------+----- Secs
    *                            |<--------->|
    *                           Flow speed mm/min
    *
    *----------------------------------------------------------------------------------------*/

   switch(tState)
   {
      default:
         if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 2)) LOG_printf("rpi-CheckKinshiFlow():UNKNOWN state %d" CRLF, tState);
         // FALLTHROUGH
      case KINS_FLOW_INIT:
         if(pstSensor->iPumpOn) 
         {
            if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 2)) LOG_printf("rpi-CheckKinshiFlow():INIT --> WAIT-OFF" CRLF);
            tState = KINS_FLOW_WAIT_OFF;
         }
         else
         {
            if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 2)) LOG_printf("rpi-CheckKinshiFlow():INIT --> WAIT-ON" CRLF);
            tState = KINS_FLOW_WAIT_ON;
         }
         break;

      case KINS_FLOW_WAIT_ON:
         if(pstSensor->iPumpOn) 
         {
            if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 2)) LOG_printf("rpi-CheckKinshiFlow():WAIT-ON --> WAIT-OFF" CRLF);
            tState = KINS_FLOW_WAIT_OFF;
         }
         break;

      case KINS_FLOW_WAIT_OFF:
         if(pstSensor->iPumpOff)
         {
            //
            // Low water marker reached: Pump has been turned OFF. The speed at which the Kinshi
            // filter fills up again should be a measure of how polluted the filter actually is.
            // Start the measurement here and measure the flow rate (mm/min).
            //
            if(pstMap->G_iSecsFlow)
            {
               // Flow measurement is enabled
               if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 2)) LOG_printf("rpi-CheckKinshiFlow():WAIT-OFF:Start flow measurement --> SAMPLE" CRLF);
               ulSecsFlow   = ulSecsNow + pstMap->G_iSecsFlow;
               flStartLevel = pstSensor->flWaterLevel;
               tState = KINS_FLOW_SAMPLE;
            }
            else
            {
               // Flow measurement has not been enabled
               if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 2)) LOG_printf("rpi-CheckKinshiFlow():WAIT-OFF:No flow measurement --> WAIT-ON" CRLF);
               tState = KINS_FLOW_WAIT_ON;
            }
         }
         break;

      case KINS_FLOW_SAMPLE:
         if(pstSensor->iPumpOn) 
         {
            //
            // OOOOps, High water level has been reached.
            // The pump is already ON before the flow measurement has completed.
            // Skip flow measurement.
            //
            ulSecs = pstMap->G_iSecsFlow - (ulSecsFlow - ulSecsNow);
            LOG_Report(0, "HST", "rpi-CheckKinshiFlow():Premature pump ON after %d Secs", ulSecs);
            if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 2)) LOG_printf("rpi-CheckKinshiFlow():SAMPLE:Premature pump ON after %d Secs --> WAIT-OFF" CRLF, ulSecs);
            tState = KINS_FLOW_WAIT_OFF;
         }
         else
         {
            // 
            // Pump is still OFF, High water level has not yet been reached.
            // Calculate flow rate if sample time is over.
            //
            if(ulSecsNow > ulSecsFlow)
            {
               double flFlowRate;
               //
               // Time is up: calculate flow while the pump is still OFF
               //
               flFlowRate  = pstSensor->flWaterLevel - flStartLevel;
               flFlowRate /= pstMap->G_iSecsFlow;
               flFlowRate *= 60;
               pstMap->G_flFlowRate = flFlowRate;
               if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 2)) LOG_printf("rpi-CheckKinshiFlow():SAMPLE:Kinshi WaterFlow = %.2f mm/min --> WAIT-ON" CRLF, flFlowRate);
               LOG_Report(0, "HST", "rpi-CheckKinshiFlow():Kinshi WaterFlow = %.2f mm/min", flFlowRate);
               tState = KINS_FLOW_WAIT_ON;
            }
         }
         break;
   }
   return(tState);
}

//
//  Function:  rpi_CheckLogFull
//  Purpose:   Check if the log drive still has space left
//
//  Parms:     Secs now
// Returns:    FALSE to abort main loop and exit
//  Note:      Since disk usage is only updated if all FDs have been closed, only
//             perform this once and then wait for theAPP restarted (by RpiDog or reboot)
//
static bool rpi_CheckLogFull(u_int32 ulSecsNow)
{
   static u_int32 ulSecsLogCheck=0;
   bool           fCc=TRUE;
   double         flFree, flMinFree=MIN_LOG_DISK_FREE;

   if(ulSecsLogCheck == 0) ulSecsLogCheck = ulSecsNow + 60;
   else if(ulSecsNow > ulSecsLogCheck)
   {
      //
      // Get free space in %
      //
      flFree = FINFO_GetFreeSpacePercentage(RPI_WORK_DIR);
      //
      if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 5)) LOG_printf("rpi-CheckLogFull():Log-disk free=%3.3f, Min=%3.3f" CRLF, flFree, flMinFree);
      //
      if (flFree < flMinFree)  
      {
         if(ulSecsRestart == 0)
         {
            // First time to reduce LOG file usage
            LOG_Report(0, "HST", "rpi-CheckLogFull():Log-disk FULL: Free=%.2f%% (below minimum of %.2f%%) !!!", flFree, flMinFree);
            if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 1)) LOG_printf("rpi-CheckLogFull():Log-disk FULL: Free=%.2f%% (below minimum of %.2f%%) !!!" CRLF, flFree, flMinFree);
            fCc = rpi_CreateNewLog();
         }
         else
         {
            LOG_Report(0, "HST", "rpi-CheckLogFull():Log-disk STILL FULL(no restart): Free=%.2f%% (below minimum of %.2f%%) !!!", flFree, flMinFree);
            if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 1)) LOG_printf("rpi-CheckLogFull():Log-disk STILL FULL(no restart): Free=%.2f%% (below minimum of %.2f%%) !!!" CRLF, flFree, flMinFree);
         }
      }
      ulSecsLogCheck = ulSecsNow + (15 * 60);
   }
   return(fCc);
}

//
// Function:   rpi_CheckMail
// Purpose:    Check if mail has been send out nicely
//
// Parms:      Secs now
// Returns:    TRUE if OK
// Note:       
//             
static bool rpi_CheckMail(u_int32 ulSecsNow)
{
   //
   // If we have mail to be send, ulEmailPending has been set
   //
   if( ulEmailPending && (ulEmailPending < ulSecsNow) )
   {
      //
      // Mail did NOT yet go out
      //
      if(RPI_SendMail())
      {
         LOG_Report(0, "HST", "rpi-CheckMail():Email send OKee");
         ulSecsMailTimeout = ulSecsNow + MAILX_TIMEOUT;
         ulEmailPending    = 0;
         iMaxEmailRetries--;
      }
      else
      {
         if(iMaxEmailRetries > 0)
         {
            //
            // Error sending mail: retry in MAILX_RETRY secs
            //
            LOG_Report(0, "HST", "rpi-CheckMail():Email send ERROR, retry");
            ulSecsMailTimeout = 0;
            ulEmailPending   += MAILX_RETRY;
         }
         else 
         {
            LOG_Report(0, "HST", "rpi-CheckMail():Email send ERROR, retried too often. Abort.");
            ulSecsMailTimeout = 0;
            ulEmailPending    = 0;
         }
      }
   }
   //
   // if mail has been sent, <ulSecsMailTimeout> has the absolute system secs
   // to check if the email client has completed (default 120 secs).
   // If after this time the email client is still running, terminate it. 
   //
   if(ulSecsMailTimeout)
   {
      ulSecsMailTimeout = RPI_WaitMailxCompletion(ulSecsNow, ulSecsMailTimeout);
   }
   return(TRUE);
}

//
// Function:   rpi_CheckMidnight
// Purpose:    Check if midnight
//
// Parms:      Secs now
// Returns:    TRUE if midnight
// Note:       
//             
static bool rpi_CheckMidnight(u_int32 ulSecsNow)
{
   static u_int32 ulSecsMidnight=0;
   //
   bool  fCc=FALSE;

   if(ulSecsMidnight == 0) 
   {
      ulSecsMidnight = RTC_GetMidnight(ulSecsNow) + (24*3600);
   }
   else if(ulSecsNow > ulSecsMidnight)
   {
      //
      // Midnight
      //
      ulSecsMidnight  += (24*3600);
      fCc              = TRUE;
   }
   return(fCc);
}

//
// Function:   rpi_CheckSmartSensor
// Purpose:    Check if the smart sensor is still responding once in a while. If not for some time, reboot.
//             Called every 30 secs by default
// Parms:      Secs now
// Returns:    FALSE to abort main loop and exit
// Note:       iSecsTout = 0  Init timeout
//             iSecsTout > 0  Generic timeout secs for sensor replies. Within this period, SmartSensor data
//                            should be coming in. If not, the sensor is not in SENSOR mode.
//             iSecsTout < 0  Timeout has been disabled by the COMM thread
//
//             iSecsMiac      Timer for regular MIAC RAM/EEPROM update requests
//                            For HTTP uCtl RAM/ROM updates: 
//                            http://192.168.178.211/parms.json?MiacMem=10 
//                            to transfer uController memory to Host every 10 Secs:
//
static bool rpi_CheckSmartSensor(u_int32 ulSecsNow)
{
   bool  fCc=TRUE;

   if(pstMap->G_stCmd.iSecsTout > 0)
   {
      // 
      // Sensor timeout has been set
      // Verify that the uCtl did send Sensor data to the COMM thread.
      //
      if( ulSecsNow > pstMap->G_stCmd.iSecsTout)
      {
         //
         // Sensor timeout: Check if the Smart sensor is in the correct mode !
         //
         if(pstMap->G_stCmd.iStatus != GEN_STATUS_SENSOR)
         {
            //
            // No, it's not: try to correct it. If it keeps failing, reboot
            //
            LOG_Report(0, "HST", "rpi-CheckSmartSensor():Force (%d) SmartSensor to SENSOR mode", iSensorResets);
            pstMap->G_stCmd.iSecsTout += MAX_SECS_NO_SENS;
            pstMap->G_stCmd.iCommand   = SMCMD_DLD_SENSOR;
            GLOBAL_Signal(PID_COMM, SIGUSR2);
         }
         if(--iSensorResets == 0)
         {
            PRINTF("rpi-CheckSmartSensor():Haven't seen Smart Sensor data in %ld secs: reboot!" CRLF, MAX_SECS_NO_SENS * MAX_RETRIES_NO_SENS);
            LOG_Report(0, "HST", "rpi-CheckSmartSensor():Haven't seen Smart Sensor data in %ld secs: reboot!", MAX_SECS_NO_SENS * MAX_RETRIES_NO_SENS);
            GLOBAL_Notify(PID_HOST, GLOBAL_ALL_HST_RBT, SIGUSR1);
            return(FALSE);
         }
      }
      if(RPI_GetFlag(VERBOSE_MODE_MEMORY))
      {
         //
         // Manual TRIGGER to read Sensor RAM/EEPROM ONCE
         //
         pstMap->G_iVerbose &= ~VERBOSE_MODE_MEMORY;
         if(GLOBAL_DebugPrint(MM_DEBUG_SENSOR, 1)) LOG_printf("rpi-CheckSmartSensor():TRIGGER Smart Sensor memory read once" CRLF);
         LOG_Report(0, "HST", "rpi-CheckSmartSensor():TRIGGER Smart Sensor memory read once");
         pstMap->G_stCmd.iSecsMiac = 0;
         iTransferIndex            = 0;
      }
      else if(RPI_GetFlag(VERBOSE_MODE_SENSOR))
      {
         //
         // Ignore actual uCtl RAM/EEPROM contents if Sensor is SENSOR mode
         //
         pstMap->G_stCmd.iSecsMiac = 0;
      }
      else
      {
         //
         // if it is time : 
         // Download actual uCtl memory contents
         //
         if( pstMap->G_stCmd.iSecsMiac && (ulSecsNow > pstMap->G_stCmd.iSecsMiac))
         {
            if(GLOBAL_DebugPrint(MM_DEBUG_PRINT, 1)) LOG_printf("rpi-CheckSmartSensor():Read Smart Sensor memory" CRLF);
            LOG_Report(0, "HST", "rpi-CheckSmartSensor():Read Smart Sensor memory");
            pstMap->G_stCmd.iSecsMiac = 0;
            iTransferIndex            = 0;

         }
      }
   }
   else if(pstMap->G_stCmd.iSecsTout < 0)
   {
      //
      // Sensor timeout has been disabled by the COMM thread, probably because
      // the Smart Sensor is in a non-SENSOR data mode
      //
   }
   else
   {
      //
      // Initial setup: 
      //
      if(GLOBAL_DebugPrint(MM_DEBUG_SENSOR, 1)) LOG_printf("rpi-CheckSmartSensor():Initial Smart Sensor timing." CRLF);
      pstMap->G_stCmd.iSecsTout = ulSecsNow + MAX_SECS_NO_SENS;
      iSensorResets             = MAX_RETRIES_NO_SENS;
      //
      // Check if the RAM/ROM memory transfer from uCtl to Host has been setup through the 
      // HTTP server.
      //
      rpi_SensorSetTimers(ulSecsNow);
   }
   return(fCc);
}

//
// Function:   rpi_CheckPowerLevel
// Purpose:    Check power charge level, turn OFF power on low level
//
// Parms:      Secs now
// Returns:    FALSE to abort main loop and exit
// Note:       MIN_POWER_LEVEL will switch off power
//             
static bool rpi_CheckPowerLevel(u_int32 ulSecsNow)
{
#ifdef FEATURE_PI_JUICE
   static u_int32 ulSecsLevelCheck=0;
   //
   bool  fCc=TRUE;
   int   iChargeLevel;

   if(ulSecsLevelCheck == 0) ulSecsLevelCheck = ulSecsNow + 60;
   else if(ulSecsNow > ulSecsLevelCheck)
   {
      //
      // Check if we still have sufficient power
      //
      if(PJ_GetStatus(PJ_CHARGE, &iChargeLevel, 1))
      {
         PRINTF("rpi-CheckPowerLevel():PiJuice Battery Charge Level = %d%%" CRLF, iChargeLevel);
         LOG_Report(0, "HST", "PiJuice Battery Charge Level = %d%%", iChargeLevel);
         if(iChargeLevel < MIN_POWER_LEVEL)
         {
            PRINTF("rpi-CheckPowerLevel():PiJuice Battery Charge Level too low: power down !!" CRLF);
            LOG_Report(0, "HST", "PiJuice Battery Charge Level too low: power down !!");
            //
            fRpiPowerOff = TRUE;
            fCc          = FALSE;
         }
      }
      else
      {
         PRINTF("rpi-CheckPowerLevel():PiJuice Battery Charge Level ERROR" CRLF);
         LOG_Report(0, "HST", "PiJuice Battery Charge Level ERROR");
      }
      ulSecsLevelCheck += 60;
   }
   return(fCc);

#else    //FEATURE_PI_JUICE
   return(TRUE);
#endif   //FEATURE_PI_JUICE
}

//
//  Function:  rpi_CheckPowerOff
//  Purpose:   Check if we requested poweroff through the Smart Sensor Controller Mode 0x55
//
//  Parms:     Secs now
// Returns:    FALSE to abort main loop and exit
//  Note:      Smart Sensor Modes:
//             KINS_MODE_AUTO       = 0x00,  // Yellow OFF     :Auto mode (Ultrasoon sensor)
//             KINS_MODE_PUMP_ON    = 0x01,  //        1 blink :Manual ON
//             KINS_MODE_PUMP_OFF   = 0x05,  //        2 blinks:Manual OFF
//             KINS_MODE_TIMER      = 0x15,  //        3 blinks:Timer  ON/OFF
//             KINS_MODE_POWEROFF   = 0x55   //        4 blinks:Poweroff request
//
//             Global Mode55 determins the actual action:
//             G_pcRestart:
//             "(O)ff"              : Disabled (do nothing)
//             "(P)oweroff"         : Power Off in 1 minute
//             "(R)eboot"           : Reboot NOW
//             "(D)elayed Reboot"   : Reboot in 2 minutes
//
static bool rpi_CheckPowerOff(u_int32 ulSecsNow, SENS *pstSensor)
{
   bool     fCc=TRUE;

   switch(pstSensor->iPumpMode)
   {
      case KINS_MODE_POWEROFF:
         //
         // The Smart Sensor Controller has been switched (manually) into POWEROFF mode
         // If it stays there for NUM_SECS_POWEROFF secs, request power OFF to rpidog.
         //
         if(ulSecsPoweroff == 0) ulSecsPoweroff = ulSecsNow + NUM_SECS_POWEROFF;
         break;
         
      default:
         //
         // The Smart Sensor Controller POWEROFF mode is OFF. Cancel request.
         //
         ulSecsPoweroff = 0;
         break; 
   }
   //
   // Check if Smart Sensor Controller Mode 0x55 has been activated. If so, check if it is more than
   // NUM_SECS_POWEROFF.
   //
   if(ulSecsPoweroff)
   {
      //
      // Smart Sensor Controller Mode 0x55 has been active for NUM_SECS_POWEROFF seconds: handle 0x55 mode request 
      // 
      if( ulSecsNow > ulSecsPoweroff)
      {
         if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 5)) LOG_printf("rpi-CheckPoweroff():" CRLF);
         switch(*pstMap->G_pcRestart)
         {
            default:
            case 'O':
            case 'o':
               // OFF: Ignore 0x55 mode
               ulSecsPoweroff = 0;
               break;

            case 'P':
            case 'p':
               // Power off
               LOG_Report(0, "HST", "rpi-CheckPoweroff():Kinshi Mode = Power OFF!");
               rpi_PowerOff();
               fCc = FALSE;
               break;

            case 'D':
            case 'd':
               // Delayed reboot
               LOG_Report(0, "HST", "rpi-CheckPoweroff():Kinshi Mode = Delayed reboot!");
               rpi_Reboot(TRUE);
               fCc = FALSE;
               break;

            case 'R':
            case 'r':
               // Reboot now
               LOG_Report(0, "HST", "rpi-CheckPoweroff():Kinshi Mode = Reboot NOW");
               rpi_Reboot(FALSE);
               fCc = FALSE;
               break;
         }
      }
   }
   return(fCc);
}

// 
// Function:   rpi_CheckPump
// Purpose:    Handle the Pump-at-night mode
// 
// Parameters: Current system secs, state
// Returns:    state
// Note:       If enabled (G_iPumpMinutes>0):
//                o Turn OFF the pump for this number of minutes.
//                o Measure the Waterlevel after the OFF period
//                o Turn ON the pump again untill the next OFF moment
//
static KPUMP rpi_CheckPump(u_int32 ulSecs, KPUMP tState)
{
   static u_int32 ulPumpSecsOff=0;
   static int     iLevelSamples=0;

   switch(tState)
   {
      default:
         if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 5)) LOG_printf("rpi-CheckPump():Unknown state %d" CRLF, tState);
         // FALLTHROUGH
      case KINS_PUMP_INIT:
         if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 5)) LOG_printf("rpi-CheckPump():Init" CRLF);
         ulPumpSecsOff = 0;
         tState        = KINS_PUMP_WAIT_OFF;
         break;

      case KINS_PUMP_WAIT_OFF:
         //
         // Wait here until the moment has come to turn the pump
         // into the night-mode
         //
         if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 5)) LOG_printf("rpi-CheckPump():Wait for nightmode" CRLF);
         break;

      case KINS_PUMP_TURN_OFF:
         if(pstMap->G_iPumpMinutes)
         {
            ulPumpSecsOff = 60 * pstMap->G_iPumpMinutes;
            if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 5)) LOG_printf("rpi-CheckPump():Nightmode: Turn Pump OFF for %d:%02d Hours" CRLF, ulPumpSecsOff/3600, (ulPumpSecsOff%3600)/60);
            LOG_Report(0, "HST", "rpi-CheckPump():Nightmode: Turn Pump OFF for %d:%02d Hours", ulPumpSecsOff/3600, (ulPumpSecsOff%3600)/60);
            ulPumpSecsOff += ulSecs;
            //
            // Time to silence the Kinshi waterpump for Nighttime operations
            //
            tState                   = KINS_PUMP_WAIT_ON;
            pstMap->G_stCmd.iCommand = SMCMD_HTTP_PUMP;
            pstMap->G_iPumpMode      = KINS_MODE_PUMP_OFF;
            GLOBAL_Signal(PID_COMM, SIGUSR2);
         }
         else 
         {
            ulPumpSecsOff = 0;
            tState        = KINS_PUMP_WAIT_OFF;
            if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 5)) LOG_printf("rpi-CheckPump():Nightmode is DISABLED." CRLF);
            LOG_Report(0, "HST", "rpi-CheckPump():Nightmode is DISABLED.");
         }
         break;

      case KINS_PUMP_WAIT_ON:
         //============================================================================================================
         // Night time 
         // Wait until it is time to power up the waterpump again. Before that, sample some waterlevels, since
         // at this stage we are able to measure the actual waterlevel in the pond itself.
         //============================================================================================================
         if(ulPumpSecsOff && (ulSecs > ulPumpSecsOff))
         {
            //
            // Time to turn the pump back on.
            // The waterlevel in the Kinshi filter is now equal to the actual pond waterlevel, 
            // so this is the best moment to determine the actual waterlevel in the pond itself. 
            //
            if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 5)) LOG_printf("rpi-CheckPump():Nightmode is over, measure actual WL" CRLF);
            LOG_Report(0, "HST", "rpi-CheckPump():Nightmode is over, measure actual WL.");
            iLevelSamples = POND_LEVEL_SAMPLES;
            tState        = KINS_PUMP_WATERLEVEL;
            pstMap->G_flPondLevel = 0.0;
         }
         else if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 5)) LOG_printf("rpi-CheckPump():Nightmode ongoing" CRLF);
         break;

      case KINS_PUMP_WATERLEVEL:
         if(iLevelSamples)
         {
            //
            // Sample some pond waterlevels
            //
            if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 5)) LOG_printf("rpi-CheckPump():WL=%.1f mm" CRLF, pstMap->G_stSensNow.flWaterLevel);
            LOG_Report(0, "HST", "rpi-CheckPump():WL=%.1f mm", pstMap->G_stSensNow.flWaterLevel);
            pstMap->G_flPondLevel += pstMap->G_stSensNow.flWaterLevel;
            iLevelSamples--;
         }
         else
         {
            //
            // All waterlevels from the pond itself (and NOT the Kinshi filter) have been done: calculate
            // the average and store.
            // Trigger the E-mail containing the Kinshi summary data.
            //
            pstMap->G_flPondLevel /= POND_LEVEL_SAMPLES;
            ulEmailPending   = ulSecs + MAILX_RETRY;
            iMaxEmailRetries = 3;
            tState = KINS_PUMP_TURN_ON;
         }
         break;

      case KINS_PUMP_TURN_ON:
         LOG_Report(0, "HST", "rpi-CheckPump():Turn Pump mode back to AUTO");
         LOG_Report(0, "HST", "rpi-CheckPump():Pond waterlevel=%.1f mm", pstMap->G_flPondLevel);
         LOG_Report(0, "HST", "rpi-CheckPump():Kinshi Hi-water=%.1f mm", pstMap->G_flHiWater);
         //
         if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 5)) 
         {
            LOG_printf("rpi-CheckPump():Turn Pump mode back to AUTO" CRLF);
            LOG_printf("rpi-CheckPump():Pond waterlevel=%.1f mm" CRLF, pstMap->G_flPondLevel);
            LOG_printf("rpi-CheckPump():Kinshi Hi-water=%.1f mm" CRLF, pstMap->G_flHiWater);
         }
         // 
         // Time to turn the Kinshi waterpump for Nighttime operations back to AUTO
         //
         pstMap->G_stCmd.iCommand = SMCMD_HTTP_PUMP;
         pstMap->G_iPumpMode      = KINS_MODE_AUTO;
         GLOBAL_Signal(PID_COMM, SIGUSR2);
         //
         ulPumpSecsOff = 0;
         tState        = KINS_PUMP_WAIT_OFF;
         break;

      case KINS_PUMP_TEST_OFF:
         ulPumpSecsOff =  RTC_GetSystemSecs() + 60;
         if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 5)) LOG_printf("rpi-CheckPump():TEST MODE: 60 secs" CRLF);
         tState = KINS_PUMP_WAIT_ON;
         break;
   }
   return(tState);
}

//
//  Function:  rpi_CheckRestart
//  Purpose:   Check if we requested a restart a while agoo. If not so, reboot.
//
//  Parms:     Secs now
// Returns:    FALSE to abort main loop and exit
//  Note:      
//
static bool rpi_CheckRestart(u_int32 ulSecsNow)
{
   bool     fCc=TRUE;
   u_int32  ulSecs;

   if(ulSecsRestart)
   {
      // Restart has been triggered
      ulSecs = ulSecsNow - ulSecsRestart;
      if( ulSecs > MAX_SECS_RESTART)
      {
         PRINTF("rpi-CheckRestart():Still no restart by rpidog in %ld secs: reboot!" CRLF, ulSecs);
         LOG_Report(0, "HST", "rpi-CheckRestart():Still no restart by rpidog in %ld secs: reboot!", ulSecs);
         rpi_Reboot(TRUE);
         fCc = FALSE;
      }
   }
   return(fCc);
}

//
// Function:   rpi_CheckWan
// Purpose:    Retrieve the WAN address if it is time to verify it
//
// Parms:      Secs now
// Returns:    TRUE if OK
// Note:       This call checks if the WAN address hasn't changed
//             It will update pstMap->G_iWanAddr to:
//             WAN_NOT_FOUND     0: Not yet retrieved
//             WAN_SAME          1: WAN retrieved and no change
//             WAN_CHANGED       2: WAN retrieved and has changed
//             WAN_ERROR        -1: ERROR retrieving
//
//             It will be reset when the latest 24 hour status has been emailed !
//
static bool rpi_CheckWan(u_int32 ulSecsNow)
{
   static u_int32 ulSecsWanCheck=0;
   //
   const char    *pcAny="";
   char          *pcIp;

   if(ulSecsWanCheck == 0) 
   {
      ulSecsWanCheck = ulSecsNow + WAN_CHECK_SECS;
      pstMap->G_iWanAddr = WAN_NOT_FOUND;
      GEN_MEMSET(pstMap->G_pcWanAddr, 0x00, MAX_ADDR_LENZ);
   }
   //
   // If it is time, poll the HazIp server for our WAN IP Address
   //
   if(ulSecsNow > ulSecsWanCheck)
   {
      if(GEN_STRLEN(pstMap->G_pcWanUrl))
      {
         //
         // Check if we still have a network connection
         //
         pcIp = safemalloc(256);
         if( HTTP_GetFromNetwork(pstMap->G_pcWanUrl, (char *)pcAny, pcIp, 255, 20) )
         {
            //PRINTF1("rpi-CheckWan():WAN IP-Address found: %s" CRLF, pcIp);
            if(GEN_STRCMP(pcIp, pstMap->G_pcWanAddr) != 0)
            {
               LOG_Report(0, "HST", "rpi-CheckWan():WAN Changed from [%s] to [%s]", pstMap->G_pcWanAddr, pcIp);
               GEN_STRNCPY(pstMap->G_pcWanAddr, pcIp, MAX_ADDR_LEN);
               pstMap->G_iWanAddr |= WAN_CHANGED;
            }
            else 
            {
               //LOG_Report(0, "HST", "rpi-CheckWan():WAN still %s", pcIp);
               pstMap->G_iWanAddr |= WAN_SAME;
            }
         }
         else
         {
            PRINTF("rpi-CheckWan():WAN IP-address not resolved" CRLF);
            LOG_Report(errno, "HST", "rpi-CheckWan():Not able to connect with WAN service: ");
            pstMap->G_iWanAddr |= WAN_ERROR;
         }
         safefree(pcIp);
      }
      else
      {
         pstMap->G_iWanAddr |= WAN_NO_URL;
      }
      ulSecsWanCheck += WAN_CHECK_SECS;
   }
   return(TRUE);
}

// 
// Function:   rpi_CreateNewLog
// Purpose:    Save logfile to persistent fs and reset log
// 
// Parameters: 
// Returns:    FALSE to abort main loop and exit
// Note:       
// 
static bool rpi_CreateNewLog()
{
   bool  fCc=TRUE;
   int   iState=0;

   LOG_ReportCloseFile();
   //
   if(rpi_BackupLogfile() == 0)
   {
      // OKee
      if(truncate(RPI_PERS_LOG_PATH, 0))        iState |= 1;   // Mark delete error persistent file
      else if( remove(RPI_PUBLIC_LOG_PATH) < 0) iState |= 2;   // Mark delete error work       file
   }
   else
   {
      // Error
      iState |= 4;
   }
   LOG_ReportOpenFile(RPI_PUBLIC_LOG_PATH);
   LOG_Report(0, "HST", "New LOG file: v%s Build:%s (%s)", pcSwVersion, RPI_BUILT, RPI_IO_BOARD);
   //
   switch(iState)
   {
      case 0:
         //
         // Resize and backup went fine: since other threads also use the LOG file,
         // actual disk size will not change untill all threads have closed. For this
         // we will use the guard-dog task. If no guard-dog found, we will reboot from main.
         //
         ulSecsRestart = RTC_GetSystemSecs();
         LOG_Report(0, "HST", "rpi-CreateNewLog():Request RpiDog to restart theApp.");
         GLOBAL_Notify(PID_HOST, GLOBAL_ALL_HST_RST, SIGUSR1);
         fCc = FALSE;
         break;

      case 1:
         LOG_Report(errno, "HST", "rpi-CreateNewLog():Truncate persistant log ERROR:");
         break;

      case 2:
         LOG_Report(errno, "HST", "rpi-CreateNewLog():Delete work log ERROR:");
         break;

      case 3:
         LOG_Report(errno, "HST", "rpi-CreateNewLog():Delete persistant log ERROR:");
         LOG_Report(errno, "HST", "rpi-CreateNewLog():Delete work log ERROR:");
         break;

      default:
      case 4:
         LOG_Report(errno, "HST", "rpi-CreateNewLog():Backup ERROR:");
         break;
   }
   return(fCc);
}

// 
// Function:   rpi_ExtractSystemVars
// Purpose:    Extract data from the system
// 
// Parameters: System var SVAR_xxxx
// Returns:    TRUE if OKee
// 
static bool rpi_ExtractSystemVars(SVAR tVar)
{
   bool     fCc=FALSE;
   FILE    *tFp;
   double   flVal;
   double  *pflVal;
   char    *pcFile;
   char     pcBuffer[64];

   if(tVar < NUM_SVARS)
   {
      pcFile = stSystemVars[tVar].pcFile;
      if(pcFile == NULL) return(FALSE);
      //
      if( (tFp = fopen(pcFile, "r")) != NULL)
      {
         switch(tVar)
         {
            default:
            case SVAR_NONE:
               break;   
            
            case SVAR_TEMPERATURE:
               if(fgets(pcBuffer, sizeof(pcBuffer), tFp) != NULL)
               {
                  flVal   = atof(pcBuffer);
                  flVal  /= 1000;
                  pflVal  = (double *)stSystemVars[tVar].pvData;
                  *pflVal = flVal;
                  fCc     = TRUE;
               }
               else LOG_Report(errno, "HST", "rpi-ExtractSystemVars():Read ERROR:");
               break;   
         }
         fclose(tFp);
      }
      else
      {
         LOG_Report(errno, "HST", "rpi-ExtractSystemVars():Open ERROR:");
      }
   }
   return(fCc);
}

//
// Function:   rpi_PowerOff
// Purpose:    Turn OFF power
//
// Parms:      
// Returns:     
// Note:   
//             
static void rpi_PowerOff()
{
   system(pcPowerOff);
}

//
// Function:   rpi_Reboot
// Purpose:    Reboot the system
//
// Parms:      TRUE for delayed
// Returns:     
// Note:   
//             
static void rpi_Reboot(bool fDelayed)
{
   if(fDelayed)   system(pcRebootDelayed);
   else           system(pcRebootNow);
}

//
// Function:   rpi_RestoreCsvFile
// Purpose:    Copy the cvs file from SD to RAM
//             
// Parms:     
// Returns:    cc
// Note:       
//
static int rpi_RestoreCsvFile()
{
   int      iCc=1; 
   char     cSrc, cDst;

   GLOBAL_GetFileInfo((char *)pcDirBackup, &cSrc);
   GLOBAL_GetFileInfo((char *)pcDirWork, &cDst);
   //
   if( (cSrc == 'D') && (cDst == 'D') )
   {
      iCc = system(pcShellRestorCsv);
      //
      PRINTF("rpi-RestoreCsvFile():%s" CRLF, pcShellRestorCsv);
      LOG_Report(0, "HST", "rpi-RestoreCsvFile():%s", pcShellRestorCsv);
   }
   else LOG_Report(0, "HST", "rpi-RestoreCsvFile():NO SUCH DIRs !! Shell=%s", pcShellRestorCsv);
   return(iCc);
}

//
//  Function:   rpi_SetupGpio()
//  Purpose:    Setup GPIO for various IO alternatives
//
//  Parms:      
//  Returns:    
//
static void rpi_SetupGpio()
{
   printf("rpi-SetupGpio():" RPI_IO_BOARD CRLF);

#ifdef FEATURE_IO_NONE
   //=============================================================================
   // Use NO I/O board
   //=============================================================================
#endif


#ifdef FEATURE_IO_PATRN
   //=============================================================================
   // Used Patrn.nl IO board (wiringPi API)
   //=============================================================================
   if( (wiringPiSetup() == -1) )
   {
      printf("rpi-SetupGpio(): wiringPiSetup failed!" CRLF);
   }
   //
   INP_GPIO(LED_Y);     OUT_GPIO(LED_Y);
   INP_GPIO(LED_G);     OUT_GPIO(LED_G);
#endif
}

//
//  Function:   rpi_SetupUser
//  Purpose:    Setup the actual User for the app
//
//  Parms:      Username (NULL if we stay root)
//  Returns:    TRUE if OKee
//
static bool rpi_SetupUser(const char *pcUser)
{
   bool  fOkee=TRUE;
   uid_t tUid;
   gid_t tGid;

   if(pcUser)
   {
      #ifdef  FEATURE_SHOW_USER
      struct passwd *pstPwd;

      errno  = 0;
      pstPwd = getpwnam(pcUser);
      if (pstPwd == NULL) 
      {
         printf("rpi-SetupUser(): Error using <%s>:%s"  CRLF, pcUser, strerror(errno));
      }
      else
      {
         printf("rpi-SetupUser(): Name  = %s" CRLF, pstPwd->pw_name);
         printf("rpi-SetupUser(): Psw   = %s" CRLF, pstPwd->pw_passwd);
         printf("rpi-SetupUser(): Gecos = %s" CRLF, pstPwd->pw_gecos);
         printf("rpi-SetupUser(): Dir   = %s" CRLF, pstPwd->pw_dir);
         printf("rpi-SetupUser(): Shell = %s" CRLF, pstPwd->pw_shell);
      }
      #endif   //FEATURE_SHOW_USER
   }
   //
   // Some action require root permission
   //
   if(getuid() == 0)
   {
      //
      // We are ROOT
      //
      if(!GEN_DirectoryExists(pcCacheDir))  
      {
         //
         // Cache dir does not yet exist: create it
         //
         printf("rpi-SetupUser(): Create directory [%s]" CRLF, pcCacheDir);
         if( (fOkee = GEN_DirectoryCreate((char *)pcCacheDir, ATTR_RWXR_XR_X, FALSE)) )
         {
            if(pcUser)
            {
               //
               // We need to run in userspace
               //
               if((fOkee = GEN_ChangeOwner(pcCacheDir, pcNewUser, pcGroupWww, ATTR_RWXR_XR_X, TRUE)) )
               {
                  int   iFd;
                  //
                  // Create the CSV file
                  //
                  if( (iFd = safeopen2((char *)pcCsvFile, O_RDWR|O_CREAT|O_TRUNC, 0640)) < 0) fOkee = FALSE;
                  else
                  {
                     safeclose(iFd);
                     fOkee = GEN_ChangeOwner(pcCsvFile, pcNewUser, pcGroupWww, ATTR_RWXR_XR_X, FALSE);
                  }
               }
            }
         }
         else printf("rpi-SetupUser(): ERROR creating directory [%s]" CRLF, pcCacheDir);
      }
      //
      if(fOkee)
      {
         printf("rpi-SetupUser(): Directory [%s] OKee" CRLF, pcCacheDir);
         //
         // Cache Dir exists, attributes changed. 
         // Create/copy mapfile as root, then switch to userspace if required
         //
         GLOBAL_Init();
         if(pcUser)
         {
            fOkee = GEN_ChangeOwner(pcMapFile, pcNewUser, pcGroupWww, ATTR_RW_RW_R__, FALSE);
            //
            // All dirs and files have the correct user/group ID
            // Switch to new user
            //
            tUid = GEN_GetUserIdByName(pcUser);
            tGid = GEN_GetGroupIdByName(pcGroupWww);
            //
            if( (tUid > 0) && (tGid > 0) )
            {
               if( setgid(tGid) == -1)
               {
                  printf("rpi-SetupUser():ERROR setting Group %s (%d): %s" CRLF, pcGroupWww, (int)tGid, strerror(errno));
                  fOkee = FALSE;
               }
               else 
               {
                  printf("rpi-SetupUser():Now Group %s (%d)" CRLF, pcGroupWww, (int)tGid);
               }
               if( setuid(tUid) == -1)
               {
                  printf("rpi-SetupUser():ERROR setting User %s (%d): %s" CRLF, pcUser, (int)tUid, strerror(errno));
                  fOkee = FALSE;
               }
               else 
               {
                  printf("rpi-SetupUser():Now User %s (%d)" CRLF, pcUser, (int)tUid);
               }
            }
            else 
            {
               printf("rpi-SetupUser():Error setting up User %s" CRLF, pcUser);
               fOkee = FALSE;
            }
         }
      }
      else printf("rpi-SetupUser():Error changing attributes for User %s" CRLF, pcUser);
   }
   else
   {
      printf("rpi-SetupUser():" RPI_APP_TEXT " must be started as Root !" CRLF);
      fOkee = FALSE;
   }
   return(fOkee);
}

/*----------------------------------------------------------------------
_______________SIGNAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/

//
// Function:   rpi_SignalMemoryChanged
// Purpose:    Notification from the HTTP Server that memory has been changed
//             Handle Smart Sensor memory updates
// Parms:      
// Returns:    
// Note:       G_stCmd contains the new EEPROM/RAM memory map
//             
static int rpi_SignalMemoryChanged(void *pvData)
{
   LOG_Report(0, "HST", "rpi-SignalMemoryChanged():Nfy from Server");
   return(0);
}

//
// Function:   rpi_SignalMemoryEeprom
// Purpose:    Notification from COMMs that new EEPROM data has been downloaded.
//             Handle Smart Sensor EEPROM memory updates
// Parms:      
// Returns:    
// Note:       G_stCmd.pcData contains the new EEPROM memory map
//             Switch to next transfer mode if OK, else retry
//             
static int rpi_SignalMemoryEeprom(void *pvData)
{
   bool  fNext=TRUE;
   SMCMD tCmd;

   LOG_Report(0, "HST", "rpi-SignalMemoryEeprom():Nfy from COMM");
   if(GLOBAL_DebugPrint(MM_DEBUG_SENSOR, 2)) LOG_printf("rpi-SignalMemoryEeprom():Nfy from COMM" CRLF);
   //
   if( MIAC_ParseEepromMemory())
   {
      rpi_CalculateWatermarks();
      //
      if(GLOBAL_DebugPrint(MM_DEBUG_PRINT, 1)) LOG_printf("rpi-SignalMemoryEeprom():EEPROM memory updated OKee" CRLF);
      LOG_Report(0, "HST", "rpi-SignalMemoryEeprom():EEPROM memory updated OKee");
      LOG_Report(0, "HST", "rpi-SignalMemoryEeprom():Lo/Hi watermarks are %5.1f / %5.1f mm", pstMap->G_flLoWater, pstMap->G_flHiWater);
      LOG_ListData("rpi-SignalMemoryEeprom():Eeprom memory", (char *)pstMap->G_ubRomImage, KINS_ROM_LEN);
      if(GLOBAL_DebugPrint(MM_DEBUG_PRINT, 2)) LOG_DumpData("rpi-SignalMemoryEeprom():Eeprom memory", (char *)pstMap->G_ubRomImage, KINS_ROM_LEN);
   }
   else 
   {
      LOG_Report(0, "HST", "rpi-SignalMemoryEeprom():EEPROM memory updated ERROR:Retry %d", iTransferRetry);
      if(GLOBAL_DebugPrint(MM_DEBUG_SENSOR, 1)) LOG_printf("rpi-SignalMemoryEeprom():ERROR parsing Eeprom memory %d" CRLF, iTransferRetry);
      LOG_ListData("rpi-SignalMemoryEeprom():Eeprom memory update ERROR", (char *)pstMap->G_ubRomImage, KINS_ROM_LEN);
      if(iTransferRetry)
      {
         // Retry
         iTransferRetry--;
         iTransferIndex = iTransferAgain;
         if(GLOBAL_DebugPrint(MM_DEBUG_PRINT, 1)) LOG_printf("rpi-SignalMemoryEeprom():%s" CRLF, pcSensorCommands[ptTransferCommands[iTransferAgain]]);
         fNext = FALSE;
      }
   }
   //
   // If memory transfer is running
   //
   if(fNext) iTransferIndex++;
   if(iTransferIndex >=0) 
   {
      tCmd = ptTransferCommands[iTransferIndex];
      LOG_Report(0, "HST", "rpi-SensorMemoryEeprom():Next is %s", pcSensorCommands[tCmd]);
      if(GLOBAL_DebugPrint(MM_DEBUG_PRINT, 1)) LOG_printf("rpi-SensorMemoryEeprom():Next is %s" CRLF, pcSensorCommands[tCmd]);
   }
   return(0);
}

//
// Function:   rpi_SignalMemoryError
// Purpose:    Notification from COMMs that we have a problem
//             
// Parms:      
// Returns:    
// Note:       G_stCmd.iError contains the error code
//             
static int rpi_SignalMemoryError(void *pvData)
{
   LOG_Report(0, "HST", "rpi-SignalMemoryError():COMM ERROR %d", pstMap->G_stCmd.iError);
   if(GLOBAL_DebugPrint(MM_DEBUG_SENSOR, 1)) LOG_printf("rpi-SignalMemoryError():COMM ERROR %d" CRLF, pstMap->G_stCmd.iError);
   //
   // Make sure to get back to SENSOR mode and receive regular SmartSensor data
   //
   pstMap->G_stCmd.iCommand = SMCMD_DLD_SENSOR;
   GLOBAL_Signal(PID_COMM, SIGUSR2);
   return(0);
}

//
// Function:   rpi_SignalMemoryMiac
// Purpose:    Notification from COMMs that new MIAC data has been downloaded.
//             
// Parms:      
// Returns:    
// Note:       Switch to RPi Sensor mode upon completion.
//             
static bool rpi_SignalMemoryMiac(void *pvData)
{
   LOG_Report(0, "HST", "rpi-SignalMemoryMiac():Nfy from COMM: Refresh image");
   if(GLOBAL_DebugPrint(MM_DEBUG_PRINT, 1)) LOG_printf("rpi-SignalMemoryMiac():Nfy from COMM: Refresh image" CRLF);
   //
   // Memory has been updated: refresh the images
   //
   iTransferIndex = 0;
   return(0);
}

//
// Function:   rpi_SignalMemoryRam
// Purpose:    Notification from COMMs that new RAM data has been downloaded.
//             Handle Smart Sensor RAM memory updates
// Parms:      
// Returns:    
// Note:       G_stCmd.pcData contains the new RAM memory map
//             Switch to next transfer mode if OK, else retry
//             
static int rpi_SignalMemoryRam(void *pvData)
{
   bool  fNext=TRUE;
   SMCMD tCmd;

   LOG_Report(0, "HST", "rpi-SignalMemoryRam():Nfy from COMM");
   if(GLOBAL_DebugPrint(MM_DEBUG_PRINT, 1)) LOG_printf("rpi-SignalMemoryRam()Nfy from COMM" CRLF);
   if( MIAC_ParseRamMemory())
   {
      if(GLOBAL_DebugPrint(MM_DEBUG_PRINT, 1)) LOG_printf("rpi-SignalMemoryRam():RAM memory updated OKee" CRLF);
      LOG_Report(0, "HST", "rpi-SignalMemoryRam():RAM memory updated OKee");
      LOG_ListData("rpi-SignalMemoryRam():RAM memory", (char *)pstMap->G_ubRamImage, KINS_RAM_LEN);
      if(GLOBAL_DebugPrint(MM_DEBUG_PRINT, 2)) LOG_DumpData("rpi-SignalMemoryRam():RAM memory", (char *)pstMap->G_ubRamImage, KINS_RAM_LEN);
   }
   else 
   {
      LOG_Report(0, "HST", "rpi-SignalMemoryRam():RAM memory updated ERROR, retry %d", iTransferRetry);
      LOG_ListData("rpi-SignalMemoryRam():RAM memory update ERROR", (char *)pstMap->G_ubRamImage, KINS_RAM_LEN);
      if(GLOBAL_DebugPrint(MM_DEBUG_PRINT, 1)) LOG_printf("rpi-SignalMemoryRam():ERROR parsing RAM memory %d" CRLF, iTransferRetry);
      if(iTransferRetry)
      {
         // Retry
         iTransferRetry--;
         iTransferIndex = iTransferAgain;
         if(GLOBAL_DebugPrint(MM_DEBUG_PRINT, 1)) LOG_printf("rpi-SignalMemoryRam():%s" CRLF, pcSensorCommands[ptTransferCommands[iTransferAgain]]);
         fNext = FALSE;
      }
   }
   //
   // If memory transfer/auto-update is running
   //
   if(fNext) iTransferIndex++;
   if(iTransferIndex >=0) 
   {
      tCmd = ptTransferCommands[iTransferIndex];
      LOG_Report(0, "HST", "rpi-SensorMemoryRam():Next is %s", pcSensorCommands[tCmd]);
      if(GLOBAL_DebugPrint(MM_DEBUG_PRINT, 1)) LOG_printf("rpi-SignalMemoryRam():Next is %s" CRLF, pcSensorCommands[tCmd]);
   }
   return(0);
}

//
// Function:  rpi_SignalRequestPowerOff
// Purpose:   Handle signal GLOBAL_ALL_HST_PWR
//
// Parms:     Parameter ptr
// Returns:   
//
static int rpi_SignalRequestPowerOff(void *pvParm)
{
   PRINTF("rpi-SignalRequestPowerOff():PowerOff" CRLF);
   LOG_Report(0, "HST", "rpi-SignalRequestPowerOff():SIGUSR2 received, Power Off....");
   //
   fRpiPowerOff = TRUE;
   fHostRunning = FALSE;
   return(0);
}

//
// Function:   rpi_SignalRequestReboot
// Purpose:    Handle signal GLOBAL_ALL_HST_RBT 
//
// Parms:      Parameter ptr
// Returns:   
//
static int rpi_SignalRequestReboot(void *pvParm)
{
   LOG_Report(0, "HST", "rpi-SignalRequestReboot():Reboot request!");
   rpi_Reboot(TRUE);
   fHostRunning = FALSE;
   return(0);
}

//
// Function:   rpi_SignalRequestTerminate
//  Purpose:   Handle signal GLOBAL_ALL_HST_RBT 
//
// Parms:      Parameter ptr
// Returns:   
//
static int rpi_SignalRequestTerminate(void *pvParm)
{
   PRINTF("rpi-SignalRequestTerminate():Terminate....." CRLF);
   LOG_Report(0, "HST", "rpi-SignalRequestTerminate():SIGINT/SIGTERM received, Terminate....");
   //
   fHostRunning = FALSE;
   return(0);
}

//
// Function:   rpi_SignalSegmentationFault
// Purpose:    Handle signal GLOBAL_ALL_HST_FLT 
//
// Parms:      Parameter ptr
// Returns:   
//
static int rpi_SignalSegmentationFault(void *pvParm)
{
   fHostRunning = FALSE;
   return(0);
}

//
// Function:   rpi_SignalSendEmail
//  Purpose:   Handle signal GLOBAL_ALL_HST_EML
//
// Parms:      Parameter ptr
// Returns:   
//
static int rpi_SignalSendEmail(void *pvParm)
{
   ulEmailPending   = RTC_GetSystemSecs();
   iMaxEmailRetries = 3;
   return(0);
}

//
// Function:   rpi_SignalSensorPing
//  Purpose:   Handle signal GLOBAL_COM_HST_PPI
//
// Parms:      Parameter ptr
// Returns:   
//
static int rpi_SignalSensorPing(void *pvParm)
{
   pcSensorData = pstMap->G_pcPing;
   return(0);
}

//
// Function:   rpi_SignalSensorPong
//  Purpose:   Handle signal GLOBAL_COM_HST_PPO
//
// Parms:      Parameter ptr
// Returns:   
//
static int rpi_SignalSensorPong(void *pvParm)
{
   pcSensorData = pstMap->G_pcPong;
   return(0);
}

//
// Function:   rpi_SignalSetTimers
//  Purpose:   Handle signal GLOBAL_SVR_HST_TMR
//
// Parms:      Parameter ptr
// Returns:   
//
static int rpi_SignalSetTimers(void *pvParm)
{
   rpi_SensorSetTimers(0);
   return(0);
}

//
// Function:   rpi_SignalTransferMemory
//  Purpose:   Handle signal GLOBAL_DBG_HST_UPD
//
// Parms:      Parameter ptr
// Returns:   
//
static int rpi_SignalTransferMemory(void *pvParm)
{
   SMCMD tCmd;

   if(iTransferIndex < 0)
   {
      //
      // Previous transfer has completed, OK to start a new one
      //
      if(GLOBAL_DebugPrint(MM_DEBUG_PRINT, 1)) LOG_printf("rpi-SignalTransferMemory():Start Transfer" CRLF);
      pstMap->G_stCmd.iSecsMiac = 0;
      iTransferIndex = 0;
   }
   else 
   {
      if(GLOBAL_DebugPrint(MM_DEBUG_PRINT, 1)) 
      {
         tCmd = ptTransferCommands[iTransferIndex];
         LOG_printf("rpi-SignalTransferMemory():Transfer ongoing:" CRLF, pcSensorCommands[tCmd]);
      }
   }
   return(0);
}

/*----------------------------------------------------------------------
_______________SENSOR_FUNCTIONS(){}
------------------------------x----------------------------------------*/

//
// Function:   rpi_CalculateWatermarks
// Purpose:    Calculate the HI/Lo water thresholds
//
// Parms:      
// Returns:    
//
static void rpi_CalculateWatermarks()
{
   int      iTemp;
   u_int8   ubTempHi;
   u_int8   ubTempLo;

   //
   // Calculate the HI Water level
   // Get Hi and Lo water levels as stored from uCtl EEPROM. These are the 
   // timer counter values in 0.4 uSecs ticks.
   //
   MIAC_GetEepromMemory(MIAC_E_EEHIWATERHI, &ubTempHi);
   MIAC_GetEepromMemory(MIAC_E_EEHIWATERLO, &ubTempLo);
   //
   // Calculate the actual Hi water level:
   // Level in mm = timerticks * 0.4 / 5.8824
   //             = timerticks * 0.0680 (pulse speed in uSecs/mm)
   //
   iTemp  = (( (int) ubTempHi) << 8 ) + (int) ubTempLo;
   pstMap->G_flHiWater = pstMap->G_flSensorBottom - pstMap->G_flFloater - ((double) iTemp * flMmPerTimerTick);
   //
   // Calculate the LO Water level
   // Get Hi and Lo water levels as stored from uCtl EEPROM. These are the 
   // timer counter values in 0.4 uSecs ticks.
   //
   MIAC_GetEepromMemory(MIAC_E_EELOWATERHI, &ubTempHi);
   MIAC_GetEepromMemory(MIAC_E_EELOWATERLO, &ubTempLo);
   //
   // Calculate the actual Hi water level:
   // Level in mm = timerticks *.0.4 / 5.8824 (pulse speed in uScs/mm)
   //             = timerticks /
   //
   iTemp  = (( (int) ubTempHi) << 8 ) + (int) ubTempLo;
   pstMap->G_flLoWater = pstMap->G_flSensorBottom - pstMap->G_flFloater - ((double) iTemp * flMmPerTimerTick);
}

//
// Function:   rpi_CheckValidSample
// Purpose:    Check if samples went OKee. LOG good/bad ones
//
// Parms:      Current sample completetion, Previous sample, current sample
// Returns:    TRUE if all is (still) well.
// Note:       Check if we have consequetive bad samples. If so, reset the sensor.
//             Check also if previous sample was bad. If so log current one as well.
//              
static bool rpi_CheckValidSample(bool fSample, char *pcPrev, char *pcCurr)
{
   static bool fLogSample=FALSE;
   static int  iMaxInvalid=MAX_INVALID_SAMPLES;
   //
   bool        fCc=TRUE;

   if(fLogSample)
   {
      //
      // Previous sample had problems: log this one as well for comparison
      //
      LOG_Report(0, "HST", "rpi-CheckValidSample(BAD+1):%2d [%s]", iMaxInvalid, pcCurr);
      fLogSample = FALSE;
   }
   if(fSample)
   {
      //
      // Sample is OKee: reset invalide sample count
      //
      iMaxInvalid=MAX_INVALID_SAMPLES;
   }
   else
   {
      //
      // Sample is NOT OKee: log previous, this and the next one.
      // If samples remain BAD: (try to) reset the sensor.
      //
      LOG_Report(0, "HST", "rpi-CheckValidSample(BAD-1):%2d [%s]", iMaxInvalid, pcPrev);
      LOG_Report(0, "HST", "rpi-CheckValidSample(BAD  ):%2d [%s]", iMaxInvalid, pcCurr);
      fLogSample = TRUE;
      //
      if(iMaxInvalid-- == 0) fCc = FALSE;

   }
   GEN_STRNCPY(pcPrev, pcCurr, KINS_SIZE_LEN);
   return(fCc);
}

//
// Function:   rpi_HandleCsvRecord
// Purpose:    Open/Write/close CSV record
//
// Parms:      Time, CSV record
// Returns:    Number written or -1 on error
// Note:       
//             
static int rpi_HandleCsvRecord(u_int32 ulSecs, char *pcCsvBuffer)
{
   int      iNr=-1;
   int      iDay, iLen;
   char    *pcFile;
   char     cTemp[4];

   iDay = RTC_GetDay(ulSecs);
   if(iDay != iCsvDay)
   {
      if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 9)) LOG_printf("rpi-HandleCsvRecord():New Day=%d" CRLF, iDay);
      //
      // New day: Close previous dayfile (if any)
      //
      if(tCsvFile)
      {
         safeclose(tCsvFile);
         tCsvFile = 0;
      }
      iCsvDay = iDay;
   }
   if(tCsvFile == 0)
   {
      //
      // Build sample CSV file  "/mnt/rpicache/SmartSensor_XXX.csv"
      //             pcCsvFile  "%s/SmartSensor_%s.csv"
      // where XXX = day of the week
      //
      iLen   = RTC_ConvertDateTimeSize(TIME_FORMAT_DAY_OF_WEEK);
      iLen  += GEN_STRLEN(pstMap->G_pcRamDir);
      iLen  += GEN_STRLEN(pcCsvFile);
      pcFile = safemalloc(iLen);
      //
      RTC_ConvertDateTime(TIME_FORMAT_DAY_OF_WEEK, cTemp, ulSecs);
      GEN_SNPRINTF(pcFile, iLen, pcCsvFile, pstMap->G_pcRamDir, cTemp);
      if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 1)) LOG_printf("rpi-HandleCsvRecord():Open %s" CRLF, pcFile);
      //
      tCsvFile = safeopen2(pcFile, O_RDWR|O_CREAT|O_APPEND, (S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH));
      if(tCsvFile <= 0) LOG_Report(errno, "HST", "rpi-HandleCsvRecord():CSV-file %s Open ERROR:", pcFile);
      //
      safefree(pcFile);
   }
   //
   // Append record
   //
   if(tCsvFile > 0)
   {
      iLen = GEN_STRLEN(pcCsvBuffer);
      iNr  = safewrite(tCsvFile, pcCsvBuffer, iLen);
      if(iNr == iLen) 
      {
         if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 5)) LOG_printf("rpi-HandleCsvRecord():[%s]" CRLF, pcCsvBuffer);
         safewrite(tCsvFile, CRLF, 2);
      }
      else             
      {
         // Problems with CSV file: retry new file
         PRINTF("rpi-HandleCsvRecord():ERROR write csv file" CRLF, strerror(errno));
         LOG_Report(errno, "HST", "rpi-HandleCsvRecord():ERROR write csv file: retry !");
         safeclose(tCsvFile);
         tCsvFile = 0;
      }
   }
   return(iNr);
}

//
// Function:   rpi_HandleSensorTransfer
// Purpose:    Handle the transfer/auto-update of the Smart sensor memory
//
// Parms:      Command or SMCMD_NONE if list (ptTransferCommands)
// Returns:    TRUE if still transferring memory data from uCtl to COMM thread
// Note:       iTransferIndex is the index into the transfer sequence
//             
static bool rpi_HandleSensorTransfer(SMCMD tCmd)
{
   static SMCMD tCurrCmd=SMCMD_NONE;
   //
   bool  fTransferring=TRUE;

   if(tCmd == SMCMD_NONE)
   {
      if( (iTransferIndex >= 0) && (iTransferIndex < iNumTransferCmds) )
      {
         tCmd = ptTransferCommands[iTransferIndex];
         if(tCurrCmd != tCmd)
         {
            tCurrCmd = tCmd;
            LOG_Report(0, "HST", "rpi-HandleSensorTransfer():%s", pcSensorCommands[tCmd]);
            if(GLOBAL_DebugPrint(MM_DEBUG_PRINT, 1)) LOG_printf("rpi-HandleSensorTransfer():%s" CRLF, pcSensorCommands[tCmd]);
         }
      }
      else 
      {
         //
         // No transfer in progress
         //
         fTransferring = FALSE;
      }
   }
   switch(tCmd)
   {
      case SMCMD_INIT:
         iLoopTimeout = 1000;
         iTransferIndex++;
         break;

      default:
      case SMCMD_IDLE_MODE:
         // Waiting for COMM nfy
         break;

      case SMCMD_READY:
         //
         // Ready
         // Make sure to get back to SENSOR mode and receive regular SmartSensor data
         //
         pstMap->G_stCmd.iCommand = SMCMD_DLD_SENSOR;
         GLOBAL_Signal(PID_COMM, SIGUSR2);
         //
         // Setup auto load memory time again, if enabled
         //
         rpi_SensorSetTimers(0);
         //
         // Kickstart the debugger now all sensor data is in
         //
         PRINTF("rpi-HandleSensorTransfer():Signal DEBUG permission" CRLF);
         GLOBAL_Notify(PID_DEBUG, GLOBAL_HST_DBG_RUN, SIGUSR1);
         //
         // Disable the Transfer/Update index
         //
         if(GLOBAL_DebugPrint(MM_DEBUG_PRINT, 1)) LOG_printf("rpi-HandleSensorTransfer():Ready" CRLF);
         iLoopTimeout   = DEFAULT_TIMEOUT;
         iTransferIndex = -1;
         fTransferring  = FALSE;
         break;

      case SMCMD_DELAY:
         GEN_Sleep(500);
         iTransferIndex++;
         break;

      case SMCMD_SET_RAM:
      case SMCMD_SET_ROM:
         iTransferRetry = 3;
         iTransferIndex++;
         iTransferAgain = iTransferIndex;
         if(GLOBAL_DebugPrint(MM_DEBUG_PRINT, 1)) LOG_printf("rpi-HandleSensorTransfer():Set retry to %s" CRLF, pcSensorCommands[ptTransferCommands[iTransferAgain]]);
         break;

      case SMCMD_DLD_ROM:
      case SMCMD_DLD_RAM:
         // Run next command
         if(GLOBAL_DebugPrint(MM_DEBUG_PRINT, 1)) LOG_printf("rpi-HandleSensorTransfer():Start download" CRLF);
         pstMap->G_stCmd.iCommand = tCmd;
         GLOBAL_Signal(PID_COMM, SIGUSR2);
         iTransferIndex++;
         break;
   }
   return(fTransferring);
}

//
// Function:   rpi_SensorAverage
// Purpose:    Average applicable Smart Sensor structure members
//
// Parms:      Destination ptr, Sensor ptr
// Returns:    
// Note:       The distance taken from the Sensor is in mm.
//              
static void rpi_SensorAverage(SMDATA *pstDest, SENS *pstSensor)
{
   double   flSamples;

   if(pstSensor->iSamples)
   {
      //
      // We have data
      //
      flSamples = (double) pstSensor->iSamples;
      //
      pstDest->flDistance    = pstSensor->flDistance    / flSamples;
      pstDest->flTemperature = pstSensor->flTemperature / flSamples;
      pstDest->flWaterLevel  = pstSensor->flWaterLevel  / flSamples;
   }
}

//
// Function:   rpi_SensorCheckCrc
// Purpose:    Check if the CRC of the buffer is OKee
//
// Parms:      Buffer ptr
// Returns:    TRUE if CRC OK
// Note:       CRC is a simple bytewise sum up to and including �!�
//              
static bool rpi_SensorCheckCrc(char *pcBuffer)
{
   bool     fCc=FALSE, fBusy=TRUE;
   int      iLen=0;
   u_int16  usCrc;
   u_int32  ulCrc;
   char    *pcStart=NULL;

   if(pcBuffer)
   {
      pcStart = pcBuffer;
      //
      // Determine record size
      //
      while(fBusy)
      {
         switch(*pcBuffer++)
         {
            case '!':
               //
               // EOR reached: check CRC
               //
               ulCrc = strtoul(pcBuffer, NULL, 16);
               usCrc = COMM_CalculateCrc((u_int8 *)pcStart, iLen+1);
               fCc = (usCrc == (u_int16)(ulCrc & 0xFFFF));
               //
               if(!fCc) 
               {
                  if(GLOBAL_DebugPrint(MM_DEBUG_SENSOR, 1)) LOG_printf("rpi-SensorCheckCrc():ERROR: Act CRC=%04X - Buffer says %04lX" CRLF, usCrc, ulCrc);
               }
               fBusy = FALSE;
               break;
            
            default:
               iLen++;
               break;
         
            case '\0':
               if(GLOBAL_DebugPrint(MM_DEBUG_SENSOR, 1)) LOG_printf("rpi-SensorCheckCrc():ERROR: No end of record '!' found (%d bytes)" CRLF, iLen);
               fBusy = FALSE;
               break;
         }
      }
   }
   return(fCc);
}

//
// Function:   rpi_SensorConvertData
// Purpose:    Convert SENSOR buffer which contains the smart sensor response
//
// Parms:      Buffer, SENSOR data
// Returns:    TRUE if all sensor data is OKee
//             pstSensSor has SENS totals = New totals
// Note:       Buffer contains uCtl record like:
//             "Dist=2a4f, Count=0004, Pump=1, X10=0, Temp=00bf, Solar=00e0, Time=003246, Mode=00, Sensor=v1.04-064!3a\n\r"
//
static bool rpi_SensorConvertData(char *pcBuffer, SENS *pstSensor)
{
   bool     fCc;
   int      iSafeCrcs;
   double   flWaterLevel;

   if(GLOBAL_DebugPrint(MM_DEBUG_SENSOR, 2)) LOG_printf("rpi-SensorConvertData():[%s]" CRLF, pcBuffer);
   iSafeCrcs = pstSensor->iCrcErrors;
   //
   GEN_MEMSET(pstSensor, 0x00, sizeof(SENS));
   //
   pstSensor->iCrcErrors = iSafeCrcs;
   pstSensor->iSamples   = 1;
   //
   // Parse through the ping/pong buffer and fill result struct
   //
   fCc = SENS_LookupCode(pcBuffer, pstSensor);
   //
   if(pstSensor->flTemperature == 0.0)
   {
      //
      // Smart Sensor does NOT give temperature: settle for the RPi internal Temp instead
      //
      pstSensor->flTemperature = flCpuTemp;
   }
   //===========================================================================================
   // pstSensor has the latest readings from the Smart Sensor.
   // Time to rework distance from sensor to water into actual level of the water in mm.
   //
   //               Sensor
   //             |       |
   //       +     +XXX=XXX+  +            S  = Offset Sensor to Bottom
   //       |     |       |  |            F  = Offset Water to floater
   //       |                |            Dx = Measured Raw Hex Distance by uController (in timerticks)
   //       |                |Dx          W  = Actual Waterlevel
   //       |     +       +  |Dm  
   //       |     |       |  |                  Dx * 0.4
   //       |     |=+===+=|  +   +        Dm = ---------- mm = 0.06800 * Dx mm
   //       |S    | |   | |      |F              5.88235
   //       |     | |   | |      |
   //       |~~~~~|~~~~~~~|~~~~~~+~~~ 
   //       |     | |   | |      |        
   //       |     |=+===+=|      |    
   //       |     |       |      |W       W = S - F - Dm
   //       |     |       |      |        pstMap->G_pcWaterLevel   = W  (mm)
   //       |     |       |      |        pstMap->G_flDm           = Dm (mm)
   //       |     |       |      |        pstMap->G_flSensorBottom = S  (mm)
   //       |     |       |      |        pstMap->G_Floater        = F  (mm)
   //   ----+-----+-------+------+--- 
   //                            
   // The Sensor delivers the distance from the sensor to the top of the Floater Dx in timer ticks.
   // Save this value for later reference and rework the actual water Height as W = S - F - Dm
   //                            
   pstMap->G_flDm = pstSensor->flDistance;
   flWaterLevel   = pstMap->G_flSensorBottom - pstMap->G_flFloater - pstMap->G_flDm;
   //
   // LOG the raw sensor distance in mm if in CALIBRATE mode (for calibration of the Floater height)
   //
   if(RPI_GetFlag(VERBOSE_LOG_CALIBRATE))
   {
      PRINTF("rpi-SensorConvertData():CALIBRATE: Dm=%5.1f mm, SensorHeight=%5.1f mm, Waterlevel=%5.1f mm" CRLF, 
                        pstMap->G_flDm, pstMap->G_flSensorBottom, flWaterLevel);
      //
      LOG_Report(0, "HST", "rpi-SensorConvertData():CALIBRATE: Dm=%5.1f mm, SensorHeight=%5.1f mm, Waterlevel=%5.1f mm", 
                        pstMap->G_flDm, pstMap->G_flSensorBottom, flWaterLevel);
   }
   //
   // Make sure the waterlevel does not exceed the max possible
   //
   if(flWaterLevel > MAX_WATERLEVEL) 
   {
      PRINTF("rpi-SensorConvertData():Waterlevel too high: Mode=0x%x: Dm=%5.1f mm, SensorHeight=%5.1f mm, WATERLEVEL=%5.1f mm" CRLF, 
            pstSensor->iPumpMode, pstMap->G_flDm, pstMap->G_flSensorBottom, flWaterLevel);
      //
      LOG_Report(0, "HST", "rpi-SensorConvertData():Waterlevel too high: Mode=0x%x: Dm=%5.1f mm, SensorHeight=%5.1f mm, WATERLEVEL=%5.1f mm", 
            pstSensor->iPumpMode, pstMap->G_flDm, pstMap->G_flSensorBottom, flWaterLevel);
      //
      LOG_ListData("rpi-SensorConvertData():", pcBuffer, KINS_SIZE_LENZ);
      flWaterLevel = MAX_WATERLEVEL_OVERFLOW;
   }
   pstSensor->flWaterLevel = flWaterLevel;
   return(fCc);
}

//
// Function:   rpi_SensorSetTimers
// Purpose:    Check/set the Smart Sensor memory update timers
//             
// Parms:      ulSecsNow or 0
// Returns:    
// Note:       To handle MIAC memory buffer updates, set the appropriate value in 
//             HTTP: MiacMem=10 (secs) --> pstMap->G_iMiacMemory = 10
//             
static void rpi_SensorSetTimers(u_int32 ulSecsNow)
{
   static int  iMiacTimerOld=0;

   if(ulSecsNow == 0)
   {
      ulSecsNow     = RTC_GetSystemSecs();
      iMiacTimerOld = -1;
   }
   if(iMiacTimerOld != pstMap->G_iMiacMemory)
   {
      // Auto-update Memory timer changed
      if(pstMap->G_iMiacMemory)  pstMap->G_stCmd.iSecsMiac = ulSecsNow + pstMap->G_iMiacMemory;
      else                       pstMap->G_stCmd.iSecsMiac = 0;
      //
      iMiacTimerOld = pstMap->G_iMiacMemory;
   }
}

//
// Function:   rpi_SensorSum
// Purpose:    Sum applicable Smart Sensor structure members
//
// Parms:      pstDest, pstSensNow
// Returns:    
// Note:       
//              
static void rpi_SensorSum(SENS *pstSensSum, SENS *pstSensNow)
{
   if(pstSensSum->iCounts != pstSensNow->iCounts)
   {
      //
      // We have new data
      //
      pstSensSum->iTimeInSecs      = pstSensNow->iTimeInSecs;
      pstSensSum->iCounts          = pstSensNow->iCounts;
      //
      pstSensSum->iCrcErrors      += pstSensNow->iCrcErrors;
      pstSensSum->iSamples        += pstSensNow->iSamples;
      pstSensSum->flDistance      += pstSensNow->flDistance;
      pstSensSum->flTemperature   += pstSensNow->flTemperature;
      pstSensSum->flWaterLevel    += pstSensNow->flWaterLevel;
      //
      pstSensSum->iPumpMode        = pstSensNow->iPumpMode;
      if(pstSensNow->iPumpOn)        pstSensSum->iPumpOn++;
      if(pstSensNow->iPumpOff)       pstSensSum->iPumpOff++;
      //
      if(pstSensNow->iX10Active)     pstSensSum->iX10Active++;
      if(pstSensNow->iX10Bypass)     pstSensSum->iX10Bypass++;
   }
}

//
// Function:   rpi_SensSampleCsv
// Purpose:    Sample csv record
//
// Parms:      Secs now, SENS csv, SENS now
// Returns:    
// Note:       csv record:
//             "year, month, day, hour, min, sec, status, waterlevel"
//
static void rpi_SensSampleCsv(u_int32 ulSecsNow, SENS *pstSensCsv, SENS *pstSensNow)
{
   int      iLen;
   SMDATA   stSensorTemp;
   char    *pcBuffer;
   char    *pcTemp;
   
   GEN_MEMSET(&stSensorTemp, 0x00, sizeof(SMDATA));
   //
   if(pstSensCsv->iPeriod == -1)
   {
      //
      // One time init
      //
      GEN_MEMSET(pstSensCsv, 0x00, sizeof(SENS));
   }
   //
   // Keep summing data until sampling time is over. Then average result
   // and create the CSV record.
   //
   rpi_SensorSum(pstSensCsv, pstSensNow);
   if(pstSensCsv->ulSecsSample < ulSecsNow)
   {
      //
      // Sampling a single CSV record is over: calculate and store the data
      //
      rpi_SensorAverage(&stSensorTemp, pstSensCsv);
      rpi_SensUpdateGlobals(KINS_CSV, pstSensCsv, &stSensorTemp);
      if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 5)) LOG_printf("rpi-SensSampleCsv():=AGV:Waterlevel=%5.3f, %d Samples" CRLF, stSensorTemp.flWaterLevel, pstSensCsv->iSamples);
      //
      iLen   = RTC_ConvertDateTimeSize(TIME_FORMAT_CSV_YYYY_MM_DD_HH_MM_SS);
      iLen  += 32;
      pcTemp = safemalloc(iLen);
      RTC_ConvertDateTime(TIME_FORMAT_CSV_YYYY_MM_DD_HH_MM_SS, pcTemp, ulSecsNow);
      //
      // Create record 
      //
      iLen    += GEN_STRLEN(pstMap->G_pcWaterLevel);
      pcBuffer = safemalloc(iLen);
      GEN_SNPRINTF(pcBuffer, iLen, "%s,%d,%s", pcTemp, pstSensNow->iPumpOn, pstMap->G_pcWaterLevel);
      if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 5)) LOG_printf("rpi-SensSampleCsv():Create CSV record:[%s]" CRLF, pcBuffer);
      //
      rpi_HandleCsvRecord(ulSecsNow, pcBuffer);
      GEN_MEMSET(pstSensCsv, 0x00, sizeof(SENS));
      pstSensCsv->ulSecsSample = ulSecsNow + iCsvSampleSecs;
      safefree(pcBuffer);
      safefree(pcTemp);
   }
}

//
// Function:   rpi_SensSampleDay
// Purpose:    Check if sampling day is over
//
// Parms:      Secs now, SENS today, SENS now (delta's)
// Returns:    
// Note:       
//
static void rpi_SensSampleDay(u_int32 ulSecsNow, SENS *pstSensDay, SENS *pstSensNow)
{
   int      iThisDay, iLastDay;

   //
   // RTC_GetDay() gets the day 1..31: make Index 0..30
   //
   iThisDay = RTC_GetDay(ulSecsNow) - 1;
   //
   // Check if sampling day is over
   //
   if(pstSensDay->iPeriod == iThisDay)
   {
      //
      // It's still the same day:
      // Save the secs timestamp
      // Update daily sample number
      //
      rpi_SensorSum(pstSensDay, pstSensNow);
      pstSensDay->ulSecsSample = ulSecsNow;
   }
   else
   {
      if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 9)) LOG_printf("rpi-SensSampleDay():This day=%d" CRLF, iThisDay);
      //
      // Sample time (if it's not the first one)
      //
      if(pstSensDay->iPeriod != -1)
      {
         iLastDay = pstSensDay->iPeriod;
         //
         // It's time to move to the next day, save previous day value's
         // Reset samples for the new day
         //
         rpi_SensorAverage(&pstMap->G_stThisMonth[iLastDay], pstSensDay);
         rpi_SensUpdateGlobals(KINS_DAY, pstSensDay, &pstMap->G_stSensorLife);
         //
         // It's time to move to the next day 
         // Reset quarter samples for this day
         //
         GEN_MEMCPY(pstMap->G_stLastDay, pstMap->G_stThisDay,   sizeof(pstMap->G_stLastDay));
         GEN_MEMSET(pstMap->G_stThisDay, 0x00, sizeof(pstMap->G_stThisDay));
         //
         // Save previous day to file yymmdd.bin
         //
         if( RPI_SaveDay(pstSensDay->ulSecsSample, (u_int8 *)pstMap->G_stLastDay, sizeof(pstMap->G_stLastDay)) )
         {
            RPI_DAY("Day saved");
            if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 5)) LOG_printf("rpi-SensSampleDay():Last day=%d saved." CRLF, iLastDay);
         }
         else
         {
            RPI_DAY("Day not yet saved");
            if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 5)) LOG_printf("rpi-SensSampleDay():Last day=%d NOT YET saved." CRLF, iLastDay);
         }
      }
      else rpi_SensUpdateGlobals(KINS_DAY, pstSensDay, &pstMap->G_stSensorLife);
      //
      // Restart and Copy static values
      //
      GEN_MEMSET(pstSensDay, 0x00, sizeof(SENS));
      GEN_STRNCPY(pstSensDay->cEquipment, pstSensNow->cEquipment, MAX_SENS);
      pstSensDay->iPeriod      = iThisDay;
      pstSensDay->ulSecsSample = ulSecsNow;
   }
}  

//
// Function:   rpi_SensSampleMin
// Purpose:    Check is sampling minute is over
//
// Parms:      Secs now, SENS Minute, SENS now
// Returns:    
// Note:       
//
static void rpi_SensSampleMin(u_int32 ulSecsNow, SENS *pstSensMin, SENS *pstSensNow)
{
   int   iMin;

   iMin = RTC_GetMinute(ulSecsNow);
   //
   // Check if sampling period is over
   //
   if(pstSensMin->iPeriod == iMin)
   {
      //
      // It's still the same minute: SUM all delta's
      // Save the timestamp
      //
      rpi_SensorSum(pstSensMin, pstSensNow);
      pstSensMin->ulSecsSample = ulSecsNow;
   }
   else
   {
      if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 9)) LOG_printf("rpi-SensSampleMin():This minute=%d" CRLF, iMin);
      //
      // Sample time (if it's not the first one)
      //
      if(pstSensMin->iPeriod != -1)
      {
         //
         // Smart Sensor does NOT give temperature: settle for the RPi internal Temp instead
         //
         if(!rpi_ExtractSystemVars(SVAR_TEMPERATURE)) LOG_Report(0, "HST", "rpi-SensSampleMin():Temperature read ERROR");
         //
         // It's time to move to the next minute
         // Calculate results each type
         //
         rpi_SensorAverage(&pstMap->G_stSensorLife, pstSensMin);
         rpi_SensUpdateGlobals(KINS_MINUTE, pstSensMin, &pstMap->G_stSensorLife);
      }
      else rpi_SensUpdateGlobals(KINS_MINUTE, pstSensMin, &pstMap->G_stSensorLife);
      //
      // Copy static values
      // Reset samples for the new minute
      //
      GEN_MEMSET(pstSensMin, 0x00, sizeof(SENS));
      GEN_STRNCPY(pstSensMin->cEquipment, pstSensNow->cEquipment, MAX_SENS);
      pstSensMin->ulSecsSample = ulSecsNow;
      pstSensMin->iPeriod      = iMin;
      if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 5)) LOG_printf("rpi-SensSampleMin():Updated." CRLF);
   }
}

//
// Function:   rpi_SensSampleMon
// Purpose:    Check is sampling month is over
//
// Parms:      Secs now, SENS this month, SENS now (delta's)
// Returns:    
// Note:       
//
static void rpi_SensSampleMon(u_int32 ulSecsNow, SENS *pstSensMon, SENS *pstSensNow)
{
   int      iThisMonth;

   //
   // RTC_GetMonth() gets the month 1..12: make Index 0..11
   //
   iThisMonth = RTC_GetMonth(ulSecsNow) - 1;
   //
   // Check if sampling month is over
   //
   if(pstSensMon->iPeriod == iThisMonth)
   {
      //
      // It's still the same month: SUM all delta's per month
      // Save the secs timestamp
      //
      rpi_SensorSum(pstSensMon, pstSensNow);
      pstSensMon->ulSecsSample = ulSecsNow;
   }
   else
   {
      if(pstSensMon->iPeriod != -1)
      {
         if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 9)) LOG_printf("rpi-SensSampleMon():This month=%d" CRLF, iThisMonth);
         //
         // It's time to move to the next month
         // Reset samples for this month
         //
         rpi_SensUpdateGlobals(KINS_MONTH, pstSensMon, &pstMap->G_stSensorLife);
         GEN_MEMCPY(pstMap->G_stLastMonth, pstMap->G_stThisMonth, sizeof(pstMap->G_stLastMonth));
         GEN_MEMSET(pstMap->G_stThisMonth, 0x00, sizeof(pstMap->G_stThisMonth));
         //
         // If we have a previous month, save it to file yymm.bin
         //
         if( RPI_SaveMonth(pstSensMon->ulSecsSample, (u_int8 *)pstMap->G_stLastMonth, sizeof(pstMap->G_stLastMonth)) )
         {
            if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 5)) LOG_printf("rpi-SensSampleMon():Last month=%d saved." CRLF, RTC_GetMonth(pstSensMon->ulSecsSample));
         }
         else
         {
            if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 5)) LOG_printf("rpi-SensSampleMon():Last month=%d NOT YET saved." CRLF, RTC_GetMonth(pstSensMon->ulSecsSample));
         }
      }
      else rpi_SensUpdateGlobals(KINS_MONTH, pstSensMon, &pstMap->G_stSensorLife);
      //
      // Restart and Copy static values
      //
      GEN_MEMSET(pstSensMon, 0x00, sizeof(SENS));
      GEN_STRNCPY(pstSensMon->cEquipment, pstSensNow->cEquipment, MAX_SENS);
      pstSensMon->iPeriod      = iThisMonth;
      pstSensMon->ulSecsSample = ulSecsNow;
   }
}

//
// Function:   rpi_SensSampleQtr
// Purpose:    Check is sampling quarter hour is over
//
// Parms:      Secs now, SENS quarter, SENS now (delta's)
// Returns:    
// Note:       
//
static void rpi_SensSampleQtr(u_int32 ulSecsNow, SENS *pstSensQtr, SENS *pstSensNow)
{
   int      iCurQtr, iPrevQtr;

   iCurQtr = (int)RTC_GetCurrentQuarter(ulSecsNow);
   //
   // Check if sampling quarter is over
   //
   if(pstSensQtr->iPeriod == iCurQtr)
   {
      //
      // It's still the same quarter: SUM all delta's per quarter hour
      // Save the timestamp
      //
      rpi_SensorSum(pstSensQtr, pstSensNow);
      pstSensQtr->ulSecsSample = ulSecsNow;
   }
   else
   {
      if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 9)) LOG_printf("rpi-SensSampleQtr():This quarter=%d" CRLF, iCurQtr);
      //
      // Sample time (if it's not the first one)
      //
      if(pstSensQtr->iPeriod != -1)
      {
         iPrevQtr = pstSensQtr->iPeriod;
         //
         // It's time to move to the next quarter hour
         // Calculate results each type
         // Reset samples for the new quarter
         //
         rpi_SensorAverage(&pstMap->G_stThisDay[iPrevQtr], pstSensQtr);
         rpi_SensUpdateGlobals(KINS_QUARTER, pstSensQtr, &pstMap->G_stSensorLife);
         RPI_DAY("New Quarter");
      }
      else
      {
         rpi_SensUpdateGlobals(KINS_QUARTER, pstSensQtr, &pstMap->G_stSensorLife);
         RPI_DAY("SKIP Quarter");
      }
      //
      // Restart and Copy static values
      //
      GEN_MEMSET(pstSensQtr, 0x00, sizeof(SENS));
      GEN_STRNCPY(pstSensQtr->cEquipment, pstSensNow->cEquipment, MAX_SENS);
      pstSensQtr->ulSecsSample = ulSecsNow;
      pstSensQtr->iPeriod      = iCurQtr;
   }
}

//
// Function:   rpi_SensSampleTot
// Purpose:    Sample totals
//
// Parms:      Secs now, SENS totals, SENS now (delta's)
// Returns:    
// Note:       
//
static void rpi_SensSampleTot(u_int32 ulSecsNow, SENS *pstSensTot, SENS *pstSensNow)
{
   int      iMin;
   SMDATA   stSensorTemp;

   iMin = RTC_GetMinute(ulSecsNow);
   //
   if(pstSensTot->iPeriod == -1)
   {
      //
      // One-time init:
      // Reset samples for the new minute
      // Copy static values
      //
      GEN_MEMSET(pstSensTot, 0x00, sizeof(SENS));
      GEN_STRNCPY(pstSensTot->cEquipment, pstSensNow->cEquipment, MAX_SENS);
      pstSensTot->iPeriod = iMin;
   }
   if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 9)) LOG_printf("rpi-SensSampleTot():This minute=%d" CRLF, iMin);
   //
   // Totals update time always
   //
   rpi_SensorSum(pstSensTot, pstSensNow);
   if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 5)) LOG_printf("rpi-SensSampleTot():=Pump-On/Off=%d/%d, Samples=%d" CRLF, pstSensTot->iPumpOn, pstSensTot->iPumpOff, pstSensTot->iSamples);
   //
   if(pstSensTot->iPeriod != iMin)
   {
      //
      // New minute: update globals with running average
      //
      if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 5)) LOG_printf("rpi-SensSampleTot():Update" CRLF);
      pstSensTot->ulSecsSample = ulSecsNow;
      //
      stSensorTemp.flPumpOn    = (((double) (1000 * pstSensTot->iPumpOn))    / pstSensTot->iSamples) / 10.0;
      stSensorTemp.flX10Bypass = (((double) (1000 * pstSensTot->iX10Bypass)) / pstSensTot->iSamples) / 10.0;
      //
      rpi_SensUpdateGlobals(KINS_TOTAL, pstSensTot, &stSensorTemp);
      pstSensTot->iPeriod = iMin;
   }
}

//
// Function:   rpi_SensUpdateGlobals
// Purpose:    Update actual globals
//
// Parms:      Period, SENS ptr, SMDATA data ptr
// Returns:
// Note:
//
static void rpi_SensUpdateGlobals(KINSP tPer, SENS *pstSensor, SMDATA *pstData)
{
   int   iPumpOn=0;

   switch(tPer)
   {
      default:
         break;

      case KINS_NOW:
         RTC_ConvertDateTime(TIME_FORMAT_HH_MM_SS, pstMap->G_pcTimeInSecs, pstSensor->iTimeInSecs);
         //
         GEN_SNPRINTF(pstMap->G_pcWaterLevel,      MAX_PARM_LEN, "%5.1f",             pstSensor->flWaterLevel);
         GEN_SNPRINTF(pstMap->G_pcPumpMode,        MAX_PARM_LEN, "0x%02x",            pstSensor->iPumpMode);
         //
         // SmartSensor v2.06 and lower: iPumpMode does not give the correct status
         //                              Fix it here
         //
         switch(pstSensor->iPumpMode)
         {
            default:
            case KINS_MODE_TIMER:
            case KINS_MODE_POWEROFF:
            case KINS_MODE_AUTO:       iPumpOn = pstSensor->iPumpOn; break;
            //
            case KINS_MODE_PUMP_ON:    iPumpOn = 1;                  break;
            case KINS_MODE_PUMP_OFF:   iPumpOn = 0;                  break;
         }
         if(iPumpOn) GEN_SNPRINTF(pstMap->G_pcPumpState, MAX_PARM_LEN, "Aan");
         else        GEN_SNPRINTF(pstMap->G_pcPumpState, MAX_PARM_LEN, "Uit");
         //
         if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 1)) LOG_printf("rpi-SensUpdateGlobals(now):[%s]: WaterLevel=%s mm Pump Mode=%s State=%s" CRLF,
                        pstMap->G_pcTimeInSecs,
                        pstMap->G_pcWaterLevel,
                        pstMap->G_pcPumpMode,
                        pstMap->G_pcPumpState);
         break;

      case KINS_MINUTE:
         GEN_SNPRINTF(pstMap->G_pcTemperature,     MAX_PARM_LEN, "%5.1f",           pstData->flTemperature);
         GEN_SNPRINTF(pstMap->G_pcSysFlags,        MAX_PARM_LEN, "%d",              pstSensor->iSysFlags);
         GEN_SNPRINTF(pstMap->G_pcDropped,         MAX_PARM_LEN, "%d",              pstSensor->iDropped);
         if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 1)) LOG_printf("rpi-SensUpdateGlobals(min):[%s]: Water Level=%s mm, Dropped=%s, Temp=%s C, Sys=%s (CrcErrors=%d)" CRLF,
                        pstMap->G_pcTimeInSecs,
                        pstMap->G_pcWaterLevel,
                        pstMap->G_pcDropped,
                        pstMap->G_pcTemperature,
                        pstMap->G_pcSysFlags,
                        pstSensor->iCrcErrors);
         break;

      case KINS_QUARTER:
         break;

      case KINS_DAY:
         GEN_SNPRINTF(pstMap->G_pcEquipment,       MAX_PARM_LEN, "%s",              pstSensor->cEquipment);
         GEN_SNPRINTF(pstMap->G_pcSmartSamples,    MAX_PARM_LEN, "%d",              pstSensor->iSamples);
         break;

      case KINS_MONTH:
         break;

      case KINS_YEAR:
         break;

      case KINS_TOTAL:
         GEN_SNPRINTF(pstMap->G_pcPumpOn,          MAX_PARM_LEN, "%5.1f",           pstData->flPumpOn);
         GEN_SNPRINTF(pstMap->G_pcX10Bypass,       MAX_PARM_LEN, "%5.1f",           pstData->flX10Bypass);
         if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 1)) LOG_printf("rpi-SensUpdateGlobals(total):Pump-On=%s%%" CRLF, pstMap->G_pcPumpOn);
         break;

      case KINS_CSV:
         GEN_SNPRINTF(pstMap->G_pcWaterLevel,      MAX_PARM_LEN, "%5.1f",           pstData->flWaterLevel);
         if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 5)) LOG_printf("rpi-SensUpdateGlobals(csv):Water Level=%s mm" CRLF, pstMap->G_pcWaterLevel);
         break;
   }
}

//
//  Function:  rpi_Startup
//  Purpose:   Startup all threads
//
//  Parms:     
//  Returns:   Startup markers
//
static int rpi_Startup(void)
{
   int   iStartUp=GLOBAL_HST_ALL_INI;

   //
   // Start threads:
   //
   iStartUp |= SRVR_Init();
   iStartUp |= GRD_Init();
   iStartUp |= COMM_Init();
   iStartUp |= DBG_Init();
   return(iStartUp);
}

//
// Function:   rpi_ValidateSample
// Purpose:    Validate latest distance to max/min rise/drop rates
//
// Parms:      SecsNow, SENS ptr
// Returns:    TRUE if rise and fall are OKee 
// Note:       Spurious distance values ar off due to noise on the ultrasoon sensor.
//             Just drop sample if rise/drop rates exceed limits.
//
static bool rpi_ValidateSample(u_int32 ulSecsNow, SENS *pstSensor)
{
   static u_int32 ulSecsSta=0;
   static u_int32 ulSecsSto=0;
   static int     iNumTrans=0;
   static double  flDistNow=0.0;
   static double  flDistLast=0.0;

   bool           fDrop=FALSE;
   bool           fRise=FALSE;
   int            iSecs;
   double         flTransient;

   if(ulSecsSto)
   {
      if(ulSecsNow > ulSecsSto)
      {
         //
         // Calculate Transient period is over: check rise or drop
         //
         iSecs       = (int)(ulSecsSto - ulSecsSta);
         flDistNow  /= iNumTrans;
         flTransient = flDistNow  - flDistLast;
         //
         // Pump is ON :   We should have a FALLING  transient
         //         OFF:   We should have a DROPPING transient
         //
         if( (pstSensor->iPumpOn)  && (flTransient < 0.1) ) fDrop = TRUE;
         if( (pstSensor->iPumpOff) && (flTransient > 0.1) ) fRise = TRUE;
         //
         // Calculate drop in mm/min
         //
         flTransient  = fabs(flTransient);
         flTransient /= iSecs;
         flTransient *= 60;
         //
         if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 2)) LOG_printf("rpi-ValidateSample():WL=%.1f (%d Secs)" CRLF, flDistNow, iSecs);
         //
         if( fDrop && (flTransient > pstMap->G_flMaxDrop) )
         {
            //
            // Waaaaay to steep a drop
            //
            LOG_Report(0, "HST", "rpi-ValidateSample(); WARNING:DROP too steep: %.1f mm -> %.1f mm: %.2f mm/min (%d Secs)", 
                     flDistLast, flDistNow, flTransient, iSecs);
            if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 5)) LOG_printf("rpi-ValidateSample():DROP too steep: %.1f mm -> %.1f mm: %.2f mm/min (%d Secs)" CRLF, 
                     flDistLast, flDistNow, flTransient, iSecs);
         }
         if( fRise && (flTransient > pstMap->G_flMaxRise) )
         {
            //
            // Waaaaay to steep a rise
            //
            LOG_Report(0, "HST", "rpi-ValidateSample(); WARNING:RISE too steep: %.1f mm -> %.1f mm: %.2f mm/min (%d Secs)", 
                     flDistLast, flDistNow, flTransient, iSecs);
            //
            if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 5)) LOG_printf("rpi-ValidateSample():RISE too steep: %.1f mm -> %.1f mm: %.2f mm/min (%d Secs)" CRLF, 
                     flDistLast, flDistNow, flTransient, iSecs);
         }
         //
         // Save new thresholds
         //
         ulSecsSta  = ulSecsNow;
         ulSecsSto  = ulSecsNow + NUM_TRANSIENT_SECS;
         iNumTrans  = 0;
         flDistLast = flDistNow;
      }
      else
      {
         iNumTrans++;
         flDistNow += pstSensor->flWaterLevel;
         if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 2)) LOG_printf("rpi-ValidateSample():Sample %d" CRLF, iNumTrans);
      }
   }
   else
   {
      //
      // First time init
      //
      if(pstMap->G_flMaxDrop == 0.0)   pstMap->G_flMaxDrop = MAX_DROP_PER_MIN;
      if(pstMap->G_flMaxRise == 0.0)   pstMap->G_flMaxRise = MAX_RISE_PER_MIN;
      if(pstMap->G_iMaxInval == 0)     pstMap->G_iMaxInval = MAX_INVALID_SAMPLES;
      //
      ulSecsSta  = ulSecsNow;
      ulSecsSto  = ulSecsNow + NUM_TRANSIENT_SECS;
      iNumTrans   = 0;
      flDistNow   = 0.0;
      flDistLast  = 0.0;
   }
   return(TRUE);
}

//
//  Function:  rpi_WaitStartup
//  Purpose:   Wait until all threads are doing fine
//
//  Parms:     
//  Returns:   True if all OK
//
static bool rpi_WaitStartup()
{
   bool  fWaiting=TRUE;
   bool  fCc=FALSE;
   int   iInit=pstMap->G_iGuards;
   int   iStartup=0, iHalfSecs = 20;

   //
   // Mask only INIT threads
   //
   iInit &= GLOBAL_XXX_ALL_INI;
   //
   while(fWaiting)
   {
      switch( GLOBAL_SemaphoreWait(PID_HOST, 500) )
      {
         case 0:
            // Thread signalled: verify completion
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_HST_ALL_INI)) { LOG_Report(0, "HST", "rpi-WaitStartup():HOST  OKee"); iStartup |= GLOBAL_HST_ALL_INI;}
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_SVR_ALL_INI)) { LOG_Report(0, "HST", "rpi-WaitStartup():SRVR  OKee"); iStartup |= GLOBAL_SVR_ALL_INI;}
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_GRD_ALL_INI)) { LOG_Report(0, "HST", "rpi-WaitStartup():GUARD OKee"); iStartup |= GLOBAL_GRD_ALL_INI;}
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_COM_ALL_INI)) { LOG_Report(0, "HST", "rpi-WaitStartup():COMM  OKee"); iStartup |= GLOBAL_COM_ALL_INI;}
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_DBG_ALL_INI)) { LOG_Report(0, "HST", "rpi-WaitStartup():DEBUG OKee"); iStartup |= GLOBAL_DBG_ALL_INI;}
            //
            if(iStartup == iInit)
            {
               LOG_Report(0, "HST", "rpi-WaitStartup():All inits OKee");
               fWaiting = FALSE;
               fCc      = TRUE;
            }
            //pwjh else
            //pwjh {
            //pwjh    PRINTF("rpi-WaitStartup(): Waiting for 0x%08X (now 0x%08X)" CRLF, iInit, iStartup);
            //pwjh }
            break;

         case -1:
            // Error
            fWaiting = FALSE;
            break;

         case 1:
            if(iHalfSecs-- == 0)
            {
               // Timeout: problems
               LOG_Report(0, "HST", "rpi-WaitStartup(): Timeout: Init=0x%08X, Actual==0x%08X", iInit, iStartup);
               PRINTF("rpi-WaitStartup(): Timeout: Init=0x%08X, Actual==0x%08X" CRLF, iInit, iStartup);
               fWaiting = FALSE;
            }
            break;
      }
      if(iStartup == iInit)
      {
         PRINTF("rpi-WaitStartup():All inits OKee" CRLF);
         LOG_Report(0, "HST", "rpi-WaitStartup():All inits OKee");
         fWaiting = FALSE;
         fCc      = TRUE;
      }
      //pwjh else
      //pwjh {
      //pwjh    PRINTF("rpi-WaitStartup(): Waiting for 0x%08X (now 0x%08X)" CRLF, iInit, iStartup);
      //pwjh }
   }
   return(fCc);
}

/*----------------------------------------------------------------------
_____________CMD_LINE_FUNCTIONS(){}
------------------------------x----------------------------------------*/

//
//  Function:   rpi_Help
//  Purpose:    CLI Help
//
//  Parms:      
//  Returns:    
//
static void rpi_Help()
{
   printf("main():%s v%s-%d.%d (%s)" CRLF, RPI_APP_TEXT, pcSwVersion, pstMap->G_iVersionMajor, pstMap->G_iVersionMinor, RPI_BUILT);
   printf(pcHelp);
   //
   printf("Sizeof Hostname   = %d\n" , sizeof(pstMap->G_pcHostname));   // 32
   printf("Sizeof PID List   = %d\n",  sizeof(pstMap->G_stPidList));    // 100
   printf("Sizeof stDynPages = %d\n" , sizeof(pstMap->G_stDynPages));   // 3584
   printf("Sizeof stLog      = %d\n" , sizeof(pstMap->G_stLog));        // 96
   printf("Sizeof Semaphore  = %d\n",  sizeof(pstMap->G_tMutex));       // 24
   printf("Sizeof sem_t      = %d\n",  sizeof(sem_t));                  // 16
   printf("Sizeof pid_t      = %d\n" , sizeof(pid_t));                  // 4
   printf("Sizeof size_t     = %d\n",  sizeof(size_t));                 // 4
   printf("Sizeof double     = %d\n" , sizeof(double));                 // 8
   printf("Sizeof long       = %d\n" , sizeof(long));                   // 8
   printf("Sizeof int        = %d\n" , sizeof(int));                    // 4
   printf("\n");
}

//
// Function:   rpi_RestoreActualData
// Purpose:    Restore map data from month/day files
//
// Parms:      
// Returns:    TRUE if OKee    
// Note:       CLI: -R : Restore MAP G_stThis* data from file and exit().
//
//             If map file data is lost, the month and day data could be restored from file.
//             Month : SmartS_201712.bin     
//             Day   : SmartS_20171214.bin   
//              
static bool rpi_RestoreActualData(void)
{
   bool     fCc;
   u_int32  ulSecs;

   //
   // Get secs for the month,day
   //
   ulSecs = RTC_GetSystemSecs();
   //
   if(!(fCc = rpi_RestoreMapData(ulSecs)) )
   {
      LOG_printf("rpi-RestoreActualData():Restoring NOT successfull" CRLF);
   }
   //
   // Copy actual data (unknown dates have cleared data)
   //
   GEN_MEMCPY(pstMap->G_stThisMonth, pstMap->G_stDispMonth, sizeof(pstMap->G_stThisMonth));
   GEN_MEMCPY(pstMap->G_stThisDay,   pstMap->G_stDispDay,   sizeof(pstMap->G_stThisDay));
   return(fCc);
}

//
// Function:   rpi_RestoreMapData
// Purpose:    Restore map data from month/day files to DISPLAY buffers
//
// Parms:      ulSecs of the day/month
// Returns:    TRUE if OKee and the data is in the Month/Day display data
// Note:       Month : SmartS_201712.bin     size=[NUM_EMS][MAX_DAYS];
//             Day   : SmartS_20171214.bin   size=[NUM_EMS][MAX_QRTS];
//              
static bool rpi_RestoreMapData(u_int32 ulSecs)
{
   bool     fCc=TRUE;
   int      iMonth, iDay;

   iMonth = RTC_GetMonth(ulSecs);
   iDay   = RTC_GetDay(ulSecs);
   //
   // Restore this month
   //
   if(RPI_RestoreMonth(ulSecs, (u_int8 *)pstMap->G_stDispMonth, sizeof(pstMap->G_stDispMonth)))
   {
      LOG_printf("rpi-RestoreMapData():Restoring Month(%d) OKee" CRLF, iMonth);
   }
   else
   {
      // Not found: clear data to be sure
      LOG_printf("rpi-RestoreMapData():Restoring Month(%d) NOT successfull" CRLF, iMonth);
      GEN_MEMSET((u_int8 *)pstMap->G_stDispMonth, 0x00, sizeof(pstMap->G_stDispMonth));
      fCc = FALSE;
   }
   //
   // Restore today
   //
   if(RPI_RestoreDay(ulSecs, (u_int8 *)pstMap->G_stDispDay, sizeof(pstMap->G_stDispDay)))
   {
      LOG_printf("rpi-RestoreMapData():Restoring Day(%d) OKee" CRLF, iDay);
   }
   else
   {
      // Not found: clear data to be sure
      LOG_printf("rpi-RestoreMapData():Restoring Day(%d) NOT successfull: Clear data" CRLF, iDay);
      GEN_MEMSET((u_int8 *)pstMap->G_stDispDay, 0x00, sizeof(pstMap->G_stDispDay));
      fCc = FALSE;
   }
   return(fCc);
}

//
// Function:   rpi_RestoreMonthData
// Purpose:    Restore a month data from day files
//
// Parms:      Year (2016...), month (1..12)
// Returns:    
// Note:       CLI: -Y 2017 -B 12
//
//             If month file data is lost due to normal exit before end of the month, 
//             the month file can be restored using the day-file data.
//             Month : SmartS_201711.bin     size=[NUM_EMS][MAX_DAYS];
//             Day   : SmartS_20171103.bin   size=[NUM_EMS][MAX_QRTS];
//              
static void rpi_RestoreMonthData(int iYear, int iMonth)
{
   bool     fDoRestore=TRUE;
   int      iDay; 
   u_int32  ulSecs, ulSecsDay;

   if(iYear  < 2016)                   fDoRestore = FALSE;
   if( (iMonth < 1) || (iMonth > 12) ) fDoRestore = FALSE;
   //
   if(fDoRestore)
   {
      //
      // Get secs for first day of the month in question
      //
      ulSecs = RTC_GetSecs(iYear, iMonth, 1);
      //
      LOG_printf("rpi-RestoreMonthData():Year=%d, Month=%d Day=%d (secs=%d)" CRLF, 
                  RTC_GetYear(ulSecs), RTC_GetMonth(ulSecs), RTC_GetDay(ulSecs), ulSecs);
      //
      // Read month
      //
      if(!RPI_RestoreMonth(ulSecs, (u_int8 *)pstMap->G_stDispMonth, sizeof(pstMap->G_stDispMonth)))
      {
         GEN_MEMSET((u_int8 *)pstMap->G_stDispMonth, 0x00, sizeof(pstMap->G_stDispMonth));
      }
      //
      // Month is open(restored partly from file or cleared) : augment day-file data
      //
      ulSecsDay = ulSecs;
      //
      for(iDay=0; iDay<MAX_DAYS; iDay++)
      {
         //
         // Check if we are still withing the same month
         //
         if(iMonth == RTC_GetMonth(ulSecsDay))
         {
            if(RPI_RestoreDay(ulSecsDay, (u_int8 *)pstMap->G_stDispDay, sizeof(pstMap->G_stDispDay)))
            {
               //
               // Day is on file: Average each type and store in in month
               //
            }
         }
         //
         // Next day
         //
         ulSecsDay += (24*60*60);
      }
      if( RPI_SaveMonth(ulSecs, (u_int8 *)pstMap->G_stDispMonth, sizeof(pstMap->G_stDispMonth)) )
      {
         LOG_printf("rpi-RestoreMonthData():Restoring Year(%d)-Month(%d) OKee" CRLF, RTC_GetYear(ulSecs), RTC_GetMonth(ulSecs) );
      }
      else
      {
         LOG_printf("rpi-RestoreMonthData():ERROR restoring Year(%d)-Month(%d)" CRLF, RTC_GetYear(ulSecs), RTC_GetMonth(ulSecs) );
      }
      rpi_RestoreActualData();
   }
   else
   {
      LOG_printf("rpi-RestoreMonthData():Year(%d)-Month(%d) need to be 2016-1..12 !" CRLF, iYear, iMonth);
   }
}

//
// Function:   rpi_ShowDay
// Purpose:    Show whole Day
//
// Parms:      
// Returns:    
// Note:       
//              
static void rpi_ShowDay()
{
   int      iQtr;

   //
   // Show all data
   // Day:
   //
   for(iQtr=0; iQtr<MAX_QRTS; iQtr++)
   {
      LOG_printf("Qtr[%02d]:WaterLevel=%5.1f mm  Pump On=%5.1f" CRLF, 
                  iQtr+1,
                  pstMap->G_stDispDay[iQtr].flWaterLevel,
                  pstMap->G_stDispDay[iQtr].flPumpOn);
   }
}

//
// Function:   rpi_ShowMonth
// Purpose:    Show whole month
//
// Parms:      Year (2016...), month (1..12), Day (1..31)
// Returns:    
// Note:       
//              
static void rpi_ShowMonth()
{
   int      iDay;
   //
   // Show all data
   // Month:
   for(iDay=0; iDay<MAX_DAYS; iDay++)
   {
      LOG_printf("Day[%02d]:WaterLevel=%5.1f mm  Pump On=%5.1f" CRLF, 
                  iDay+1,
                  pstMap->G_stDispMonth[iDay].flWaterLevel,
                  pstMap->G_stDispMonth[iDay].flPumpOn);
   }
}

//
// Function:   rpi_ShowSensorData
// Purpose:    Show the SENSOR data from file/mapfile
//
// Parms:      Year (2016...), month (1..12), Day (1..31)
// Returns:    
// Note:       Show MAP data if today,else from file
//             Month : SmartS_201711.bin     size=[NUM_EMS][MAX_DAYS];
//             Day   : SmartS_20171103.bin   size=[NUM_EMS][MAX_QRTS];
//              
static void rpi_ShowSensorData(int iDispYear, int iDispMonth, int iDispDay)
{
   int      iYear, iMonth, iDay;
   u_int32  ulSecs;

   //
   // Get secs for the month,day
   //
   ulSecs = RTC_GetSystemSecs();
   //
   iYear  = RTC_GetYear(ulSecs);
   iMonth = RTC_GetMonth(ulSecs);
   iDay   = RTC_GetDay(ulSecs);
   //
   if( (iYear == iDispYear) && (iMonth == iDispMonth) && (iDay == iDispDay) )
   {
      // This day: copy MAP data 
      GEN_MEMCPY(pstMap->G_stDispMonth, pstMap->G_stThisMonth, sizeof(pstMap->G_stDispMonth));
      GEN_MEMCPY(pstMap->G_stDispDay,   pstMap->G_stThisDay,   sizeof(pstMap->G_stDispDay));
   }
   else
   {
      ulSecs = RTC_GetSecs(iDispYear, iDispMonth, iDispDay);
      if(!rpi_RestoreMapData(ulSecs) )
      {
         LOG_printf("rpi-ShowSensorData():Restoring %04d-%02d-%02d NOT successfull" CRLF, iDispYear, iDispMonth, iDispDay);
         return;
      }
   }
   LOG_printf("%s" CRLF, pcSeparator);
   LOG_printf("Month=%d" CRLF, iDispMonth);
   LOG_printf("%s" CRLF, pcSeparator);
   rpi_ShowMonth();
   //
   // Display current Month and Day data
   //
   LOG_printf("%s" CRLF, pcSeparator);
   LOG_printf("Day=%d" CRLF, iDispDay);
   LOG_printf("%s" CRLF, pcSeparator);
   rpi_ShowDay();
}


/*----------------------------------------------------------------------
________________DEBUG_FUNCTIONS(){}
------------------------------x----------------------------------------*/

#ifdef   DEBUG_ASSERT
//
// Function:   rpi_DisplaySmartFloats
// Purpose:    Display smart sensor data
//
// Parms:      Title
// Returns:    
// Note:       
//
static void rpi_DisplaySmartFloats(const char *pcTitle)
{
   bool     fNew;
   int      iIdx;
   double   flLast, flThis;

   fNew = TRUE;
   for(iIdx=0; iIdx<MAX_QRTS; iIdx++)
   {
      flLast = pstMap->G_stLastDay[iIdx].flWaterLevel;
      flThis = pstMap->G_stThisDay[iIdx].flWaterLevel;
      //
      if( (flLast > (double) 0.0) || (flThis > (double) 0.0) )
      {
         #ifdef RPI_DISPLAY_FILE
            if(fNew) LOG_Report(0, "HST", "rpi-DisplaySmartFloats(): Dist (%s)", pcTitle);
            LOG_Report(0, "HST", "rpi-DisplaySmartFloats():%2d:Last=%5.1f This=%5.1f", iIdx, flLast, flThis);
         #else
            if(fNew) LOG_printf("rpi-DisplaySmartFloats(): Dist (%s)" CRLF, pcTitle);
            LOG_printf("rpi-DisplaySmartFloats():%2d:Last=%5.1f This=%5.1f" CRLF, iIdx, flLast, flThis);
         #endif
         fNew = FALSE;
      }
   }
   fNew = TRUE;
   for(iIdx=0; iIdx<MAX_QRTS; iIdx++)
   {
      flLast = pstMap->G_stLastDay[iIdx].flPumpOn;
      flThis = pstMap->G_stThisDay[iIdx].flPumpOn;
      //
      if( (flLast > (double) 0.0) || (flThis > (double) 0.0) )
      {
         #ifdef RPI_DISPLAY_FILE
            if(fNew) LOG_Report(0, "HST", "rpi-DisplaySmartFloats(): Pump (%s)", pcTitle);
            LOG_Report(0, "HST", "rpi-DisplaySmartFloats():%2d:Last=%5.1f This=%5.1f", iIdx, flLast, flThis);
         #else
            if(fNew) LOG_printf("rpi-DisplaySmartFloats(): Pump (%s)" CRLF, pcTitle);
            LOG_printf("rpi-DisplaySmartFloats():%2d:Last=%5.1f This=%5.1f" CRLF, iIdx, flLast, flThis);
         #endif
         fNew = FALSE;
      }
   }
}

//
// Function:   rpi_DisplaySmartSensor
// Purpose:    Display smart sensor data
//
// Parms:      Hdr, struct ptr
// Returns:    
// Note:       
//
static void rpi_DisplaySmartSensor(const char *pcHdr, SENS *pstData)
{
   LOG_printf("%3s:Period=%2d Samples=%4d CrcErrors=%4d WaterLevel=%5.1f Pump=%4d" CRLF, 
                  pcHdr, 
                  pstData->iPeriod,
                  pstData->iSamples,
                  pstData->iCrcErrors,
                  pstData->flWaterLevel,
                  pstData->iPumpOn);
}
#endif   //DEBUG_ASSERT

/*----------------------------------------------------------------------
_______________SYSTEM_FUNCTIONS(){}
------------------------------x----------------------------------------*/

//
//  Function:   rpi_SignalRegister
//  Purpose:    Register all SIGxxx
//
//  Parms:      Blockset
//  Returns:    TRUE if delivered
//
static bool rpi_SignalRegister(sigset_t *ptBlockset)
{
  bool fCC = TRUE;

  // SIGUSR1:
  if( signal(SIGUSR1, &rpi_ReceiveSignalUser1) == SIG_ERR)
  {
     LOG_Report(errno, "HST", "rpi-SignalRegister(): SIGUSR1 ERROR");
     fCC = FALSE;
  }
  // SIGUSR2:
  if( signal(SIGUSR2, &rpi_ReceiveSignalUser2) == SIG_ERR)
  {
     LOG_Report(errno, "HST", "rpi-SignalRegister(): SIGUSR2 ERROR");
     fCC = FALSE;
  }
  // CTL-X handler
  if( signal(SIGTERM, &rpi_ReceiveSignalTerm) == SIG_ERR)
  {
     LOG_Report(errno, "HST", "rpi-SignalRegister(): SIGTERM ERROR");
     fCC = FALSE;
  }
  // Ctl-C handler
  if( signal(SIGINT, &rpi_ReceiveSignalInt) == SIG_ERR)
  {
     LOG_Report(errno, "HST", "rpi-SignalRegister(): SIGINT ERROR");
     fCC = FALSE;
  }
  // Segmentation fault
  if( signal(SIGSEGV, &rpi_ReceiveSignalSegmnt) == SIG_ERR)
  {
     LOG_Report(errno, "HST", "rpi-ReceiveSignalSegmnt(): SIGSEGV ERROR");
     fCC = FALSE;
  }

  if(fCC)
  {
     //
     // Setup the SIGxxx events
     //
     sigemptyset(ptBlockset);
     sigaddset(ptBlockset, SIGTERM);
     sigaddset(ptBlockset, SIGINT);
     sigaddset(ptBlockset, SIGUSR1);
     sigaddset(ptBlockset, SIGUSR2);
     sigaddset(ptBlockset, SIGSEGV);
  }
  return(fCC);
}

//
//  Function:   rpi_ReceiveSignalTerm
//  Purpose:    Add the SIGTERM command in the buffer (Ctl-X)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void rpi_ReceiveSignalTerm(int iSignal)
{
   if(fHostRunning)
   {
      LOG_Report(0, "HST", "rpi-ReceiveSignalTerm()");
      //
      GLOBAL_SetSignalNotification(PID_HOST, GLOBAL_ALL_HST_END);
   }
}

//
//  Function:   rpi_ReceiveSignalInt
//  Purpose:    Add the SIGINT command in the buffer (Ctl-C)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void rpi_ReceiveSignalInt(int iSignal)
{
   if(fHostRunning)
   {
      LOG_Report(0, "HST", "rpi-ReceiveSignalInt()");
      //
      GLOBAL_SetSignalNotification(PID_HOST, GLOBAL_ALL_HST_END);
   }
}

//
//  Function:   rpi_ReceiveSignalSegmnt
//  Purpose:    Add the SIGSEGV command in the buffer (Ctl-C)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void rpi_ReceiveSignalSegmnt(int iSignal)
{
   if(fHostRunning)
   {
      LOG_SegmentationFault(__FILE__, __LINE__);
      //
      // (TRY TO) Cleanup background threads:
      //
      GLOBAL_SetSignalNotification(PID_HOST, GLOBAL_ALL_HST_FLT);
      LOG_Report(errno, "HST", "rpi-ReceiveSignalSegmnt():Pid=%d", getpid());
      LOG_printf("rpi-ReceiveSignalSegmnt():Pid=%d", getpid());
      fHostRunning = FALSE;
      GLOBAL_SemaphorePost(PID_HOST);
   }
}

//
// Function:   rpi_ReceiveSignalUser1
// Purpose:    SIGUSR1 signal
//
// Parms:
// Returns:    
// Note:       SIGUSR1 is used to signal a buffer is ready
//
static void rpi_ReceiveSignalUser1(int iSignal)
{
   //PRINTF("rpi-ReceiveSignalUser1()" CRLF);
   GLOBAL_SemaphorePost(PID_HOST);
}

//
// Function:   rpi_ReceiveSignalUser2
// Purpose:    System callback on receiving the SIGUSR2 signal
//
// Parms:
// Returns:    
// Note:       SIGUSR2 is used to handle the power-off button, send by the PID_SRVR daemon.
//
static void rpi_ReceiveSignalUser2(int iSignal)
{
   GLOBAL_SetSignalNotification(PID_HOST, GLOBAL_ALL_HST_PWR);
   LOG_Report(0, "HST", "rpi-ReceiveSignalUser2()");
   GLOBAL_SemaphorePost(PID_HOST);
}

/*----------------------------------------------------------------------
______________COMMENT_FUNCTIONS(){}
------------------------------x----------------------------------------*/



#ifdef COMMENT


static bool    rpi_CheckPump              (u_int32, bool);
// 
// Function:   rpi_CheckPump
// Purpose:    Handle the Pump-at-night mode
// 
// Parameters: Current system secs, TRUE if starttime
// Returns:    TRUE if pump should be ON
// Note:       If enabled (G_iPumpMinutes>0), turn off the pump for this number of minutes.
//
static bool rpi_CheckPump(u_int32 ulSecs, bool fStart)
{
   static u_int32 ulPumpSecsOff=0;
   bool           fCc=TRUE;

   if(fStart)
   {
      ulPumpSecsOff = 0;
      if(pstMap->G_iPumpMinutes)
      {
         LOG_Report(0, "HST", "rpi-CheckPump():Passed midnight: Turn Pump OFF for %d Minutes", pstMap->G_iPumpMinutes);
         //
         // Time to silence the Kinshi waterpump for Nighttime operations
         //
         pstMap->G_stCmd.iCommand = SMCMD_HTTP_PUMP;
         pstMap->G_iPumpMode      = KINS_MODE_PUMP_OFF;
         GLOBAL_Signal(PID_COMM, SIGUSR2);
         ulPumpSecsOff = ulSecs + (60 * pstMap->G_iPumpMinutes);
         fCc = FALSE;
      }
   }
   else
   {
      if(ulPumpSecsOff && (ulSecs > ulPumpSecsOff))
      {
         LOG_Report(0, "HST", "rpi-CheckPump():Turn Pump mode back to AUTO");
         // 
         // Time to turn the Kinshi waterpump for Nighttime operations back to AUTO
         //
         pstMap->G_stCmd.iCommand = SMCMD_HTTP_PUMP;
         pstMap->G_iPumpMode      = KINS_MODE_AUTO;
         GLOBAL_Signal(PID_COMM, SIGUSR2);
         ulPumpSecsOff = 0;
      }
   }
   return(fCc);
}

//
// Function:   rpi_ValidateSample
// Purpose:    Validate latest distance to max/min rise/drop rates
//
// Parms:      SecsNow, SENS ptr
// Returns:    TRUE if rise and fall are OKee 
// Note:       Spurious distance values ar off due to noise on the ultrasoon sensor.
//             Just drop sample if rise/drop rates exceed limits.
//
static bool rpi_ValidateSample(u_int32 ulSecsNow, SENS *pstSensor)
{
   static u_int32 ulSecsLast=0;
   static double  flDistLast=0.0;

   bool           fCc=FALSE;
   bool           fNews=FALSE;
   double         flSecs, flTransient;

   if(ulSecsLast && (ulSecsNow > ulSecsLast))
   {
      flSecs      = (double)(ulSecsNow - ulSecsLast);
      flTransient = pstSensor->flWaterLevel - flDistLast;
      //
      if(fabs(flTransient) > 0.10)
      {
         //
         // We have a transient
         //
         if(flTransient < 0)
         {
            //
            // Waterlevel is dropping: calculate drop in mm/sec
            // MaxDrop is negative,so transient should stay above MaxDrop
            //
            flTransient /= flSecs;
            if(flTransient > pstMap->G_flMaxDrop)
            {
               // Fine.
               if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 5)) LOG_printf("rpi-ValidateSample(); Drop OKee, Old=%5.1f, New=%5.1f --> Drop=%4.2f" CRLF, 
                        flDistLast, pstSensor->flWaterLevel, flTransient);
               //
               fNews = TRUE;
               fCc   = TRUE;
            }
            else
            {
               // Waaaaay to steep a drop: DO NOT ignore this one ANYMORE
               LOG_Report(0, "HST", "rpi-ValidateSample(); WARNING: Drop too steep, Old=%5.1f, New=%5.1f --> Drop=%4.2f", 
                        flDistLast, pstSensor->flWaterLevel, flTransient);
               //
               if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 5)) LOG_printf("rpi-ValidateSample(); Drop too steep, Old=%5.1f, New=%5.1f --> Drop=%4.2f" CRLF, 
                        flDistLast, pstSensor->flWaterLevel, flTransient);
               //
               fNews = TRUE;
               fCc   = TRUE;
            }
         }
         else
         {
            //
            // Waterlevel is rising: calculate rise in mm/sec
            // MaxRise is positive, so transient should stay below MaxRise
            //
            flTransient /= flSecs;
            if(flTransient < pstMap->G_flMaxRise)
            {
               // Fine.
               if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 5)) LOG_printf("rpi-ValidateSample(); Rise OKee, Old=%5.1f, New=%5.1f --> Rise=%4.2f" CRLF, 
                        flDistLast, pstSensor->flWaterLevel, flTransient);
               //
               fNews = TRUE;
               fCc   = TRUE;
            }
            else
            {
               // Waaaaay to steep a rise: DO NOT ignore this one ANYMORE
               LOG_Report(0, "HST", "rpi-ValidateSample(); WARNING: Rise too steep, Old=%5.1f, New=%5.1f --> Rise=%2.3f", 
                        flDistLast, pstSensor->flWaterLevel, flTransient);
               //
               if(GLOBAL_DebugPrint(MM_DEBUG_HOST, 5)) LOG_printf("rpi-ValidateSample(); Rise too steep, Old=%5.1f, New=%5.1f --> Rise=%4.2f" CRLF, 
                        flDistLast, pstSensor->flWaterLevel, flTransient);
               //
               fNews = TRUE;
               fCc   = TRUE;
            }
         }
      }
      else 
      {
         //
         // Transient is close to zero: OK
         //
         fCc = TRUE;
      }
   }
   else
   {
      if(ulSecsLast == 0)
      {
         //
         // First time init
         //
         if(pstMap->G_flMaxDrop == 0.0)   pstMap->G_flMaxDrop = MAX_DROP_PER_SEC;
         if(pstMap->G_flMaxRise == 0.0)   pstMap->G_flMaxRise = MAX_RISE_PER_SEC;
         if(pstMap->G_iMaxInval == 0)     pstMap->G_iMaxInval = MAX_INVALID_SAMPLES;
         fNews = TRUE;
      }
      fCc = TRUE;
   }
   if(fNews)
   {
      ulSecsLast = ulSecsNow;
      flDistLast = pstSensor->flWaterLevel;
   }
   return(fCc);
}

#endif