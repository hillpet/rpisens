/*  (c) Copyright:  2020..2024  Patrn, Confidential Data
 *
 *  Workfile:           rpi_comm.c
 *  Purpose:            Reading data from a MicroChip 16F628A controller
 *                      Ultrasoon sensor :  HC-SR04
 *                      Settings         :  9600 baud, 8 bits, no parity
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PIC 16f628A Ultrasoon sensor
 *  Note:               Raspberry Pi has several ways to access the serial port. For each type this differs:
 *                      
 *                      RPI-1: /dev/ttyAMA0 is the first UART
 *                      RPI-2: /dev/ttyAMA0 is the first UART
 *                      RPI-3: /dev/ttyAMA0 is the Bluetooth device (chosen because of the high bandwidth)
 *                             /dev/ttyS0   is the first UART
 *                      RPI-4: /dev/ttyAMA0 is the Bluetooth device (chosen because of the high bandwidth)
 *                             /dev/ttyS0   is the first UART
 *
 *                      To overcome incompatibilities, raspbian (from 20160510) has two aliases for this:
 *
 *                            /dev/serial0 is ttyS0   (UART0)
 *                            /dev/serial1 is ttyAMA0 (Bluetooth)
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    01 Nov 2020:      Ported from rpislim
 *    18 Nov 2020:      Add Commands
 *    22 Nov 2020:      Serial read on single char basis (non-canonical)
 *    02 Dec 2020:      Add RAM/EEPROM Simulation
 *    01 May 2021:      Add CLI option for startup mode
 *    12 Aug 2021:      Add Pump Mode command
 *    23 Mar 2022:      Debug RAM/EEPROM dump
 *    29 Mar 2022:      Add Variadic macro's for PRINTF()
 *    30 Mar 2022:      Add blocking comms
 *    19 Aug 2022:      Improve sim mode handling
 *    01 Sep 2022:      Turn OFF pump at midnight till G_iPumpMinutes
 *    19 Apr 2024:      Add MM_DEBUG_ switches
 *    24 Apr 2024:      Move to global/local startup 
 *    02 May 2024:      SIGUSR2 to receive uCtl commands
 *    03 May 2024:      Simplify tCommMode detection
 * 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <sched.h>
#include <errno.h>
#include <time.h>
#include <termios.h>
#include <unistd.h>
#include <ctype.h>
//
#include <common.h>
#include "config.h"
#include "locals.h"
#include "globals.h"
//
#include "rpi_func.h"
#include "rpi_comm.h"

//#define USE_PRINTF
#include <printx.h>

//
// Local prototypes
//
//
static void    comm_Controller            (int);
static void    comm_CommandIdleLoop       (int);
static bool    comm_CommandHttpMiacMode   (int);
static bool    comm_CommandHttpPumpMode   (int);
static void    comm_CommandMicroCtlLoop   (int);
static bool    comm_CommandMicroCtlMode   (int);
static void    comm_CommandMemoryLoop     (int, SMCMD);
static bool    comm_CommandRamMode        (int);
static bool    comm_CommandRomMode        (int);
static void    comm_CommandSensorLoop     (int);
static bool    comm_CommandSensorMode     (int);
//
static bool    comm_CheckRunMode          (void);
static void    comm_CheckSignals          (int);
static bool    comm_DetermineMode         (int);
static bool    comm_ForceReset            (int);
static void    comm_HandleController      (int);
static int     comm_Init                  (void);
static void    comm_LogSensorMode         (int);
static bool    comm_ParseMiacCommands     (int, char *);
static int     comm_SensorRead            (int, int);
static bool    comm_SendCommand           (int, const u_int8 *);
static char   *comm_SendDigits            (int, char *);
static bool    comm_SendWait              (int, const u_int8);
static bool    comm_SwitchAnyToMiac       (int, GENSTAT);
static bool    comm_SwitchAnyToSensor     (int);
static void    comm_UpdateStatus          (GENSTAT, GENERR);
static void    comm_UpdateSensorTimeout   (bool);
static bool    comm_WaitForCorrectMode    (int, GENSTAT);
static bool    comm_WaitSignal            (int);
//
static void    comm_SimulateSetup         (void);
static void    comm_SimulateUpdate        (u_int32);
//
static bool    comm_SignalRegister        (sigset_t *);
static void    comm_ReceiveSignalInt      (int);
static void    comm_ReceiveSignalTerm     (int);
static void    comm_ReceiveSignalUser1    (int);
static void    comm_ReceiveSignalUser2    (int);
static void    comm_ReceiveSignalSegmnt   (int);
//
//#define  FEATURE_DUMP_DATA
#ifdef   FEATURE_DUMP_DATA
#define  LOG_DUMPDATA(x,y,z)              LOG_DumpData(x,y,z)
#else    //FEATURE_DUMP_DATA
#define  LOG_DUMPDATA(x,y,z)
#endif   //FEATURE_DUMP_DATA
//
static bool    fCommRunning            = TRUE;
static int     iCommVerbose            = 0;
static GENSTAT tCommMode               = GEN_STATUS_IDLE;
static GENERR  tCommError              = GEN_ERROR_NONE;
static SMCMD   tCommCommand            = SMCMD_NONE;
//
// Default values format string
//
static int     iMiacIdx                = 0;
static int     iTotals[NUM_EMS]        = {  0,  0,  0,  0,  0 };
static int     iUpdate[NUM_EMS]        = { 10,  0, 10,  0,  0 };
static int     iCounts                 = 0;
static int     iSimMode                = SMART_SENSOR_SIM;
static int     iSmartSensor            = SMART_SENSOR_IDLE;
static u_int16 usCrc                   = 0;
static char   *pcRcvBuffer             = NULL;
//
// Sensor command strings
//
static const char *pcSensorCommands[] = 
{
#define  EXTRACT_SMC(a,b)   b,
#include "gen_cmds.h"
#undef   EXTRACT_SMC
};
//
// Smart sensor test data
//
static const char *pcEms[NUM_EMS]   =
{
   "Dist",
   "Temp",
   "Sols",
   "Pump",
   "X10b"
};
// 
static const char *pcSensorTestData = "Dist=%04x, Flags=0, Count=%04x, Pump=%d, Time=%06x, Mode=0, Sensor=v1.00-cr064!";
//
// RPi mode commands
// Assume MIAC Mode
//
//pwjh static const u_int8  pubAnyToSensorMode[] = {0x1b, 'P', 'q', 0};
//pwjh static const u_int8  pubAnyToMiacMode[]   = {0x1b, 0};
//
static const u_int8  pubModeQuery[]       = {' ', 0};
static const u_int8  pubModeReset[]       = {0x1b, '!', 0};
static const u_int8  pubLiveToMiacMode[]  = {0x1b, 0};
static const u_int8  pubLiveToSensorMode[]= {0x1b, 'P', 'q', 0};
static const u_int8  pubMiacToSensorMode[]= {'P', 'q', 0};
static const u_int8  pubToggleMiacMode[]  = {'r', 0};
//
// Single character commands (only in SENSOR mode)
// These will NOT be echo'ed by the uCtlr
// 
static const u_int8  pubPumpModeAuto[]    = {'A', 0};
static const u_int8  pubPumpModeOn[]      = {'P', 0};
static const u_int8  pubPumpModeOff[]     = {'p', 0};
static const u_int8  pubPumpModeTimer[]   = {'T', 0};
// MIAC changes
//
static const u_int8  ubEqual              = '=';
static const u_int8  ubPlus               = '+';
static const u_int8  ubMiacList           = 'l';
static const u_int8  ubMiacToggle         = 'r';
static const u_int8  ubCr                 = 0x0d;
//
//   HC-SR04 Ultrasoon Sensor                                    
//                                           Sensor                          |
//   Speed:        340   m/s                 +----+                          |
//   Factor:       58,82 uSec/cm             |    |                          |
//   Xtal:         10    MHz                 |    |------------------------->|
//   Prescaler:    1                         |    |<-------------------------|
//   Timer Clock:  0,4   uSec                |    |         X mm             |
//                                           |    |                          |
//                                           +----+                          |
//   Distance  Pulse Width   Timer Value                         1000         
//    (cm)      (uSec)       (Ticks)                 T = 2 * X * ---- uSecs   
//   -----------------------------------                         340
//     0         0            0
//     10,0      588          1471                   T = 5.8824 * X uSec
//     20,0      1176         2941
//     30,0      1765         4412
//     40,0      2353         5882
//     50,0      2941         7353
//     60,0      3529         8824
//     70,0      4118         10294
//     80,0      4706         11765
//     90,0      5294         13235
//     100,0     5882         14706
//
//    To rework the distance from sensor to water into actual level of the water in mm:
//
//               Sensor
//             |       |
//       +     +XXX=XXX+  +            S  = Offset Sensor to Bottom
//       |     |       |  |            F  = Offset Water to floater
//       |     |       |  |            Dx = Measured Raw Hex Distance by uController (in timerticks)
//       |     |       |  |Dx          W  = Actual Waterlevel
//       |     |       |  |Dm  
//       |     |       |  |                  Dx * 0.4
//       |     |=+===+=|  +   +        Dm = ---------- mm = 0.06800 * Dx mm
//       |S    | |   | |      |F              5.88235
//       |     | |   | |      |
//       | ~~~~|~~~~~~~|~~~~~~+~~~
//       |     | |   | |      |        
//       |     |=+===+=|      |    
//       |     |       |      |W       W = S - F - Dm
//       |     |       |      |        pstMap->G_pcWaterLevel   = W  (mm)
//       |     |       |      |        pstMap->G_flDm           = Dm (mm)
//       |     |       |      |        pstMap->G_flSensorBottom = S  (mm)
//       |     |       |      |        pstMap->G_Floater        = F  (mm)
//    ---+-----+-------+------+--- 
//                            
// The Sensor delivers the distance from the sensor to the top of the Floater Dx in timer ticks.
// Save this value for later reference and rework the actual water Height as W = S - F - Dm
//
//
// SIM RAM
//=============================================================================
// W(hi)= 360 mm ==> Dx = (5.8824/0.4) * (S - 360 - F ) = 3235 = 0x0CA3
// W(lo)= 162 mm ==> Dx = (5.8824/0.4) * (S - 162 - F ) = 6147 = 0x1803
//
#define  SIM_DIST_HI       3235
#define  SIM_DIST_LO       6147
// 
static char pcSimRom[] = 
{
   "B=00 \n\r"
   "00=00 11 0C A3 18 03 00 00 \n\r"
   "08=0A 10 02 0F 05 00 00 00 \n\r"
   "10=00 00 00 00 00 00 00 00 \n\r"
   "18=FF FF FF FF FF FF FF FF \n\r"
   "20=FF FF FF FF FF FF FF FF \n\r"
   "28=FF FF FF FF FF FF FF FF \n\r"
   "30=FF FF FF FF FF FF FF FF \n\r"
   "38=FF FF FF FF FF FF FF FF \n\r"
   "40=00 02 00 00 00 00 00 00 \n\r"
   "48=00 00 00 00 00 00 00 00 \n\r"
   "50=FF FF FF FF FF FF FF FF \n\r"
   "58=FF FF FF FF FF FF FF FF \n\r"
   "60=FF FF FF FF FF FF FF FF \n\r"
   "68=FF FF FF FF FF FF FF FF \n\r"
   "70=FF FF FF FF FF FF FF FF \n\r"
   "78=FF FF FF FF FF FF FF FF \n\r"
   "!5C8C\n\r"                            // Make sure to update CRC (from the log) on value updates !
   "\n\r"
   "E>"
};
//
// SIM RAM
//
static char pcSimRam[] = 
{
   "B=00 \n\r"
   "00=00 B4 2D 18 04 00 07 00 \n\r"
   "08=00 00 07 E0 82 00 41 04 \n\r"
   "10=08 0D 04 00 00 09 09 1C \n\r"
   "18=90 20 6C 00 00 00 00 07 \n\r"
   "20=02 08 02 02 04 02 20 38 \n\r"
   "28=00 01 29 32 00 00 0A FF \n\r"
   "30=F9 FB 00 41 02 80 6C 6C \n\r"
   "38=05 81 20 00 00 14 01 00 \n\r"
   "40=00 0C 07 00 34 01 00 10 \n\r"
   "48=FF 04 41 06 00 00 00 00 \n\r"
   "50=00 00 00 00 00 00 00 00 \n\r"
   "58=00 00 00 00 00 00 00 00 \n\r"
   "60=00 00 00 00 00 00 00 00 \n\r"
   "68=00 00 00 00 00 00 00 00 \n\r"
   "70=00 02 06 07 9D 49 8F 00 \n\r"
   "78=00 00 00 00 00 03 81 00 \n\r"
   "!4FB4\n\r"
   "\n\r"
   "R>"
};

/*------  Local functions separator -------------------------------------------
______________GLOBAL_FUNCTIONS(){};
-----------------------------X------------------------------------------------*/

//  
// Function:   COMM_Init
// Purpose:    Init the smart E meter thread
//             If in SIMULATION mode: act as dummy.
// Parameters: 
// Returns:    Startup code XXX_INI
// Note:       The serial port thread will send out SIGUSRx on buffer completion:
//             SIGUSR1: Buffer is complete (ping or pong)
//             SIGUSR2: Not used
//             The Host will sens commands:
//             SIGUSR1: System commands
//             SIGUSR2: uCtl Commands
//  
int COMM_Init()
{
   int      iFd;
   pid_t    tPid;

   tPid = fork();
   //
   switch(tPid)
   {
      default:
         // Parent (PID_HOST)
         GLOBAL_PidPut(PID_COMM, tPid);
         break;

      case 0:
         // child:
         GLOBAL_PidSetInit(PID_COMM, NULL);
         //
         // Open serial comms with the Smart Sensor
         // Fd=0 means dummy Smart Sensor reads.
         //
         iFd = comm_Init();
         if(iFd >= 0) 
         {
            comm_Controller(iFd);
            LOG_Report(0, "COM", "comm-Daemon():Exit normally");
            if(iFd) close(iFd);
            exit(EXIT_CC_OKEE);
         }
         LOG_Report(errno, "COM", "comm-Daemon():Exit Init ERROR:");
         exit(EXIT_CC_GEN_ERROR);
         break;

      case -1:
         // Error
         LOG_Report(errno, "COM", "comm-Daemon():Exit fork ERROR:");
         exit(EXIT_CC_GEN_ERROR);
         break;
   }
   return(GLOBAL_COM_ALL_INI);
}

// 
// Function:   COMM_CalculateCrc
// Purpose:    Compute the pseudo CRC for the data buffer
//             "Dist=2e68, Count=54, Pump=1, X10=0, Temp=13, .... !3a2d\r\n"
//              <------------- data ------------------------------>
// Parms:      Data pointer, number of bytes in the buffer
//
// Returns:    CRC
// Note:       CRC = 16 bits SUM(data)
// 
u_int16 COMM_CalculateCrc(const u_int8 *pubBuffer, int iLen)
{
   int      i;
   int      iCrc=0;

   //
   // Sum buffer
   //
   for(i=0; i<iLen; i++) iCrc += (int) *pubBuffer++;
   return((u_int16) (iCrc & 0xFFFF));
}



/*------  Local functions separator ------------------------------------
______________DAEMON_FUNCTIONS(){};
-----------------------------X------------------------------------------------*/

//  
// Function:   comm_Init
// Purpose:    Init the smart E meter comms
//             Since Debug uses char based IO, make sure to turn off stdout buffering
// 
// Parameters: 
// Returns:    Fd
// Note:       Fd==0 is SIM mode!
//  
static int comm_Init(void)
{
   int            iFd=0;
   sigset_t       tBlockset;
   struct termios stOpt;

   if(comm_SignalRegister(&tBlockset) == FALSE) 
   {
      LOG_Report(errno, "COM", "comm-Init():Exit:Register ERROR:");
      exit(EXIT_CC_GEN_ERROR);
   }
   GLOBAL_SemaphoreInit(PID_COMM);
   //
   if(comm_CheckRunMode())
   {
      //
      // Smart Sensor MicroController mode
      //
      if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-Init():Open UART" CRLF);
      //
      // OPEN THE UART
      // The flags (defined in fcntl.h):
      // Access modes (use 1 of these):
      //    O_RDONLY -  Open for reading only.
      //    O_RDWR   -  Open for reading and writing.
      //    O_WRONLY -  Open for writing only.
      //
      // O_NDELAY    
      // O_NONBLOCK  -  Enables nonblocking mode. When set read requests on the file can return immediately with a failure status
      //                if there is no input immediately available (instead of blocking). Likewise, write requests can also return
      //                immediately with a failure status if the output can't be written immediately.
      //
      // O_NOCTTY -     When set and path identifies a terminal device, open() shall not cause the terminal device to become the 
      //                controlling terminal for the process.
      //
      if( (iFd = open("/dev/serial0", O_RDWR | O_NOCTTY | O_SYNC)) == -1)
      {
         LOG_Report(errno, "COM", "comm-Init(): Error - Unable to open UART.  Ensure it is not in use by another application");
         PRINTF("comm-Init(): Error - Unable to open UART.  Ensure it is not in use by another application" CRLF);
         return(-1);
      }
      //
      // Configure the UART:
      //
      // The flags (defined in /usr/include/termios.h:
      // Baud rate:  B1200, B2400, B4800, B9600, B19200, B38400, B57600, B115200, ...
      // CSIZE:      CS5, CS6, CS7, CS8
      // CLOCAL:     Ignore modem status lines
      // CREAD:      Enable receiver
      //
      // IGNPAR:     Ignore parity errors
      // PARENB:     Parity enable
      // PARODD:     Odd parity (else even)
      //
      // ICANON:     Canonical mode
      //
      //    Canonical Mode: In canonical mode, read on the serial port will not return until a new line, 
      //    EOF or EOL character is received by the Linux Kernel. In this mode, read will always return 
      //    an entire line, no matter how many bytes are requested.
      //
      //    Non-Canonical Mode: In non-canonical mode, read on the serial port will not wait until the new line. 
      //    Number of bytes read depends on the MIN and TIME settings.
      //
      //         MIN:  Minimum number of bytes that must be available in the input queue in order for read to return
      //         TIME: How long to wait for input before returning, in units of 0.1 seconds.
      //
      //         MIN = 0, TIME = 0
      //         Read returns immediately with as many characters available in the queue, up to the number requested. 
      //         If no characters in the queue, read returns 0
      //
      //         MIN = 0, TIME > 0
      //         Read waits for TIME time for input to become available, if we receive a single character in the TIME 
      //         duration, read returns immediately. It returns as many characters up to the number requested. 
      //         If timer expires and there is no receive characters, read returns 0
      //
      //         MIN > 0, TIME = 0
      //         Read waits until at least MIN bytes are available in the input queue. At the time, read returns as 
      //         many characters are available up to the number requested.
      //
      //         MIN > 0, TIME > 0
      //         Read returns until either MIN bytes have arrived in input queue, or TIME elapses with no further input. 
      //         read always block until the first character  arrives, even if TIME elapses first.
      //
      GEN_MEMSET(&stOpt, 0x00, sizeof(struct termios));
      tcgetattr(iFd, &stOpt);
      //
      stOpt.c_cflag = B9600|CS8|CLOCAL|CREAD;
      stOpt.c_iflag = IGNPAR;
      stOpt.c_lflag = 0;                     // NON-Canonical

      #ifdef SMARTS_COMM_NON_BLOCKING
      PRINTF("comm-Init():UART Non-Blocking mode" CRLF);
      stOpt.c_cc[VMIN]  = 0;                 // Min # chars
      stOpt.c_cc[VTIME] = 0;                 // * 0.1 seconds read timeout
      #else    //SMARTS_COMM_NON_BLOCKING
      PRINTF("comm-Init():UART Blocking mode" CRLF);
      stOpt.c_cc[VMIN]  = 1;                 // Min # chars
      stOpt.c_cc[VTIME] = 5;                 // * 0.1 seconds read timeout
      #endif   //SMARTS_COMM_NON_BLOCKING

      tcflush(iFd, TCIFLUSH);
      tcsetattr(iFd, TCSANOW, &stOpt);
      //
      // Turn OFF line buffering to stdout
      //
      setbuf(stdout, NULL);
      if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-Init():UART Open OKee" CRLF);
   }
   else
   {
      //
      // SIM mode: Init variables with sensible values
      // Retrieve from the last stored values
      //
      comm_SimulateSetup();
      if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-Init(): ** SIMULATION Mode **" CRLF);
   }
   return(iFd);
}

/*------  Local functions separator ------------------------------------
____________COMMAND_READ_LOOPS(){};
-----------------------------X------------------------------------------------*/

//  
// Function:   comm_CommandIdleLoop
// Purpose:    Handle Idle mode command loop
//             
// Parameters: Comm-port Fd (0 is SIM)
// Returns:    
// Note:       Just read and drop all serial port data until the HOST
//             sends a new command.
//
//             This is the default situation after reboot:
//             Default startup option in rpisens.service
//
//             iSmartSensor is SMART_SENSOR_IDLE
//             tCommCommand is SMCMD_IDLE_MODE
//
//             To take us out of SMART_SENSOR_IDLE mode, the HOST will send a SIGUSR2
//             with the appropriate new mode in pstMap->G_stCmd.iCommand
//  
static void comm_CommandIdleLoop(int iFd)
{
   if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-CommandIdleLoop():ENTER (%s)" CRLF, pstMap->G_pcStatus);
   //
   comm_UpdateSensorTimeout(FALSE);
   tCommCommand = SMCMD_IDLE_MODE;
   while(tCommCommand == SMCMD_IDLE_MODE)
   {
      //
      // Just eat all incoming data
      // Timeout 1*100mSecs
      //
      comm_SensorRead(iFd, 1);
      //
      // Check incoming signals
      //
      comm_CheckSignals(iFd);
   }
   if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-CommandIdleLoop():EXIT (%s)" CRLF, pstMap->G_pcStatus);
}

//  
// Function:   comm_CommandMicroCtlLoop
// Purpose:    Handle non-conical reading sensor data from the serial port and
//             sending each incoming char to STDOUT for debugging purposes.
// Parameters: Comm-port Fd (0 is SIM)
// Returns:    
// Note:       
//  
static void comm_CommandMicroCtlLoop(int iFd)
{
   int      iNr;
   char     cChar;
   
   if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-CommandMicroCtlLoop():ENTER (%s)" CRLF, pstMap->G_pcStatus);
   //
   comm_UpdateSensorTimeout(FALSE);
   tCommCommand = SMCMD_UCTL_MODE;
   while(tCommCommand == SMCMD_UCTL_MODE)
   {
      //
      // Just send all incoming data to STDOUT
      // Timeout 1*100mSecs
      //
      iNr = comm_SensorRead(iFd, 1);
      if(iNr > 0)
      {
         cChar = *pcRcvBuffer;
         putchar((int)cChar);
      }
      //
      // Check incoming signals
      //
      comm_CheckSignals(iFd);
   }
   if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-CommandMicroCtlLoop():EXIT (%s)" CRLF, pstMap->G_pcStatus);
}

//  
// Function:   comm_CommandMemoryLoop
// Purpose:    Handle non-conical reading sensor Memory from the serial port
//             We come here only to download uController memory from
//                - comm_CommandRamMode()
//                - comm_CommandRomMode()
// Parameters: Comm-port Fd (0 is SIM), Command
// Returns:    
// Note:       The RAM/EEPROM memory will be stored at pstMap->G_stCmd.pcData
//             Notification to the Host with:
//                o GLOBAL_COM_HST_RAM
//                o GLOBAL_COM_HST_ROM
//             will result in parsing this data into the appropriate storage locations:
//                o pstMap->G_ubRamImage
//                o pstMap->G_ubRomImage
//  
static void comm_CommandMemoryLoop(int iFd, SMCMD tCmd)
{
   bool     fNewData=FALSE, fCrc=FALSE, fEor=FALSE;
   int      iRd, iTotal=0;
   int      iAct=0;
   char    *pcDest;
   
   if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-CommandMemoryLoop():ENTER (%s)" CRLF, pstMap->G_pcStatus);
   //
   pcDest = pstMap->G_stCmd.pcData;
   //
   GEN_MEMSET(pcDest,      0x00, KINS_SIZE_LENZ);
   GEN_MEMSET(pcRcvBuffer, 0x00, KINS_SIZE_LENZ);
   //
   // To download sensor memory, the uController has been send
   // RAM:     ESC, 'l'
   // EEPROM:  ESC, 'r', 'l'
   // and has received the reply to the command. It will now send the memory data:
   // "B=00\n\r"
   // "00=xx xx xx .....\n\r"
   // "70=xx xx xx .....\n\r"
   // "!xxxx\n\r"
   // "R>" or
   // "E>"
   //
   comm_UpdateSensorTimeout(FALSE);
   tCommCommand = tCmd;
   while(tCommCommand == tCmd)
   {
      //
      // Reading is on a single char basis into the serial port receive buffer (pcRcvBuffer).
      // Timeout 20*100mSecs
      //
      iRd = comm_SensorRead(iFd, 20);
      if(iRd > 0)
      {
         iTotal += iRd;
         switch(*pcRcvBuffer)
         {
            case '!':
               pcDest[iAct++] = *pcRcvBuffer;
               //
               // We're in MIAC mode where the Smart Sensor has just send the CRC marker.
               // Memory contents (RAM or EEPROM) has been send, wait for the EOR before
               // signalling the host.
               //
               //PRINTF("comm-CommandMemoryLoop():CRC" CRLF);
               //LOG_DUMPDATA("Memory(!):", pcDest, KINS_SIZE_LENZ);
               fCrc = TRUE;
               break;

            case '\r':
               if(fCrc) 
               {
                  //
                  // EOR for the memory dump command: drop other data until the MIAC command 
                  // prompt has been received and we can exit back to SENSOR mode.
                  // Terminate caller buffer
                  //
                  pcDest[iAct++] = *pcRcvBuffer;
                  //PRINTF("comm-CommandMemoryLoop():EOR" CRLF);
                  //LOG_DUMPDATA("Memory(LF):", pcDest, KINS_SIZE_LENZ);
                  fEor = TRUE;
               }
               else pcDest[iAct++] = *pcRcvBuffer;
               break;

            case '>':
               pcDest[iAct++] = *pcRcvBuffer;
               //PRINTF("comm-CommandMemoryLoop():>" CRLF);
               //LOG_DUMPDATA("Memory(>):", pcDest, KINS_SIZE_LENZ);
               if(fEor) fNewData = TRUE;
               else     iAct     = 0;         // Still command prompt in the buffer
               break;

            default:
               //
               // Put data in the buffer
               //
               pcDest[iAct++] = *pcRcvBuffer;
               break;
         }
         pcDest[iAct] = 0;
         //
         if(fNewData)
         {
            if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-CommandMemoryLoop():Notify Host, %d read" CRLF, iTotal);
            //
            // Buffer parsed: EOR was amongs it. Signal the host we have new data
            // Exit memory read mode and return to IDLE mode.
            //
            LOG_DUMPDATA("comm-CommandMemoryLoop(): List", pcDest, KINS_SIZE_LENZ);
            LOG_Report(0, "COM", "comm-CommandMemoryLoop():OK");
            if(tCmd == SMCMD_DLD_ROM) GLOBAL_Notify(PID_HOST, GLOBAL_COM_HST_ROM, SIGUSR1);
            else                      GLOBAL_Notify(PID_HOST, GLOBAL_COM_HST_RAM, SIGUSR1);
            //
            tCommCommand = SMCMD_IDLE_MODE;
         }
      }
      else if(iRd == 0) 
      {
         if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 2)) LOG_printf("comm-CommandMemoryLoop():(Waiting for mode data)" CRLF);
      }
      else
      {
         // Error
         if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-CommandMemoryLoop():Error reading Smart Sensor comms" CRLF);
         LOG_Report(errno, "COM", "comm-CommandMemoryLoop():Error reading Smart Sensor comms");
      }
      //
      // Check if we do not run out of buffer space !
      //
      if(iAct >= KINS_SIZE_LEN)
      {
         if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-CommandMemoryLoop():Error: Bufferoverflow" CRLF);
         LOG_Report(0, "COM", "comm-CommandMemoryLoop():Error:Buffer overflow!");
         iAct = KINS_SIZE_LEN - 1;
      }
      //
      // Check incoming signals
      //
      comm_CheckSignals(iFd);
   }  // end while
   //
   // Completed (OK or NOT)
   //
   if(RPI_GetFlag(VERBOSE_LOG_SENSOR2))
   {
      LOG_Report(0, "COM", "comm-CommandMemoryLoop(): %d bytes New data", iAct);
      LOG_ListData("comm-CommandMemoryLoop():", pcDest, KINS_SIZE_LENZ);
   }
   if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-CommandMemoryLoop():EXIT (%s)" CRLF, pstMap->G_pcStatus);
}

//  
// Function:   comm_CommandSensorLoop
// Purpose:    Handle non-conical reading sensor data from the serial port
//             Here is where we would receive the sensor data records
//             "Dist=0B25, Flags=01, Count=0440, Pump=1, Time=000EE, Mode=00, Sensor=02.11!1234\n\r"
// Parameters: Comm-port Fd (0 is SIM)
// Returns:    
// Note:       
//  
static void comm_CommandSensorLoop(int iFd)
{
   bool     fCrc=FALSE, fNewData=FALSE;
   int      iRd;
   int      iCrLf=0, iAct=0;
   int      iPingPong=0;
   char    *pcDest;
   
   if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-CommandSensorLoop():ENTER (%s)" CRLF, pstMap->G_pcStatus);
   //
   pcDest = pstMap->G_pcPing;
   GEN_MEMSET(pcDest,      0x00, KINS_SIZE_LENZ);
   GEN_MEMSET(pcRcvBuffer, 0x00, KINS_SIZE_LENZ);
   //
   // iPingPong=0: G_pcPing
   // iPingPong=1: G_pcPong
   // Reserve terminator for ASCIIZ
   //
   comm_UpdateSensorTimeout(TRUE);
   tCommCommand = SMCMD_DLD_SENSOR;
   while(tCommCommand == SMCMD_DLD_SENSOR)
   {
      //
      // Reading is on a SINGLE CHAR BASIS into the serial port receive buffer (pcRcvBuffer),
      // except in SIM mode where the SIM data is pseudo-random sensor data minus the CRC.
      // Timeout 20*100mSecs
      //
      // Sync on 1st record:
      // "Dist=0B25, Flags=01, Count=0440, Pump=1, Time=000EE, Mode=00, Sensor=02.11!1234\n\r"
      //
      iRd = comm_SensorRead(iFd, 50);
      if(iRd > 0)
      {
         switch(*pcRcvBuffer)
         {
            case '!':
               // Start of CRC16
               //PRINTF("comm-CommandSensorLoop():CRC field...." CRLF);
               fCrc  = TRUE;
               iCrLf = 0;
               break;

            case '\n':
            case '\r':
               // EOR
               //PRINTF("comm-CommandSensorLoop():EOR" CRLF);
               iCrLf++;
               if(fCrc && (iCrLf == 2)) fNewData = TRUE;
               break;

            default:
               if(iCrLf == 2)
               {
                  //
                  // Previous 2 chars were EOR ("\r\n" or "\n\r"): this one should be the starting char of a SENSOR record.
                  //
                  if(*pcRcvBuffer != 'D')
                  {
                     // 
                     // If not, this is NOT a sensor record
                     // 
                     tCommCommand = SMCMD_ERROR;
                     tCommError   = GEN_ERROR_SENSOR;
                     if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) 
                     {
                        LOG_printf("comm-CommandSensorLoop():ERROR: Not in SENSOR mode" CRLF);
                        LOG_DumpData("comm-CommandSensorLoop():", pcDest, KINS_SIZE_LENZ);
                     }
                  }
               }
               iCrLf = 0;               
               break;
         }
         pcDest[iAct++] = *pcRcvBuffer;
         pcDest[iAct]   = 0;
         //
         // Buffer parsed: check if EOR was amongs it. If so, signal the host we have new data.
         // Setup to receive the next SENSOR record.
         //
         if(fNewData)
         {
            //PRINTF("comm-CommandSensorLoop():New data" CRLF);
            if(RPI_GetFlag(VERBOSE_LOG_SENSOR2))
            {
               LOG_Report(0, "COM", "comm-CommandSensorLoop(): %d bytes New data", iAct);
               LOG_ListData("comm-CommandSensorLoop():", pcDest, KINS_SIZE_LENZ);
            }
            //
            // iPingPong=0: G_pcPing
            // iPingPong=1: G_pcPong
            //
            if(iPingPong & 1) 
            {
               // Signal Pong buffer; Setup new Ping buffer
               GLOBAL_Notify(PID_HOST, GLOBAL_COM_HST_PPO, SIGUSR1);
               pcDest    = pstMap->G_pcPing;
               iPingPong = 0;
            }
            else
            {
               // Signal Ping buffer; Setup new Pong buffer
               GLOBAL_Notify(PID_HOST, GLOBAL_COM_HST_PPI, SIGUSR1);
               pcDest    = pstMap->G_pcPong;
               iPingPong = 1;
            }
            iAct     = 0;
            fNewData = FALSE;
            fCrc     = FALSE;
            GEN_MEMSET(pcDest, 0x00, KINS_SIZE_LENZ);
         }
      }
      else if(iRd == 0)
      {
         if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 2)) LOG_printf("comm-CommandSensorLoop():(Waiting for mode data)" CRLF);
      }
      else
      {
         // Error
         if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-CommandSensorLoop():Error reading Smart Sensor comms" CRLF);
         LOG_Report(errno, "COM", "comm-CommandSensorLoop():Error reading Smart Sensor comms");
      }
      //
      // Check if we do not run out of buffer space !
      //
      if(iAct >= KINS_SIZE_LEN)
      {
         if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-CommandSensorLoop():Error: Bufferoverflow" CRLF);
         LOG_Report(0, "COM", "comm-CommandSensorLoop():Error:Buffer overflow!");
         if(RPI_GetFlag(VERBOSE_LOG_SENSOR2))
         {
            LOG_ListData("comm-CommandSensorLoop():Error:Buffer overflow!", pcDest, KINS_SIZE_LENZ);
         }
         iAct      = 0;
         iPingPong = 0;
         fNewData  = FALSE;
         fCrc      = FALSE;
         pcDest    = pstMap->G_pcPing;
      }
      //
      // Check incoming signals
      //
      comm_CheckSignals(iFd);
   }
   if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-CommandSensorLoop():EXIT (%s)" CRLF, pstMap->G_pcStatus);
}



/*------  Local functions separator ------------------------------------
_____________COMMAND_FUNCTIONS(){};
-----------------------------X------------------------------------------------*/

//  
// Function:   comm_Controller
// Purpose:    Handle the Smart Sensor controller
// 
// Parameters: Comm-port Fd (0 is SIM)
// Returns:    
// Note:       At startup, the Verbose flag determines the inital action:
//                o Reset uCtl
//                o Read the RAM & EEPROM memory from the uCtl and safe it. 
//                o Then enter SENSOR mode.
//  
static void comm_Controller(int iFd)
{
   pcRcvBuffer = safemalloc(KINS_SIZE_LENZ);
   //
   GLOBAL_PidTimestamp(PID_COMM, 0);
   GLOBAL_PidSaveGuard(PID_COMM, GLOBAL_COM_ALL_INI);
   //
   if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-Controller()" CRLF);
   if(RPI_GetFlag(VERBOSE_MODE_RESET)) 
   {
      //
      // uCtl reset: the uController mode is always SENSOR after reset
      //
      comm_ForceReset(iFd);
   }
   if(comm_DetermineMode(iFd))
   {
      if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-Controller():Initial CommMode=%s" CRLF, pstMap->G_pcStatus);
   }
   else
   {
      if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-Controller():Initial CommMode=UNKNOWN !!" CRLF);
      LOG_Report(0, "COM", "comm-Controller():Initial CommMode=UNKNOWN !!");
   }
   //
   // Put the uController mode in SENSOR
   //
   comm_SwitchAnyToSensor(iFd);
   //==========================================================================
   // Determine the current uCtl mode, and keep track of any changes calling 
   // comm_SensorRead().
   // tCommMode =
   //       GEN_STATUS_IDLE         Idle
   //   --> GEN_STATUS_SENSOR       Sensor
   //       GEN_STATUS_TRACE        Trace
   //       GEN_STATUS_MIAC_RAM     Miac RAM
   //       GEN_STATUS_MIAC_ROM     Miac EEPROM
   //       GEN_STATUS_ERROR        Error
   //       GEN_STATUS_UCTL         uController
   //==========================================================================
   //
   // Default startup option:
   //    Verbose flags command line:
   //       0x01  VERBOSE_MODE_IDLE
   //       0x02  VERBOSE_MODE_RESET
   //       0x04  VERBOSE_MODE_SENSOR
   //       0x08  VERBOSE_MODE_MEMORY
   //    iSmartSensor
   //             SMART_SENSOR_IDLE    Do nothing
   //             SMART_SENSOR_XFER    Transfer RAM/EEPROM uCtrl memory
   //             SMART_SENSOR_SENSOR  Run Sensor mode
   //
   switch(iSmartSensor)
   {
      case SMART_SENSOR_IDLE:
         tCommCommand = SMCMD_IDLE_MODE;
         if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-Controller():Startup mode=IDLE" CRLF);
         LOG_Report(0, "COM", "comm-Controller():Startup mode=IDLE");
         break;

      case SMART_SENSOR_XFER:
         tCommCommand = SMCMD_DLD_ROM;
         if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-Controller():Startup mode=TRANSFER" CRLF);
         LOG_Report(0, "COM", "comm-Controller():Startup mode=TRANSFER");
         break;

      default:
      case SMART_SENSOR_RECS:
         tCommCommand = SMCMD_DLD_SENSOR;
         if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-Controller():Startup mode=SENSOR" CRLF);
         LOG_Report(0, "COM", "comm-Controller():Startup mode=SENSOR");
         break;
   }
   //
   // Init ready
   // Wait for Host Run/Execute
   //
   GLOBAL_SetSignalNotification(PID_HOST, GLOBAL_COM_ALL_INI);
   comm_WaitSignal(GLOBAL_HST_ALL_RUN);
   if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-Controller():Received Host run permission" CRLF);
   LOG_Report(0, "COM", "comm-Controller():Received Host run persission");
   //==============================================================================================
   // Comm Main Loop
   // Initial Smart Sensor controller command is:
   //    o SMCMD_IDLE_MODE:   Enter IDLE Loop
   //    o SMCMD_DLD_ROM:     Enter uCtl RAM/EEPROM memory transfer Loop
   //    o SMCMD_DLD_SENSOR:  Enter normal Smart Sensor operation
   //==============================================================================================
   while(fCommRunning)
   {
      //
      // Keep tabs on verbose flags to see if changed run-time
      //
      comm_CheckRunMode();
      //
      switch(tCommCommand)
      {
         default:
         case SMCMD_READY:
         case SMCMD_IDLE_MODE:
            comm_CommandIdleLoop(iFd);
            break;

         case SMCMD_ERROR:
            comm_UpdateStatus(GEN_STATUS_ERROR, tCommError);
            LOG_Report(0, "COM", "comm-Controller():Command ERROR");
            GLOBAL_SetSignalNotification(PID_HOST, GLOBAL_COM_HST_ERR);
            comm_CommandIdleLoop(iFd);
            break;

         case SMCMD_UCTL_MODE:
            //
            // uController mode
            //
            if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1))  LOG_printf("comm-Controller():Switch uController mode" CRLF);
            if(RPI_GetFlag(VERBOSE_LOG_SENSOR1))      LOG_Report(0, "COM", "comm-Controller():Switch uController mode");
            //
            if(comm_CommandMicroCtlMode(iFd))         comm_CommandMicroCtlLoop(iFd);
            else                                      
            {
               tCommCommand = SMCMD_ERROR;
               tCommError   = GEN_ERROR_UCTL;
            }
            break;

         case SMCMD_DLD_SENSOR:
            if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1))  LOG_printf("comm-Controller():Smart Sensor mode" CRLF);
            if(RPI_GetFlag(VERBOSE_LOG_SENSOR1))      LOG_Report(0, "COM", "comm-Controller():Smart Sensor mode");
            //
            if(comm_CommandSensorMode(iFd))           comm_CommandSensorLoop(iFd);
            else                                      
            {
               tCommCommand = SMCMD_ERROR;
               tCommError   = GEN_ERROR_SENSOR;
            }
            break;

         case SMCMD_DLD_RAM:
            if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1))  LOG_printf("comm-Controller():Download Sensor RAM" CRLF);
            if(RPI_GetFlag(VERBOSE_LOG_SENSOR1))      LOG_Report(0, "COM", "comm-Controller():Download Sensor RAM");
            //
            if(comm_CommandRamMode(iFd))              comm_CommandMemoryLoop(iFd, SMCMD_DLD_RAM);
            else                                      
            {
               tCommCommand = SMCMD_ERROR;
               tCommError   = GEN_ERROR_RAM;
            }
            break;

         case SMCMD_DLD_ROM:
            if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1))  LOG_printf("comm-Controller():Download Sensor EEPROM" CRLF);
            if(RPI_GetFlag(VERBOSE_LOG_SENSOR1))      LOG_Report(0, "COM", "comm-Controller():Download Sensor EEPROM");
            //
            if(comm_CommandRomMode(iFd))              comm_CommandMemoryLoop(iFd, SMCMD_DLD_ROM);
            else                                      
            {
               tCommCommand = SMCMD_ERROR;
               tCommError   = GEN_ERROR_ROM;
            }
            break;

         case SMCMD_HTTP_MIAC:
            //
            // Callback Dyn HTTP "MiacCmd" (JSON) and "miac=" (HTML)
            //
            if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1))  LOG_printf("comm-Controller():MIAC cmd=%s" CRLF, pstMap->G_pcMiacCmd);
            if(RPI_GetFlag(VERBOSE_LOG_SENSOR1))      LOG_Report(0, "COM", "comm-Controller():MIAC cmd=%s", pstMap->G_pcMiacCmd);
            //
            if(comm_CommandHttpMiacMode(iFd))         tCommCommand = SMCMD_DLD_SENSOR;
            else                                      
            {
               tCommCommand = SMCMD_ERROR;
               tCommError   = GEN_ERROR_HTTP1;
            }
            break;

         case SMCMD_HTTP_PUMP:
            // Change pump mode
            if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1))  LOG_printf("comm-Controller():HTTP switch Pump mode" CRLF);
            if(RPI_GetFlag(VERBOSE_LOG_SENSOR1))      LOG_Report(0, "COM", "comm-Controller():HTTP switch Pump mode");
            //
            if(comm_CommandHttpPumpMode(iFd))         tCommCommand = SMCMD_DLD_SENSOR;
            else                                      
            {
               tCommCommand = SMCMD_ERROR;
               tCommError   = GEN_ERROR_HTTP2;
            }
            break;

         case SMCMD_HTTP_SENSOR:
            // HTTP entries to switch to Sensor mode
            //    o sensor
            //    o sensor.json
            //    o sensor.html
            if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1))  LOG_printf("comm-Controller():HTTP switch Sensor mode" CRLF);
            if(RPI_GetFlag(VERBOSE_LOG_SENSOR1))      LOG_Report(0, "COM", "comm-Controller():HTTP switch Sensor mode");
            //
            if(comm_CommandSensorMode(iFd))           comm_CommandSensorLoop(iFd);
            else                                      
            {
               tCommCommand = SMCMD_ERROR;
               tCommError   = GEN_ERROR_HTTP3;
            }
            break;

         case SMCMD_RESET:
            //
            // Reset the Smart Sensor Controller
            //
            if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1))  LOG_printf("comm-Controller():uCtl RESET" CRLF);
            if(RPI_GetFlag(VERBOSE_LOG_SENSOR1))      LOG_Report(0, "COM", "comm-Controller():uCtl RESET");
            //
            comm_ForceReset(iFd);
            tCommCommand = SMCMD_DLD_SENSOR;
            break;

         case SMCMD_EXIT:
            fCommRunning = FALSE;
            break;
      }
   }
   GLOBAL_SemaphoreDelete(PID_COMM);
   safefree(pcRcvBuffer);
}

//  
// Function:   comm_CommandRomMode
// Purpose:    Handle non-conical reading sensor EEPROM Memory from the serial port
// 
// Parameters: Comm-port Fd (0 is SIM)
// Returns:    TRUE if OK
// Note:       
//  
static bool comm_CommandRomMode(int iFd)
{
   bool  fCc=TRUE;
   int   iLen;

   if(iFd)
   {
      if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-CommandRomMode():(%s)" CRLF, pstMap->G_pcStatus);
      switch(tCommMode)
      {
         case GEN_STATUS_ERROR:
            //
            // ERROR
            //
            LOG_Report(0, "COM", "comm-CommandRomMode():We're in ERROR mode");
            if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-CommandRomMode():We're in ERROR mode" CRLF);
            fCc = FALSE;
            break;

         case GEN_STATUS_SENSOR:
         case GEN_STATUS_TRACE:
         case GEN_STATUS_MIAC_RAM:
            //
            // Switch to MIAC_ROM
            //
            fCc = comm_SwitchAnyToMiac(iFd, GEN_STATUS_MIAC_ROM );
            break;
            
         case GEN_STATUS_MIAC_ROM:
            //
            // MIAC Mode is already MIAC_ROM
            //
            break;

         case GEN_STATUS_IDLE:
         case GEN_STATUS_RESET:
         case GEN_STATUS_TIMEOUT:
         case GEN_STATUS_REJECTED:
         case GEN_STATUS_REDIRECT:
         case GEN_STATUS_UCTL:
         case GEN_STATUS_MIAC_ANY:
         case GEN_STATUS_LIVE_ANY:
         default:
            //
            // Undefined mode: ERROR
            //
            LOG_Report(0, "COM", "comm-CommandRomMode():Undefined mode %d", tCommMode);
            if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-CommandRomMode():Undefined mode: %d" CRLF, tCommMode);
            fCc = FALSE;
            break;
      }
      if(fCc)
      {
         //
         // MIAC Mode is EEPROM: execute the memory dump command
         //
         fCc = comm_SendWait(iFd, ubMiacList);
      }
   }
   else
   {
      //
      // SIM mode
      //
      iLen = sizeof(pcSimRom);
      if(iLen > KINS_SIZE_LEN) iLen = KINS_SIZE_LEN;
      if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-CommandRomMode():SIM:Copy %d bytes" CRLF, iLen);
      GEN_MEMCPY(pstMap->G_stCmd.pcData, pcSimRom, iLen);
      GLOBAL_Notify(PID_HOST, GLOBAL_COM_HST_ROM, SIGUSR1);
   }
   return(fCc);
}

//  
// Function:   comm_CommandRamMode
// Purpose:    Handle non-conical reading sensor RAM Memory from the serial port
// 
// Parameters: Comm-port Fd (0 is SIM)
// Returns:    TRUE if OK
// Note:       
//  
static bool comm_CommandRamMode(int iFd)
{
   bool  fCc=TRUE;
   int   iLen;

   if(iFd)
   {
      if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-CommandRamMode():(%s)" CRLF, pstMap->G_pcStatus);
      switch(tCommMode)
      {
         case GEN_STATUS_ERROR:
            //
            // ERROR
            //
            LOG_Report(0, "COM", "comm-CommandRamMode():We're in ERROR mode");
            if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-CommandRamMode():We're in ERROR mode" CRLF);
            fCc = FALSE;
            break;

         case GEN_STATUS_SENSOR:
         case GEN_STATUS_TRACE:
         case GEN_STATUS_MIAC_ROM:
            //
            // Switch to MIAC_RAM
            //
            fCc = comm_SwitchAnyToMiac(iFd, GEN_STATUS_MIAC_RAM );
            break;
            
         case GEN_STATUS_MIAC_RAM:
            //
            // MIAC Mode is already MIAC_RAM
            //
            break;

         case GEN_STATUS_IDLE:
         case GEN_STATUS_RESET:
         case GEN_STATUS_TIMEOUT:
         case GEN_STATUS_REJECTED:
         case GEN_STATUS_REDIRECT:
         case GEN_STATUS_UCTL:
         case GEN_STATUS_MIAC_ANY:
         case GEN_STATUS_LIVE_ANY:
         default:
            //
            // Undefined mode: ERROR
            //
            LOG_Report(0, "COM", "comm-CommandRamMode():Undefined mode %d", tCommMode);
            if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-CommandRamMode():Undefined mode: %d" CRLF, tCommMode);
            fCc = FALSE;
            break;
      }
      if(fCc)
      {
         //
         // MIAC Mode is EEPROM: execute the memory dump command
         //
         fCc = comm_SendWait(iFd, ubMiacList);
      }
   }
   else
   {
      //
      // SIM mode
      //
      iLen = sizeof(pcSimRam);
      if(iLen > KINS_SIZE_LEN) iLen = KINS_SIZE_LEN;
      if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-CommandRamMode():SIM:Copy %d bytes" CRLF, iLen);
      GEN_MEMCPY(pstMap->G_stCmd.pcData, pcSimRam, iLen);
      GLOBAL_Notify(PID_HOST, GLOBAL_COM_HST_RAM, SIGUSR1);
   }
   return(fCc);
}

//  
// Function:   comm_CommandHttpMiacMode
// Purpose:    Handle Memory Inspect and Change command through HTTP
// 
// Parameters: Comm-port Fd (0 is SIM)
// Returns:    TRUE if OK
// Note:       The actual MIAC command sequence is stored in
//             pstMap->G_pcMiacCmd
//             and is delivered by HTTP://<ip-addr>/parms.json?E:00=12,34,ab,cd,ff
//  
static bool comm_CommandHttpMiacMode(int iFd)
{
   bool     fCc=TRUE;
   char    *pcCmds;
   char    *pcDest;
   
   pcCmds = pstMap->G_pcMiacCmd;
   pcDest = pstMap->G_pcMiacRes;
   GEN_MEMSET(pcDest, 0x00, MIAC_CIRC_SIZE);
   //
   if(RPI_GetFlag(VERBOSE_LOG_MIAC)) LOG_Report(0, "COM", "comm-CommandHttpMiacMode()");
   //
   fCc = comm_SwitchAnyToMiac(iFd, GEN_STATUS_MIAC_RAM);
   //
   switch(*pcCmds++)
   {
      case 'E':
         if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-CommandHttpMiacMode():ROM" CRLF);
         if(tCommMode == GEN_STATUS_MIAC_RAM)
         {
            //
            // MIAC Mode is RAM: Switch to EEPROM first
            //
            comm_SendWait(iFd, ubMiacToggle);
            if(tCommMode == GEN_STATUS_MIAC_ROM) comm_LogSensorMode(iFd);
            else                                 fCc = FALSE;
         }
         break;

      case 'R':
         if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-CommandHttpMiacMode():RAM" CRLF);
         if(tCommMode == GEN_STATUS_MIAC_ROM)
         {
            //
            // MIAC Mode is EEPROM: Switch to RAM first
            //
            comm_SendWait(iFd, ubMiacToggle);
            if(tCommMode == GEN_STATUS_MIAC_RAM) comm_LogSensorMode(iFd);
            else                                 fCc = FALSE;
         }
         break;

      default:
         fCc = FALSE;
         break;
   }
   if(fCc)
   {
      if(*pcCmds++ == ':')
      {
         //
         // All is fine: go parse the command buffer
         //
         if(RPI_GetFlag(VERBOSE_LOG_MIAC)) LOG_Report(0, "COM", "comm-CommandHttpMiacMode():Correct MIAC mode: Parse commands");
         fCc = comm_ParseMiacCommands(iFd, pcCmds);
      }
      else
      {
         if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-CommandHttpMiacMode():Syntax ERROR!" CRLF);
         LOG_Report(0, "COM", "comm-CommandHttpMiacMode():Syntax ERROR!");
         fCc = FALSE;
      }
   }
   if(fCc)
   {
      //
      // MIAC went OKee: Notify Host
      //
      if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-CommandHttpMiacMode():OKee completion" CRLF);
      if(RPI_GetFlag(VERBOSE_LOG_MIAC))        LOG_Report(0, "COM", "comm-CommandHttpMiacMode():OKee completion");
   }
   else
   {
      if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-CommandHttpMiacMode():ERROR completion" CRLF);
      LOG_Report(0, "COM", "comm-CommandHttpMiacMode():ERROR completion");
   }
   //
   // Nfy Host
   //
   GLOBAL_Notify(PID_HOST, GLOBAL_COM_HST_MEM, SIGUSR1);
   return(fCc);
}

//  
// Function:   comm_CommandHttpPumpMode
// Purpose:    Send single byte Pump Mode command
// 
// Parameters: Comm-port Fd (0 is SIM)
// Returns:    TRUE if OK
// Note:       
//  
static bool comm_CommandHttpPumpMode(int iFd)
{
   bool  fCc=FALSE;

   switch(pstMap->G_iPumpMode)
   {
      case KINS_MODE_AUTO:
         fCc = comm_SendCommand(iFd, pubPumpModeAuto);
         LOG_Report(0, "COM", "comm-CommandPumpMode():Auto");
         break;

      case KINS_MODE_PUMP_ON:
         fCc = comm_SendCommand(iFd, pubPumpModeOn);
         LOG_Report(0, "COM", "comm-CommandPumpMode():Manual ON");
         break;

      case KINS_MODE_PUMP_OFF:
         fCc = comm_SendCommand(iFd, pubPumpModeOff);
         LOG_Report(0, "COM", "comm-CommandPumpMode():Manual OFF");
         break;

      case KINS_MODE_TIMER:
         fCc = comm_SendCommand(iFd, pubPumpModeTimer);
         LOG_Report(0, "COM", "comm-CommandPumpMode():Timer");
         break;

      case KINS_MODE_POWEROFF:
         LOG_Report(0, "COM", "comm-CommandPumpMode():POWER OFF not possible !!");
         break;

      default:
         LOG_Report(0, "COM", "comm-CommandPumpMode():Unknown");
         break;
   }
   return(fCc);
}

//  
// Function:   comm_CommandSensorMode
// Purpose:    Switch to Sensor mode
// 
// Parameters: Comm-port Fd (0 is SIM)
// Returns:    TRUE if OK
// Note:       On any ERROR caller should select IDLE mode
//  
static bool comm_CommandSensorMode(int iFd)
{
   bool  fCc=TRUE;
   int   iLen;

   if(iFd)
   {
      if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-CommandSensorMode():ENTER:%s" CRLF, pstMap->G_pcStatus);
      switch(tCommMode)
      {
         case GEN_STATUS_ERROR:
            //
            // ERROR
            //
            LOG_Report(0, "COM", "comm-CommandSensorMode():We're in ERROR mode");
            if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-CommandSensorMode():We're in ERROR mode" CRLF);
            fCc = FALSE;
            break;

         case GEN_STATUS_SENSOR:
            //
            // Already Sensor mode: OK
            //
            break;
            
         case GEN_STATUS_TRACE:
         case GEN_STATUS_MIAC_RAM:
         case GEN_STATUS_MIAC_ROM:
            //
            // Switch to Sensor mode
            //
            fCc = comm_SwitchAnyToSensor(iFd);
            break;

         case GEN_STATUS_IDLE:
         case GEN_STATUS_RESET:
         case GEN_STATUS_TIMEOUT:
         case GEN_STATUS_REJECTED:
         case GEN_STATUS_REDIRECT:
         case GEN_STATUS_UCTL:
         case GEN_STATUS_MIAC_ANY:
         case GEN_STATUS_LIVE_ANY:
         default:
            //
            // Undefined mode: ERROR
            //
            LOG_Report(0, "COM", "comm-CommandSensorMode():Undefined mode %d", tCommMode);
            if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-CommandSensorMode():Undefined mode: %d" CRLF, tCommMode);
            fCc = FALSE;
            break;
      }
   }
   else
   {
      //
      // SIM mode
      //
      iLen = sizeof(pcSimRom);
      if(iLen > KINS_SIZE_LEN) iLen = KINS_SIZE_LEN;
      if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-CommandSensorMode():SIM:Copy %d bytes" CRLF, iLen);
      GEN_MEMCPY(pstMap->G_stCmd.pcData, pcSimRom, iLen);
      GLOBAL_Notify(PID_HOST, GLOBAL_COM_HST_ROM, SIGUSR1);
   }
   if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-CommandSensorMode():EXIT:%s" CRLF, pstMap->G_pcStatus);
   return(fCc);
}

//  
// Function:   comm_CommandMicroCtlMode
// Purpose:    Switch to uCtl mode
// 
// Parameters: Comm-port Fd (0 is SIM)
// Returns:    TRUE
// Note:       
//  
static bool comm_CommandMicroCtlMode(int iFd)
{
   if(RPI_GetFlag(VERBOSE_LOG_SENSOR1))     LOG_Report(0, "COM", "comm-CommandMicroCtlMode()");
   //
   comm_UpdateStatus(GEN_STATUS_UCTL, GEN_ERROR_NONE);
   return(TRUE);
}



/*------  Local functions separator ------------------------------------
_______________LOCAL_FUNCTIONS(){};
-----------------------------X------------------------------------------------*/

//  
// Function:   comm_CheckRunMode
// Purpose:    Check if the SmartSensor mode of operation has been altered and
//             is in Sim or uCtl mode
// Parameters: 
// Returns:    FALSE if Simulation mode, TRUE if real uCtl mode
// Note:       The SmartSensor mode MAY be altered run-time by updating the Verbose flag:
//                VERBOSE_MODE_IDLE     Force IDLE mode
//                VERBOSE_MODE_RESET    Force Microcontroller restart
//                VERBOSE_MODE_SENSOR   Smart Sensor mode
//  
static bool comm_CheckRunMode()
{
   bool fRunMode=TRUE;

   if(iCommVerbose == RPI_GetFlags())
   {
      //
      // No change: keep all as is
      //
      if(RPI_GetFlag(VERBOSE_MODE_SIM)) fRunMode = FALSE;
   }
   else
   {
      LOG_Report(0, "COM", "comm-CheckRealMode():Verbose changed: was 0x%04X", iCommVerbose);
      //
      // Verbose has changed: update
      //
      if(RPI_GetFlag(VERBOSE_MODE_IDLE))
      {
         LOG_Report(0, "COM", "comm-CheckRealMode():IDLE Sensor mode");
         iSmartSensor = SMART_SENSOR_IDLE;
      }
      else if(RPI_GetFlag(VERBOSE_MODE_SENSOR))
      {
         LOG_Report(0, "COM", "comm-CheckRealMode():Sensor mode");
         iSmartSensor = SMART_SENSOR_RECS;
      }
      else
      {
         LOG_Report(0, "COM", "comm-CheckRealMode():Transfer Sensor RAM/EEPROM mode");
         iSmartSensor = SMART_SENSOR_XFER;
      }
      if(RPI_GetFlag(VERBOSE_MODE_SIM))
      {
         iSimMode = SMART_SENSOR_SIM; 
         LOG_Report(0, "COM", "comm-CheckRealMode():Sim mode");
         if(pstMap->G_iSimulateTime == 0)
         {
            LOG_Report(0, "COM", "comm-CheckRealMode():Invalid Simtime: use default!");
            pstMap->G_iSimulateTime = 10000;
         }
         fRunMode = FALSE;
      }
      else iSimMode = iSmartSensor; 
      //
      iCommVerbose = RPI_GetFlags();
      if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 2)) LOG_printf("comm-CheckRealMode():Verbose=0x%04X" CRLF, iCommVerbose);
      LOG_Report(0, "COM", "comm-CheckRealMode():Verbose changed: now 0x%04X", iCommVerbose);
   }
   return(fRunMode);
}

//  
// Function:   comm_CheckSignals
// Purpose:    Check and handle incoming signals
// 
// Parameters: Fd
// Returns:    
// Note:       
//             
static void comm_CheckSignals(int iFd)
{
   GLOBAL_PidSetGuard(PID_COMM);
   if( GLOBAL_GetSignalNotification(PID_COMM, GLOBAL_GRD_ALL_RUN) ) LOG_Report(0, "COM", "COM-HandleGuard():Warning from Guard!");
   if( GLOBAL_GetSignalNotification(PID_COMM, GLOBAL_DBG_COM_CMD) ) comm_HandleController(iFd);
}

//  
// Function:   comm_DetermineMode
// Purpose:    Determine the Smart Sensor Controller mode
// 
// Parameters: Comm-port Fd (0 is SIM)
// Returns:    TRUE if OKee
// Note:       Just read and drop all serial port data until we are able to
//             determine the Smart Sensor mode:
//             
//             GEN_STATUS_SENSOR       : "\n\rDist=........"
//             GEN_STATUS_TRACE        : "\n\rTxxx........."
//             GEN_STATUS_MIAC_RAM     : "\n\rR>"
//             GEN_STATUS_MIAC_ROM     : "\n\rE>"
//             GEN_STATUS_ERROR        : Ooops
//  
static bool comm_DetermineMode(int iFd)
{
   int   fCc=FALSE;
   bool  fHaveMode=FALSE;
   int   iMaxTries=1;
   int   iRd;
   
   //
   // Make sure we do NOT end up in the situation where the Smart Sensor
   // controller has just send it's last MIAC command prompt :
   // "R>" or
   // "E>"
   // In this case we will get a timeout, and we need to send a single query command, reply should be:
   //          Mode              Reply
   //    GEN_STATUS_MIAC_RAM     " \n\rR>"
   //    GEN_STATUS_MIAC_ROM     " \n\rE>"
   //
   // In other modes, we will periodically receive:
   //    GEN_STATUS_SENSOR       "Dist=0417, Count=0010, Pump=0, X10=0, Temp=0000, Solar=0000, Time=000003!1342\n\r"
   //    GEN_STATUS_TRACE        "Txxx.........\n\r"
   //                            "wL xxx .... \n\r"
   //
   tCommMode = GEN_STATUS_IDLE;
   while(!fHaveMode)
   {
      //
      // Reading is on a SINGLE CHAR BASIS from the serial port receive buffer (pcRcvBuffer).
      // Timeout 20*100mSecs
      //
      iRd = comm_SensorRead(iFd, 50);
      //
      if(iRd > 0)
      {
         //
         // We have a char from the uCtl. If tCommMode has changed, this is the correct new mode
         //
         if(tCommMode != GEN_STATUS_IDLE) 
         {
            fHaveMode = TRUE;
            fCc       = TRUE;
         }
      }
      else if(iRd == 0)
      {
         //
         // Timeout: Perhaps the uController was in MIAC mode, waiting for a user response.
         //          Respond with sending a space to exit MIAC mode and return to the command prompt
         //          >R or
         //          >E
         //
         if(iMaxTries--)
         {
            if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-DetermineMode():Timeout: Retry" CRLF);
            fCc = comm_SendCommand(iFd, pubModeQuery);
         }
         else
         {
            if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-DetermineMode():No data received" CRLF);
            LOG_Report(0, "COM", "comm-DetermineMode():No data received");
            //
            // Update status
            //
            comm_UpdateStatus(GEN_STATUS_ERROR, GEN_ERROR_MODE);
            fHaveMode = TRUE;
            fCc       = FALSE;
         }
      }
      else
      {
         // Error
         if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-DetermineMode():Error reading Smart Sensor comms" CRLF);
         LOG_Report(errno, "COM", "comm-DetermineMode():Error reading Smart Sensor comms");
         //
         // Update status
         //
         comm_UpdateStatus(GEN_STATUS_ERROR, GEN_ERROR_COMM);
         fHaveMode = TRUE;
         fCc       = FALSE;
      }
      //
      // Check incoming signals
      //
      comm_CheckSignals(iFd);
   }
   return(fCc);
}

//  
// Function:   comm_ForceReset
// Purpose:    Force microcontroller restart
// 
// Parameters: Comm-port Fd (0 is SIM)
// Returns:    TRUE if reset OKee
// Note:       The microController will respond with:
//             "\n\r"
//             "Sensor=02.07\n\r"
//             "T000xxx\n\r"
//             "T000xxx\n\r"
//             "Dist=043E, Count=0024, Pump=1, X10=0, Temp=00bf, Solar=00e0, Time=003246, Mode=00, Sensor=v1.04-064!3a\n\r"
//             "Dist=043E, Count=0028, Pump=1, X10=0, Temp=00bf, Solar=00e0, Time=003246, Mode=00, Sensor=v1.04-064!3a\n\r"
//             "..."
//                
static bool comm_ForceReset(int iFd)
{
   bool  fWaitReply=TRUE, fCc=TRUE;
   int   iRd;
   int   iWaitRecs=5;

   if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-ForceReset():Reset microController..." CRLF);
   LOG_Report(0, "COM", "comm-ForceReset():Reset microController...");
   //
   if(iFd)
   {
      comm_SendCommand(iFd, pubModeReset);
      //
      // Wait for restart reply from the MicroController
      //
      while(fWaitReply)
      {
         //
         // Reading is on a single char basis into the serial port receive buffer (pcRcvBuffer).
         // Timeout 10*100mSecs
         //
         iRd = comm_SensorRead(iFd, 10);
         if(iRd > 0)
         {
            //
            // Skip CrLf's and Log replies
            //
            switch(*pcRcvBuffer)
            {
               case '\n':
                  break;

               case '\r':
                  if(--iWaitRecs == 0) fWaitReply = FALSE;
                  else if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 2)) LOG_printf("comm-ForceReset():EOR %d" CRLF, iWaitRecs);
                  break;
               
               default:
                  break;
            }
         }
         else if(iRd == 0) 
         {
            fWaitReply = FALSE;
            fCc        = FALSE;
         }
         if(iRd < 0)
         {
            // Error
            if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-ForceReset():Error Smart Sensor reset" CRLF);
            LOG_Report(errno, "COM", "comm-ForceReset():Error Smart Sensor reset");
            fWaitReply = FALSE;
            fCc        = FALSE;
         }
      }
      if(fCc)
      {
         if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-ForceReset():Reset microController: OK" CRLF);
         LOG_Report(0, "COM", "comm-ForceReset():Reset microController: OK");
      }
   }
   return(fCc);
}

// 
// Function:   comm_HandleController
// Purpose:    Respond to the debug uController command request
// 
// Parameters: Comms Fd
// Returns:    
// Note:       This is the debug thread way of sending data to the uCtl.
//             pstMap->G_pcMiacCmd is the command string
// 
static void comm_HandleController(int iFd)
{
   if(GLOBAL_DebugPrint(MM_DEBUG_PRINT, 2)) 
   {
      int   x=0;
      char  cData;

      printf("Sending to uCtl: ");
      while( (cData = pstMap->G_pcMiacCmd[x++]) )
      {
         if(isprint(cData)) printf("%c ", cData);
         else               printf("%x ", cData);
      }
   }
   comm_SendCommand(iFd, (u_int8 *)pstMap->G_pcMiacCmd);
}

//  
// Function:   comm_LogSensorMode
// Purpose:    LOG the current sensor mode
// 
// Parameters: Comm-port Fd (0 is SIM)
// Returns:    
// Note:       
//
static void comm_LogSensorMode(int iFd)
{
   if(RPI_GetFlag(VERBOSE_LOG_SENSOR2)) 
   {
      switch(tCommMode)
      {
         case GEN_STATUS_MIAC_RAM:
            if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-LogSensorMode():RAM mode" CRLF);
            LOG_Report(0, "COM", "comm-LogSensorMode():RAM mode");
            break;

         case GEN_STATUS_MIAC_ROM:
            if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-LogSensorMode():ROM mode" CRLF);
            LOG_Report(0, "COM", "comm-LogSensorMode():ROM mode");
            break;

         case GEN_STATUS_IDLE:
            if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-LogSensorMode():IDLE mode" CRLF);
            LOG_Report(0, "COM", "comm-LogSensorMode():IDLE mode");
            break;

         case GEN_STATUS_TRACE:
            if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-LogSensorMode():TRACE mode" CRLF);
            LOG_Report(0, "COM", "comm-LogSensorMode():TRACE mode");
            break;

         case GEN_STATUS_SENSOR:
            if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-LogSensorMode():SENSOR mode" CRLF);
            LOG_Report(0, "COM", "comm-LogSensorMode():SENSOR mode");
            break;
      
         case GEN_STATUS_ERROR:
            if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-LogSensorMode():ERROR mode" CRLF);
            LOG_Report(0, "COM", "comm-LogSensorMode():ERROR mode");
            break;
      
         case GEN_STATUS_UCTL:
            if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-LogSensorMode():UCTL mode" CRLF);
            LOG_Report(0, "COM", "comm-LogSensorMode():UCTL mode");
            break;
      
         default:
            if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-LogSensorMode():Unknown mode" CRLF);
            LOG_Report(0, "COM", "comm-LogSensorMode():Unknown mode");
            break;
      }
   }
}

//  
// Function:   comm_ParseMiacCommands
// Purpose:    Parse the MIAC Command buffer
// 
// Parameters: Comm-port Fd (0 is SIM), Command
// Returns:    TRUE if OKee
// Note:       MIAC Command buffer, f.i:
//             "20=ff,ff,ff"
//             "02=11,14,aa,0a,20=ff,ff,ff"
//                
static bool comm_ParseMiacCommands(int iFd, char *pcCmd)
{
   bool  fCc=TRUE;
   bool  fDoParse=TRUE;

   if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-ParseMiacCommands():Cmds=[%s]" CRLF, pcCmd);
   if(RPI_GetFlag(VERBOSE_LOG_MIAC)) LOG_Report(0, "COM", "comm-ParseMiacCommands():Cmds=[%s]", pcCmd);
   //
   // We are already in the correct MIAC Mode (Eeprom or Ram)
   // 1. Send the address:
   //    "25="    Reply: "25\n\r25:xx "
   // 2. Send new data
   //    "16\r+"  Reply: "16\n\r25:16\n\r26:xx "  
   //    Send new data
   //    "aa\r+"  Reply: "aa\n\r26:aa\n\r27:xx "  
   //    Send new data
   //    "ff\r+"  Reply: "ff\n\r27:ff\n\r28:xx " 
   // 3. List new memory
   //    "l"      Reply: "..... whole memory map ...\n\rE>"
   // 4. Terminate
   //    "Pq"     Reply: "Pq\n\r\n\r ..Sensor data..."
   //
   while(fDoParse)
   {
      pcCmd = comm_SendDigits(iFd, pcCmd);
      switch(*pcCmd++)
      {
         case '=':
            // New Address
            comm_SendWait(iFd, ubEqual);
            break;

         case ',':
            // New data
            comm_SendWait(iFd, ubCr);
            comm_SendWait(iFd, ubPlus);
            break;

         case 0:
         case ' ':
            comm_SendWait(iFd, ubCr);
            // End of command buffer: Exit
            if(RPI_GetFlag(VERBOSE_LOG_MIAC)) LOG_Report(0, "COM", "comm-ParseMiacCommands():END");
            fDoParse = FALSE;
            break;

         default:
            if(RPI_GetFlag(VERBOSE_LOG_MIAC)) LOG_Report(0, "COM", "comm-ParseMiacCommands():Syntax ERROR!");
            fCc      = FALSE;
            fDoParse = FALSE;
            break;
      }
   }
   if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-ParseMiacCommands():Done" CRLF);
   return(fCc);
}

//  
// Function:   comm_SensorRead
// Purpose:    Handle non-conical reading data from the serial port or dummy port
// 
// Parameters: Comm-port Fd (0 is SIM), Timeout in 100 mSecs
// Returns:    Number read
// Note:       Depending Verbose flags and VERBOSE_MODE_SIM, the data comes from the COM port
//             (and smart-meter) or from a buffer with test data
//                
static int comm_SensorRead(int iFd, int iTimeout)
{
   static int iCrLf=0;  // Used to capture EOR
   //
   int      iRd=0;
   GENSTAT  tMode=tCommMode;
   u_int32  ulSecs, ulSecsNow;

   switch(iSimMode)
   {
      default:
         while(iTimeout)
         {
            //
            // Read actual data from Smart Sensor COM port
            //
            if(iFd) iRd = read(iFd, pcRcvBuffer, 1);
            if(iRd > 0)
            {
               //
               // Keep track of the actual Sensor mode
               // tCommMode
               //
               switch(*pcRcvBuffer)
               {
                  case '\n':
                  case '\r':
                     iCrLf++;
                     break;

                  case 'D':
                     if(iCrLf == 2) 
                     {
                        tMode = GEN_STATUS_SENSOR;
                        iCrLf = 0;
                     }
                     break;

                  case 'T':
                     if(iCrLf == 2) 
                     {
                        tMode = GEN_STATUS_TRACE;
                        iCrLf = 0;
                     }
                     break;

                  case 'R':
                     if(iCrLf == 2) 
                     {
                        tMode = GEN_STATUS_MIAC_RAM;
                        iCrLf = 0;
                     }
                     break;

                  case 'E':
                     if(iCrLf == 2) 
                     {
                        tMode = GEN_STATUS_MIAC_ROM;
                        iCrLf = 0;
                     }
                     break;

                  case 0x1b:
                     if(GLOBAL_DebugPrint(MM_DEBUG_PRINT, 2)) LOG_printf("comm-SensorRead():ESC" CRLF);
                  default:
                     iCrLf = 0;
                     break;
               }
               //
               // If new status, update
               //
               if(tMode != tCommMode) 
               {
                  comm_UpdateStatus(tMode, GEN_ERROR_NONE);
                  if(GLOBAL_DebugPrint(MM_DEBUG_PRINT, 2)) LOG_printf("comm-SensorRead():New CommMode=%s" CRLF, pstMap->G_pcStatus);
               }
               if(GLOBAL_DebugGet(MM_DEBUG_SERIAL)) putchar((int)*pcRcvBuffer);
               if(isprint(*pcRcvBuffer))
               {
                  // MIAC_CIRC_MASK is also the circular index mask
                  pstMap->G_pcMiacRes[iMiacIdx++] = *pcRcvBuffer;
                  iMiacIdx &= MIAC_CIRC_MASK;
                  pstMap->G_pcMiacRes[iMiacIdx] = 0;
               }
               else iMiacIdx = 0;
               iTimeout = 0;
            }
            else if(iRd < 0) 
            {
               if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-SensorRead():Error: %s" CRLF, strerror(errno));
               iTimeout = 0;
            }
            else
            {
               // No key in the buffer
               // Wait a while and retry
               GEN_Sleep(100);
               iTimeout--;
            }
         }
         break;

      case SMART_SENSOR_SIM:
         //
         // SIM Mode: Dummy read delay (-T Cmd-Line option)
         //
         //PRINTF("comm-SensorRead():SIM: Wait %d secs" CRLF, pstMap->G_iSimulateTime/1000);
         GEN_Sleep(pstMap->G_iSimulateTime);
         //
         ulSecs    = RTC_GetSystemSecs();
         ulSecsNow = ulSecs - RTC_GetMidnight(ulSecs);
         //
         GEN_SNPRINTF(pcRcvBuffer, KINS_SIZE_LEN, pcSensorTestData, 
               iTotals[EMS_LEV], iCounts, iTotals[EMS_PMP], ulSecsNow);
         //
         if(RPI_GetFlag(VERBOSE_LOG_SENSOR1)) LOG_Report(0, "COM", "comm-SensorRead():SIM [%s]" CRLF, pcRcvBuffer);
         //
         iRd   = GEN_STRLEN(pcRcvBuffer);
         usCrc = COMM_CalculateCrc((u_int8 *)pcRcvBuffer, iRd);
         iCounts++;
         //
         // Drop the last char: report that one together with the CRC
         //
         iRd--;
         //PRINTF("comm-SensorRead():SIM Dist=%x" CRLF, iTotals[EMS_LEV]);
         iSimMode = SMART_SENSOR_CRC;
         break;

      case SMART_SENSOR_CRC:
         //
         // SIM Mode: Update and return CRC16
         //
         GEN_SNPRINTF(pcRcvBuffer, KINS_SIZE_LEN, "!%04X\n\r", usCrc);
         iRd = GEN_STRLEN(pcRcvBuffer);
         //PRINTF("comm-SensorRead():SIM:CRC=%s" CRLF, pcRcvBuffer);
         //
         // Update dummy figures
         //
         ulSecs = RTC_GetSystemSecs();
         comm_SimulateUpdate(ulSecs);
         //
         iSimMode = SMART_SENSOR_SIM;
         break;
   }
   return(iRd);
}

//
//  Function:  comm_SimulateSetup
//  Purpose:   Setup all Smart sensor updates
//
//  Parms:     
//  Returns:    
//  Note:      
//
static void comm_SimulateSetup()
{
   //
   // Init Pseudo Random Generator
   //
   srand(RTC_GetSystemSecs());
   //
   iTotals[EMS_LEV] = SIM_DIST_HI;           // DIST
   iTotals[EMS_TMP] = 0;
   iTotals[EMS_DIS] = 100;
   iTotals[EMS_PMP] = 0;
   iTotals[EMS_X10] = 0;
}

//
//  Function:  comm_SendCommand
//  Purpose:   Send out a Command to the Smart Sensor Controller
//
//  Parms:     Comm-port Fd, Cmd ptr
//  Returns:   TRUE if OKee 
//  Note:      
//
static bool comm_SendCommand(int iFd, const u_int8 *pubCmd)
{
   int   x, iNr=0, iWr=0;

   if(iFd)
   {
      iNr = GEN_STRLEN((char *)pubCmd);
      for (x=0; x<iNr; x++)
      {
         iWr += write(iFd, &pubCmd[x], 1);
         //
         // Delay some after each char, since it is non-buffered IO
         //
         if(pubCmd[x] == '!')  GEN_Sleep(2000);
         else                  GEN_Sleep(500);
      }
      if(GLOBAL_DebugPrint(MM_DEBUG_PRINT, 2)) LOG_DumpData("comm-SendCommand():", (char *)pubCmd, iNr);
      if(RPI_GetFlag(VERBOSE_LOG_SENSOR2)) LOG_ListData("comm-SendCommand():", (char *)pubCmd, iNr);
   }
   return(iWr == iNr);
}

//  
// Function:   comm_SendDigits
// Purpose:    Send out digits untill non-digit
// 
// Parameters: Comm-port Fd (0 is SIM), Data
// Returns:    Updated data ptr
// Note:       "012345="
//                
static char *comm_SendDigits(int iFd, char *pcData)
{
   while(isxdigit(*pcData))
   {
      if(iFd) 
      {
         comm_SendWait(iFd, *pcData);
      }
      pcData++;
   }
   return(pcData);
}

//
//  Function:  comm_SendWait
//  Purpose:   Send out a byte to the Smart Sensor Controller and wait for its echo
//
//  Parms:     Comm-port Fd, Cmd
//  Returns:   TRUE if OKee 
//  Note:      In MIAC mode, the uCtlr will echo all incoming bytes
//             We should use this here to have some kind of handshake while sending
//             command data.
//
static bool comm_SendWait(int iFd, const u_int8 ubCmd)
{
   bool     fCc=FALSE;
   u_int8   ubRcv;
   int      iRd;
   int      iRcvRetries;      // 100 mSecs retry receive echo
   int      iXmtRetries=3;    // Resend Retries 

   if(iFd)
   {
      while(iXmtRetries)
      {
         iRcvRetries = 50;
         if(write(iFd, &ubCmd, 1) == 1)
         {
            //
            // Wait for the echo
            //
            while(iRcvRetries)
            {
               iRd = comm_SensorRead(iFd, 1);
               ubRcv = *pcRcvBuffer;
               //pwjh iRd = read(iFd, &ubRcv, 1);
               if(iRd > 0)
               {
                  if(ubRcv == ubCmd)
                  {
                     // Correct echo: proceed
                     iRcvRetries = 0;
                     iXmtRetries = 0;
                     fCc         = TRUE;
                  }
                  else 
                  {
                     // Bad echo, Retry Xmt
                     iRcvRetries = 0;
                     GEN_Sleep(500);
                  }
               }
               else if(iRd < 0) 
               {
                  LOG_Report(0, "COM", "comm-SendWait():Error: %s", strerror(errno));
                  GEN_Sleep(500);
                  iRcvRetries = 0;
                  iXmtRetries--;
               }
               else
               {
                  // Wait a while and retry
                  GEN_Sleep(100);
                  iRcvRetries--;
               }
            }
         }
         else 
         {
            GEN_Sleep(500);
            iXmtRetries--;
         }
      }
   }
   return(fCc);
}

//
//  Function:  comm_SimulateUpdate
//  Purpose:   Simulate all Smart sensor updates
//
//  Parms:     System Secs
//  Returns:    
//  Note:      
//
static void comm_SimulateUpdate(u_int32 ulSecs)
{
   static u_int32 ulSecsLast=0;
   static bool    fRising=TRUE;
   //
   int            iRand;
   EMS            tEms;

   if(ulSecsLast)
   {
      for(tEms=0; tEms<NUM_EMS; tEms++)
      {
         iRand = rand();
         switch(tEms)
         {
            default:
               break;

            case EMS_LEV:
               if(fRising)
               {
                  if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-SimulateUpdate():OFF Dist=%d + (%d)" CRLF, iTotals[tEms], iUpdate[tEms]);
                  // Pump is OFF
                  // Dx or uCtl DIST rising to Hi threshold
                  iUpdate[EMS_PMP] = 0;
                  iUpdate[tEms]    = -(iRand % 50);
                  if(iTotals[tEms] < SIM_DIST_HI) fRising = FALSE;
               }
               else 
               {
                  if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-SimulateUpdate():ON Dist=%d + (%d)" CRLF, iTotals[tEms], iUpdate[tEms]);
                  // Pump is ON
                  // Dx or uCtl DIST dropping to Lo threshold
                  iUpdate[EMS_PMP] = 1;
                  iUpdate[tEms]    = iRand % 100;
                  if(iTotals[tEms] > SIM_DIST_LO) fRising = TRUE;
               }
               break;

            case EMS_TMP:
               // Dummy temperature
                    if(iTotals[tEms] < 100)   iUpdate[tEms] =   iRand % 10;
               else if(iTotals[tEms] > 250)   iUpdate[tEms] = -(iRand % 10);
               break;

            case EMS_DIS:
               // Dummy solar
                    if(iTotals[tEms] < 100)   iUpdate[tEms] =   iRand % 10;
               else if(iTotals[tEms] > 200)   iUpdate[tEms] = -(iRand % 10);
               break;

            case EMS_PMP:
               // Pump on/off determined by Level up/down
                  iTotals[EMS_PMP] = 0;
               break;

            case EMS_X10:
               iTotals[tEms] = iRand & 1;
               break;
         }
         if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) 
         {
            LOG_printf("comm-SimulateUpdate():%s = T(%d) + U(%d) (R=%d)" CRLF, pcEms[tEms], iTotals[tEms], iUpdate[tEms], iRand);
         }
         iTotals[tEms] += iUpdate[tEms];
      }
   }
   else 
   {
      if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-SimulateUpdate():First one." CRLF);
   }
   ulSecsLast = ulSecs;
}

//
//  Function:  comm_SwitchAnyToMiac
//  Purpose:   Switch from any mode to Miac mode
//
//  Parms:     Comm-port Fd, Miac Mode
//  Returns:   
//  Note:      
//
static bool comm_SwitchAnyToMiac(int iFd, GENSTAT tMiacMode)
{
   if(iFd)
   {
      if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-SwitchAnyToSensor():Switch to %s" CRLF, GLOBAL_GetStatusText(tMiacMode));
      switch(tCommMode)
      {
         default:
         case GEN_STATUS_TRACE:
         case GEN_STATUS_SENSOR:
            //
            // The uCtl is not in the correct mode yet
            // Switch to one of the MIAC modes
            //
            if(!comm_SendCommand(iFd, pubLiveToMiacMode))          return(FALSE);
            //
            // Wait till we have MIAC mode
            // Then toggle to the correct MIAC mode
            //
            if(!comm_WaitForCorrectMode(iFd, GEN_STATUS_MIAC_ANY)) return(FALSE);
            // Fallthrough
         case GEN_STATUS_MIAC_RAM:
         case GEN_STATUS_MIAC_ROM:
            //
            // We are in MIAC mode: select the correct one
            //
            if(tCommMode != tMiacMode) 
            {
               comm_SendCommand(iFd, pubToggleMiacMode);
               if(!comm_WaitForCorrectMode(iFd, tMiacMode))        return(FALSE);
            }
            break;
      }
      if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-SwitchAnyToMiac():Now %s" CRLF, pstMap->G_pcStatus);
   }
   else comm_UpdateStatus(tMiacMode, GEN_ERROR_NONE);
   return(TRUE);
}

//
//  Function:  comm_SwitchAnyToSensor
//  Purpose:   Switch from any mode to Sensor mode
//
//  Parms:     Comm-port Fd, Miac Mode
//  Returns:   TRUE if OK
//  Note:      
//
static bool comm_SwitchAnyToSensor(int iFd)
{
   if(iFd)
   {
      switch(tCommMode)
      {
         case GEN_STATUS_SENSOR:
            if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-SwitchAnyToSensor():Already %s" CRLF, pstMap->G_pcStatus);
            break;

         case GEN_STATUS_MIAC_ROM:
         case GEN_STATUS_MIAC_RAM:
            if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-SwitchAnyToSensor():%s to Sensor" CRLF, pstMap->G_pcStatus);
            //
            // The uCtl is in non-sensor mode
            // Switch to SENSOR mode
            // Wait till we have Sensor mode
            //
            if(!comm_SendCommand(iFd, pubMiacToSensorMode))      return(FALSE);
            if(!comm_WaitForCorrectMode(iFd, GEN_STATUS_SENSOR)) return(FALSE);
            break;

         default:
            if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-SwitchAnyToSensor():%s to Sensor" CRLF, pstMap->G_pcStatus);
            //
            // The uCtl is in non-sensor mode
            // Switch to SENSOR mode
            // Wait till we have Sensor mode
            //
            if(!comm_SendCommand(iFd, pubLiveToSensorMode))      return(FALSE);
            if(!comm_WaitForCorrectMode(iFd, GEN_STATUS_SENSOR)) return(FALSE);
            break;
      }
      if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-SwitchAnyToSensor():Now %s" CRLF, pstMap->G_pcStatus);
   }
   else comm_UpdateStatus(GEN_STATUS_SENSOR, GEN_ERROR_NONE);
   return(TRUE);
}

//  
// Function:   comm_UpdateStatus
// Purpose:    Update the status with this mode
// 
// Parameters: Mode, error (if any)
// Returns:    
// Note:       
//                
static void comm_UpdateStatus(GENSTAT tMode, GENERR tError)
{
   tCommMode = tMode;
   pstMap->G_stCmd.iStatus = tMode;
   pstMap->G_stCmd.iError  = tError;
   GLOBAL_Status(tMode);
}

//  
// Function:   comm_UpdateSensorTimeout
// Purpose:    Update the sensor timeout mode
// 
// Parameters: TRUE is Active timeout mode
// Returns:    
// Note:       Depending on the Command Loop, dis/enable sensor data timeout:
//                Idle loop            : disable
//                uCtl loop            : disable
//                Memory transfer loop : disable
//                Sensor loop          : enable
//                
//             The host will handle the actual Smart Sensor timeouts
//                
static void comm_UpdateSensorTimeout(bool fActive)
{
   if(fActive) pstMap->G_stCmd.iSecsTout = 0;
   else        pstMap->G_stCmd.iSecsTout = -1;
}

//
// Function:   comm_WaitForCorrectMode
// Purpose:    Wait until the correct mode is detected
//
// Parms:      Mode
// Returns:    TRUE if OK
// Note:       
//
static bool comm_WaitForCorrectMode(int iFd, GENSTAT tMode)
{
   bool     fCc=FALSE;
   bool     fWaiting=TRUE;
   int      iNr, iRetries=50;

   if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-WaitForCorrectMode():Wait for %s" CRLF, GLOBAL_GetStatusText(tMode));
   //
   while(fWaiting && iRetries)
   {
      iNr = comm_SensorRead(iFd, 1);
      switch(iNr)
      {
         case 0:
            // Timeout
            iRetries--;
            if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) 
            {
               if(iRetries == 0) LOG_printf("comm-WaitForCorrectMode():Time out: mode=%s" CRLF, pstMap->G_pcStatus);
            }
            break;

         case -1:
            // Comms Error
            if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-WaitForCorrectMode():Rcv ERROR Mode=%s" CRLF, pstMap->G_pcStatus);
            fWaiting = FALSE;
            break;

         default:
            // Incoming data
            if(tMode == GEN_STATUS_MIAC_ANY)
            {
               if((tCommMode == GEN_STATUS_MIAC_RAM) || (tCommMode == GEN_STATUS_MIAC_ROM))
               {
                  fWaiting = FALSE;
                  fCc      = TRUE;
               }
            }
            else if(tMode == GEN_STATUS_LIVE_ANY)
            {
               if((tCommMode == GEN_STATUS_SENSOR) || (tCommMode == GEN_STATUS_TRACE))
               {
                  fWaiting = FALSE;
                  fCc      = TRUE;
               }
            }
            else 
            {
               if(tMode == tCommMode)
               {
                  fWaiting = FALSE;
                  fCc      = TRUE;
               }
            }
            break;
      }
   }
   if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) 
   {
      if(fCc) LOG_printf("comm-WaitForCorrectMode():OK. Now %s" CRLF, pstMap->G_pcStatus);
      else    LOG_printf("comm-WaitForCorrectMode():BAD. Mode=%s(%d), should be %d" CRLF, pstMap->G_pcStatus, tCommMode, tMode);
   }
   return(fCc);
}

//
// Function:   comm_WaitSignal
// Purpose:    Wait for signal
//
// Parms:      Signal(s) to wait for, No semaphore waits though
// Returns:    
// Note:       
//
static bool comm_WaitSignal(int iSignal)
{
   bool     fWaiting=TRUE;

   while(fCommRunning && fWaiting)
   {
      if( GLOBAL_GetSignalNotification(PID_COMM, iSignal) ) fWaiting = FALSE;
      else  GEN_Sleep(100);
   }
   return(TRUE);
}



/*------  Local functions separator ------------------------------------
______________SYSTEM_FUNCTIONS(){};
-----------------------------X------------------------------------------------*/

//
//  Function:   comm_SignalRegister
//  Purpose:    Register all SIGxxx
//
//  Parms:      Blockset
//  Returns:    TRUE if delivered
//
static bool comm_SignalRegister(sigset_t *ptBlockset)
{
  bool fCC = TRUE;

  // SIGUSR1:
  if( signal(SIGUSR1, &comm_ReceiveSignalUser1) == SIG_ERR)
  {
     LOG_Report(errno, "COM", "comm-SignalRegister(): SIGUSR1 ERROR");
     fCC = FALSE;
  }
  // SIGUSR2:
  if( signal(SIGUSR2, &comm_ReceiveSignalUser2) == SIG_ERR)
  {
     LOG_Report(errno, "COM", "comm-SignalRegister(): SIGUSR2 ERROR");
     fCC = FALSE;
  }
  // CTL-X handler
  if( signal(SIGTERM, &comm_ReceiveSignalTerm) == SIG_ERR)
  {
     LOG_Report(errno, "COM", "comm-SignalRegister(): SIGTERM ERROR");
     fCC = FALSE;
  }
  // Ctl-C handler
  if( signal(SIGINT, &comm_ReceiveSignalInt) == SIG_ERR)
  {
     LOG_Report(errno, "COM", "comm-SignalRegister(): SIGINT ERROR");
     fCC = FALSE;
  }
  // Segmentation fault
  if( signal(SIGSEGV, &comm_ReceiveSignalSegmnt) == SIG_ERR)
  {
     LOG_Report(errno, "COM", "comm-ReceiveSignalSegmnt(): SIGSEGV ERROR");
     fCC = FALSE;
  }
  //
  if(fCC)
  {
     //
     // Setup the SIGxxx events
     //
     sigemptyset(ptBlockset);
     sigaddset(ptBlockset, SIGTERM);
     sigaddset(ptBlockset, SIGINT);
     sigaddset(ptBlockset, SIGUSR1);
     sigaddset(ptBlockset, SIGUSR2);
     sigaddset(ptBlockset, SIGSEGV);
  }
  return(fCC);
}

//
//  Function:   comm_ReceiveSignalTerm
//  Purpose:    Add the SIGTERM command in the buffer (Ctl-X)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void comm_ReceiveSignalTerm(int iSignal)
{
   LOG_Report(0, "COM", "comm-ReceiveSignalTerm()");
   //
   fCommRunning = FALSE;
   tCommCommand = SMCMD_EXIT;
}

//
//  Function:   comm_ReceiveSignalInt
//  Purpose:    Add the SIGINT command in the buffer (Ctl-C)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void comm_ReceiveSignalInt(int iSignal)
{
   LOG_Report(0, "COM", "comm-ReceiveSignalInt()");
   //
   fCommRunning = FALSE;
   tCommCommand = SMCMD_EXIT;
}

//
//  Function:   comm_ReceiveSignalSegmnt
//  Purpose:    Add the SIGSEGV command in the buffer (Ctl-C)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void comm_ReceiveSignalSegmnt(int iSignal)
{
   if(fCommRunning)
   {
      GLOBAL_SegmentationFault(__FILE__, __LINE__);
      LOG_SegmentationFault(__FILE__, __LINE__);
      LOG_Report(errno, "COM", "comm-ReceiveSignalSegmnt()");
      GLOBAL_Notify(PID_HOST, GLOBAL_ALL_HST_FLT, SIGTERM);
      fCommRunning = FALSE;
      tCommCommand = SMCMD_EXIT;
   }
   else exit(EXIT_CC_GEN_ERROR);
}

//
// Function:   comm_ReceiveSignalUser1
// Purpose:    SIGUSR1 signal
//
// Parms:
// Returns:    
// Note:       SIGUSR1 is used to signal system GLOBAL_xx_COM_xxx commands
//
static void comm_ReceiveSignalUser1(int iSignal)
{
   if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-ReceiveSignalUser1()" CRLF);
}

//
// Function:   comm_ReceiveSignalUser2
// Purpose:    System callback on receiving the SIGUSR2 signal
//
// Parms:
// Returns:    
// Note:       
//
static void comm_ReceiveSignalUser2(int iSignal)
{
   tCommCommand = pstMap->G_stCmd.iCommand;
   if(GLOBAL_DebugPrint(MM_DEBUG_COMMS, 1)) LOG_printf("comm-ReceiveSignalUser2():Command=%s" CRLF, pcSensorCommands[tCommCommand]);
}


/*------  Local functions separator ------------------------------------
____________OBSOLETE_FUNCTIONS(){};
-----------------------------X------------------------------------------------*/

#ifdef COMMENT



#endif   //COMMENT
