/*  (c) Copyright:  2020..2024  Patrn, Confidential Data
 *
 *  Workfile:           sens.h
 *  Purpose:            Macro def file smart sensor codes
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    01 Nov 2020:      Ported from rpislim
 *    23 Apr 2024:      Split globals.h and locals.h
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

//
// Macro:
//       
//             Enum           pcSensor          pFunct                        Help text
EXTRACT_SENS(SENS_ADM_DIST,   "Dist=",          sens_AdmDistance,             "Sensor distance to water"                                                     )
EXTRACT_SENS(SENS_ADM_COUNT,  "Count=",         sens_AdmCounts,               "Number of valid samples"                                                      )
EXTRACT_SENS(SENS_ADM_PUMP,   "Pump=",          sens_AdmPumpOn,               "Pump ON percentage"                                                           )
EXTRACT_SENS(SENS_ADM_X10,    "X10=",           sens_AdmX10Bypass,            "X10 bypass percentage"                                                        )
EXTRACT_SENS(SENS_ADM_TEMP,   "Temp=",          sens_AdmTemperature,          "Temperature"                                                                  )
EXTRACT_SENS(SENS_ADM_SOLAR,  "Solar=",         sens_AdmSolarStrength,        "Solar Strength"                                                               )
EXTRACT_SENS(SENS_ADM_EQUIP,  "Sensor=",        sens_AdmEquipment,            "Equipment identifier"                                                         )
EXTRACT_SENS(SENS_ADM_TIME,   "Time=",          sens_AdmTimeInSeconds,        "Seconds counter"                                                              )
EXTRACT_SENS(SENS_ADM_MODE,   "Mode=",          sens_AdmPumpMode,             "Pump mode "                                                                   )
EXTRACT_SENS(SENS_ADM_FLAGS,  "Flags=",         sens_AdmSystemFlags,          "System flags "                                                                )
EXTRACT_SENS(SENS_ADM_DROP,   "Drop=",          sens_AdmDropped,              "Dropped samples "                                                             )
