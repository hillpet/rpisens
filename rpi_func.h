/*  (c) Copyright:  2020..2024  Patrn, Confidential Data 
 *
 *  Workfile:           rpi_func.h
 *  Purpose:            Headerfile for rpi_func
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    01 Nov 2020:      Ported from rpislim
 *    21 May 2021:      Add HTTP Delete
 *    23 Apr 2024:      Split globals.h and locals.h
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _RPI_FUNC_H_
#define _RPI_FUNC_H_
//
// Prototypes Collect Callback
//
bool  http_CollectDebug                      (int);
bool  http_CollectRcuKey                     (int);
bool  http_CollectPumpMode                   (int);
bool  http_CollectMiacCommand                (int);
bool  http_CollectMiacUpdate                 (int);
//
bool  RPI_BuildHtmlMessageBody               (NETCL *);
bool  RPI_BuildJsonMessageBody               (NETCL *, int);
bool  RPI_BuildJsonMessageArgs               (NETCL *, int);
bool  RPI_CheckDelete                        (void);
int   RPI_CollectParms                       (NETCL *, FTYPE);
bool  RPI_GetFlag                            (int);
int   RPI_GetFlags                           (void);
void  RPI_ReportBusy                         (NETCL *);
void  RPI_ReportError                        (NETCL *);
bool  RPI_RestoreDay                         (u_int32, u_int8 *, int);
bool  RPI_RestoreMonth                       (u_int32, u_int8 *, int);
bool  RPI_SaveDay                            (u_int32, u_int8 *, int);
bool  RPI_SaveMonth                          (u_int32, u_int8 *, int);


#endif /* _RPI_FUNC_H_ */
