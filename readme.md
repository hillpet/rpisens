=====================================================================
Readme.md
=====================================================================

rpikinshi:

This systemd service is used to communicate with the Kinshi Ultrasoon 
waterlevel controller over a serial connection (9600b, 8N1). 
The signal itself is unconditioned TTL level so the Raspberry Pi RxD 
input understands.

The Kinshi Usoon controller will send out one package of data to the 
serial port which will be parsed by the service and periodically (5 mins) 
appended to a monthly CSV-file.

1 Nov 2020
Hillpet
Patrn ESS
