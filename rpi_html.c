/*  (c) Copyright:  2020..2024  Patrn, Confidential Data
 *
 *  Workfile:           rpi_html.c
 *  Purpose:            Misc functions
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    01 Nov 2020:      Ported from rpislim
 *    29 Mar 2022:      Add Variadic macro's for PRINTF()
 *    24 Apr 2024:      Move to global/local startup 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sched.h>
#include <time.h>
//
#include <common.h>
#include "config.h"
#include "locals.h"
#include "globals.h"
//
#include "rpi_page.h"
#include "rpi_html.h"

//#define USE_PRINTF
#include <printx.h>

//
// Local prototypes
//
static RPIDATA   *html_AllocateObject     (int);
static void       html_FreeObject         (RPIDATA *);
static RPIDATA   *html_Insert             (RPIDATA *, const char *);

extern const char *pcHttpResponseLength;

/*------  Local functions separator ------------------------------------
__GLOBAL_FUNCTIONS(){};
----------------------------------------------------------------------------*/

/*
 * Function    : HTML_InsertData
 * Description : Insert single data into object
 *
 * Parameters  : Object, value, style
 * Returns     : Object
 *
 */
RPIDATA *HTML_InsertData(RPIDATA *pstObj, char *pcValue, int iStyle)
{
   if(pstObj == NULL)
   {
      pstObj = html_AllocateObject(HTTP_DEFAULT_DATA_SIZE);
   }
   //
   pstObj->iLastComma = -1;
   //
   //PRINTF("HTML_InsertData():value=%s, style=%d" CRLF, pcValue, iStyle);
   //
   if(iStyle & HE_SPACE1)  pstObj = html_Insert(pstObj, "&nbsp;");
   if(iStyle & HE_SPACE2)  pstObj = html_Insert(pstObj, "&nbsp;&nbsp;");
   if(iStyle & HE_SPACE4)  pstObj = html_Insert(pstObj, "&nbsp;&nbsp;&nbsp;&nbsp;");
   if(iStyle & HE_VLS)     pstObj = html_Insert(pstObj, "|");
   if(iStyle & HE_TEXT)    pstObj = html_Insert(pstObj, "\"");
   if(pcValue)             pstObj = html_Insert(pstObj, (const char *)pcValue);
   if(iStyle & HE_TEXT)    pstObj = html_Insert(pstObj, "\"");
   if(iStyle & HE_VLE)     pstObj = html_Insert(pstObj, "|");
   if(iStyle & HE_COMMA) { pstObj->iLastComma = pstObj->iIdx;    pstObj = html_Insert(pstObj, ", "); }
   if(iStyle & HE_BR)      pstObj = html_Insert(pstObj, "<br/>");
   if(iStyle & HE_CRLF)    pstObj = html_Insert(pstObj, "\r\n");
   //
   return(pstObj);
}

//  
// Function:   HTML_GetObjectSize
// Purpose:    return HTML object size
// 
// Parameters: HTML object
// Returns:    Size
// Note:       
//  
int HTML_GetObjectSize(RPIDATA *pstObj)
{
   //PRINTF("HTML_GetObjectSize():%d" CRLF, pstObj->iIdx);
   return(pstObj->iIdx);
}

//  
// Function:   HTML_ReleaseObject
// Purpose:    Release HTML objects
// 
// Parameters: HTML object
// Returns:    NULL
// Note:       
//  
RPIDATA *HTML_ReleaseObject(RPIDATA *pstObj)
{
   //PRINTF("HTML_ReleaseObject()" CRLF);
   //
   html_FreeObject(pstObj);
   return(NULL);
}

//  
// Function:   HTML_RespondObject
// Purpose:    Send data object back through the socket
// 
// Parameters: Socket, data object
// Returns:    TRUE
// Note:       Reset the used index
//  
bool HTML_RespondObject(NETCL *pstCl, RPIDATA *pstObj)
{
   HTTP_BuildGeneric(pstCl, pstObj->pcObject);
   pstObj->iFree = pstObj->iSize;
   pstObj->iIdx  = 0;
   return(TRUE);
}

//  
// Function:   HTML_TerminateObject
// Purpose:    Terminate data object
// 
// Parameters: Data object
// Returns:    
// Note:       Handles final comma deletion
//  
RPIDATA *HTML_TerminateObject(RPIDATA *pstObj)
{
   if(pstObj->iLastComma > 0)
   {
      pstObj->pcObject[pstObj->iLastComma] = ' ';
      pstObj->iLastComma = -1;
   }
   PRINTF("HTML_TerminateObject():%s" CRLF, pstObj->pcObject);
   return(pstObj);
}

/*------  Local functions separator ------------------------------------
__LOCAL_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

//  
// Function:   html_AllocateObject
// Purpose:    Build empty HTML object
// 
// Parameters: Initial size
// Returns:    HTML object
// Note:       
//  
static RPIDATA *html_AllocateObject(int iLength)
{
   RPIDATA *pstObj = safemalloc(sizeof(RPIDATA));

   pstObj->pcObject     = safemalloc(iLength);
   pstObj->iIdx         = 0;
   pstObj->iLastComma   = -1;
   pstObj->iSize        = iLength;
   pstObj->iFree        = iLength;
   pstObj->tType        = HTTP_HTML;
   //
   PRINTF("html_AllocateObject():%d" CRLF, iLength);
   return(pstObj);
}

//  
// Function:   html_FreeObject
// Purpose:    Free HTML object
// 
// Parameters: HTML Object
// Returns:    
// Note:       
//  
static void html_FreeObject(RPIDATA *pstObj)
{
   PRINTF("html_FreeObject():%d bytes used, %d bytes free" CRLF, pstObj->iIdx, pstObj->iFree);
   //
   if(pstObj)
   {
      safefree(pstObj->pcObject);
      safefree(pstObj);
   }
}

//  
// Function:   html_Insert
// Purpose:    Insert HTML object
// 
// Parameters: HTML Object, data
// Returns:    HTML Object (might have changed)
// Note:       
//  
static RPIDATA *html_Insert(RPIDATA *pstObj, const char *pcData)
{
   RPIDATA *pstNew = pstObj;

   int   iIdx=pstNew->iIdx;
   int   iSize, iLen=GEN_STRLEN(pcData);

   //PRINTF("html_Insert():[%s]" CRLF, pcData);
   //
   if(iLen >= pstObj->iFree)
   {
      //
      // Calc min required size (allocate more)
      //
      iSize = pstObj->iSize + iLen - pstObj->iFree;
      PRINTF("html_Insert(%4d):Need more space: Free=%d" CRLF, iLen, pstNew->iFree);
      pstNew = html_AllocateObject(iSize + HTTP_DEFAULT_DATA_SIZE);
      GEN_STRNCPY(pstNew->pcObject, pstObj->pcObject, pstObj->iSize);
      // Copy new values or necessary values from old object
      pstNew->iIdx       = iIdx;
      pstNew->iFree      = pstNew->iFree - iIdx;
      pstNew->iLastComma = pstObj->iLastComma;
      html_FreeObject(pstObj);
   }
   GEN_STRNCPY(&pstNew->pcObject[iIdx], pcData, pstNew->iFree);
   pstNew->iIdx  += iLen;
   pstNew->iFree -= iLen;
   //
   PRINTF("html_Insert(%4d):Now: Size=%4d, Idx=%4d, Free=%4d" CRLF, iLen, pstObj->iSize, pstObj->iIdx, pstNew->iFree);
   return(pstNew);
}

