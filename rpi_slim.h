/*  (c) Copyright:  2020..2024  Patrn, Confidential Data
 *
 *  Workfile:           rpi_slim.h
 *  Purpose:            Handling dynamic HTTP pages for the Kinshi Smart Ultrasoon sensor
 *                      Ultrasoon sensor :  HC-SR04
 *                      Settings         :  9600 baud, 8 bits, no parity
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Changes:       
 *    01 Nov 2020:      Ported from rpislim
 *    23 Apr 2024:      Split globals.h and locals.h
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _RPI_SLIM_H_
#define _RPI_SLIM_H_

#define NUM_TARIFFS           4
#define MASK_TARIFF           0x03
//
//
#define  MIAC_MODE_NONE       0
#define  MIAC_MODE_ROM        1
#define  MIAC_MODE_RAM        2
//
void SLIM_InitDynamicPages    (int);

#endif /* _RPI_SLIM_H_ */
