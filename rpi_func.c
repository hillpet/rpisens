/*  (c) Copyright:  2020..2024  Patrn, Confidential Data
 *
 *  Workfile:           rpi_func.c
 *  Purpose:            Misc functions
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    01 Nov 2020:      Ported from rpislim
 *    12 Aug 2021:      Add Pump Mode command
 *    25 Mar 2022:      Add RPI_GetFlag()
 *    29 Mar 2022:      Add Variadic macro's for PRINTF()
 *    18 Aug 2022:      Add Misc Addr/Data callbacks
 *    24 Apr 2024:      Move to global/local startup 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sched.h>
#include <time.h>
//
#include <common.h>
#include "config.h"
#include "locals.h"
#include "globals.h"
//
#include "gen_http.h"
#include "rpi_page.h"
#include "rpi_json.h"
#include "rpi_comm.h"
#include "rpi_func.h"

//#define USE_PRINTF
#include <printx.h>

//
// Enums
//
enum
{
#define  EXTRACT_DYN(a,b,c,d,e,f)           a,
#include "pages.h"
#undef EXTRACT_DYN
   NUM_PAGES_DYNS
};
//
// Index list to global Arguments G_xxxx
//
static const HTTPARGS stHttpArguments[] =
{
//
//    Macro elements:               Example
//  
//       a  Enum              x     PAR_VERSION
//       b  iFunction         x     WB|PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_ALL
//       c  pcJson            x     "Version"
//       d  pcHtml            x     "vrs="
//       e  pcOption                "-v"
//       f  iValueOffset      x     offsetof(RPIMAP, G_pcSwVersion)
//       g  iValueSize        x     MAX_PARM_LEN
//       h  iChangedOffset    x     ALWAYS_CHANGED or offsetof(RPIMAP, G_ubVersionChanged)
//       i  Changed                 0
//       j  pcDefault               "v1.00-cr103"
//       k  pfHttpFun         x     http_CollectDebug
//
//       a                 b                                c           d        f                             g              h                                      k
//   
#define  EXTRACT_PAR(a,b,c,d,e,f,g,h,i,j,k)    {a,b,c,d,f,g,h,k},
#include "par_defs.h"
#undef   EXTRACT_PAR
   {     NUM_GLOBAL_DEFS,  0,                               NULL,       NULL,    0,                            0,             0,                                     NULL}
};


//
// Index list to global Rcu/Fp keys
//
static const char *pcRcuKeyLut[] =
{
#define  EXTRACT_KEY(a,b)    b,
#include "key_defs.h"
#undef   EXTRACT_KEY
         NULL
};

//
// Local prototypes
//
static bool       rpi_HandleParm          (char *, FTYPE, int);
static bool       rpi_ParmSetChanged      (const HTTPARGS *, bool);
//
#ifdef  FEATURE_JSON_SHOW_REPLY
static void rpi_ShowNetReply              (const char *, int, char *);
#define SHOW_NET_REPLY(a,b,c)             rpi_ShowNetReply(a,b,c)
#else
#define SHOW_NET_REPLY(a,b,c)
#endif  //FEATURE_JSON_SHOW_REPLY
//
static const char pcWebPageCommandBusy[]  = "<p>Raspicam Error: Ongoing cam action!</p>" NEWLINE;
static const char pcWebPageCommandBad[]   = "<p>Raspicam Error: Unknown command!</p>" NEWLINE;
//
static const char   *pcFltFile             = RPI_FLT_FILE;
static const char   *pcAmpAnd              = "&";
static const char   *pcParmDelims          = "=:";

/*------  Local functions separator ------------------------------------
__GLOBAL_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//
// Function:   RPI_BuildJsonMessageBody
// Purpose:    Build the JSON response object to be send back through the socket
//
// Parms:      Socket descriptor, variables mask
// Returns:    TRUE if OKee
//
bool RPI_BuildJsonMessageBody(NETCL *pstCl, int iArgs)
{
   bool     fCc;

   PRINTF("RPI_BuildJsonMessageBody()" CRLF);
   fCc = RPI_BuildJsonMessageArgs(pstCl, iArgs);
   return(fCc);
}

//
// Function:   RPI_BuildJsonMessageArgs
// Purpose:    Build the JSON response object to be send back through the socket
//
// Parms:      Socket descriptor, Args 
// Returns:    TRUE if OKee
//
bool RPI_BuildJsonMessageArgs(NETCL *pstCl, int iArgs)
{
   bool            fCc;
   int             x;
   char            cTemp[32];
   RPIDATA        *pstCam=NULL;
   const HTTPARGS *pstArgs=stHttpArguments;

   //
   // Build JSON object
   //
   pstCam = JSON_InsertParameter(pstCam, NULL, NULL, JE_CURLB|JE_CRLF);
   for(x=0; x<NUM_GLOBAL_DEFS; x++)
   {
      if(pstArgs->iFunction & iArgs)
      {
         if(pstArgs->iFunction & PAR_A)
         {
            char *pcValue;

            // These are ASCII parms
            pcValue = GLOBAL_GetGlobalParameter(x);
            //pwjh LOG_printf("RPI_BuildJsonMessageArgs():ASCII: Offset=0x%x %s=%p-->%s" CRLF, pstArgs->iValueOffset, pstArgs->pcJson, pcValue, pcValue);
            //
            // Add all parameters to JSON object
            //
            if(pstArgs->iFunction & JSN_TXT) pstCam = JSON_InsertParameter(pstCam, pstArgs->pcJson, pcValue, JE_TEXT|JE_COMMA|JE_CRLF);
            else                             pstCam = JSON_InsertParameter(pstCam, pstArgs->pcJson, pcValue,         JE_COMMA|JE_CRLF);
         }
         if(pstArgs->iFunction & PAR_B)
         {
            int  *piValue;

            // These are Int-BCD parms
            piValue = GLOBAL_GetGlobalParameter(x);
            GEN_SNPRINTF(cTemp, 31, "%d", *piValue);
            //pwjh LOG_printf("RPI_BuildJsonMessageArgs():BCD-I: Offset=0x%x %s=%p-->%d" CRLF, pstArgs->iValueOffset, pstArgs->pcJson, piValue, *piValue);
            //
            // Add all parameters to JSON object
            //
            if(pstArgs->iFunction & JSN_TXT) pstCam = JSON_InsertParameter(pstCam, pstArgs->pcJson, cTemp, JE_TEXT|JE_COMMA|JE_CRLF);
            else                             pstCam = JSON_InsertParameter(pstCam, pstArgs->pcJson, cTemp,         JE_COMMA|JE_CRLF);
         }
         if(pstArgs->iFunction & PAR_H)
         {
            int  *piValue;

            // These are Int-HEX parms
            piValue = GLOBAL_GetGlobalParameter(x);
            GEN_SNPRINTF(cTemp, 31, "0x%x", *piValue);
            //pwjh LOG_printf("RPI_BuildJsonMessageArgs():BCD-H: Offset=0x%x %s=%p-->0x%x" CRLF, pstArgs->iValueOffset, pstArgs->pcJson, piValue, *piValue);
            //
            // Add all parameters to JSON object
            //
            if(pstArgs->iFunction & JSN_TXT) pstCam = JSON_InsertParameter(pstCam, pstArgs->pcJson, cTemp, JE_TEXT|JE_COMMA|JE_CRLF);
            else                             pstCam = JSON_InsertParameter(pstCam, pstArgs->pcJson, cTemp,         JE_COMMA|JE_CRLF);
         }
         if(pstArgs->iFunction & PAR_F)
         {
            double  *pflValue;

            // These are float parms
            pflValue = GLOBAL_GetGlobalParameter(x);
            GEN_SNPRINTF(cTemp, 31, "%.1f", *pflValue);
            //pwjh LOG_printf("RPI_BuildJsonMessageArgs():DOUBL: Offset=0x%x %s=%p-->%f" CRLF, pstArgs->iValueOffset, pstArgs->pcJson, pflValue, *pflValue);
            //
            // Add all parameters to JSON object
            //
            if(pstArgs->iFunction & JSN_TXT) pstCam = JSON_InsertParameter(pstCam, pstArgs->pcJson, cTemp, JE_TEXT|JE_COMMA|JE_CRLF);
            else                             pstCam = JSON_InsertParameter(pstCam, pstArgs->pcJson, cTemp,         JE_COMMA|JE_CRLF);
         }
      }
      pstArgs++;
   }
   pstCam = JSON_TerminateObject(pstCam);
   fCc    = JSON_RespondObject(pstCl, pstCam);
   //
   // Done with this function: exit
   //
   JSON_ReleaseObject(pstCam);
   return(fCc);
}

//
// Function:   RPI_CollectParms
// Purpose:    Collect PiCam parameter passing from HTTP-data into the MMAP global area
//
// Parms:      Client area, HTTP-type
// Returns:    Nr of parms found
// Note:       
//             pstCl->pcUrl-> "/all/public/www/cam.json?Command=snapshot&Height=576&Ext=bmp&Preview=860,0,860,540&Rotation=180&Width=1024"
//                             |               |   |    |
//             iPathIdx = -----+               |   |    |
//             iFileIdx = ---------------------+   |    |
//             iExtnIdx = -------------------------+    |
//             iPaylIdx = ------------------------------+
//  
int RPI_CollectParms(NETCL *pstCl, FTYPE tType)
{
   int             iNr=0;
   char           *pcParm;
   const HTTPARGS *pstArgs=stHttpArguments;

   //
   // Set first parameter
   //
   pstCl->iNextIdx = pstCl->iPaylIdx;
   //
   // Before collecting new parms, reset all changed markers
   //
   while(pstArgs->iFunction)
   {
      rpi_ParmSetChanged(pstArgs, 0);
      pstArgs++;
   }

#ifdef FEATURE_HTML_DEFAULT_COMMAND
   //
   // HTML syntax has the command first:
   // so:   cam.html?snapshot&w=400
   // else: cam.html?cmd=snapshot&w=400
   //
   if(tType == HTTP_HTML)
   {
      if( (pcParm = HTTP_CollectGetParameter(pstCl)) )
      {
         iNr++;
         GEN_STRNCPY(pstMap->G_pcCommand, pcParm, MAX_PARM_LEN);
      }
   }
#endif   //FEATURE_HTML_DEFAULT_COMMAND
   //
   // Retrieve and store all arguments
   //
   do
   {
      pcParm = HTTP_CollectGetParameter(pstCl);
      if(pcParm)
      {
         iNr++;
         rpi_HandleParm(pcParm, tType, PAR_ALL);
      }
   }
   while(pcParm);
   //
   PRINTF("RPI_CollectParms(): %d arguments found" CRLF, iNr);
   return(iNr);
}

// 
// Function:   RPI_GetFlag
// Purpose:    Decode the flag from the G_ PAR_ area
// 
// Parameters: Flag
// Returns:    Flag value
// Note:       
//
bool RPI_GetFlag(int iFlag)
{
   int      iFlags;

   iFlags = RPI_GetFlags();
   return((iFlags & iFlag) == iFlag);
}

// 
// Function:   RPI_GetFlags
// Purpose:    Get all flags from the G_ PAR_ area
// 
// Parameters: 
// Returns:    Flags
// Note:       
//
int RPI_GetFlags()
{
   int     *piFlags;
   int      iFlags;

   piFlags = GLOBAL_GetGlobalParameter(PAR_VERBOSE);
   iFlags  = *piFlags;
   iFlags &= ~VERBOSE_LEVEL_MASK;
   return(iFlags);
}

//
// Function:   RPI_ReportBusy
// Purpose:    Report that the server thread is busy
//
// Parms:      Socket descriptor
// Returns:    
// Note:       
//             
void RPI_ReportBusy(NETCL *pstCl)
{
   int      iIdx, tType;

   iIdx  = pstMap->G_iCurDynPage;
   tType = GEN_GetDynamicPageType(iIdx);        // HTTP_HTML, HTTP_JSON
   //
   switch(tType)
   {
      default:
      case HTTP_HTML:
         // Use HTML/JS API
         HTTP_BuildRespondHeader(pstCl);        //"HTTP/1.0 200 OK" NEWLINE "Content-Type=text/html" NEWLINE NEWLINE;
         HTTP_BuildStart(pstCl);                // Title - eyecandy
         HTTP_BuildGeneric(pstCl, pcWebPageCommandBusy);
         HTTP_BuildLineBreaks(pstCl, 1);
         HTTP_BuildEnd(pstCl);
         break;
   
      case HTTP_JSON:
         // Use JSON API
         RPI_BuildJsonMessageBody(pstCl, pstMap->G_iHttpArgs);
         break;
   }
}

//
// Function:   RPI_ReportError
// Purpose:    Report error
//
// Parms:      Socket descriptor
// Returns:    
// Note:       
//             
void RPI_ReportError(NETCL *pstCl)
{
   int      iIdx, tType;

   iIdx  = pstMap->G_iCurDynPage;
   tType = GEN_GetDynamicPageType(iIdx);        // HTTP_HTML, HTTP_JSON
   //
   switch(tType)
   {
      default:
      case HTTP_HTML:
         // Use HTML/JS API
         LOG_Report(0, "RPI", "RPI_ReportError():HTML");
         PRINTF("RPI_ReportError():HTML" CRLF);
         HTTP_BuildRespondHeader(pstCl);        //"HTTP/1.0 200 OK" NEWLINE "Content-Type=text/html" NEWLINE NEWLINE;
         HTTP_BuildStart(pstCl);                // Title - eyecandy
         HTTP_BuildGeneric(pstCl, pcWebPageCommandBad);
         HTTP_BuildLineBreaks(pstCl, 1);
         HTTP_BuildEnd(pstCl);
         break;
   
      case HTTP_JSON:
         // Use JSON API
         LOG_Report(0, "RPI", "RPI_ReportError():JSON");
         PRINTF("RPI_ReportError():JSON" CRLF);
         RPI_BuildJsonMessageBody(pstCl, pstMap->G_iHttpArgs);
         break;
   }
}

//
// Function:   RPI_RestoreDay
// Purpose:    Restore a day from file
//
// Parms:      Secs timestamp, Buffer ptr, Buffer size
// Returns:    TRUE if correctly restored the day data
// Note:
//
bool RPI_RestoreDay(u_int32 ulSecs, u_int8 *pubData, int iSize)
{
   bool     fCc=FALSE;
   int      iLen, tFile;
   char    *pcFile, cSrc;
   char    *pcTemp;
   
   if(ulSecs)
   {
      //
      // Build bin file "/WwwDir/SmartE_yyyymmdd.bin"
      //
      iLen   = RTC_ConvertDateTimeSize(TIME_FORMAT_YYYY_MM_DD);
      iLen  += 32;
      iLen  += GEN_STRLEN(pstMap->G_pcWwwDir);
      iLen  += GEN_STRLEN(pcFltFile);
      pcFile = safemalloc(iLen);
      pcTemp = safemalloc(iLen);
      //
      RTC_ConvertDateTime(TIME_FORMAT_YYYY_MM_DD, pcTemp, ulSecs);
      //
      GEN_SNPRINTF(pcFile, iLen, pcFltFile, pstMap->G_pcWwwDir, pcTemp);
      //
      FINFO_GetFileInfo((char *)pcFile, FI_FILEDIR, &cSrc, 1);
      if(cSrc == 'F')
      {
         PRINTF("RPI-RestoreDay():Open %s" CRLF, pcFile);
         tFile = safeopen(pcFile, O_RDONLY);
         if(tFile > 0)
         {
            saferead(tFile, pubData, iSize);
            safeclose(tFile);
            PRINTF("RPI-RestoreDay():Day-file %s OKee" CRLF, pcFile);
            LOG_Report(0, "SMA", "slim_RestoreDay():Day-file %s OKee", pcFile);
            fCc = TRUE;
         }
         else
         {
            LOG_Report(errno, "SMA", "slim_RestoreDay():Error opening day %s!", pcFile);
            PRINTF("RPI-RestoreDay():Error opening day %s" CRLF, pcFile);
            GEN_MEMSET(pubData, 0, iSize);
         }
      }
      else 
      {
         LOG_Report(errno, "SMA", "slim_RestoreDay():No such day %s !", pcFile);
         PRINTF("RPI-RestoreDay():No such day %s" CRLF, pcFile);
         GEN_MEMSET(pubData, 0, iSize);
      }
      safefree(pcFile);
      safefree(pcTemp);
   }
   return(fCc);
}

//
// Function:   RPI_RestoreMonth
// Purpose:    Restore a month from file
//
// Parms:      Secs timestamp, buffer, size
// Returns:
// Note: 
//
bool RPI_RestoreMonth(u_int32 ulSecs, u_int8 *pubData, int iSize)
{
   bool     fCc=FALSE;
   int      iLen, tFile;
   char    *pcFile, cSrc;
   char    *pcTemp;
   
   if(ulSecs)
   {
      //
      // Build bin file "/WwwDir/SmartE_yyyymm.bin"
      //
      iLen   = RTC_ConvertDateTimeSize(TIME_FORMAT_YYYY_MM);
      iLen  += 32;
      iLen  += GEN_STRLEN(pstMap->G_pcWwwDir);
      iLen  += GEN_STRLEN(pcFltFile);
      pcFile = safemalloc(iLen);
      pcTemp = safemalloc(iLen);
      //
      RTC_ConvertDateTime(TIME_FORMAT_YYYY_MM, pcTemp, ulSecs);
      //
      GEN_SNPRINTF(pcFile, iLen, pcFltFile, pstMap->G_pcWwwDir, pcTemp);
      PRINTF("RPI-RestoreMonth():Sec=%d  [%s]" CRLF, ulSecs, pcTemp);
      //
      FINFO_GetFileInfo((char *)pcFile, FI_FILEDIR, &cSrc, 1);
      if(cSrc == 'F')
      {
         PRINTF("RPI-RestoreMonth():Open %s" CRLF, pcFile);
         tFile = safeopen(pcFile, O_RDONLY);
         if(tFile > 0)
         {
            saferead(tFile, pubData, iSize);
            safeclose(tFile);
            PRINTF("RPI-RestoreMonth():Month-file %s OKee" CRLF, pcFile);
            LOG_Report(0, "SMA", "RPI-RestoreMonth():Month-file %s OKee",  pcFile);
            fCc = TRUE;
         }
         else
         {
            PRINTF("RPI-RestoreMonth():ERROR opening month %s" CRLF, pcFile);
            GEN_MEMSET(pubData, 0, iSize);
         }
      }
      else 
      {
         PRINTF("RPI-RestoreMonth():No such month %s" CRLF, pcFile);
         GEN_MEMSET(pubData, 0, iSize);
      }
      safefree(pcFile);
      safefree(pcTemp);
   }
   return(fCc);
}

//
// Function:   RPI_SaveDay
// Purpose:    Save a day to file
//
// Parms:      Secs timestamp, dataptr
// Returns:
// Note:
//
bool RPI_SaveDay(u_int32 ulSecs, u_int8 *pubData, int iSize)
{
   bool     fCc=FALSE;
   int      iLen, tFile;
   char    *pcFile;
   char    *pcTemp;
   
   if(ulSecs)
   {
      //
      // Build bin file "/WwwDir/SmartE_yyyymmdd.bin"
      //
      iLen   = RTC_ConvertDateTimeSize(TIME_FORMAT_YYYY_MM_DD);
      iLen  += 32;
      iLen  += GEN_STRLEN(pstMap->G_pcWwwDir);
      iLen  += GEN_STRLEN(pcFltFile);
      pcFile = safemalloc(iLen);
      pcTemp = safemalloc(iLen);
      //
      RTC_ConvertDateTime(TIME_FORMAT_YYYY_MM_DD, pcTemp, ulSecs);
      //
      GEN_SNPRINTF(pcFile, iLen, pcFltFile, pstMap->G_pcWwwDir, pcTemp);
      PRINTF("RPI-SaveDay():Open %s" CRLF, pcFile);
      //
      tFile = safeopen2(pcFile, O_RDWR|O_CREAT, (S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH));
      if(tFile > 0)
      {
         safewrite(tFile, (char *)pubData, iSize);
         LOG_Report(0, "SMA", "RPI-SaveDay():Day-file %s OKee",  pcFile);
         fCc = TRUE;
      }
      else
      {
         PRINTF("RPI-EmtrSaveDay():ERROR write day file" CRLF);
         LOG_Report(errno, "SMA", "RPI-SaveDay():ERROR write day file");
      }
      safefree(pcFile);
      safefree(pcTemp);
   }
   else
   {
      LOG_Report(0, "SMA", "RPI-SaveDay():Skip first initial day !");
   }
   return(fCc);
}

//
// Function:   RPI_SaveMonth
// Purpose:    Save a month to file
//
// Parms:      Secs timestamp, dataptr
// Returns:
// Note: 
//
bool RPI_SaveMonth(u_int32 ulSecs, u_int8 *pubData, int iSize)
{
   bool     fCc=FALSE;
   int      iLen, tFile;
   char    *pcFile;
   char    *pcTemp;
   
   if(ulSecs)
   {
      //
      // Build bin file "/WwwDir/SmartE_yyyymm.bin"
      //
      iLen   = RTC_ConvertDateTimeSize(TIME_FORMAT_YYYY_MM);
      iLen  += 32;
      iLen  += GEN_STRLEN(pstMap->G_pcWwwDir);
      iLen  += GEN_STRLEN(pcFltFile);
      pcFile = safemalloc(iLen);
      pcTemp = safemalloc(iLen);
      //
      RTC_ConvertDateTime(TIME_FORMAT_YYYY_MM, pcTemp, ulSecs);
      //
      GEN_SNPRINTF(pcFile, iLen, pcFltFile, pstMap->G_pcWwwDir, pcTemp);
      PRINTF("RPI-SaveMonth():Sec=%d  [%s]" CRLF, ulSecs, pcTemp);
      PRINTF("RPI-SaveMonth():Open %s" CRLF, pcFile);
      //
      tFile = safeopen2(pcFile, O_RDWR|O_CREAT, (S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH));
      if(tFile > 0)
      {
         safewrite(tFile, (char *)pubData, iSize);
         LOG_Report(0, "SMA", "RPI-SaveMonth():Month-file %s OKee",  pcFile);
         fCc = TRUE;
      }
      else
      {
         PRINTF("RPI-SaveMonth():ERROR write month file" CRLF);
         LOG_Report(errno, "SMA", "RPI-SaveMonth():ERROR write month file");
      }
      safefree(pcFile);
      safefree(pcTemp);
   }
   else
   {
      LOG_Report(0, "SMA", "RPI-SaveMonth():Skip first initial month !");
   }
   return(fCc);
}


/*------  Local functions separator ------------------------------------
__LOCAL_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//
// Function:   rpi_HandleParm
// Purpose:    Lookup an argement in the main Args list and copy its value (if any) into the Global mmap area
//
// Parms:      Ptr tp parm "XXX=500", HTTP_HTML|HTTP_JSON|..., PAR_xxx
// Returns:    TRUE if parm OKee
// Note:       
//             
static bool rpi_HandleParm(char *pcParm, FTYPE tType, int iFunction)
{
   bool            fCc=FALSE;
   int             iSize, x;
   const char     *pcArg;
   const HTTPARGS *pstArgs=stHttpArguments;
   HTTPFUN         pfCallback;

   PRINTF("rpi-HandleParm(): Parm=<%s>" CRLF, pcParm);
   //
   for(x=0; x<NUM_GLOBAL_DEFS; x++)
   {
      //
      // iFunction is the bitwise function-filter (PAR_xxx)
      //
      if(pstArgs->iFunction & iFunction)
      {
         switch(tType)
         {
            case HTTP_JSON:   
               pcArg = pstArgs->pcJson; 
               break;
            
            case HTTP_HTML:
            default:          
               pcArg = pstArgs->pcHtml; 
               break;
         }
         //
         // Check parameter_to_variable delimiter "=" or ":"
         //
         iSize = GEN_SizeToDelimiters(pcParm, (char *)pcParmDelims);
         if(iSize == 0)
         {
            //
            // No delimiter: check variable without value (flag, ...)
            //
            iSize = GEN_SizeToDelimiters(pcParm, (char *)pcAmpAnd);
         }
         if( (iSize > 0) && (GEN_STRNCMPI(pcParm, pcArg, iSize) == 0) )
         {
            PRINTF("rpi-HandleParm(): cmp(%s,%s,%d) match" CRLF, pcArg, pcParm, iSize);
            //
            // Option found: copy value
            //
            pcArg = GEN_FindDelimiters(pcParm, (char *)pcParmDelims);
            if(pcArg)
            {
               if(pstArgs->iFunction & PAR_A)
               {
                  char *pcValue;

                  // These are ASCII parms
                  pcValue = GLOBAL_GetGlobalParameter(x);
                  GEN_CopyToDelimiter((char)*pcAmpAnd, pcValue, (char *)pcArg, pstArgs->iValueSize);
                  rpi_ParmSetChanged(pstArgs, 1);
                  PRINTF("rpi-HandleParm(): Ascii: Opt=<%s>:Value=<%s>" CRLF, pcParm, pcValue);
                  fCc = TRUE;
               }
               if(pstArgs->iFunction & PAR_B)
               {
                  int  *piValue;

                  // These are Int-BCD parms
                  piValue = GLOBAL_GetGlobalParameter(x);
                 *piValue = (int)strtol(pcArg, NULL, 10);
                  rpi_ParmSetChanged(pstArgs, 1);
                  PRINTF("rpi-HandleParm(): BCD-Int: Opt=<%s>:Value=%d" CRLF, pcParm, *piValue);
                  fCc = TRUE;
               }
               if(pstArgs->iFunction & PAR_H)
               {
                  int  *piValue;

                  // These are Int-HEX parms
                  piValue = GLOBAL_GetGlobalParameter(x);
                 *piValue = (int)strtol(pcArg, NULL, 16);
                  PRINTF("rpi-HandleParm(): BCD-Hex: Opt=<%s>:Value=0x%x" CRLF, pcParm, *piValue);
                  fCc = TRUE;
               }
               if(pstArgs->iFunction & PAR_F)
               {
                  double  *pflValue;

                  // These are float parms
                  pflValue = GLOBAL_GetGlobalParameter(x);
                 *pflValue = (double)strtod(pcArg, NULL);
                  PRINTF("rpi-HandleParm(): Float: Opt=<%s>:Value=%f" CRLF, pcParm, *pflValue);
                  fCc = TRUE;
               }
            }
            else
            {
               //
               // ParameterX without value:
               //    "ParameterX&ParameterY=12033"
               //    "ParameterX"
               //
               if(pstArgs->iFunction & PAR_A)
               {
                  char *pcValue;

                  // These are ASCII parms
                  pcValue = GLOBAL_GetGlobalParameter(x);
                  GEN_STRCPY(pcValue, "1");
                  rpi_ParmSetChanged(pstArgs, 1);
                  PRINTF("rpi-HandleParm(): Ascii: Opt:<%s>:No Value:force <1> or TRUE" CRLF, pcParm);
                  fCc = TRUE;
               }
               if(pstArgs->iFunction & PAR_B)
               {
                  int  *piValue;

                  // These are Int-BCD parms
                  piValue = GLOBAL_GetGlobalParameter(x);
                 *piValue = 1;
                  rpi_ParmSetChanged(pstArgs, 1);
                  PRINTF("rpi-HandleParm(): BCD-Int: Opt:<%s>:No Value:force <1> or TRUE" CRLF, pcParm);
                  fCc = TRUE;
               }
               if(pstArgs->iFunction & PAR_H)
               {
                  int  *piValue;

                  // These are Int-HEX parms
                  piValue = GLOBAL_GetGlobalParameter(x);
                 *piValue = 1;
                  PRINTF("rpi-HandleParm(): BCD-Hex: Opt:<%s>:No Value:force <1> or TRUE" CRLF, pcParm);
                  fCc = TRUE;
               }
               if(pstArgs->iFunction & PAR_F)
               {
                  double  *pflValue;

                  // These are float parms
                  pflValue = GLOBAL_GetGlobalParameter(x);
                 *pflValue = 1.0;
                  PRINTF("rpi-HandleParm(): Double: Opt:<%s>:No Value:force <1> or TRUE" CRLF, pcParm);
                  fCc = TRUE;
               }

               #ifdef COMMENT
               pcValue = (char *)pstMap + pstArgs->iValueOffset;
               GEN_STRCPY(pcValue, "1");
               rpi_ParmSetChanged(pstArgs, 1);
               PRINTF("rpi-HandleParm(): Opt:<%s>:No Value:force <1> or TRUE" CRLF, pcParm);
               fCc = TRUE;
               #endif   //COMMENT
            }
            //
            // Call the parameter callback function, if any
            //
            pfCallback = pstArgs->pfHttpFun;
            if(pfCallback) 
            {
               PRINTF("rpi-HandleParm(): Id=%d" CRLF, pstArgs->iId);
               fCc = pfCallback(pstArgs->iId);
            }
            //
            // Arg found: no need to search for more !
            //
            break;
         }
         else
         {
            //PRINTF("rpi-HandleParm(): cmp(%s,%s,%d) NO match" CRLF, pcArg, pcParm, iSize);
         }
      }
      pstArgs++;
   }
   return(fCc);
}

//
// Function:   rpi_ParmSetChanged
// Purpose:    Set the parm changed marker
//
// Parms:      Parm ^, set/unset
// Returns:    TRUE if parm has changed successfully
// Note:       Some parms cannot be changed (NEVER_CHANGED)
//             Some are always volatile (ALWAYS_CHANGED)
//
static bool rpi_ParmSetChanged(const HTTPARGS *pstArgs, bool fChanged)
{
   u_int8     *pubChanged;
   int         iChangedOffset=pstArgs->iChangedOffset;

   switch(iChangedOffset)
   {
      case ALWAYS_CHANGED:
         // Do not alter the change marker
         break;

      case NEVER_CHANGED:
         // Cannot alter the change marker
         if(fChanged)  fChanged = FALSE;
         break;

      default:
         pubChanged = (u_int8 *)pstMap + iChangedOffset;
        *pubChanged = (u_int8)fChanged;
         fChanged   = TRUE;
         break;
   }
   return(fChanged);
}

#ifdef  FEATURE_JSON_SHOW_REPLY
/*
 * Function    : rpi_ShowNetReply
 * Description : Print out the network json payload reply
 *
 * Parameters  : Title, port, JSON Object
 * Returns     : 
 *
 */
static void rpi_ShowNetReply(const char *pcTitle, int iPort, char *pcData)
{
   LOG_printf("%s(p=%d):Data=%s" CRLF, pcTitle, iPort, pcData);
}
#endif   //FEATURE_JSON_SHOW_REPLY


/*------  Local functions separator ------------------------------------
__CALLBACK_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//
// Function:   http_CollectDebug
// Purpose:    Callback function G_pcDebugMask
//
// Parms:      Parameter Enum
// Returns:    TRUE if OKee
// Note:       
//             
bool http_CollectDebug(int iId)
{
   GLOBAL_ConvertDebugMask(TRUE);
   //
   PRINTF("http-CollectDebug(): ID=%ld" CRLF, iId);
   return(TRUE);
}

//
// Function:   http_CollectRcuKey
// Purpose:    Callback function or remote RCU key
//
// Parms:      Parameter Enum
// Returns:    TRUE if found
// Note:       
//             
bool http_CollectRcuKey(int iId)
{
   bool     fCc=FALSE;
   int      iIdx, iLen;

   for(iIdx=0; iIdx<NUM_RCU_KEYS; iIdx++)
   {
      iLen = GEN_STRLEN(pcRcuKeyLut[iIdx]);
      if(GEN_STRNCMPI(pcRcuKeyLut[iIdx], pstMap->G_pcRcuKey, iLen) == 0)
      {
         LOG_Report(0, "FPK", "http-CollectRcuKey(): ID=%ld, Key=%d (%s)", iId, iIdx, pcRcuKeyLut[iIdx]);
         pstMap->G_iRcuKey = iIdx;
         GLOBAL_Signal(PID_SRVR, SIGUSR1);
         fCc = TRUE;
         break;
      }
   }
   return(fCc);
}

//
// Function:   http_CollectPumpMode
// Purpose:    Callback function to change the current pump mode
//
// Parms:      Parameter Enum
// Returns:    TRUE if found
// Note:       
//             
bool http_CollectPumpMode(int iId)
{
   LOG_Report(0, "RPI", "http-CollectPumpMode(%d):Request pump mode 0x%02X", iId, pstMap->G_iPumpMode);
   pstMap->G_stCmd.iCommand = SMCMD_HTTP_PUMP;
   GLOBAL_Signal(PID_COMM, SIGUSR2);
   return(TRUE);
}

//
// Function:   http_CollectMiacCommand
// Purpose:    Callback function to supply MIAC command for the uCtl
//
// Parms:      Parameter Enum
// Returns:    TRUE if found
// Note:       
//             
bool  http_CollectMiacCommand(int iId)
{
   LOG_Report(0, "RPI", "http-CollectMiacCommand():%s", pstMap->G_pcMiacCmd);
   pstMap->G_stCmd.iCommand = SMCMD_HTTP_MIAC;
   GLOBAL_Signal(PID_COMM, SIGUSR2);
   return(TRUE);
}

//
// Function:   http_CollectMiacUpdate
// Purpose:    Callback function to set the UpdateTimer for the uCtl MIAC downloads
//
// Parms:      Parameter Enum
// Returns:    TRUE if found
// Note:       PAR_MIAC_MEM      parms.json?MiacMem=10   // Set Memory update timer 10 Secs
//             PAR_MIAC_ROMA     rom?roma=0x10           // Set ROM address
//             PAR_MIAC_RAMA     ram?rama=0x23           // Set RAM address
//             PAR_MIAC_DATA     ram?data=0xff           // Write data to address
//             
bool  http_CollectMiacUpdate(int iId)
{
   bool     fRama=FALSE, fRoma=FALSE;
   int      iAddr;
   u_int8   ubData;

   switch(iId)
   {
      case PAR_MIAC_MEM:
         PRINTF("http-CollectMiacUpdate():Memory=%d" CRLF, pstMap->G_iMiacMemory);
         LOG_Report(0, "RPI", "http-CollectMiacUpdate():Memory=%d", pstMap->G_iMiacMemory);
         GLOBAL_Notify(PID_HOST, GLOBAL_SVR_HST_TMR, SIGUSR1);
         break;

      case PAR_MIAC_RAMA:
         PRINTF("http-CollectMiacUpdate():Ram Addr=0x%03x" CRLF, pstMap->G_iRamAddr);
         LOG_Report(0, "RPI", "http-CollectMiacUpdate():Ram Addr=0x%03x", pstMap->G_iRamAddr);
         fRama = TRUE;
         break;

      case PAR_MIAC_ROMA:
         PRINTF("http-CollectMiacUpdate():Eeprom Addr=0x%03x" CRLF, pstMap->G_iRomAddr);
         LOG_Report(0, "RPI", "http-CollectMiacUpdate():Eeprom Addr=0x%03x", pstMap->G_iRomAddr);
         fRoma = TRUE;
         break;

      case PAR_MIAC_DATA:
         ubData = (u_int8) (pstMap->G_iMiacData & 0xFF);
         //
         // Update RAM or EEPROM image
         //
         if(fRama)
         {
            iAddr = pstMap->G_iRamAddr;
            if( (iAddr >= 0) && (iAddr < KINS_RAM_LEN) )
            {
               PRINTF("http-CollectMiacUpdate():RAM 0x%03x=0x%02x" CRLF, iAddr, ubData);
               LOG_Report(0, "RPI", "http-CollectMiacUpdate():RAM 0x%03x=0x%02x", iAddr, ubData);
               pstMap->G_ubRamImage[iAddr] = ubData;
            }
         }
         if(fRoma)
         {
            iAddr = pstMap->G_iRomAddr;
            if( (iAddr >= 0) && (iAddr < KINS_ROM_LEN) )
            {
               PRINTF("http-CollectMiacUpdate():ROM 0x%03x=0x%02x" CRLF, iAddr, ubData);
               LOG_Report(0, "RPI", "http-CollectMiacUpdate():ROM 0x%03x=0x%02x", iAddr, ubData);
               pstMap->G_ubRomImage[iAddr] = ubData;
            }
         }
         GLOBAL_Notify(PID_HOST, GLOBAL_SVR_HST_MEM, SIGUSR1);
         break;
   }
   return(TRUE);
}
