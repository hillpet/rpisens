/*  (c) Copyright:  2020..2024  Patrn, Confidential Data 
 *
 *  Workfile:           rpi_miac.c
 *  Purpose:            Smart Sensor RAM/EEPROM Memory interface
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    30 Nov 2020:      Created
 *    22 Mar 2022:      Fix MIAC_Get/SetRamMemory() bug
 *    29 Mar 2022:      Add Variadic macro's for PRINTF()
 *    24 Apr 2024:      Move to global/local startup 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sched.h>
#include <time.h>
//
#include <common.h>
#include "config.h"
#include "locals.h"
#include "globals.h"
//
#include "rpi_func.h"
#include "rpi_miac.h"

//#define USE_PRINTF
#include <printx.h>

//
// Local generic prototypes
//
static bool    miac_AddressRom               (char *, u_int8 *);
static bool    miac_AddressRam               (char *, u_int8 *);
static u_int8  miac_FindMemoryValue          (char *);
static int     miac_GetValue                 (char *, int *);
static bool    miac_LookupSymbol             (char *);
static char    miac_ParseMemory              ();
//
// Local prototypes
//
static const SMMEM stMemoryDefinition[] =
{
#define EXTRACT_MIAC(a,b,c,d) {a,b,c,d},
#include "miac.h"
#undef   EXTRACT_MIAC
//
   {  MIAC_NONE,  NULL,    NULL,    NULL }
};

//
// RAM/EEPROM lookup table
//
static u_int8  ubMiacMemory[MAX_NUM_MIAC];

/*------  Local functions separator ------------------------------------
__GLOBAL_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//  
// Function:   MIAC_Init
// Purpose:    Init the symbol lookup 
//
// Parameters: Xref filename
// Returns:    Number of symbola found
// Note:       
//  
int MIAC_Init(const char *pcFile)
{
   FILE *ptFile;
   int   iNr=0;
   char *pcRecord;
   char *pcRes;

   GEN_MEMSET(ubMiacMemory, 0x00, sizeof(ubMiacMemory));
   //
   // Open Symbol file and extract the required symbols:
   // Record:        "b0Task01                  00000021 \r\n"
   // Definition:    Enum                       pcSymbol             pfHandler               pcInfo
   //                MIAC_R_TASK01,             "b0Task01",          miac_AddressRam,        "Pump Control"
   // Found:
   // Call handler function with <MIAC_R_TASK01> 
   // Store actual address <00000021> in lookup table:
   //    ubMiacMemory[MIAC_R_TASK01] = 0x21
   //
   ptFile = safefopen((char *)pcFile, "r");
   if(ptFile)
   {
      pcRecord = safemalloc(MAX_REC_LENZ);
      do
      {
         pcRes = fgets(pcRecord, MAX_REC_LEN, ptFile);
         if(pcRes)
         {
            if( miac_LookupSymbol(pcRes) ) 
            {
               // We have successfully looked up the symbol: count it
               iNr++;
            }
         }
      }
      while(pcRes);
      safefree(pcRecord);
      safefclose(ptFile);
   }
   else
   {
      LOG_Report(errno, "MIA", "MIAC_Init():Xref file %s open ERROR ", pcFile);
   }
   return(iNr);
}

//  
// Function:   MIAC_GetEepromMemory
// Purpose:    Get a EEPROM memory value from the symbol lookup table
//
// Parameters: Enum MIACID
// Returns:    TRUE if OKee
// Note:       
//  
bool MIAC_GetEepromMemory(MIACID tMem, u_int8 *pubVal)
{
   bool     fCc=FALSE;
   u_int8   ubAddr;

   if(tMem < MAX_NUM_MIAC)
   {
      ubAddr  = ubMiacMemory[tMem];
      *pubVal = pstMap->G_ubRomImage[ubAddr];
      fCc     = TRUE;
   }
   return(fCc);
}

//  
// Function:   MIAC_GetEepromMemoryAddress
// Purpose:    Get the ROM memory address from the symbol lookup table
//
// Parameters: Enum MIACID
// Returns:    Address or -1 on error
// Note:       
//  
int MIAC_GetEepromMemoryAddress(MIACID tMem)
{
   int   iAddr=-1;

   if(tMem < MAX_NUM_MIAC)
   {
      iAddr = (int)ubMiacMemory[tMem];
   }
   return(iAddr);
}

//  
// Function:   MIAC_GetRamMemory
// Purpose:    Get a RAM memory value from the symbol lookup table
//
// Parameters: Enum MIACID
// Returns:    TRUE if OKee
// Note:       
//  
bool MIAC_GetRamMemory(MIACID tMem, u_int8 *pubVal)
{
   bool     fCc=FALSE;
   u_int8   ubAddr;

   if(tMem < MAX_NUM_MIAC)
   {
      ubAddr  = ubMiacMemory[tMem];
      *pubVal = pstMap->G_ubRamImage[ubAddr];
      fCc     = TRUE;
   }
   return(fCc);
}

//  
// Function:   MIAC_GetRamMemoryAddress
// Purpose:    Get the RAM memory address from the symbol lookup table
//
// Parameters: Enum MIACID
// Returns:    Address or -1 on error
// Note:       
//  
int MIAC_GetRamMemoryAddress(MIACID tMem)
{
   int   iAddr=-1;

   if(tMem < MAX_NUM_MIAC)
   {
      iAddr = (int)ubMiacMemory[tMem];
   }
   return(iAddr);
}

//  
// Function:   MIAC_ParseEepromMemory
// Purpose:    Parse buffer with EEPROM memory
//
// Parameters: 
// Returns:    TRUE if OKee
// Note:       The data received from the Smart Sensor Controller is:
//                "B=00\n\r"
//                "00=xx xx xx .....\n\r"
//                "70=xx xx xx .....\n\r"
//                "!xxxx\n\r"
//                "E>"
//  
bool MIAC_ParseEepromMemory()
{
   bool     fCc=FALSE;
   u_int8  *pubRom;

   pubRom = safemalloc(KINS_ROM_LEN);
   switch(miac_ParseMemory(pubRom, KINS_ROM_LEN))
   {
      default:
      case 'S':
         // Buffer syntax error
         if(RPI_GetFlag(VERBOSE_LOG_MIAC)) LOG_Report(0, "MIA", "MIAC-ParseEepromMemory():Syntax Error");
         break;

      case 'C':
         // Buffer checksum error
         if(RPI_GetFlag(VERBOSE_LOG_MIAC)) LOG_Report(0, "MIA", "MIAC-ParseEepromMemory():CRC Error");
         break;

      case 'R':
         if(RPI_GetFlag(VERBOSE_LOG_MIAC)) LOG_Report(0, "MIA", "MIAC-ParseEepromMemory():ERROR:Data has RAM content");
         break;

      case 'E':
         // Correct type of memory: save it
         if(RPI_GetFlag(VERBOSE_LOG_MIAC)) LOG_ListData("MIAC-ParseEepromMemory():OK", (char *)pubRom, KINS_ROM_LEN);
         GEN_MEMCPY(pstMap->G_ubRomImage, pubRom, KINS_ROM_LEN);
         fCc = TRUE;
         break;
   }
   safefree(pubRom);
   return(fCc);
}

//  
// Function:   MIAC_ParseRamMemory
// Purpose:    Parse buffer with RAM memory
//
// Parameters: 
// Returns:    TRUE if OKee
// Note:       The data received from the Smart Sensor Controller is:
//                "B=00\n\r"
//                "00=xx xx xx .....\n\r"
//                "70=xx xx xx .....\n\r"
//                "!xxxx\n\r"
//                "R>"
//  
bool MIAC_ParseRamMemory()
{
   bool     fCc=FALSE;
   u_int8  *pubRam;

   pubRam = safemalloc(KINS_RAM_LEN);
   switch(miac_ParseMemory(pubRam, KINS_RAM_LEN))
   {
      default:
      case 'S':
         // Buffer syntax error
         if(RPI_GetFlag(VERBOSE_LOG_MIAC)) LOG_Report(0, "MIA", "MIAC-ParseRamMemory():Syntax Error");
         break;

      case 'C':
         // Buffer checksum error
         if(RPI_GetFlag(VERBOSE_LOG_MIAC)) LOG_Report(0, "MIA", "MIAC-ParseRamMemory():CRC Error");
         break;

      case 'E':
         if(RPI_GetFlag(VERBOSE_LOG_MIAC)) LOG_Report(0, "MIA", "MIAC-ParseRamMemory():ERROR:Data has EEPROM content");
         break;

      case 'R':
         // Correct type of memory: save it
         if(RPI_GetFlag(VERBOSE_LOG_MIAC)) LOG_ListData("MIAC-ParseRamMemory():OK", (char *)pubRam, KINS_RAM_LEN);
         GEN_MEMCPY(pstMap->G_ubRamImage, pubRam, KINS_RAM_LEN);
         fCc = TRUE;
         break;
   }
   safefree(pubRam);
   return(fCc);
}

//  
// Function:   MIAC_SetEepromMemory
// Purpose:    Set a EEPROM memory value from the symbol lookup table
//
// Parameters: Enum MIACID
// Returns:    TRUE if OKee
// Note:       
//  
bool MIAC_SetEepromMemory(MIACID tMem, u_int8 pubVal)
{
   bool     fCc=FALSE;
   u_int8   ubAddr;

   if(tMem < MAX_NUM_MIAC)
   {
      fCc    = TRUE;
      ubAddr = ubMiacMemory[tMem];
      pstMap->G_ubRomImage[ubAddr] = pubVal;
   }
   return(fCc);
}

//  
// Function:   MIAC_SetRamMemory
// Purpose:    Set a RAM memory value from the symbol lookup table
//
// Parameters: Enum MIACID
// Returns:    TRUE if OKee
// Note:       
//  
bool MIAC_SetRamMemory(MIACID tMem, u_int8 pubVal)
{
   bool     fCc=FALSE;
   u_int8   ubAddr;

   if(tMem < MAX_NUM_MIAC)
   {
      fCc    = TRUE;
      ubAddr = ubMiacMemory[tMem];
      pstMap->G_ubRamImage[ubAddr] = pubVal;
   }
   return(fCc);
}


/*------  Local functions separator ------------------------------------
__LOCAL_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//  
// Function:   miac_AddressRom
// Purpose:    Handle EEPROM symbol
// 
// Parameters: Symbol ptr, result ptr
// Returns:    Value
// Note:       
//  
static bool miac_AddressRom(char *pcSymbol, u_int8 *pubVal)
{
   *pubVal = miac_FindMemoryValue(pcSymbol);
   return(TRUE);
}

//  
// Function:   miac_AddressRam
// Purpose:    Handle RAM symbol
// 
// Parameters: Symbol ptr, struct ptr, result ptr
// Returns:    Value
// Note:       
//  
static bool miac_AddressRam(char *pcSymbol, u_int8 *pubVal)
{
   *pubVal = miac_FindMemoryValue(pcSymbol);
   return(TRUE);
}

//  
// Function:   miac_FindMemoryValue
// Purpose:    Find the memory address of a symbol
// 
// Parameters: Symbol ptr
// Returns:    Value
// Note:       
//  
static u_int8 miac_FindMemoryValue(char *pcSymbol)
{
   long  lVal;

   lVal = strtol(pcSymbol, NULL, 16);
   return((u_int8) (lVal & 0xff));
}

//  
// Function:   miac_GetValue
// Purpose:    Get the int value from the Hex Ascii buffer
//
// Parameters: Hex Ascii buffer ptr, int buffer ptr
// Returns:    Nr of chars converted
// Note:       Buffer contains:
//             "00=00 \r\n"
//  
static int miac_GetValue(char *pcRec, int *piVal)
{
   char *pcRes;
   int   iNr, iVal;

   iVal = (int)strtoul(pcRec, &pcRes, 16);
   iNr  = (int)(pcRes-pcRec);
   if(iNr) *piVal = iVal;
   return(iNr);
}

//  
// Function:   miac_LookupSymbol
// Purpose:    Lookup the symbol from the list and run the handler function
//
// Parameters: Record from the Microchip 16f628 symbol of list file
// Returns:    TRUE if symbol found
// Note:       pcRec contains:
//             "b0Task01                          00000021 \r\n"
//  
static bool miac_LookupSymbol(char *pcRecord)
{
   bool        fCc=FALSE;
   int         iLen, x=0;
   MIACID      tId;
   u_int8      ubVal;
   const char *pcRes;
   char       *pcVal;
   PFMEM       pfFun;

   do
   {
      pcRes = stMemoryDefinition[x].pcSymbol;
      if(pcRes)
      {
         iLen = GEN_STRLEN(pcRes);
         if(GEN_STRNCMP(pcRecord, pcRes, iLen) == 0)
         {
            //
            // MIAC symbol found: find/rework value
            //
            pfFun = stMemoryDefinition[x].pfHandler;
            if(pfFun)
            {
               pcVal = &pcRecord[iLen];
               pcVal = GEN_FindDelimiter(pcVal, DELIM_SPACE);        // Skip Symbol
               pcVal = GEN_FindDelimiter(pcVal, DELIM_NOSPACE);      // Find Value

               if( pfFun(pcVal, &ubVal))
               {
                  tId = stMemoryDefinition[x].tMiacId;
                  ubMiacMemory[tId] = ubVal;
                  //
                  //pwjh PRINTF("miac-LookupSymbols():Found: Id=%2d [%16s] = 0x%02x" CRLF, x, pcRes, ubVal);
                  //
                  fCc   = TRUE;
                  pcRes = NULL;
               }
            }
         }
         else x++;
      }
   }
   while(pcRes);
   return(fCc);
}

//  
// Function:   miac_ParseMemory
// Purpose:    Parse buffer with memory data
//
// Parameters: Buffer ptr, length
// Returns:    Type of memory 'E' or 'R'
// Note:       The data received from the Smart Sensor Controller is:
//                "......B=00\n\r"
//                "00=xx xx xx .....\n\r"
//                "70=xx xx xx .....\n\r"
//                "!xxxx\n\r"
//                "E>"  or
//                "R>"
//  
static char miac_ParseMemory(u_int8 *pubData, int iSize)
{
   MIMODE   tMiMode = MIAC_MODE_BANK;
   char    *pcSrc;
   char     cLastChar=0;
   bool     fHaveBank=FALSE, fDoParse=FALSE;
   int      iAddr=0, iBank=-1, iLen, iSumAct, iCrcSum, iCrcIdx, iIdx=0;
   int      iNrRead, iData;

   pcSrc = pstMap->G_stCmd.pcData;
   iLen  = sizeof(pstMap->G_stCmd.pcData);
   //
   if(iLen) fDoParse = TRUE;
   if(RPI_GetFlag(VERBOSE_LOG_MIAC)) LOG_ListData("miac_ParseMemory():", pcSrc, iLen);
   //
   while(fDoParse)
   {
      switch(tMiMode)
      {
         case MIAC_MODE_BANK:
            // Memory dump MUST start with the Bank number. 
            // Maybe some garbage preceeds the data, strip it!
            // ".....B=00 \n\r"
            //  ^
            PRINTF("miac-ParseMemory():BANK:Idx=%3d, Addr=0x%02x, Len=%3d" CRLF, iIdx, iAddr, iLen);
            iNrRead = 1;
            switch(pcSrc[iIdx])
            {
               case 'B': 
                  fHaveBank = TRUE;
                  iCrcIdx   = iIdx;
                  break;
               
               case '=': 
               case '\n': 
               case ' ': 
                  // Discard
                  break;

               case 0:
                  // OOOps premature exit 
                  if(RPI_GetFlag(VERBOSE_LOG_MIAC)) LOG_Report(0, "MIA", "miac_ParseMemory():BANK:Idx=%d", iIdx);
                  tMiMode = MIAC_MODE_EXIT;
                  break;

               case '\r': 
                  // EOR
                  // Discard until the Bank number has been found
                  if(fHaveBank)
                  {
                     if(RPI_GetFlag(VERBOSE_LOG_MIAC)) LOG_Report(0, "MIA", "miac_ParseMemory():Bank=%d", iBank);
                     PRINTF("miac-ParseMemory():Bank=%d" CRLF, iBank);
                     tMiMode = MIAC_MODE_ADDR;
                  }
                  break;

               default:
                  // Discard all garbage data until the Bank number has been found
                  if(fHaveBank)
                  {
                     iNrRead = miac_GetValue(&pcSrc[iIdx], &iData);
                     if(iNrRead) iBank     = iData;
                     else        fHaveBank = FALSE;
                  }
                  break;
            }
            break;

         case MIAC_MODE_ADDR:
            // "20=xx xx xx xx xx ...  xx \n\r" or
            // "!xxxx\n\r"                      or
            //  ^
            PRINTF("miac-ParseMemory():ADDR:Idx=%3d, Addr=0x%02x, Len=%3d" CRLF, iIdx, iAddr, iLen);
            iNrRead = 1;
            switch(pcSrc[iIdx])
            {
               case '=':
                  tMiMode = MIAC_MODE_DATA;
                  break;

               case '!': 
                  PRINTF("miac-ParseMemory():Checksum found at %d" CRLF, iIdx);
                  tMiMode = MIAC_MODE_CSUM;
                  break;

               case 'R': 
               case '>': 
               case ' ': 
               case '\n': 
               case '\r': 
                  // BAD: return last char ('R' or 'E') as memory type
                  LOG_Report(0, "MIA", "miac-ParseMemory():Unexpected CR error at %d: Abort" CRLF, iIdx);
                  PRINTF("miac-ParseMemory():Unexpected CR error at %d: Abort" CRLF, iIdx);
                  fDoParse = FALSE;
                  break;

               case 0:
                  // OOOps premature exit 
                  if(RPI_GetFlag(VERBOSE_LOG_MIAC)) LOG_Report(0, "MIA", "miac_ParseMemory():ADDR:Idx=%d", iIdx);
                  tMiMode = MIAC_MODE_EXIT;
                  break;

               default:
                  iNrRead = miac_GetValue(&pcSrc[iIdx], &iData);
                  if(iNrRead) iAddr = iData;
                  else
                  {
                     // BAD: return last char ('R' or 'E') as memory type
                     LOG_Report(0, "MIA", "miac-ParseMemory():Unexpected NON-Hex ADDR at %d: Abort", iIdx);
                     PRINTF("miac-ParseMemory():Unexpected NON-Hex ADDR at %d: Abort" CRLF, iIdx);
                     fDoParse = FALSE;
                  }
                  break;
            }
            break;

         case MIAC_MODE_DATA:
            // "20=xx xx xx xx xx ...  xx \n\r"
            //        ^
            PRINTF("miac-ParseMemory():DATA:Idx=%3d, Addr=0x%02x, Len=%3d" CRLF, iIdx, iAddr, iLen);
            iNrRead = 1;
            switch(pcSrc[iIdx])
            {
               case '\n': 
               case ' ': 
                  break;

               case '\r': 
                  // EOR
                  tMiMode = MIAC_MODE_ADDR;
                  break;

               case 0:
                  // OOOps premature exit 
                  if(RPI_GetFlag(VERBOSE_LOG_MIAC)) LOG_Report(0, "MIA", "miac_ParseMemory():DATA:Idx=%d", iIdx);
                  tMiMode = MIAC_MODE_EXIT;
                  break;

               default:
                  iNrRead = miac_GetValue(&pcSrc[iIdx], &iData);
                  if(iNrRead) 
                  {
                     if(iAddr < iSize) pubData[iAddr] = (u_int8) (iData & 0xFF);
                     else 
                     {
                        LOG_Report(0, "MIA", "miac-ParseMemory():Address (0x%04x) exceeds limits (0x%04x)!", iAddr, iSize);
                        PRINTF("miac-ParseMemory():Address (0x%04x) exceeds limits (0x%04x)!" CRLF, iAddr, iSize);
                     }
                  }
                  else
                  {
                     // BAD: return last char ('R' or 'E') as memory type
                     LOG_Report(0, "MIA", "miac-ParseMemory():Unexpected NON-Hex DATA at %d: Abort", iIdx);
                     PRINTF("miac-ParseMemory():Unexpected NON-Hex DATA at %d: Abort" CRLF, iIdx);
                     fDoParse = FALSE;
                  }
                  iAddr++;
                  break;
            }
            break;

         case MIAC_MODE_CSUM:
            // "!xxxx\n\r"
            //   ^
            PRINTF("miac-ParseMemory():CSUM:Idx=%3d, Addr=0x%02x, Len=%3d" CRLF, iIdx, iAddr, iLen);
            iNrRead = 1;
            switch(pcSrc[iIdx])
            {
               case '\n':
               case ' ':
                  break;

               case '\r':
                  tMiMode = MIAC_MODE_TYPE;
                  break;
               
               case 0:
                  // OOOps premature exit 
                  if(RPI_GetFlag(VERBOSE_LOG_MIAC)) LOG_Report(0, "MIA", "miac_ParseMemory():CSUM:Idx=%d", iIdx);
                  tMiMode = MIAC_MODE_EXIT;
                  break;

               default:
                  iNrRead = miac_GetValue(&pcSrc[iIdx], &iSumAct);
                  if(iNrRead)
                  {
                     // Calculate checksum
                     // Start with the Bank Nr
                     // Prior to the B=xx are CrLf (0xd+0xa=23), which need to taken in consideration as well !
                     for(iData=iCrcIdx, iCrcSum = 23; iData<iIdx; iData++) iCrcSum += pcSrc[iData];
                     //
                     if(iCrcSum != iSumAct)
                     {
                        LOG_Report(0, "MIA", "miac-ParseMemory():Checksum ERROR Act=0x%04x Cal=0x%04x", iSumAct, iCrcSum);
                        PRINTF("miac-ParseMemory():Checksum ERROR Act=0x%04x Cal=0x%04x" CRLF, iSumAct, iCrcSum);
                        cLastChar = 'C';
                        fDoParse = FALSE;
                     }
                     else PRINTF("miac-ParseMemory():Checksum OK" CRLF);
                  }
                  else
                  {
                     // BAD: return 'C' as checksum error
                     LOG_Report(0, "MIA", "miac-ParseMemory():Unexpected NON-Hex CSUM at %d: Abort!", iIdx);
                     PRINTF("miac-ParseMemory():Unexpected NON-Hex CSUM at %d: Abort!" CRLF, iIdx);
                     cLastChar = 'C';
                     fDoParse = FALSE;
                  }
                  break;
            }
            break;

         case MIAC_MODE_TYPE:
            // "E>" or
            // "R>"
            //  ^
            PRINTF("miac-ParseMemory():TYPE:Idx=%3d, Addr=0x%02x, Len=%3d" CRLF, iIdx, iAddr, iLen);
            iNrRead = 1;
            switch(pcSrc[iIdx])
            {
               case '>':
                  PRINTF("miac-ParseMemory():OKEE: Type = %c" CRLF, cLastChar);
                  fDoParse = FALSE;
                  break;

               case '\n':
               case '\r':
                  break;

               case 0:
                  // EOB : Exit
                  if(RPI_GetFlag(VERBOSE_LOG_MIAC)) LOG_Report(0, "MIA", "miac_ParseMemory():TYPE:Idx=%d", iIdx);
                  fDoParse = FALSE;
                  break;

               default:
                  cLastChar = pcSrc[iIdx];
                  break;
            }
            break;

         case MIAC_MODE_EXIT:
            LOG_Report(0, "MIA", "miac-ParseMemory():Unexpected End of Data: Abort");
            fDoParse = FALSE;
            break;
      }
      iIdx += iNrRead;
      iLen -= iNrRead;
   }
   PRINTF("miac-ParseMemory():Done, Mem=%c, Idx=%3d, Addr=0x%02x, Len=%3d" CRLF, cLastChar, iIdx, iAddr, iLen);
   if(RPI_GetFlag(VERBOSE_LOG_MIAC)) LOG_Report(0, "MIA", "miac-ParseMemory():Done, Mem=%c, Idx=%3d, Addr=0x%02x, Len=%3d", cLastChar, iIdx, iAddr, iLen);
   return(cLastChar);
}



