/*  (c) Copyright:  2020..2024  Patrn, Confidential Data
 *
 *  Workfile:           rpi_main.h
 *  Purpose:            Main entry point RpiSense app
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    01 Nov 2020:      Ported from rpislim
 *    23 Apr 2024:      Split globals.h and locals.h
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _RPI_MAIN_H_
#define _RPI_MAIN_H_

#define  DEFAULT_TIMEOUT            30000       // Msecs Poll timeout
//
#define  MAX_RETRIES_NO_SENS        3           // Max nr of retries to get Smart Sensor data
#define  MAX_SECS_NO_SENS           (15*60)     // Max secs no Smart Sensor data
#define  MAX_SECS_RESTART           120         // max secs for restart  by rpidog
//
#define  NUM_SECS_POWEROFF          60          // Num secs till poweroff by rpidog
//
#define  MAX_WATERLEVEL             475.0       // Max waterlevel value
#define  MAX_WATERLEVEL_OVERFLOW    499.9       // Overflow waterlevel value
//
#define  MAX_INVALID_SAMPLES        5           // Max invalid samples
#define  NUM_TRANSIENT_SECS         30          // Transient period in secs
#define  MAX_RISE_PER_MIN           20.00       // Default Max waterlevel rise per minute
#define  MAX_DROP_PER_MIN           20.00       // Default Max waterlevel drop per minute

#endif /* _RPI_MAIN_H_ */
