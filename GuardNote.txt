
Guard thread checks
-------------------

Thread PID List:
   tPid
   iNotify
   iFlag

Host init:
   Call thread Init functions, if the thread requires guard protection 
   then return GLOBAL_xxx_ALL_INI.
   G_iGuards collects these bits for guard duties.

Each thread init:
   GLOBAL_SemaphoreInit(PID_xxx);
   GLOBAL_PidSetInit(PID_xxx, Restart_Function_Ptr)
   GLOBAL_PidTimestamp(PID_xxx, 0)
   GLOBAL_PidSaveGuard(PID_xxx, GLOBAL_xxx_ALL_INI)
   // or if Guard thread monitoring is not required:
   // GLOBAL_PidSaveGuard(PID_xxx, 0)
   GLOBAL_SetSignalNotification(PID_HOST, GLOBAL_xxx_ALL_INI)
   return(GLOBAL_xxx_ALL_INI)
   
Each thread main loop:
   // At max SET_GUARD_SECS, call:
   GLOBAL_PidSetGuard(PID_xxx)
   // This will force pstList->iFlag to 1 if the startup return value
   // has requested Guard thread monitoring.

Guard thread:
   Call at regular intervals (5-10 Mins):
   GLOBAL_PidCheckGuards()
   // The Guard thread will verify that all threads have pulled their 
   // pstList->iFlag to 1 (if they require Guard checks).
   // If this bit is still zero, the Guard thread could:
   //    - restart the thread immediately
   //    - restart the whole App immediately
   //    - restart the whole App later if it is persistent behavior
   //    - reboot the system
   // by sending the appropriate signal to the Host thread:
   GLOBAL_Notify(PID_HOST, GLOBAL_ALL_HST_RST, SIGUSR1);
   GLOBAL_Notify(PID_HOST, GLOBAL_ALL_HST_RBT, SIGUSR1);
   // If all threads fare well, restart the cycle: 
   GLOBAL_PidClearGuards()
   
