/*  (c) Copyright:  2024  Patrn, Confidential Data
 *
 *  Workfile:           locals.h
 *  Purpose:            Define the local variables, common over all threads.
 *  
 *    
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PIC 16f628A Ultrasoon sensor
 *  Note:               
 * 
 *  Author:             Peter Hillen
 *  Changes:
 *    23 Apr 2024:      Split globals.h and locals.h
 *    
 *    
 *    
 *    
 *    
 *    
 *    
 *    
 *    
 *    
 *    
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _LOCALS_H_
#define _LOCALS_H_

//
// local area array sizes
//
#define  MIAC_CIRC_MASK       511            // MIAC Circular buffer         
#define  MIAC_CIRC_SIZE       520            // Additional zero's
//
#define  KINS_SIZE_LEN        2047
#define  KINS_SIZE_LENZ       (KINS_SIZE_LEN+1)
#define  KINS_RAM_LEN         128
#define  KINS_ROM_LEN         128
//
// HTTP Functions (callback through HTML or JSON)
//
// Global over all project (0x0000.0001...0x000.00008)
// are in globals.h
//
// Local over this project (Start at 0x0010)
#define  PAR_MIA              0x00000010     // Vector
//
// Notification flags
//
#define  GLOBAL_HST_ALL_INI   0x01000000     // Init: Host
#define  GLOBAL_SVR_ALL_INI   0x02000000     // Init: HTTP Server
#define  GLOBAL_GRD_ALL_INI   0x04000000     // Init: Guard 
#define  GLOBAL_COM_ALL_INI   0x08000000     // Init: Serial Communication
#define  GLOBAL_DBG_ALL_INI   0x10000000     // Init: Debug
#define  GLOBAL_MLX_ALL_INI   0x80000000     // Init: MailX helper
#define  GLOBAL_XXX_ALL_INI   0xFF000000     // Init Mask
// Dest ALL:
#define  GLOBAL_GRD_ALL_RUN   0x00010000     // Guard Run
#define  GLOBAL_HST_ALL_MID   0x00020000     // Midnight signal from Host
#define  GLOBAL_SVR_ALL_NFY   0x00040000     // Http Server notify to threads
#define  GLOBAL_HST_ALL_RUN   0x00080000     // Run/Execute command from Host
#define  GLOBAL_XXX_ALL_MSK   0x00FF0000     // Mask
// Host
#define  GLOBAL_COM_HST_PPI   0x00000001     // Controller Ping buffer Nfy
#define  GLOBAL_COM_HST_PPO   0x00000002     // Controller Pong buffer Nfy
#define  GLOBAL_COM_HST_RAM   0x00000004     // Controller RAM    Memory Nfy
#define  GLOBAL_COM_HST_ROM   0x00000008     // Controller EEPROM Memory Nfy
#define  GLOBAL_COM_HST_MEM   0x00000010     // Controller MIAC Memory Nfy
#define  GLOBAL_DBG_HST_MEM   0x00000020     // Controller Memory transfer
#define  GLOBAL_COM_HST_ERR   0x00000040     // Controller ERROR
#define  GLOBAL_SVR_HST_REQ   0x00000080     // Generic HTTP request
#define  GLOBAL_SVR_HST_MEM   0x00000100     // Server memory data update
#define  GLOBAL_SVR_HST_TMR   0x00000200     // Server timer update
#define  GLOBAL_ALL_HST_END   0x00000400     // Host Exit
#define  GLOBAL_ALL_HST_FLT   0x00000800     // Segmentation fault host nfy (exit)
#define  GLOBAL_ALL_HST_PWR   0x00001000     // Power Off request
#define  GLOBAL_ALL_HST_RBT   0x00002000     // Reboot
#define  GLOBAL_ALL_HST_RST   0x00004000     // Restart theApp
#define  GLOBAL_ALL_HST_EML   0x00008000     // Send summary email
#define  GLOBAL_ALL_HST_XXX   0x0000FFFF     // Mask
// HTTP Server
#define  GLOBAL_ALL_SVR_VAR   0x00000001     // HTTP Server Variable update
#define  GLOBAL_ALL_SVR_XXX   0x0000FFFF     // Mask
// Guard
#define  GLOBAL_HST_GRD_MID   0x00000001     // Guard midnight
#define  GLOBAL_HST_GRD_NFY   0x00000002     // Host        Nfy
#define  GLOBAL_SVR_GRD_NFY   0x00000004     // HTTP Server Nfy
#define  GLOBAL_DBG_GRD_NFY   0x00000008     // Debug       Nfy
#define  GLOBAL_ALL_GRD_XXX   0x0000FFFF     // Mask
// Comms
#define  GLOBAL_DBG_COM_CMD   0x00000001     // Debug command for the uController
#define  GLOBAL_ALL_COM_XXX   0x0000FFFF     // Mask
// Debug
#define  GLOBAL_HST_DBG_RUN   0x00000001     // Host allows debug run
#define  GLOBAL_COM_DBG_PPI   0x00000002     // Somms has sensor data Ping
#define  GLOBAL_COM_DBG_PPO   0x00000004     // Somms has sensor data Pong
#define  GLOBAL_ALL_DBG_XXX   0x0000FFFF     // Mask
//
// G_iDebug:
//
#define  MM_DEBUG_FILE_COMMS     0x00000001  // File communications with Sensor
//                               0x00000080  // 
//
#define  MM_DEBUG_STDOUT         0x0000FF00  // All STDOUT bits
#define  MM_DEBUG_HOST           0x00000100  // STDOUT Host debug data 
#define  MM_DEBUG_SENSOR         0x00000200  // STDOUT Host debug data 
#define  MM_DEBUG_COMMS          0x00000400  // STDOUT Comm debug data 
#define  MM_DEBUG_PRINT          0x00000800  // STDOUT Misc debug data 
#define  MM_DEBUG_GUARD          0x00001000  // STDOUT Misc guard data 
#define  MM_DEBUG_MAIL           0x00002000  // STDOUT Mail debug data
#define  MM_DEBUG_SERIAL         0x00008000  // STDOUT RAW STDIN serial data
//
#define  MM_DEBUG_ACTIVE         0x10000000  // Debug thread is active
#define  MM_DEBUG_IDLE           0x20000000  // Debug thread is IDLE
#define  MM_DEBUG_UCTL           0x40000000  // uController mode 
#define  MM_DEBUG_ENABLED        0x80000000  // Debug thread is enabled
#define  MM_DEBUG_ALL            0xFFFFFFFF  // All used bits
//
// Generic status strings/enums
//
typedef enum _genstat_
{
#define  EXTRACT_ST(a,b)   a,
#include "gen_stats.h"
#undef   EXTRACT_ST
   //
   NUM_GEN_STATUS,
   GEN_STATUS_ASK                            // Return current status
}  GENSTAT;
//
typedef struct _global_command_
{
   int      iCommand;                        // Command
   GENSTAT  iStatus;                         // Sensor mode/status
   int      iError;                          // Error
   //
   int      iSecsTout;                       // Timeout for sensor replies
   int      iSecsMiac;                       // Time for MIAC RAM/EEPROM
   int      iSpare[32];                      // Spare
   //              
   char     pcData[KINS_SIZE_LENZ];          // Data buffer
}  GLOCMD;
//
// Generic status error codes
//
typedef enum _generror_
{
   GEN_ERROR_NONE = 0,                       // No error
   GEN_ERROR_MODE,                           // CommMode detection no data
   GEN_ERROR_COMM,                           // CommMode read error
   GEN_ERROR_RAM,                            // Controller RAM transfer error
   GEN_ERROR_ROM,                            // Controller EEPROM transfer error
   GEN_ERROR_SENSOR,                         // Controller Sensor error
   GEN_ERROR_UCTL,                           // Controller uCtl error
   GEN_ERROR_HTTP1,                          // Controller HTTP Miac error
   GEN_ERROR_HTTP2,                          // Controller HTTP Pump error
   GEN_ERROR_HTTP3,                          // Controller HTTP Sensor error
   //
   NUM_GEN_ERRORS,
}  GENERR;
//
//
typedef enum _kinshi_samples_
{
   EMS_LEV     = 0,                          // Waterlevel
   EMS_TMP,                                  // Temperature
   EMS_DIS,                                  // Ultrasoon distance
   EMS_PMP,                                  // % Pump ON
   EMS_X10,                                  // % X10 Bypass
   //
   NUM_EMS,
}  EMS;
//
#define  MAX_SENS             31             // 
#define  MAX_SENSZ            MAX_SENS+1     // SENS Equipment storage space
#define  POND_LEVEL_SAMPLES   4              // Number of samples to determine pond water level
//
// Smart Sensor stucture
//
typedef struct _smart_sensor_
{
   u_int32  ulSecsSample;                    // Next sampling time
   int      iPeriod;                         // Trigger for next period 
   int      iSamples;                        // Total number of Ultrasoon samples
   int      iCrcErrors;                      // Total number of CRC errors
   int      tSensor;                         // SENSOR Enum
   //
   int      iSysFlags;                       // System flags
   int      iPumpMode;                       // Pump mode (00, 01, 05, 15, 55)
   int      iPumpOn;                         // Pump ON     (counter)
   int      iPumpOff;                        // Pump OFF    (counter)
   int      iX10Active;                      // X10  Active (counter)
   int      iX10Bypass;                      // X10  Bypass (counter)
   int      iTimeInSecs;                     // Smart Sensor time in secs
   int      iCounts;                         // Valid sample counter
   int      iDropped;                        // Dropped sample counter
   double   flWaterLevel;                    // WaterLevel in mm
   double   flTemperature;                   // Temperature in C
   double   flDistance;                      // Dm Distance in mm
   //
   char     cEquipment[MAX_SENSZ];           // Smart Sensor identifier
}  SENS;
//
typedef struct SMDATA
{
   double   flWaterLevel;                    // Actual waterlevel in mm
   double   flTemperature;                   // Temperature in C
   double   flDistance;                      // Distance in mm
   double   flPumpOn;                        // Pump ON
   double   flX10Bypass;                     // X10  Bypass
}  SMDATA;

//
typedef enum _kins_periods_
{
   KINS_NOW       = 0,
   KINS_MINUTE,
   KINS_QUARTER,
   KINS_DAY,
   KINS_MONTH,
   KINS_YEAR,
   KINS_CSV,
   KINS_TOTAL
}  KINSP;
//
// Kinshi dayly waterpump ON/OFF states
//                                   WATERLEVEL
//             TURN_OFF              TURN_ON                  TURN_OFF
//   WAIT_OFF     |    WAIT_ON           |                         |  WAIT_OFF
//  --------------+----------------------+-------------------------+---------------
//              00:00                  07:00                      00:00
//
typedef enum _kins_pump_
{
   KINS_PUMP_INIT = 0,                       // Init pump state
   KINS_PUMP_WAIT_OFF,                       // Wait till pump can be turned OFF
   KINS_PUMP_TURN_OFF,                       // Turn pump OFF
   KINS_PUMP_WAIT_ON,                        // Wait till pump can be turned ON
   KINS_PUMP_WATERLEVEL,                     // Measure actual waterlevel after OFF time
   KINS_PUMP_TURN_ON,                        // Turn pump ON
   //
   KINS_PUMP_TEST_OFF,                       // TEST MODE: Turn pump OFF
}  KPUMP;
//
// Kinshi flow states
//
typedef enum _kins_flow_
{
   KINS_FLOW_INIT = 0,                       // Init flow state
   KINS_FLOW_WAIT_ON,                        // Wait till smart sensor goes 0 --> 1
   KINS_FLOW_SAMPLE,                         // Sample Kinshi waterflow
   KINS_FLOW_WAIT_OFF,                       // Wait till smart sensor goes 1 --> 0
}  KFLOW;
//
// Kinshi Controller Operation modes
// Write to b0Task01 (Mask =0x07)
//
typedef enum _kins_task_
{
   KINS_TASK_IDLE       = 0,                 // Idle
   KINS_TASK_INIT,                           // Init
   KINS_TASK_AUTO,                           // Yellow OFF     :Auto mode (Ultrasoon sensor)
   KINS_TASK_PUMP_ON,                        //        1 blink :Manual ON
   KINS_TASK_PUMP_OFF,                       //        2 blinks:Manual OFF
   KINS_TASK_TIMER,                          //        3 blinks:Timer  ON/OFF
   KINS_TASK_POWEROFF,                       //        4 blinks:Poweroff request
   KINS_TASK_RESTART                         // Restart
}  KINST;
//
// Kinshi Controller
//
typedef enum _kins_mode_
{
   KINS_MODE_AUTO       = 0x00,              // Yellow OFF     :Auto mode (Ultrasoon sensor)
   KINS_MODE_PUMP_ON    = 0x01,              //        1 blink :Manual ON
   KINS_MODE_PUMP_OFF   = 0x05,              //        2 blinks:Manual OFF
   KINS_MODE_TIMER      = 0x15,              //        3 blinks:Timer  ON/OFF
   KINS_MODE_POWEROFF   = 0x55               //        4 blinks:Poweroff request
}  KINSM;
//
#define NUM_SAMPLES           (MAX_DAYS * NUM_EMS)
#define NUM_SAMPLES_MONTH     (MAX_DAYS * NUM_EMS)
#define NUM_SAMPLES_DAY       (MAX_QRTS * NUM_EMS)
//
#define MAX_LINES             25
#define NUM_SCALES            16
//
// Global parms
//
typedef enum _global_opt_
{
   GLOOPT_NONE = 0,                          // 
   GLOOPT_SWITCH_ON,                         // Option is flag and  ON
   GLOOPT_SWITCH_OFF,                        //                     OFF
   GLOOPT_VALUE_ON,                          //           value and present
   GLOOPT_VALUE_OFF,                         //                     not present
}  GLOOPT;

#endif /* _LOCALS_H_ */
