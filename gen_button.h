/*  (c) Copyright:  2020..2024  Patrn, Confidential Data
 *
 *  Workfile:           gen_button.h
 *  Purpose:            Global configurations
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PIC 16f628A Ultrasoon sensor
 * 
 *  Author:             Peter Hillen
 *  Changes:
 *    01 Nov 2020:      Ported from rpislim
 *    24 Apr 2024:      Move to global/local startup 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _GEN_BUTTON_H_
#define _GEN_BUTTON_H_

bool GEN_Init           (void);
bool GEN_Exit           (void);


#endif /* _GEN_BUTTON_H_ */
