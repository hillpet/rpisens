/*  (c) Copyright:  2020..2024 Patrn, Confidential Data 
 *
 *  Workfile:           gen_stats.h
 *  Purpose:            Status defines   
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    01 Nov 2020:      Ported from rpislim
 *    06 Dec 2020:      Move to Sensor Mode 
 *    20 Apr 2024:      Add uCTL status
 *    24 Apr 2024:      Move to global/local startup 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/
 
EXTRACT_ST(GEN_STATUS_IDLE,         "Idle"               )
EXTRACT_ST(GEN_STATUS_SENSOR,       "Sensor"             )
EXTRACT_ST(GEN_STATUS_TRACE,        "Trace"              )
EXTRACT_ST(GEN_STATUS_MIAC_RAM,     "Miac RAM"           )
EXTRACT_ST(GEN_STATUS_MIAC_ROM,     "Miac EEPROM"        )
EXTRACT_ST(GEN_STATUS_RESET,        "Reset"              )
EXTRACT_ST(GEN_STATUS_ERROR,        "Error"              )
EXTRACT_ST(GEN_STATUS_TIMEOUT,      "HTTP Timeout"       )
EXTRACT_ST(GEN_STATUS_REJECTED,     "HTTP Rejected"      )
EXTRACT_ST(GEN_STATUS_REDIRECT,     "HTTP Redirected"    )
EXTRACT_ST(GEN_STATUS_UCTL,         "uController"        )
EXTRACT_ST(GEN_STATUS_MIAC_ANY,     "MIAC RAM/EEPROM"    )
EXTRACT_ST(GEN_STATUS_LIVE_ANY,     "LIVE Sensor/Trace"  )
