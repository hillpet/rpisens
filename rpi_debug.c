/*  (c) Copyright:  2024  Patrn ESS, Confidential Data 
 *
 *  Workfile:           rpi_debug.c
 *  Purpose:            This thread handles the debug mode
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    18 Apr 2024:      Ported from rpiocr
 *    24 Apr 2024:      Move to global/local startup 
 *    
 *    
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/
#include <stdio.h>
#include <string.h>
#include <semaphore.h>
#include <stddef.h>
#include <stdlib.h>
#include <termios.h>           //termios, TCSANOW, ECHO, ICANON
#include <unistd.h>            //STDIN_FILENO
#include <time.h>
#include <resolv.h>
#include <sys/types.h>
#include <sys/signal.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <ctype.h>
#include <errno.h>
#include <execinfo.h>
//
#include <common.h>
#include "config.h"
#include "locals.h"
#include "globals.h"
//
#include "rpi_func.h"
#include "rpi_miac.h"
#include "rpi_comm.h"
#include "rpi_debug.h"
//
#define USE_PRINTF
#include <printx.h>

extern const double flTimerTickPerMm;
extern const double flMmPerTimerTick;
//
// Local functions
//
static void    dbg_CheckSignals           (void);
static bool    dbg_CollectHexBcd          (int *, int, int);
static bool    dbg_CollectText            (char *, int);
static void    dbg_ControllerCommand      (char);
static void    dbg_DisplayDebugBits       (void);
static int     dbg_Execute                (void);
static bool    dbg_KeyboardHit            (void);
static bool    dbg_WaitSignal             (int, int);
//
// Debug thread commands
//
static bool    dbg_ControllerModeOff      (const DBCMD *);
static bool    dbg_ControllerModeOn       (const DBCMD *);
static bool    dbg_ControllerHiWater      (const DBCMD *);
static bool    dbg_ControllerLoWater      (const DBCMD *);
static bool    dbg_ControllerTransfer     (const DBCMD *);
static bool    dbg_DebugCommsPrint        (const DBCMD *);
static bool    dbg_DebugGenericPrint      (const DBCMD *);
static bool    dbg_DebugGuardPrint        (const DBCMD *);
static bool    dbg_DebugHostPrint         (const DBCMD *);
static bool    dbg_DebugMailPrint         (const DBCMD *);
static bool    dbg_DebugNoPrint           (const DBCMD *);
static bool    dbg_DebugSensorPrint       (const DBCMD *);
static bool    dbg_DebugRawSerialPrint    (const DBCMD *);
static bool    dbg_Help                   (const DBCMD *);
static bool    dbg_Linefeed               (const DBCMD *);
static bool    dbg_MallocBalance          (const DBCMD *);
static bool    dbg_Quit                   (const DBCMD *);
static bool    dbg_QuitTerminate          (const DBCMD *);
static bool    dbg_SendEmail              (const DBCMD *);
static bool    dbg_SetDebugLevel          (const DBCMD *);
static bool    dbg_SetVerboseLevel        (const DBCMD *);
static bool    dbg_TruncateLogfile        (const DBCMD *);
//
static bool    dbg_SignalRegister         (sigset_t *);
static void    dbg_ReceiveSignalSegmnt    (int);
static void    dbg_ReceiveSignalInt       (int);
static void    dbg_ReceiveSignalTerm      (int);
static void    dbg_ReceiveSignalUser1     (int);
static void    dbg_ReceiveSignalUser2     (int);

static const char *pcLogfile              = RPI_PUBLIC_LOG_PATH;
static const DBCMD stDebugCommands[]      =
{
   // cCmd        pcHemp      pfDbCmd 
   #define  EXTRACT_DEB(a,b,c,d)          {b,c,d},
   #include "cmd_debug.h"
   #undef EXTRACT_DEB
};
static int iNumDebugCommands = sizeof(stDebugCommands) / sizeof(DBCMD);
//
// Local arguments
//
static bool fThreadRunning     = TRUE;

/*------  Local functions separator ------------------------------------
_______________GLOBAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/

//
// Function:   DBG_Init
// Purpose:    Handle running debug
//
// Parms:      
// Returns:    Exit codes
// Note:       Called from main() to split off the debug thread 
//             Setting MM_DEBUG_ENABLED will have the Debug thread handling the
//             stdin keys.
//
int DBG_Init()
{
   int      iCc;
   pid_t    tPid;

   tPid = fork();
   //
   switch(tPid)
   {
      default:
         // Parent
         GLOBAL_PidPut(PID_DEBUG, tPid);
         GLOBAL_PidSetInit(PID_DEBUG, NULL);
         break;

      case 0:
         // Child:
         LOG_printf("DBG-Init():Debugger running" CRLF);
         GEN_Sleep(500);
         iCc = dbg_Execute();
         LOG_Report(0, "DBG", "DBG-Init():Exit normally, cc=%d", iCc);
         GLOBAL_PidPut(PID_DEBUG, 0);
         exit(iCc);
         break;

      case -1:
         // Error
         LOG_printf("DBG-Init(): Error!" CRLF);
         LOG_Report(errno, "DBG", "DBG-Init():Exit fork ERROR:");
         exit(EXIT_CC_GEN_ERROR);
         break;
   }
   return(GLOBAL_DBG_ALL_INI);
}

/*----------------------------------------------------------------------
________________LOCAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/

//  
// Function:   dbg_CheckSignals
// Purpose:    Check and handle incoming signals
// 
// Parameters: 
// Returns:    
// Note:       
//             
static void dbg_CheckSignals()
{
   GLOBAL_PidSetGuard(PID_DEBUG);
   //
   if( GLOBAL_GetSignalNotification(PID_DEBUG, GLOBAL_COM_DBG_PPI) )
   {
      GEN_PRINTF("%s", pstMap->G_pcPing);
   }
   if( GLOBAL_GetSignalNotification(PID_DEBUG, GLOBAL_COM_DBG_PPO) )
   {
      GEN_PRINTF("%s", pstMap->G_pcPong);
   }
   if( GLOBAL_GetSignalNotification(PID_DEBUG, GLOBAL_GRD_ALL_RUN) )
   {
      LOG_Report(0, "DBG", "DBG-CheckSignals():Warning from Guard!");
   }
}

//
// Function:   dbg_CollectHexBcd
// Purpose:    Collect a Hex or BCD value from STDIN
//
// Parms:      Int ptr, min, max
// Returns:    TRUE if OK
// Note:       0x is HEX, -/+ is sign
//             Return the value at piValue
//
static bool dbg_CollectHexBcd(int *piValue, int iMin, int iMax)
{
   bool  fOk=FALSE, fCollecting=TRUE;
   bool  fVal, fHex=FALSE, fNeg=FALSE;
   int   iBits, iNr=0, iKey, iValue=0;
   int   iOri=*piValue;

   //
   // Disable STDOUT while assembling new data
   //
   iBits = GLOBAL_DebugGet(MM_DEBUG_STDOUT);
   GLOBAL_DebugClr(MM_DEBUG_STDOUT);
   //
   GEN_PRINTF(" (%d):", iOri);
   while(fCollecting)
   {
      fVal = TRUE;
      iKey = getchar();
      GEN_PRINTF("%c", (char)iKey);
      iKey = tolower(iKey);
      //
      if(iKey == 0x1b) return(FALSE);
      else if(iKey == (int)'-') { fNeg = TRUE;  iKey = 0;}
      else if(iKey == (int)'+') { fNeg = FALSE; iKey = 0;}
      else if(iKey == (int)'x') 
      {  // Hex entry: restart acquisition
         iNr++;
         iValue = 0;
         fVal   = FALSE;
         fHex   = TRUE; 
      }
      else if((iKey >= (int)'0') && (iKey <= (int)'9'))
      {
         iNr++;
         iKey -= (int)'0';
      }
      else if(fHex && (iKey >= (int)'a') && (iKey <= (int)'f')) 
      {
         iNr++;
         iKey -= ((int)'a' - 10);
      }
      else if(iKey == (int)'\n') 
      {
         //
         // Collecting is done: check min/max
         //
         if(fNeg) iValue = -iValue;
         if(iNr == 0) iValue = iOri;
         if( (iValue >= iMin) && (iValue <= iMax) )
         {
            *piValue    = iValue;
            fOk         = TRUE;
            fCollecting = FALSE;
         }
         else
         {
            GEN_PRINTF(" BAD Value=%d. Must be %d ... %d" CRLF, iValue, iMin, iMax);
            fCollecting = FALSE;
         }
      }
      else 
      {
         GEN_PRINTF(" Unknown Key=%d" CRLF, (char)iKey);
         fCollecting = FALSE;
      }
      if(fCollecting && fVal)
      {
         if(fHex) iValue *= 16;
         else     iValue *= 10;
         //
         iValue += iKey;
      }
   }
   GLOBAL_DebugSet(iBits);
   return(fOk);
}

//
// Function:   dbg_CollectText
// Purpose:    Collect a text string from STDIN
//
// Parms:      Buffer ptr, max size
// Returns:    TRUE if OK
// Note:       
//
static bool dbg_CollectText(char *pcText, int iMax)
{
   bool  fOk=FALSE, fCollecting=TRUE;
   int   iBits, iKey, iNr=0;

   //
   // Disable STDOUT while assembling new data
   //
   iBits = GLOBAL_DebugGet(MM_DEBUG_STDOUT);
   GLOBAL_DebugClr(MM_DEBUG_STDOUT);
   //
   GEN_PRINTF(" (%s):", pcText);
   while(fCollecting)
   {
      iKey = getchar();
      GEN_PRINTF("%c", (char)iKey);
      //
      if(iKey == 0x1b) fCollecting = FALSE;
      else if(isprint(iKey))
      {
         pcText[iNr+0] = (char) (iKey & 0xff);
         pcText[iNr+1] = 0;
         if(iNr < iMax) iNr++;
      }
      else 
      {
         fOk         = TRUE;
         fCollecting = FALSE;
      }
   }
   GLOBAL_DebugSet(iBits);
   return(fOk);
}

//
// Function:   dbg_DisplayDebugBits
// Purpose:    Display the current debug bits
//
// Parms:      
// Returns:    TRUE if OK
// Note:       
//
static void dbg_DisplayDebugBits()
{
   int iBits = GLOBAL_DebugGet(MM_DEBUG_ALL);

   if(iBits)
   {
      if(iBits & MM_DEBUG_COMMS)          GEN_PRINTF("DEBUG: (C) Comms print" CRLF);
      if(iBits & MM_DEBUG_GUARD)          GEN_PRINTF("DEBUG: (G) Guard print" CRLF);
      if(iBits & MM_DEBUG_FILE_COMMS)     GEN_PRINTF("DEBUG: (F) uCtl data to file" CRLF);
      if(iBits & MM_DEBUG_HOST)           GEN_PRINTF("DEBUG: (H) Host print" CRLF);
      if(iBits & MM_DEBUG_MAIL)           GEN_PRINTF("DEBUG: (M) Mail print" CRLF);
      if(iBits & MM_DEBUG_PRINT)          GEN_PRINTF("DEBUG: (P) Generic print" CRLF);
      if(iBits & MM_DEBUG_SERIAL)         GEN_PRINTF("DEBUG: (R) uCtl raw serial data" CRLF);
      if(iBits & MM_DEBUG_SENSOR)         GEN_PRINTF("DEBUG: (S) Sensor print" CRLF);
   }
   else                                   GEN_PRINTF("No Debug bits set" CRLF);
   //
   GEN_PRINTF("SafeMalloc balance=%d" CRLF, GLOBAL_GetMallocs());
   GEN_PRINTF("DebugLevel = %d" CRLF, pstMap->G_iDebugLevel);
}

// 
// Function:   dbg_ControllerCommand
// Purpose:    Send a KB char to the comms thread
// 
// Parameters: 
// Returns:    
// Note:       Special keys:
//             '<':  Switch uCtl mode OFF
// 
static void dbg_ControllerCommand(char cKey)
{
   switch(cKey)
   {
      case '<':
         GEN_PRINTF(CRLF CRLF);
         GEN_PRINTF("uController mode OFF" CRLF);
         //
         // Enable Sensor mode
         //
         pstMap->G_stCmd.iCommand  = SMCMD_HTTP_SENSOR;
         pstMap->G_stCmd.iSecsTout = 0;
         GLOBAL_DebugClr(MM_DEBUG_UCTL);
         GLOBAL_Signal(PID_COMM, SIGUSR2);
         break;

      default:
         //GEN_PRINTF("Signal Comms (0x%x)" CRLF, cKey);
         pstMap->G_pcMiacCmd[0] = cKey;
         pstMap->G_pcMiacCmd[1] = 0;
         GLOBAL_Notify(PID_COMM, GLOBAL_DBG_COM_CMD, SIGUSR1);
         break;
   }
}

//
// Function:   dbg_Execute
// Purpose:    Handle debug thread
//
// Parms:      
// Returns:    Exit codes
// Note:       
//
static int dbg_Execute()
{
   bool           fPrompt=FALSE;
   int            iCc=EXIT_CC_OKEE;
   int            iIdx, iKey;
   struct termios stOldt, stNewt;
   sigset_t       tBlockset;
   const DBCMD   *pstDbCmd;

   if(dbg_SignalRegister(&tBlockset) == FALSE)
   {
      LOG_Report(errno, "DBG", "dbg-Execute():Exit Register ERROR:");
      return(EXIT_CC_GEN_ERROR);
   }
   GLOBAL_SemaphoreInit(PID_DEBUG);
   GLOBAL_PidTimestamp(PID_DEBUG, 0);
   GLOBAL_PidSaveGuard(PID_DEBUG, GLOBAL_DBG_ALL_INI);
   //
   // Init ready
   // Wait for Host Run/Execute
   //
   GLOBAL_SetSignalNotification(PID_HOST, GLOBAL_DBG_ALL_INI);
   LOG_printf("dbg-Execute():Waiting for Host ALL Run" CRLF);
   dbg_WaitSignal(GLOBAL_HST_ALL_RUN, 0);
   //
   // Init ready
   //
   GLOBAL_DebugClr(MM_DEBUG_IDLE);
   GLOBAL_DebugClr(MM_DEBUG_UCTL);
   //
   // Run/Execute
   //   tcgetattr gets the parameters of the current terminal.
   //   Save terminal settings, and set non-canonical settings
   //
   tcgetattr(STDIN_FILENO, &stOldt);
   stNewt = stOldt;
   //
   //   ICANON normally takes care that one line at a time will be processed
   //   that means it will return if it sees a "\n" or an EOF or an EOL
   //
   stNewt.c_lflag &= ~(ICANON|ECHO);          
   //
   //   Those new settings will be set to stdin
   //   TCSANOW tells tcsetattr to change attributes immediately.
   //
   tcsetattr(STDIN_FILENO, TCSANOW, &stNewt);
   //
   // Turn OFF line buffering to stdout
   //
   setbuf(stdout, NULL);
   //
   // Default debug state is OFF
   //
   GLOBAL_DebugSet(MM_DEBUG_IDLE);
   LOG_printf("dbg-Execute():Waiting for DEBUG permission" CRLF);
   //
   dbg_WaitSignal(GLOBAL_HST_DBG_RUN, 0);
   //
   LOG_printf("dbg-Execute():Received Host run permission" CRLF);
   //
   while(fThreadRunning)
   {
      if(GLOBAL_DebugGet(MM_DEBUG_ENABLED))
      {
         //
         // Switch back and forth between Active and inactive mode
         //
         if(GLOBAL_DebugGet(MM_DEBUG_IDLE))
         {
            // Debugger is stand-by
            iKey = 0;
            do
            {
               if(dbg_KeyboardHit()) 
               {
                  iKey = getchar();
               }
               dbg_CheckSignals();
               GEN_Sleep(100);
            }
            while(fThreadRunning && (iKey != 0x1B));
            //pwjh GEN_PRINTF(CRLF "Debugger Active" CRLF "[DBG] ");
            fPrompt = FALSE;
            // Clean all DEBUG signals
            GLOBAL_GetSignalNotification(PID_DEBUG, GLOBAL_ALL_DBG_XXX);
            GLOBAL_DebugClr(MM_DEBUG_IDLE);
         }
         else
         {
            // Debugger is active
            if(!fPrompt)
            {
               GEN_PRINTF(CRLF "[DBG] ");
               fPrompt = TRUE;
            }
            if(dbg_KeyboardHit()) 
            {
               iKey = getchar();
               if(GLOBAL_DebugGet(MM_DEBUG_UCTL)) 
               {
                  //
                  // uController mode: signal COMMS thread
                  //
                  dbg_ControllerCommand((char)iKey);
               }
               else
               {
                  //
                  // No uController mode:
                  // Need to parse the key
                  //
                  pstDbCmd = stDebugCommands;
                  for(iIdx=0; iIdx<iNumDebugCommands; iIdx++, pstDbCmd++)
                  {
                     if(iKey == pstDbCmd->iCmd)
                     {
                        GEN_PRINTF("%c  %s" CRLF, (char)iKey, pstDbCmd->pcHelp);
                        if(!pstDbCmd->pfDbCmd(pstDbCmd))
                        {
                           GEN_PRINTF(CRLF);
                           GEN_PRINTF("Command abort" CRLF);
                        }
                        break;
                     }
                  }
                  fPrompt = FALSE;
               }
            }
         }
      }
      dbg_CheckSignals();
      GEN_Sleep(10);
   }
   //
   //   restore the old settings
   //
   tcsetattr( STDIN_FILENO, TCSANOW, &stOldt);
   //
   GLOBAL_SemaphoreDelete(PID_DEBUG);
   LOG_printf("dbg-Execute():Terminated" CRLF);
   return(iCc);
}

//
// Function:   dbg_KeyboardHit
// Purpose:    Test KB key
//
// Parms:      
// Returns:    TRUE if key is in
// Note:       
//
static bool dbg_KeyboardHit()
{
   bool           fCc=FALSE;
   int            iCc;
   fd_set         tFds;
   struct timeval stTv = { 0L, 0L };

   FD_ZERO(&tFds);
   FD_SET(0, &tFds);
   iCc = select(1, &tFds, NULL, NULL, &stTv);
   if(iCc > 0)      fCc = TRUE;
   else if(iCc < 0) PRINTF("dbg-KeyboardHit():ERROR %s" CRLF, strerror(errno));
   return(fCc);
}

//
// Function:   dbg_WaitSignal
// Purpose:    Wait for signal
//
// Parms:      Signal(s) to wait for, max time in mSecs(0=indef)
// Returns:    TRUE if OK
// Note:       
//
static bool dbg_WaitSignal(int iSignal, int iMsecs)
{
   bool     fCc=TRUE;
   bool     fWaiting=TRUE;
   int      iOpt;

   while(fThreadRunning && fWaiting)
   {
      iOpt = GLOBAL_SignalWait(PID_DEBUG, iSignal, iMsecs);
      switch(iOpt)
      {
         case 0:
            // Run debugger
            fWaiting = FALSE;
            break;

         case 1:
            // Timeout 
            fWaiting = FALSE;
            fCc      = FALSE;
            break;

         default:
         case -1:
            // Error
            LOG_Report(errno, "DBG", "dbg-WaitSignal():ERROR:");
            PRINTF("dbg-WaitSignal():ERROR(%s)" CRLF, strerror(errno));
            fWaiting = FALSE;
            fCc      = FALSE;
            break;
      }
   }
   return(fCc);
}

/*------  Local functions separator ------------------------------------
________________DEBUG_FUNCTIONS(){};
------------------------------x----------------------------------------*/

//
//    To rework the distance from sensor to water into actual level of the water in mm:
//
//               Sensor
//             |       |
//       +     +XXX=XXX+  +            S  = Offset Sensor to Bottom
//       |     |       |  |            F  = Offset Water to floater
//       |                |            Dx = Measured Raw Hex Distance by uController (in timerticks)
//       |                |Dx          W  = Actual Waterlevel
//       |     +       +  |Dm  
//       |     |       |  |                  Dx * 0.4
//       |     |=+===+=|  +   +        Dm = ---------- mm = 0.06800 * Dx mm
//       |S    | |   | |      |F              5.88235
//       |     | |   | |      |
//       |~~~~~|~~~~~~~|~~~~~~+~~~     Dx = 14.7059 * Dm  ticks
//       |     | |   | |      |        
//       |     |=+===+=|      |        Dm = S - F - W
//       |     |       |      |W       W  = S - F - Dm
//       |     |       |      |        pstMap->G_pcWaterLevel   = W  (mm)
//       |     |       |      |        pstMap->G_flDm           = Dm (mm)
//       |     |       |      |        pstMap->G_flSensorBottom = S  (mm)
//       |     |       |      |        pstMap->G_Floater        = F  (mm)
//    ---+-----+-------+------+--- 
//                            
//    The Sensor delivers the distance from the sensor to the top of the Floater Dx in timer ticks.
//    Save this value for later reference and rework the actual water Height as W = S - F - Dm
//
//    S  (mm) = pstMap->G_flSensorBottom = 630.0 mm
//    F  (mm) = pstMap->G_Floater        =  55.0 mm
//
//    W (mm)   Dx(dec)  Dx(hex)
//    360.0    3162     0x0c5a
//    355.0    3235     0x0ca3
//    350.0    3309     0x0ced   <-- Hi
//    345.0    3382     0x0d36
//    340.0    3456     0x0d80
//    335.0    3529     0x0dc9
//    330.0    3603     0x0e13
//
//    160.0    6103     9x17d7
//    155.0    6176     0x1829
//    150.0    6250     0x186a
//    145.0    6324     0x18b4
//    140.0    6397     0x18fd
//    130.0    6544     0x1990   <-- Lo
//    125.0    6618     0x19da
//    120.0    6691     0x1a23

// 
// Function:   dbg_ControllerHiWater
// Purpose:    Calculate Hi water threashold
// 
// Parameters: Command struct ptr
// Returns:    TRUE if ok
// Note:       Debug command 'T'
// 
static bool dbg_ControllerHiWater(const DBCMD *pstCmd)
{
   bool     fCc;
   int      iValue;
   char     cValue[8];
   double   flValue, flLevel;

   GEN_PRINTF("Hi Water");
   //
   GEN_SNPRINTF(cValue, 7, "%5.1f", pstMap->G_flHiWater);
   if( (fCc = dbg_CollectText(cValue, 7)) )
   {
      flLevel  = strtod(cValue, NULL);
      flValue  = pstMap->G_flSensorBottom - pstMap->G_flFloater - flLevel;
      flValue *= flTimerTickPerMm;
      flValue += 0.5;
      iValue   = (int)flValue;
      GEN_PRINTF("New Hi Water Level = %d (0x%04x)" CRLF, iValue, iValue);
      //
      // Update the Hi Water value also in the EEPROM image in persistent memory
      //
      MIAC_SetEepromMemory(MIAC_E_EEHIWATERHI, (u_int8)((iValue >> 8) & 0xFF));
      MIAC_SetEepromMemory(MIAC_E_EEHIWATERLO, (u_int8)(iValue & 0xFF));
   }
   return(fCc);
}

// 
// Function:   dbg_ControllerLoWater
// Purpose:    Calculate Lo water threashold
// 
// Parameters: Command struct ptr
// Returns:    TRUE if ok
// Note:       Debug command 't'
// 
static bool dbg_ControllerLoWater(const DBCMD *pstCmd)
{
   bool     fCc;
   int      iValue;
   char     cValue[8];
   double   flValue, flLevel;

   GEN_PRINTF("Lo Water");
   //
   GEN_SNPRINTF(cValue, 7, "%5.1f", pstMap->G_flLoWater);
   if( (fCc = dbg_CollectText(cValue, 7)) )
   {
      flLevel  = strtod(cValue, NULL);
      flValue  = pstMap->G_flSensorBottom - pstMap->G_flFloater - flLevel;
      flValue *= flTimerTickPerMm;
      flValue += 0.5;
      iValue   = (int)flValue;
      GEN_PRINTF("New Lo Water Level = %d (0x%04x)" CRLF, iValue, iValue);
      //
      // Update the Lo Water value also in the EEPROM image in persistent memory
      //
      MIAC_SetEepromMemory(MIAC_E_EELOWATERHI, (u_int8)((iValue >> 8) & 0xFF));
      MIAC_SetEepromMemory(MIAC_E_EELOWATERLO, (u_int8)(iValue & 0xFF));
   }
   return(fCc);
}

// 
// Function:   dbg_ControllerModeOff
// Purpose:    Turn uCtl mode off
// 
// Parameters: Command struct ptr
// Returns:    TRUE if ok
// Note:       Debug command '<' uCtl ON
//             Handled in dbg_ControllerCommand()
//             Signal the COMMS thread we EXIT uCtl mode.
// 
static bool dbg_ControllerModeOff(const DBCMD *pstCmd)
{
   return(TRUE);
}

// 
// Function:   dbg_ControllerModeOn
// Purpose:    Turn uCtl mode on/off
//
// Parameters: Command struct ptr
// Returns:    TRUE if ok
// Note:       Debug command '>' uCtl ON
//             Signal the COMMS thread we are in uCtl mode.
//             While in uCtl mode, the Comms thread will echo ALL incoming
//             chars to STDOUT, while the Debug thread will translate and 
//             foreward all KB chars to the Comms thread to be send to the uCtl.
// 
//             MIAC Commands
//             ================================================================
//             <ESC>: Log on (disable RPi record mode)
//             q    : Log off
//             <spc>: Command mode (disable all traces)
//
//             0..9 :
//             a..f : Accumulate hex byte 0x00...0xFF
//
//             M    : Set pump mode (Auto, ...)
//             P    : RPi Record mode ON
//             p    : RPi Record mode OFF
//             r    : Toggle EEPROM/RAM mode
//
//             ##=  : Open memory location
//             ##Cr : Store ## at open memory location and re-open 
//             ##.  : re-open same     memory location
//             ##-  : re-open previous memory location
//             ##+  : re-open next     memory location
//             l    : list TOT_SRAM lines by 8 columns memory locations
//             B    : Bank++ (0..3)
//             E    : show EEPROM status EECON1, EECON2
//             ================================================================
//
static bool dbg_ControllerModeOn(const DBCMD *pstCmd)
{
   if(!GLOBAL_DebugGet(MM_DEBUG_UCTL)) 
   {
      GEN_PRINTF(CRLF CRLF);
      GEN_PRINTF("uController mode ON" CRLF);
      //
      // Enable uController mode
      //
      pstMap->G_stCmd.iCommand  = SMCMD_UCTL_MODE;
      pstMap->G_stCmd.iSecsTout = -1;
      GLOBAL_DebugSet(MM_DEBUG_UCTL);
      GEN_MEMSET(pstMap->G_pcMiacCmd, 0x00, MAX_PATH_LENZ);
      GLOBAL_Signal(PID_COMM, SIGUSR2);
   }
   return(TRUE);
}

// 
// Function:   dbg_ControllerTransfer
// Purpose:    Request memory transfer uCtl to Host
// 
// Parameters: Command struct ptr
// Returns:    TRUE if ok
// Note:       Debug command 'X'
// 
static bool dbg_ControllerTransfer(const DBCMD *pstCmd)
{
   GEN_PRINTF("Request uCtl memory transfer" CRLF);
   GLOBAL_SetSignalNotification(PID_HOST, GLOBAL_DBG_HST_MEM);
   return(TRUE);
}

// 
// Function:   dbg_DebugCommsPrint
// Purpose:    Turn Debug uCtl communication print to STDOUT on/off
// 
// Parameters: Command struct ptr
// Returns:    TRUE if ok
// Note:       Debug command 'C'
// 
static bool dbg_DebugCommsPrint(const DBCMD *pstCmd)
{
   bool     fCc;
   int      iValue=(GLOBAL_DebugGet(MM_DEBUG_COMMS) ? 1 : 0);

   GEN_PRINTF("STDOUT Comms");
   //
   if( (fCc = dbg_CollectHexBcd(&iValue, 0, 1)) )
   {
      if(iValue) GLOBAL_DebugSet(MM_DEBUG_COMMS);
      else       GLOBAL_DebugClr(MM_DEBUG_COMMS);
   }
   dbg_DisplayDebugBits();
   return(fCc);
}

// 
// Function:   dbg_DebugGenericPrint
// Purpose:    Turn Debug generic print to STDOUT on/off
// 
// Parameters: Command struct ptr
// Returns:    TRUE if ok
// Note:       Debug command 'P'
// 
static bool dbg_DebugGenericPrint(const DBCMD *pstCmd)
{
   bool     fCc;
   int      iValue=(GLOBAL_DebugGet(MM_DEBUG_PRINT) ? 1 : 0);

   GEN_PRINTF("STDOUT Generic");
   //
   if( (fCc = dbg_CollectHexBcd(&iValue, 0, 1)) )
   {
      if(iValue) GLOBAL_DebugSet(MM_DEBUG_PRINT);
      else       GLOBAL_DebugClr(MM_DEBUG_PRINT);
   }
   dbg_DisplayDebugBits();
   return(fCc);
}

// 
// Function:   dbg_DebugGuardPrint
// Purpose:    Turn Debug HOST print to STDOUT on/off
// 
// Parameters: Command struct ptr
// Returns:    TRUE if ok
// Note:       Debug command 'G'
// 
static bool dbg_DebugGuardPrint(const DBCMD *pstCmd)
{
   bool     fCc;
   int      iValue=(GLOBAL_DebugGet(MM_DEBUG_GUARD) ? 1 : 0);

   GEN_PRINTF("STDOUT Guard");
   //
   if( (fCc = dbg_CollectHexBcd(&iValue, 0, 1)) )
   {
      if(iValue) GLOBAL_DebugSet(MM_DEBUG_GUARD);
      else       GLOBAL_DebugClr(MM_DEBUG_GUARD);
   }
   dbg_DisplayDebugBits();
   return(fCc);
}

// 
// Function:   dbg_DebugHostPrint
// Purpose:    Turn Debug HOST print to STDOUT on/off
// 
// Parameters: Command struct ptr
// Returns:    TRUE if ok
// Note:       Debug command 'H'
// 
static bool dbg_DebugHostPrint(const DBCMD *pstCmd)
{
   bool     fCc;
   int      iValue=(GLOBAL_DebugGet(MM_DEBUG_HOST) ? 1 : 0);

   GEN_PRINTF("STDOUT Host");
   //
   if( (fCc = dbg_CollectHexBcd(&iValue, 0, 1)) )
   {
      if(iValue) GLOBAL_DebugSet(MM_DEBUG_HOST);
      else       GLOBAL_DebugClr(MM_DEBUG_HOST);
   }
   dbg_DisplayDebugBits();
   return(fCc);
}

// 
// Function:   dbg_DebugMailPrint
// Purpose:    Turn Debug MAIL print to STDOUT on/off
// 
// Parameters: Command struct ptr
// Returns:    TRUE if ok
// Note:       Debug command 'M'
// 
static bool dbg_DebugMailPrint(const DBCMD *pstCmd)
{
   bool     fCc;
   int      iValue=(GLOBAL_DebugGet(MM_DEBUG_MAIL) ? 1 : 0);

   GEN_PRINTF("STDOUT Email");
   //
   if( (fCc = dbg_CollectHexBcd(&iValue, 0, 1)) )
   {
      if(iValue) GLOBAL_DebugSet(MM_DEBUG_MAIL);
      else       GLOBAL_DebugClr(MM_DEBUG_MAIL);
   }
   dbg_DisplayDebugBits();
   return(fCc);
}

// 
// Function:   dbg_DebugNoPrint
// Purpose:    Turn ALL Debug print to STDOUT off
// 
// Parameters: Command struct ptr
// Returns:    TRUE if ok
// Note:       Debug command '.'
// 
static bool dbg_DebugNoPrint(const DBCMD *pstCmd)
{
   GLOBAL_DebugClr(MM_DEBUG_STDOUT);
   //
   dbg_DisplayDebugBits();
   return(TRUE);
}

// 
// Function:   dbg_DebugRawSerialPrint
// Purpose:    Turn Debug uController raw serial data print
//             to STDOUT on/off
// Parameters: Command struct ptr
// Returns:    TRUE if ok
// Note:       Debug command 'H'
// 
static bool dbg_DebugRawSerialPrint(const DBCMD *pstCmd)
{
   bool     fCc;
   int      iValue=(GLOBAL_DebugGet(MM_DEBUG_SERIAL) ? 1 : 0);

   GEN_PRINTF("STDOUT uCtl");
   //
   if( (fCc = dbg_CollectHexBcd(&iValue, 0, 1)) )
   {
      if(iValue) GLOBAL_DebugSet(MM_DEBUG_SERIAL);
      else       GLOBAL_DebugClr(MM_DEBUG_SERIAL);
   }
   dbg_DisplayDebugBits();
   return(fCc);
}

// 
// Function:   dbg_DebugSensorPrint
// Purpose:    Turn Debug Sensor print to STDOUT on/off
// 
// Parameters: Command struct ptr
// Returns:    TRUE if ok
// Note:       Debug command 'S'
// 
static bool dbg_DebugSensorPrint(const DBCMD *pstCmd)
{
   bool     fCc;
   int      iValue=(GLOBAL_DebugGet(MM_DEBUG_SENSOR) ? 1 : 0);

   GEN_PRINTF("STDOUT Sensor");
   //
   if( (fCc = dbg_CollectHexBcd(&iValue, 0, 1)) )
   {
      if(iValue) GLOBAL_DebugSet(MM_DEBUG_SENSOR);
      else       GLOBAL_DebugClr(MM_DEBUG_SENSOR);
   }
   dbg_DisplayDebugBits();
   return(fCc);
}

// 
// Function:   dbg_Help
// Purpose:    Help command
// 
// Parameters: Command struct ptr
// Returns:    TRUE if ok
// Note:       Debug command 'h'
// 
static bool dbg_Help(const DBCMD *pstCmd)
{
   int            i;
   const DBCMD   *pstDbCmd=stDebugCommands;

   for(i=0; i<iNumDebugCommands; i++, pstDbCmd++)
   {
      GEN_PRINTF("  %c : %s" CRLF, (char)pstDbCmd->iCmd, pstDbCmd->pcHelp);
   }
   dbg_DisplayDebugBits();
   return(TRUE);
}

// 
// Function:   dbg_Linefeed
// Purpose:    Linefeed command
// 
// Parameters: Command struct ptr
// Returns:    TRUE if ok
// 
static bool dbg_Linefeed(const DBCMD *pstCmd)
{
   return(TRUE);
}

// 
// Function:   dbg_MallocBalance
// Purpose:    Show malloc balance
// 
// Parameters: Command struct ptr
// Returns:    TRUE if ok
// Note:       Debug command 'm'
// 
static bool dbg_MallocBalance(const DBCMD *pstCmd)
{
   GEN_PRINTF("SafeMalloc balance=%d" CRLF, GLOBAL_GetMallocs());
   return(TRUE);
}

// 
// Function:   dbg_Quit
// Purpose:    Quit debug thread
// 
// Parameters: Command struct ptr
// Returns:    TRUE if ok
// Note:       Debug command 'q'
// 
static bool dbg_Quit(const DBCMD *pstCmd)
{
   GEN_PRINTF("Debugger IDLE" CRLF CRLF CRLF);
   GLOBAL_DebugSet(MM_DEBUG_IDLE);
   return(TRUE);
}

// 
// Function:   dbg_QuitTerminate
// Purpose:    Quit ALL threads
// 
// Parameters: Command struct ptr
// Returns:    TRUE if ok
// Note:       Debug command 'Q'
// 
static bool dbg_QuitTerminate(const DBCMD *pstCmd)
{
   GEN_PRINTF("Request terminate" CRLF CRLF);
   GLOBAL_Notify(PID_HOST, GLOBAL_ALL_HST_END, SIGUSR1);
   fThreadRunning = FALSE;
   return(TRUE);
}

// 
// Function:   dbg_SendEmail
// Purpose:    Send email
// 
// Parameters: Command struct ptr
// Returns:    TRUE if ok
// Note:       Debug command 'E'
// 
static bool dbg_SendEmail(const DBCMD *pstCmd)
{
   bool     fCc;
   int      iValue=0;

   GEN_PRINTF("Send Email");
   //
   if( (fCc = dbg_CollectHexBcd(&iValue, 0, 1)) )
   {
      if(iValue)
      {
         GEN_PRINTF("Send Email request to host" CRLF);
         GLOBAL_SetSignalNotification(PID_HOST, GLOBAL_ALL_HST_EML);
      }
   }
   return(fCc);
}

// 
// Function:   dbg_SetDebugLevel
// Purpose:    Set debug level (verbose)
// 
// Parameters: Command struct ptr
// Returns:    TRUE if ok
// Note:       Debug command '?'
// 
static bool dbg_SetDebugLevel(const DBCMD *pstCmd)
{
   bool     fCc;
   int      iValue=pstMap->G_iDebugLevel;

   GEN_PRINTF("Debug Level");
   //
   if( (fCc = dbg_CollectHexBcd(&iValue, 0, 100)) )
   {
      pstMap->G_iDebugLevel = iValue;
      GEN_PRINTF("Debug Level = %d" CRLF, iValue);
   }
   return(fCc);
}

// 
// Function:   dbg_SetVerboseLevel
// Purpose:    Set verbose level
// 
// Parameters: Command struct ptr
// Returns:    TRUE if ok
// Note:       Debug command 'v'
// 
static bool dbg_SetVerboseLevel(const DBCMD *pstCmd)
{
   bool     fCc;
   int      iValue=pstMap->G_iVerbose;

   GEN_PRINTF("Verbose");
   //
   if( (fCc = dbg_CollectHexBcd(&iValue, 0x80000000, 0x7fffffff)) )
   {
      pstMap->G_iVerbose = iValue;
      GEN_PRINTF("Verbose = %d" CRLF, iValue);
   }
   return(fCc);
}

// 
// Function:   dbg_TruncateLogfile
// Purpose:    Truncate the log file to # lines
// 
// Parameters: Command struct ptr
// Returns:    TRUE if ok
// Note:       Debug command 'L'
// 
static bool dbg_TruncateLogfile(const DBCMD *pstCmd)
{
   bool     fCc;
   int      iValue=pstMap->G_iLogLines;

   GEN_PRINTF("Log Lines");
   //
   if( (fCc = dbg_CollectHexBcd(&iValue, 0, 0x7fffffff)) )
   {
      pstMap->G_iLogLines = iValue;
      if(GEN_FileTruncate((char *)pcLogfile, pstMap->G_iLogLines) != EXIT_CC_OKEE)
      {
         GEN_PRINTF("ERROR Truncated LOGfile:%s" CRLF, strerror(errno));
      }
      else
      {
         GEN_PRINTF("Truncated LOGfile to %d lines" CRLF, iValue);
         pstMap->G_llLogFilePos = 0;
      }
   }
   return(fCc);
}

/*------  Local functions separator ------------------------------------
_______________SIGNAL_FUNCTIONS(){};
------------------------------x----------------------------------------*/

// 
// Function:   dbg_SignalRegister
// Purpose:    Register all SIGxxx
// 
// Parameters: Blockset
// Returns:    TRUE if delivered
// 
static bool dbg_SignalRegister(sigset_t *ptBlockset)
{
  bool fCC = TRUE;

  // SIGSEGV:
  if( signal(SIGSEGV, &dbg_ReceiveSignalSegmnt) == SIG_ERR)
  {
     LOG_Report(errno, "DBG", "dbg-SignalRegister(): SIGSEGV ERROR");
     fCC = FALSE;
  }
  // SIGUSR1:
  if( signal(SIGUSR1, &dbg_ReceiveSignalUser1) == SIG_ERR)
  {
     LOG_Report(errno, "DBG", "dbg-SignalRegister(): SIGUSR1 ERROR");
     fCC = FALSE;
  }
  // SIGUSR2:
  if( signal(SIGUSR2, &dbg_ReceiveSignalUser2) == SIG_ERR)
  {
     LOG_Report(errno, "DBG", "dbg-SignalRegister(): SIGUSR2 ERROR");
     fCC = FALSE;
  }
  // CTL-X handler
  if( signal(SIGTERM, &dbg_ReceiveSignalTerm) == SIG_ERR)
  {
     LOG_Report(errno, "DBG", "dbg-SignalRegister(): SIGTERM ERROR");
     fCC = FALSE;
  }
  // Ctl-C handler
  if( signal(SIGINT, &dbg_ReceiveSignalInt) == SIG_ERR)
  {
     LOG_Report(errno, "DBG", "dbg-SignalRegister(): SIGINT ERROR");
     fCC = FALSE;
  }

  if(fCC)
  {
     //
     // Setup the SIGxxx events
     //
     sigemptyset(ptBlockset);
     sigaddset(ptBlockset, SIGSEGV);
     sigaddset(ptBlockset, SIGTERM);
     sigaddset(ptBlockset, SIGINT);
     sigaddset(ptBlockset, SIGUSR1);
     sigaddset(ptBlockset, SIGUSR2);
  }
  return(fCC);
}

//
// Function:   dbg_ReceiveSignalSegmnt
// Purpose:    Handle the SIGSEGV
//
// Parms:
// Returns:    TRUE if delivered
// Note:       Do NOT use LOG_xxx() here
//
static void dbg_ReceiveSignalSegmnt(int iSignal)
{
   if(fThreadRunning)
   {
      GLOBAL_SegmentationFault(__FILE__, __LINE__);
      LOG_SegmentationFault(__FILE__, __LINE__);
      //
      // (TRY TO) Cleanup background threads:
      //
      GLOBAL_Notify(PID_HOST, GLOBAL_ALL_HST_FLT, SIGTERM);
      LOG_Report(errno, "DBG", "dbg-ReceiveSignalSegmnt():Pid=%d", getpid());
      fThreadRunning = FALSE;
      GLOBAL_SemaphorePost(PID_DEBUG);
   }
   else exit(EXIT_CC_GEN_ERROR);
}

// 
// Function:   dbg_ReceiveSignalTerm
// Purpose:    Add the SIGTERM command in the buffer (Ctl-X)
// 
// Parameters: 
// Returns:    TRUE if delivered
// 
static void dbg_ReceiveSignalTerm(int iSignal)
{
   LOG_Report(0, "DBG", "dbg-ReceiveSignalTerm()");
   fThreadRunning = FALSE;
   GLOBAL_SemaphorePost(PID_DEBUG);
}

//
// Function:   dbg_ReceiveSignalInt
// Purpose:    System callback for SIGINT (Ctl-C)
//
// Parms:
// Returns:    TRUE if delivered
//
static void dbg_ReceiveSignalInt(int iSignal)
{
   LOG_Report(0, "DBG", "dbg-ReceiveSignalInt()");
   fThreadRunning = FALSE;
   GLOBAL_SemaphorePost(PID_DEBUG);
}

//
// Function:   dbg_ReceiveSignalUser1
// Purpose:    System callback for SIGUSR1 signal
//
// Parms:
// Returns:    
// Note:
//
static void dbg_ReceiveSignalUser1(int iSignal)
{
   GLOBAL_SemaphorePost(PID_DEBUG);
}

//
// Function:   dbg_ReceiveSignalUser2
// Purpose:    System callback for SIGUSR2 signal
//
// Parms:
// Returns:    
// Note:
//
static void dbg_ReceiveSignalUser2(int iSignal)
{
   LOG_Report(0, "DBG", "dbg-ReceiveSignalUser2()");
}


/*----------------------------------------------------------------------
______________COMMENT_FUNCTIONS(){}
------------------------------x----------------------------------------*/
#ifdef COMMENT


#endif   //COMMENT
