/*  (c) Copyright:  2020..2024  Patrn, Confidential Data
 *
 *  Workfile:           rpi_page.h
 *  Purpose:            Headerfile for HTTP pages in rpi-page.c
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    01 Nov 2020:      Ported from rpislim
 *    23 Apr 2024:      Split globals.h and locals.h
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _RPI_PAGE_H_
#define _RPI_PAGE_H_

typedef bool (*PFNCL)(NETCL *);

typedef struct DYNPAGE
{
   char    *pcFilename;
   PFNCL    pfHandler;
}  DYNPAGE;

typedef struct RPINFO
{
   char    *pcTrigger;           // Source trigger text
   char    *pcTitle;             // Virtual display title text
   u_int8   ubRow, ubCol;        // Virtual display coords
   int      iOffset;             // Source text skip offset
   int      iLength;             // Virtual display max text length
}  RPINFO;

//
// Global prototypes
//
bool  HTTP_DynamicPageHandler    (NETCL *, int);
bool  HTTP_HtmlLinkSnapshot      (NETCL *, const char *);
bool  HTTP_HtmlShowSnapshot      (NETCL *, const char *);
int   HTTP_InitDynamicPages      (int);
int   HTTP_PageIsDynamic         (NETCL *);
bool  HTTP_SendTextFile          (NETCL *, char *);
//
// Generic HLMT Page build calls
//
bool  HTTP_BuildRespondHeader    (NETCL *);
bool  HTTP_BuildRespondHeaderBad (NETCL *);
bool  HTTP_BuildRespondHeaderJson(NETCL *);
bool  HTTP_BuildStart            (NETCL *);
bool  HTTP_BuildLineBreaks       (NETCL *, int);
bool  HTTP_BuildDivStart         (NETCL *);
bool  HTTP_BuildDivEnd           (NETCL *);
bool  HTTP_BuildEnd              (NETCL *);
//
bool  HTTP_BuildTableStart       (NETCL *, const char *, u_int16, u_int16, u_int16);
bool  HTTP_BuildTableRowStart    (NETCL *, u_int16);
bool  HTTP_BuildTableRowEnd      (NETCL *);
bool  HTTP_BuildTableColumnText  (NETCL *, char *, u_int16);
bool  HTTP_BuildTableColumnLink  (NETCL *, char *, u_int16, char *);
bool  HTTP_BuildTableColumnNumber(NETCL *, u_int16, u_int16);
bool  HTTP_BuildTableColumnFloat (NETCL *, double, u_int16);
bool  HTTP_BuildTableEnd         (NETCL *);


#endif   //_RPI_PAGE_H_
