/*  (c) Copyright:  2020..2024 Patrn, Confidential Data 
 *
 *  Workfile:           rpi_srvr.c
 *  Purpose:            Dynamic HTTP server
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PIC 16f628A Ultrasoon sensor
 *
 *  Entry Points:       SRVR_Init()
 *                      Threads: (assume option -D for background execution):
 *                      HOST : Thread Parent
 *                               SIGINT
 *                               SIGUSR1
 *                               SIGUSR2
 *                      SRVR : Thread RPI Server
 *                               SIGINT
 *                               SIGUSR1
 *                               SIGUSR2
 *                               SIGSEGV
 *                               SIGPIPE
 *                      SLIM : Thread Smart Sensor comms  
 *                               SIGINT
 *                               SIGUSR1
 *                               SIGUSR2
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    19 Oct 2017:      Created
 *    05 Jan 2019:      Add sigpipe log                                   
 *    21 Jan 2019:      Make global pstMap
 *    30 Jun 2019:      Use new NET files
 *    29 Mar 2022:      Add Variadic macro's for PRINTF()
 *    24 Apr 2024:      Move to global/local startup 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/


#include <stdio.h>
#include <string.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <resolv.h>
#include <sys/types.h>
#include <sys/signal.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <errno.h>
//
#include <common.h>
#include "config.h"
#include "locals.h"
#include "globals.h"
//
#include "gen_http.h"
#include "rpi_page.h"
#include "rpi_func.h"
#include "rpi_slim.h"
#include "rpi_srvr.h"

//#define USE_PRINTF
#include <printx.h>

//
// Local prototypes
//
static void    srvr_Execute               (void);
static bool    srvr_WaitSignal            (int, int);
//
static void    srvr_HandleCompletion      (NETCL *);
static bool    srvr_CallbackConnection    (NETCL *);
static int     srvr_HandleHttpReply       (NETCL *);
static int     srvr_HandleHttpReplyHtml   (NETCL *);
static int     srvr_HandleHttpReplyJson   (NETCL *);
static int     srvr_HandleHttpRequest     (NETCL *);
//
static bool    srvr_SignalRegister        (sigset_t *);
static void    srvr_ReceiveSignalInt      (int);
static void    srvr_ReceiveSignalTerm     (int);
static void    srvr_ReceiveSignalUser1    (int);
static void    srvr_ReceiveSignalUser2    (int);
static void    srvr_ReceiveSignalSegmnt   (int);
static void    srvr_ReceiveSignalPipe     (int);
//
static bool    fThreadRunning = TRUE;

/*----------------------------------------------------------------------
_______________GLOBAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/

//
//  Function:   SRVR_Init
//  Purpose:    HTTP server init
//
//  Parms:      
//  Returns:    Startup code GLOBAL_XXX_INI
//
int SRVR_Init(void)
{
   pid_t    tPid;

   tPid = fork();
   //
   switch(tPid)
   {
      default:
         // Parent
         GLOBAL_PidPut(PID_SRVR, tPid);
         break;

      case 0:
         // Child: RPI HTTP-server
         GEN_Sleep(2000);
         srvr_Execute();
         LOG_Report(0, "SVR", "srvr-Init():Exit normally");
         exit(EXIT_CC_OKEE);
         break;

      case -1:
         // Error
         GEN_Printf("srvr-Init(): Error!"CRLF);
         LOG_Report(errno, "SVR", "srvr-Init():Exit fork ERROR:");
         exit(EXIT_CC_GEN_ERROR);
         break;
   }
   return(GLOBAL_SVR_ALL_INI);
}

/*----------------------------------------------------------------------
________________LOCAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/

//
//  Function:   srvr_Execute
//  Purpose:    Run the Raspberry Pi HTTP mini-server as a deamon
//
//  Parms:
//  Returns:
//
static void srvr_Execute(void)
{
   int         iPort, iButtonOff, iCnxs;
   sigset_t    tBlockset;
   NETCON     *pstNet=NULL;

   if(srvr_SignalRegister(&tBlockset) == FALSE) 
   {
      LOG_Report(errno, "SVR", "srvr-Execute():Exit fork ERROR:");
      exit(EXIT_CC_GEN_ERROR);
   }
   GLOBAL_SemaphoreInit(PID_SRVR);
   //
   // Set up server socket 
   //
   pstNet = NET_Init(pstMap->G_pcWwwDir, pstMap->G_pcRamDir);
   if( NET_ServerConnect(pstNet, pstMap->G_pcSrvrPort, HTTP_PROTOCOL) == -1)
   {
      LOG_printf("srvr-Execute():Server connect failed: exiting" CRLF);
      LOG_Report(errno, "SVR", "srvr-Execute():Exit NET connect ERROR:");
      exit(EXIT_CC_GEN_ERROR);
   }
   iPort = atoi(pstMap->G_pcSrvrPort);
   //
   // Turn ON verbose messages
   //
   pstNet->iVerbose = NET_VERBOSE_TPKT;
   //
   HTTP_InitDynamicPages(iPort);
   SLIM_InitDynamicPages(iPort);
   GLOBAL_GetHostName();
   GLOBAL_PidTimestamp(PID_SRVR, 0);
   //
   // HTTP Server does NOT require Guard checks
   //
   GLOBAL_PidSaveGuard(PID_SRVR, GLOBAL_SVR_ALL_INI);
   GLOBAL_PidSetGuard(PID_SRVR);
   //
   // Init ready
   // Wait for Host Run/Execute
   //
   GLOBAL_SetSignalNotification(PID_HOST, GLOBAL_SVR_ALL_INI);
   //pwjh srvr_WaitSignal(GLOBAL_HST_ALL_RUN, 0);
   PRINTF("srvr-Execute():Received Host run permission" CRLF);
   //
   // Wait for data
   //
   while(fThreadRunning)
   {
      NET_BuildConnectionList(pstNet);
      //
      // Wait for http data
      //
      if(NET_WaitForConnection(pstNet, HTTP_CONNECTION_TIMEOUT_MSECS) > 0)
      {
         PRINTF("srvr-Daemon(): Socket has data" CRLF); 
         //
         // There is data on some of the sockets
         //
         NET_HandleSessions(pstNet, &srvr_CallbackConnection, pstMap->G_pcHostIp, MAX_ADDR_LEN);
      }
      else
      {
         PRINTF("srvr-Daemon(): Socket NO data" CRLF); 
         //
         // Timeout: check 24 hour activity log
         // Handle the Guard duty here
         //
         GLOBAL_PidTimestamp(PID_SRVR, 0);
         GLOBAL_PidSetGuard(PID_SRVR);
         //
         iButtonOff = BUTTON(BUTTON_OFF);
         //PRINTF("srvr-Daemon():BUTTON=%d" CRLF, iButtonOff); 
         if(iButtonOff)
         {
            //
            // Button pressed
            //
         #ifdef DEBUG_ASSERT
            //
            // Test/Debug mode: No IO buttons
            //
            PRINTF("srvr-Daemon():Power OFF button: IGNORE HERE" CRLF); 
         #else
            GLOBAL_Signal(PID_HOST, SIGUSR2);
            PRINTF("srvr-Daemon():Power OFF button: signal host" CRLF); 
         #endif
         }
         //PRINTF("srvr-Daemon():no data" CRLF); 
         //
         // Check network stack
         //
         if((iCnxs = NET_ReportServerStatus(pstNet, NET_CHECK_COUNT)) == 0)
         {
            //
            // No connections available: reset all expired ones !
            //
            iCnxs = NET_ReportServerStatus(pstNet, NET_CHECK_LOG|NET_CHECK_FREE);
            LOG_Report(0, "SVR", "srvr-Daemon(): Now %d connections free again", iCnxs);
         }
      }
   }
   LOG_printf("srvr-Execute():Normal exit" CRLF);
   //
   // Close all socket connections
   //
   NET_ServerTerminate(pstNet);
   GLOBAL_SemaphoreDelete(PID_SRVR);
}

//
// Function:   srvr_WaitSignal
// Purpose:    Wait for signal
//
// Parms:      Signal(s) to wait for, max time in mSecs(0=indef)
// Returns:    TRUE if OK
// Note:       
//
static bool srvr_WaitSignal(int iSignal, int iMsecs)
{
   bool     fCc=TRUE;
   bool     fWaiting=TRUE;
   int      iOpt;

   while(fThreadRunning && fWaiting)
   {
      iOpt = GLOBAL_SignalWait(PID_SRVR, iSignal, iMsecs);
      switch(iOpt)
      {
         case 0:
            // Run debugger
            fWaiting = FALSE;
            break;

         case 1:
            // Timeout 
            fWaiting = FALSE;
            fCc = FALSE;
            break;

         default:
         case -1:
            // Error
            LOG_Report(errno, "GRD", "srvr-WaitSignal():ERROR:");
            PRINTF("srvr-WaitSignal():ERROR(%s)" CRLF, strerror(errno));
            fWaiting = FALSE;
            fCc = FALSE;
            break;
      }
   }
   return(fCc);
}

/*----------------------------------------------------------------------
_________________MISC_FUNCTIONS(){}
------------------------------x----------------------------------------*/

//  
// Function    :  srvr_CallbackConnection
// Description :  Handle the connection list
// 
// Parameters  :  Socket descriptor
// Returns     :  TRUE if OKee
// Note:          NET_HandleSessions(..) will callback here as soon as a complete
//                HTTP Get/POST request has been acquired through the socket.
//                This Callback will verify if the HTTP request concerns:
//                o a STATIC  page: Try to read it from file and return to the socket
//                o a DYNAMIC page: Lookup the page and forward to the registered handler
//
//                The handler will respond back with a SIGUSR2 signal with the appropriate
//                notification set.
//  
//  
static bool srvr_CallbackConnection(NETCL *pstCl)
{
   bool     fCc=TRUE;
   int      iTimeout;

   PRINTF("srvr-CallbackConnection()" CRLF);
   //
   // Data came in from the socket: check if we have something to do
   //
   if( (iTimeout = srvr_HandleHttpRequest(pstCl)) > 0) 
   {
      //
      // Command has been forwarded to a handler. It will report back through the semaphore (via SIGUSR2) 
      // Wait the timeout and act accordingly
      //
      switch( GLOBAL_SemaphoreWait(PID_SRVR, iTimeout) )
      {
         case 0:
            //
            // Server has completed
            //
            if( GLOBAL_GetSignalNotification(PID_SRVR, GLOBAL_ALL_SVR_VAR) ) srvr_HandleCompletion(pstCl);
            break;

         default:
         case -1:
            LOG_Report(errno, "SVR", "srvr-CallbackConnection():ERROR sem_timedwait");
            PRINTF("srvr-CallbackConnection():ERROR Sem_timedwait(errno=%d)" CRLF, errno);
            fCc = FALSE;
            break;

         case 1:
            //
            // Timeout:
            //
            LOG_Report(errno, "SVR", "srvr-CallbackConnection():sem_timedwait");
            PRINTF("srvr-CallbackConnection():Timeout(errno=%d)" CRLF, errno);
            //
            if( srvr_HandleHttpReply(pstCl) )
            {
               //
               // The command timed out: specific reply has already been send back to the app
               //
            }
            else
            {
               //
               // Nothing specific can be done: generate a generic response
               //
               srvr_HandleCompletion(pstCl);
            }
            break;
      }
   }
   NET_FlushCache(pstCl);
   //
   // Session is finished: close connection
   //
   PRINTF("srvr-CallbackConnection():Ready, socket disconnected." CRLF);
   return(fCc);
}

//  
//  Function    : srvr_HandleCompletion
//  Description : Handle the HTTP server completion code
//  
//  Parameters  : Socket descriptor
//  Returns     : Timeout in msecs
//  
static void srvr_HandleCompletion(NETCL *pstCl)
{
   switch(GLOBAL_Status(GEN_STATUS_ASK))
   {
      case GEN_STATUS_TIMEOUT:
         PRINTF("srvr-HandleCompletion():TIMEOUT" CRLF);
         RPI_ReportError(pstCl);
         break;

      case GEN_STATUS_ERROR:
         PRINTF("srvr-HandleCompletion():ERROR" CRLF);
         RPI_ReportError(pstCl);
         break;

      case GEN_STATUS_REJECTED:
         PRINTF("srvr-HandleCompletion():REJECTED" CRLF);
         RPI_ReportBusy(pstCl);
         break;

      case GEN_STATUS_REDIRECT:
         PRINTF("srvr-HandleCompletion():REDIRECTED" CRLF);
         HTTP_DynamicPageHandler(pstCl, pstMap->G_iCurDynPage);
         break;

      default:
         //PRINTF("srvr-HandleCompletion():OKee" CRLF);
         srvr_HandleHttpReply(pstCl);
         break;
   }
}

//  
//  Function    : srvr_HandleHttpReply
//  Description : Request has been completed: handle the reply back to the browser/application
//  
//  Parameters  : Socket descriptor
//  Returns     : 
//  
static int srvr_HandleHttpReply(NETCL *pstCl)
{
   bool     fCc=FALSE;
   FTYPE    tType;

   tType = GEN_GetDynamicPageType(pstMap->G_iCurDynPage);
   switch(tType)
   {
      default:
      case HTTP_HTML:
         fCc = srvr_HandleHttpReplyHtml(pstCl);
         break;

      case HTTP_JSON:
         fCc = srvr_HandleHttpReplyJson(pstCl);
         break;
   }
   return(fCc);
}

//  
//  Function    : srvr_HandleHttpReplyHtml
//  Description : Request has been completed: handle HTML reply back to the browser/application
//  
//  Parameters  : Socket descriptor
//  Returns     : True if handled
//  
static int srvr_HandleHttpReplyHtml(NETCL *pstCl)
{
   PRINTF("srvr-HandleHttpReplyHtml()" CRLF);
   return(FALSE);
}

//  
//  Function    : srvr_HandleHttpReplyJson
//  Description : Request has been completed: handle JSON reply back to the browser/application
//  
//  Parameters  : Socket descriptor
//  Returns     : True if handled
//  
static int srvr_HandleHttpReplyJson(NETCL *pstCl)
{
   PRINTF("srvr-HandleHttpReplyJson()" CRLF);
   return(FALSE);
}

//  
//  Function    : srvr_HandleHttpRequest
//  Description : Data came in through the socket: handle it
//  
//  Parameters  : Socket descriptor
//  Returns     : Timeout in msecs
//  
static int srvr_HandleHttpRequest(NETCL *pstCl)
{
   int      iWait=0, iIdx;
   FTYPE    tType;
   pid_t    tPid;
   int      ePid;

   //
   // Handle incoming HTTP GET request:
   //
   //   "GET /index.html HTTP/1.1" CR,LF
   //   "Host: 10.0.0.231" CR,LF
   //   "User-Agent: Mozilla/5.0 (Windows; U; WinGecko/20100722 Firefox/3.6.8 (.NET CLR 3.5.30729)" CR,LF
   //   "Accept: image/png,image/*;q=0.8,*/*;q=0.5" CR,LF
   //   "Accept-Encoding: gzip,deflate" CR,LF
   //   "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7" CR,LF
   //   "Keep-Alive: 115" CR,LF
   //   "Connection: keep-alive" CR,LF
   //   CR,LF
   //
   // Check if the requested URL is a dynamically generated page or if it should
   // be on the filesystem.
   //
   if( (iIdx = HTTP_PageIsDynamic(pstCl)) < 0 )
   {
      // DFD-0-3.3 (RPI Xmt STATIC page)
      // This page is a STATIC page (needs to come from the file system)
      //
      pstMap->G_iCurDynPage = -1;
      PRINTF("srvr-HandleHttpRequest():STATIC page (%s)" CRLF, pstCl->pcUrl);
      HTTP_RequestStaticPage(pstCl);
   }
   else
   {
      //
      // This page is an internal (DYNAMICALLY build) page
      // Check if we can do this ourselves here, or if it need additional
      // handling.
      //
      PRINTF("srvr-HandleHttpRequest():DYNAMIC page (%s)" CRLF, pstCl->pcUrl);
      pstMap->G_iCurDynPage = iIdx;
      //
      tType        = GEN_GetDynamicPageType(iIdx);
      ePid         = GEN_GetDynamicPagePid(iIdx);
      iWait        = GEN_GetDynamicPageTimeout(iIdx);
      //
      // Type is HTTP_HTML or HTTP_JSON
      // HTTP_HTML: ...cam.html?xxxx&-p17-p2....
      // HTTP_JSON: ...cam.jsom?command=xxxx&para1=yyyy&....
      //
      tPid = GLOBAL_PidGet(ePid);
      RPI_CollectParms(pstCl, tType);
      //
      PRINTF("srvr-HandleHttpRequest():Dynamic URL:Idx=%d, Pid=%d, Wait=%d, URL=%s" CRLF, iIdx, tPid, iWait, GEN_GetDynamicPageName(iIdx) );
      //
      // Signal the thread to handle it (if it is not us)
      //
      if(tPid > 0)
      {
         PRINTF("srvr-HandleHttpRequest():Forward request to Pid=%d, [%s]-[%s]" CRLF, tPid, pstCl->pcUrl, pstMap->G_pcCommand);
         GLOBAL_Notify(ePid, GLOBAL_SVR_HST_REQ, SIGUSR1);
      }
      else
      {
         if(HTTP_DynamicPageHandler(pstCl, iIdx))
         {
            //
            // Dynamic page returns TRUE if the current page request should complete the HTTP request
            //
            PRINTF("srvr-HandleHttpRequest():OKee :[%s]-[%s]" CRLF, pstCl->pcUrl, pstMap->G_pcCommand);
         }
         else
         {
            //
            // No reply was set up: send back error
            //
            PRINTF("srvr-HandleHttpRequest():ERROR:[%s]-[%s]" CRLF, pstCl->pcUrl, pstMap->G_pcCommand);
         }
      }
   }
   return(iWait);
}

/*----------------------------------------------------------------------
_______________SYSTEM_FUNCTIONS(){}
------------------------------x----------------------------------------*/

//
//  Function:   srvr_SignalRegister
//  Purpose:    Register all SIGxxx
//
//  Parms:      Blockset
//  Returns:    TRUE if delivered
//
static bool srvr_SignalRegister(sigset_t *ptBlockset)
{
  bool fCC = TRUE;

  // SIGUSR1:
  if( signal(SIGUSR1, &srvr_ReceiveSignalUser1) == SIG_ERR)
  {
     LOG_Report(errno, "SVR", "srvr-SignalRegister(): SIGUSR1 ERROR");
     fCC = FALSE;
  }
  // SIGUSR2:
  if( signal(SIGUSR2, &srvr_ReceiveSignalUser2) == SIG_ERR)
  {
     LOG_Report(errno, "SVR", "srvr-SignalRegister(): SIGUSR2 ERROR");
     fCC = FALSE;
  }
  // CTL-X handler
  if( signal(SIGTERM, &srvr_ReceiveSignalTerm) == SIG_ERR)
  {
     LOG_Report(errno, "SVR", "srvr-SignalRegister(): SIGTERM ERROR");
     fCC = FALSE;
  }
  // Ctl-C handler
  if( signal(SIGINT, &srvr_ReceiveSignalInt) == SIG_ERR)
  {
     LOG_Report(errno, "SVR", "srvr-SignalRegister(): SIGINT ERROR");
     fCC = FALSE;
  }
  // Segmentation fault
  if( signal(SIGSEGV, &srvr_ReceiveSignalSegmnt) == SIG_ERR)
  {
     LOG_Report(errno, "SVR", "srvr-ReceiveSignalSegmnt(): SIGSEGV ERROR");
     fCC = FALSE;
  }
  // Pipe error
  if( signal(SIGPIPE, &srvr_ReceiveSignalPipe) == SIG_ERR)
  {
     LOG_Report(errno, "SVR", "srvr-ReceiveSignalPipe(): SIGPIPE ERROR");
     fCC = FALSE;
  }

  if(fCC)
  {
     //
     // Setup the SIGxxx events
     //
     sigemptyset(ptBlockset);
     sigaddset(ptBlockset, SIGTERM);
     sigaddset(ptBlockset, SIGINT);
     sigaddset(ptBlockset, SIGUSR1);
     sigaddset(ptBlockset, SIGUSR2);
     sigaddset(ptBlockset, SIGSEGV);
     sigaddset(ptBlockset, SIGPIPE);
  }
  return(fCC);
}

//
//  Function:   srvr_ReceiveSignalTerm
//  Purpose:    Add the SIGTERM command in the buffer (Ctl-X)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void srvr_ReceiveSignalTerm(int iSignal)
{
   LOG_Report(0, "SVR", "srvr-ReceiveSignalTerm()");
   fThreadRunning = FALSE;
   GLOBAL_SemaphorePost(PID_SRVR);
}

//
//  Function:   srvr_ReceiveSignalInt
//  Purpose:    Add the SIGINT command in the buffer (Ctl-C)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void srvr_ReceiveSignalInt(int iSignal)
{
   LOG_Report(0, "SVR", "srvr-ReceiveSignalInt()");
   fThreadRunning = FALSE;
   GLOBAL_SemaphorePost(PID_SRVR);
}

//
//  Function:   srvr_ReceiveSignalSegmnt
//  Purpose:    Add the SIGSEGV command in the buffer (Ctl-C)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void srvr_ReceiveSignalSegmnt(int iSignal)
{
   if(fThreadRunning)
   {
      GLOBAL_SegmentationFault(__FILE__, __LINE__);
      LOG_SegmentationFault(__FILE__, __LINE__);
      LOG_Report(errno, "SVR", "srvr-ReceiveSignalSegmnt()");
      GLOBAL_Notify(PID_HOST, GLOBAL_ALL_HST_FLT, SIGTERM);
      fThreadRunning = FALSE;
      GLOBAL_SemaphorePost(PID_SRVR);
   }
   else exit(EXIT_CC_GEN_ERROR);
}

//
//  Function:   srvr_ReceiveSignalPipe
//  Purpose:    Add the SIGPIPE command in the buffer (Broken pipe)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void srvr_ReceiveSignalPipe(int iSignal)
{
   LOG_Report(errno, "SVR", "srvr-ReceiveSignalPipe()");
}

//
// Function:   srvr_ReceiveSignalUser1
// Purpose:    SIGUSR1 signal
//
// Parms:
// Returns:    
// Note:       SIGUSR1 is not used 
//
static void srvr_ReceiveSignalUser1(int iSignal)
{
   PRINTF("srvr-ReceiveSignalUser1()" CRLF);
}

//
// Function:   srvr_ReceiveSignalUser2
// Purpose:    System callback on receiving the SIGUSR2 signal
//
// Parms:
// Returns:    
// Note:       SIGUSR2 is not used
//
static void srvr_ReceiveSignalUser2(int iSignal)
{
}


