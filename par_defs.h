/*  (c) Copyright:  2020..2024  Patrn, Confidential Data
 *
 *  Workfile:           par_defs.h
 *  Purpose:            All defines for extracting rpikinshi Smart Sensor data
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    01 Nov 2020:      Ported from rpislim
 *    24 Nov 2020:      Add Pump Mode
 *    01 Dec 2020:      Add raw sensor distance
 *    05 May 2021:      Make drop/rise G_parameter
 *    24 Mar 2022:      Add MIAC update timers and Lo/Hi water data
 *    11 Jan 2023:      Add G_iDelete as extension to G_pcDelete
 *    23 Apr 2024:      Split globals.h and locals.h
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

//
// Macro elements:                  Example
// 
//       a  Enum              x     PAR_VERSION
//       b  iFunction         x     WB|PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_ALL
//       c  pcJson            x     "Version"
//       d  pcHtml            x     "vrs="
//       e  pcOption          x     "-v"
//       f  iValueOffset      x     offsetof(RPIMAP, G_pcSwVersion)
//       g  iValueSize        x     MAX_PARM_LEN
//       h  iChangedOffset    x     ALWAYS_CHANGED or offsetof(RPIMAP, G_ubVersionChanged)
//       i  iChanged          x     0
//       j  pcDefault         x     "v1.00-cr103"
//       k  pfHttpFun         x     http_CollectDebug
//
// Note  WB    (WarmBoot): the default value is restored on every restart of the App.
//
//       PAR_A (Ascii)  : ASCII parameter
//       PAR_F (Float)  : Floating point parameter (double representation)
//       PAR_B (Bcd)    : Integer parameter (BCD representation) 
//       PAR_H (Hex)    : Integer parameter (HEX representation) 
//
//       JSN_TXT        : JSON Text representation    ("Variable")
//       JSN_INT        : JSON Number representation  ( Variable )
//
//
// Macro    a                 b                                                                   c                 d         e        f                                      g                  h                                     i         j                        k
//          Enum,             ____iFunction___________________________________________________    pcJson,           pcHtml    pcOption iValueOffset                           iValueSize        iChangedOffset                         iChanged  pcDefault                pfHttpFun
//                               Txt/Int
// System
EXTRACT_PAR(PAR_COMMAND,     (WB|PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_ALL), "Command",        "cmd=",    "",      offsetof(RPIMAP, G_pcCommand),         MAX_PARM_LEN,     offsetof(RPIMAP, G_ubCommandChanged),  0,        "",                      NULL)
EXTRACT_PAR(PAR_STATUS,      (WB|PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_ALL), "Status",         "",        "",      offsetof(RPIMAP, G_pcStatus),          MAX_PARM_LEN,     ALWAYS_CHANGED,                        0,        "Gen-Idle",              NULL)
EXTRACT_PAR(PAR_VERBOSE,     (   PAR_H|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_PRM|PAR_ALL), "Verbose",        "",        "",      offsetof(RPIMAP, G_iVerbose),          sizeof(int),      NEVER_CHANGED,                         0,        "0",                     NULL)
EXTRACT_PAR(PAR_DEBUG,       (   PAR_H|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_PRM|PAR_ALL), "Debug",          "",        "",      offsetof(RPIMAP, G_iDebug),            sizeof(int),      ALWAYS_CHANGED,                        0,        "0",                     NULL)
EXTRACT_PAR(PAR_LEVEL,       (   PAR_B|JSN_INT|PAR____|PAR____|PAR____|PAR____|PAR_PRM|PAR_ALL), "Level",          "",        "",      offsetof(RPIMAP, G_iDebugLevel),       sizeof(int),      ALWAYS_CHANGED,                        0,        "0",                     NULL)
EXTRACT_PAR(PAR_LOGLINES,    (   PAR_B|JSN_INT|PAR____|PAR____|PAR____|PAR____|PAR_PRM|PAR_ALL), "LogLines",       "",        "",      offsetof(RPIMAP, G_iLogLines),         sizeof(int),      ALWAYS_CHANGED,                        0,        "1000",                  NULL)
EXTRACT_PAR(PAR_COMMSTO,     (   PAR_B|JSN_INT|PAR____|PAR____|PAR____|PAR____|PAR_PRM|PAR_ALL), "Comms",          "",        "",      offsetof(RPIMAP, G_iCommsTout),        sizeof(int),      ALWAYS_CHANGED,                        0,        "10000",                 NULL)
EXTRACT_PAR(PAR_MODE_55,     (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_PRM|PAR____), "Restart",        "",        "",      offsetof(RPIMAP, G_pcRestart),         MAX_PARM_LEN,     NEVER_CHANGED,                         0,        "REBOOT",                NULL)
// Misc
EXTRACT_PAR(PAR_VERSION,     (WB|PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_ALL), "Version",        "vrs=",    "",      offsetof(RPIMAP, G_pcSwVersion),       MAX_PARM_LEN,     ALWAYS_CHANGED,                        0,        VERSION,                 NULL)
EXTRACT_PAR(PAR_NRFILES,     (WB|PAR_A|JSN_INT|PAR____|PAR____|PAR____|PAR____|PAR_PRM|PAR____), "NumFiles",       "dir=",    "",      offsetof(RPIMAP, G_pcNumFiles),        MAX_PARM_LEN,     ALWAYS_CHANGED,                        0,        "0",                     NULL)
EXTRACT_PAR(PAR_FILTER,      (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_PRM|PAR____), "Filter",         "ftr=",    "",      offsetof(RPIMAP, G_pcFilter),          MAX_PATH_LEN,     ALWAYS_CHANGED,                        0,        "*.csv",                 NULL)
EXTRACT_PAR(PAR_DELETE,      (WB|PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_PRM|PAR____), "Delete",         "del=",    "",      offsetof(RPIMAP, G_pcDelete),          MAX_PARM_LEN,     ALWAYS_CHANGED,                        0,        "no",                    NULL)
EXTRACT_PAR(PAR_NUMDEL,      (WB|PAR_B|JSN_INT|PAR____|PAR____|PAR____|PAR____|PAR_PRM|PAR____), "NumDel",         "numdel=", "",      offsetof(RPIMAP, G_iDelete),           sizeof(int),      ALWAYS_CHANGED,                        0,        "0",                     NULL)
// Smart-Sensor data :
EXTRACT_PAR(PAR_SLM_SAM,     (WB|PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_PRM|PAR____), "Samples",        "",        "",      offsetof(RPIMAP, G_pcSmartSamples),    MAX_PARM_LEN,     ALWAYS_CHANGED,                        0,        "0",                     NULL)
EXTRACT_PAR(PAR_SLM_LEV,     (WB|PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_PRM|PAR____), "WaterLevel",     "",        "",      offsetof(RPIMAP, G_pcWaterLevel),      MAX_PARM_LEN,     ALWAYS_CHANGED,                        0,        "0",                     NULL)
EXTRACT_PAR(PAR_SLM_TMP,     (WB|PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_PRM|PAR____), "Temp",           "",        "",      offsetof(RPIMAP, G_pcTemperature),     MAX_PARM_LEN,     ALWAYS_CHANGED,                        0,        "0",                     NULL)
EXTRACT_PAR(PAR_SLM_MOD,     (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_PRM|PAR____), "Mode",           "",        "",      offsetof(RPIMAP, G_pcPumpMode),        MAX_PARM_LEN,     ALWAYS_CHANGED,                        0,        "0",                     NULL)
EXTRACT_PAR(PAR_CUR_PUMP,    (   PAR_H|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_PRM|PAR____), "Pump",           "pump=",   "",      offsetof(RPIMAP, G_iPumpMode),         sizeof(int),      NEVER_CHANGED,                         0,        "0",                     http_CollectPumpMode)
EXTRACT_PAR(PAR_SLM_PMP,     (WB|PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_PRM|PAR____), "Pon",            "",        "",      offsetof(RPIMAP, G_pcPumpOn),          MAX_PARM_LEN,     ALWAYS_CHANGED,                        0,        "0",                     NULL)
//
EXTRACT_PAR(PAR_WWW_DIR,     (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_PRM|PAR____), "WwwDir",         "",        "",      offsetof(RPIMAP, G_pcWwwDir),          MAX_PATH_LEN,     ALWAYS_CHANGED,                        0,        RPI_PUBLIC_WWW,          NULL)
EXTRACT_PAR(PAR_RAM_DIR,     (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_PRM|PAR____), "RamDir",         "",        "",      offsetof(RPIMAP, G_pcRamDir),          MAX_PATH_LEN,     ALWAYS_CHANGED,                        0,        RPI_WORK_DIR,            NULL)
EXTRACT_PAR(PAR_DEFAULT,     (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_PRM|PAR____), "Default",        "",        "",      offsetof(RPIMAP, G_pcDefault),         MAX_PATH_LEN,     ALWAYS_CHANGED,                        0,        RPI_PUBLIC_DEFAULT,      NULL)
// Streamer/server
EXTRACT_PAR(PAR_HOSTNAME,    (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_ALL), "Hostname",       "",        "",      offsetof(RPIMAP, G_pcHostname),        MAX_URL_LEN,      ALWAYS_CHANGED,                        0,        "",                      NULL)
EXTRACT_PAR(PAR_WAN_URL,     (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_PRM|PAR_ALL), "WanUrl",         "",        "",      offsetof(RPIMAP, G_pcWanUrl),          MAX_URL_LEN,      ALWAYS_CHANGED,                        0,        "4.icanhazip.com",       NULL)
EXTRACT_PAR(PAR_WAN_IP,      (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_PRM|PAR____), "WanIp",          "",        "",      offsetof(RPIMAP, G_pcWanIp),           MAX_ADDR_LEN,     NEVER_CHANGED,                         0,        "",                      NULL)
EXTRACT_PAR(PAR_HOST_IP,     (WB|PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_ALL), "HostIp",         "",        "",      offsetof(RPIMAP, G_pcHostIp),          MAX_ADDR_LEN,     offsetof(RPIMAP, G_ubIpAddrChanged),   0,        "",                      NULL)
EXTRACT_PAR(PAR_SRVR_PORT,   (WB|PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_PRM|PAR____), "ServerPort",     "",        "",      offsetof(RPIMAP, G_pcSrvrPort),        MAX_PARM_LEN,     NEVER_CHANGED,                         0,        "80",                    NULL)
EXTRACT_PAR(PAR_DEBUG_MASK,  (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_PRM|PAR____), "DebugMask",      "",        "",      offsetof(RPIMAP, G_pcDebugMask),       MAX_PARM_LEN,     NEVER_CHANGED,                         0,        "0",                     http_CollectDebug )
EXTRACT_PAR(PAR_RCU_KEY,     (WB|PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_PRM|PAR____), "RcuKey",         "key=",    "",      offsetof(RPIMAP, G_pcRcuKey),          MAX_PARM_LEN,     NEVER_CHANGED,                         0,        "0",                     http_CollectRcuKey)
//
EXTRACT_PAR(PAR_SLM_YEAR,    (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_PRM|PAR____), "Year",           "Year=",   "",      offsetof(RPIMAP, G_pcYear),            MAX_PARM_LEN,     NEVER_CHANGED,                         0,        "2020",                  NULL)
EXTRACT_PAR(PAR_SLM_MONTH,   (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_PRM|PAR____), "Month",          "Month=",  "",      offsetof(RPIMAP, G_pcMonth),           MAX_PARM_LEN,     NEVER_CHANGED,                         0,        "11",                    NULL)
EXTRACT_PAR(PAR_SLM_DAY,     (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_PRM|PAR____), "Day",            "Day=",    "",      offsetof(RPIMAP, G_pcDay),             MAX_PARM_LEN,     NEVER_CHANGED,                         0,        "01",                    NULL)
//
EXTRACT_PAR(PAR_PUMP_MIN,    (   PAR_B|JSN_INT|PAR____|PAR____|PAR____|PAR_MIA|PAR_PRM|PAR____), "PumpMin",        "pmin=",   "",      offsetof(RPIMAP, G_iPumpMinutes),      sizeof(int),      ALWAYS_CHANGED,                        0,        "420",                   NULL)
EXTRACT_PAR(PAR_SEN_OFF,     (   PAR_F|JSN_INT|PAR____|PAR____|PAR____|PAR____|PAR_PRM|PAR____), "Sensor",         "",        "",      offsetof(RPIMAP, G_flSensorBottom),    sizeof(double),   NEVER_CHANGED,                         0,        "630.0",                 NULL)
EXTRACT_PAR(PAR_SEN_DM,      (   PAR_F|JSN_INT|PAR____|PAR____|PAR____|PAR____|PAR_PRM|PAR____), "Dm",             "",        "",      offsetof(RPIMAP, G_flDm),              sizeof(double),   NEVER_CHANGED,                         0,        "0.0",                   NULL)
EXTRACT_PAR(PAR_FLOATER,     (   PAR_F|JSN_INT|PAR____|PAR____|PAR____|PAR____|PAR_PRM|PAR____), "Floater",        "",        "",      offsetof(RPIMAP, G_flFloater),         sizeof(double),   NEVER_CHANGED,                         0,        "70.0",                  NULL)
EXTRACT_PAR(PAR_MAX_DROP,    (   PAR_F|JSN_INT|PAR____|PAR____|PAR____|PAR____|PAR_PRM|PAR____), "MaxDrop",        "",        "",      offsetof(RPIMAP, G_flMaxDrop),         sizeof(double),   NEVER_CHANGED,                         0,        "20.0",                  NULL)
EXTRACT_PAR(PAR_MAX_RISE,    (   PAR_F|JSN_INT|PAR____|PAR____|PAR____|PAR____|PAR_PRM|PAR____), "MaxRise",        "",        "",      offsetof(RPIMAP, G_flMaxRise),         sizeof(double),   NEVER_CHANGED,                         0,        "20.0",                  NULL)
EXTRACT_PAR(PAR_MAX_INVAL,   (   PAR_B|JSN_INT|PAR____|PAR____|PAR____|PAR____|PAR_PRM|PAR____), "MaxInval",       "",        "",      offsetof(RPIMAP, G_iMaxInval),         sizeof(int),      NEVER_CHANGED,                         0,        "5",                     NULL)
//
EXTRACT_PAR(PAR_POND_LEVEL,  (   PAR_F|JSN_INT|PAR____|PAR____|PAR____|PAR____|PAR_PRM|PAR____), "PondLevel",      "",        "",      offsetof(RPIMAP, G_flPondLevel),       sizeof(double),   NEVER_CHANGED,                         0,        "0",                     NULL)
EXTRACT_PAR(PAR_HI_WATER,    (   PAR_F|JSN_INT|PAR____|PAR____|PAR____|PAR____|PAR_PRM|PAR____), "HiWater",        "",        "",      offsetof(RPIMAP, G_flHiWater),         sizeof(double),   NEVER_CHANGED,                         0,        "0",                     NULL)
EXTRACT_PAR(PAR_LO_WATER,    (   PAR_F|JSN_INT|PAR____|PAR____|PAR____|PAR____|PAR_PRM|PAR____), "LoWater",        "",        "",      offsetof(RPIMAP, G_flLoWater),         sizeof(double),   NEVER_CHANGED,                         0,        "0",                     NULL)
EXTRACT_PAR(PAR_WL_FLOW,     (   PAR_F|JSN_INT|PAR____|PAR____|PAR____|PAR____|PAR_PRM|PAR____), "Flow",           "",        "",      offsetof(RPIMAP, G_flFlowRate),        sizeof(double),   NEVER_CHANGED,                         0,        "0",                     NULL)
EXTRACT_PAR(PAR_SECS_FLOW,   (   PAR_B|JSN_INT|PAR____|PAR____|PAR____|PAR____|PAR_PRM|PAR____), "FlowSecs",       "",        "",      offsetof(RPIMAP, G_iSecsFlow),         sizeof(int),      NEVER_CHANGED,                         0,        "120",                   NULL)
//
EXTRACT_PAR(PAR_LASTFILE,    (WB|PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_PRM|PAR____), "LastFile",       "o=",      "-o",    offsetof(RPIMAP, G_pcLastFile),        MAX_PATH_LEN,     ALWAYS_CHANGED,                        0,        "",                      NULL)
EXTRACT_PAR(PAR_LASTFILESIZE,(WB|PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR_PRM|PAR____), "LastFileSize",   "",        "",      offsetof(RPIMAP, G_pcLastFileSize),    MAX_PARM_LEN,     ALWAYS_CHANGED,                        0,        "0",                     NULL)
// Hex MIAC addr/data
EXTRACT_PAR(PAR_MIAC_RAMA,   (   PAR_H|JSN_TXT|PAR____|PAR____|PAR____|PAR_MIA|PAR_PRM|PAR____), "Rama",           "rama=",   "",      offsetof(RPIMAP, G_iRamAddr),          sizeof(int),      ALWAYS_CHANGED,                        0,        "0",                     http_CollectMiacUpdate)
EXTRACT_PAR(PAR_MIAC_ROMA,   (   PAR_H|JSN_TXT|PAR____|PAR____|PAR____|PAR_MIA|PAR_PRM|PAR____), "Roma",           "roma=",   "",      offsetof(RPIMAP, G_iRomAddr),          sizeof(int),      ALWAYS_CHANGED,                        0,        "0",                     http_CollectMiacUpdate)
EXTRACT_PAR(PAR_MIAC_DATA,   (   PAR_H|JSN_TXT|PAR____|PAR____|PAR____|PAR_MIA|PAR_PRM|PAR____), "Data",           "data=",   "",      offsetof(RPIMAP, G_iMiacData),         sizeof(int),      ALWAYS_CHANGED,                        0,        "0",                     http_CollectMiacUpdate)
//
EXTRACT_PAR(PAR_MIAC_CMD,    (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR_MIA|PAR_PRM|PAR____), "MiacCmd",        "miac=",   "",      offsetof(RPIMAP, G_pcMiacCmd),         MIAC_CIRC_MASK,   ALWAYS_CHANGED,                        0,        "",                      http_CollectMiacCommand)
EXTRACT_PAR(PAR_MIAC_RES,    (   PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR_MIA|PAR_PRM|PAR____), "MiacRes",        "",        "",      offsetof(RPIMAP, G_pcMiacRes),         MIAC_CIRC_MASK,   ALWAYS_CHANGED,                        0,        "",                      NULL)
EXTRACT_PAR(PAR_MIAC_MEM,    (   PAR_B|JSN_INT|PAR____|PAR____|PAR____|PAR_MIA|PAR_PRM|PAR____), "MiacMem",        "mem=",    "",      offsetof(RPIMAP, G_iMiacMemory),       sizeof(int),      ALWAYS_CHANGED,                        0,        "0",                     http_CollectMiacUpdate)
// Mailx
EXTRACT_PAR(PAR_MAILX_SUBJ,  (   PAR_A|JSN_TXT|PAR____|PAR____|PAR_EML|PAR____|PAR____|PAR____), "MailSubj",       "",        "-s",    offsetof(RPIMAP, G_pcMailxSubj),       MAX_MAIL_LEN,     NEVER_CHANGED,                         0,        "Misc",                  NULL)
EXTRACT_PAR(PAR_MAILX_BODY,  (   PAR_A|JSN_TXT|PAR____|PAR____|PAR_EML|PAR____|PAR____|PAR____), "MailBody",       "",        "-d",    offsetof(RPIMAP, G_pcMailxBody),       MAX_MAIL_LEN,     NEVER_CHANGED,                         0,        "",                      NULL)
EXTRACT_PAR(PAR_MAILX_ATTM,  (   PAR_A|JSN_TXT|PAR____|PAR____|PAR_EML|PAR____|PAR_PRM|PAR____), "MailAttm",       "",        "",      offsetof(RPIMAP, G_pcMailxAttm),       MAX_MAIL_LEN,     NEVER_CHANGED,                         0,        "",                      NULL)
EXTRACT_PAR(PAR_MAILX_TO,    (   PAR_A|JSN_TXT|PAR____|PAR____|PAR_EML|PAR____|PAR_PRM|PAR____), "MailTo",         "",        "",      offsetof(RPIMAP, G_pcMailxTo),         MAX_MAIL_LEN,     NEVER_CHANGED,                         0,        "peter@patrn.nl",        NULL)
EXTRACT_PAR(PAR_MAILX_CC,    (   PAR_A|JSN_TXT|PAR____|PAR____|PAR_EML|PAR____|PAR_PRM|PAR____), "MailCc",         "",        "",      offsetof(RPIMAP, G_pcMailxCc),         MAX_MAIL_LEN,     NEVER_CHANGED,                         0,        "peter.hillen@ziggo.nl", NULL)
