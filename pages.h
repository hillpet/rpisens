/*  (c) Copyright:  2020..2024  Patrn, Confidential Data
 *
 *  Workfile:           pages.h
 *  Purpose:            Dynamic HTML snd JSON pages exposed to the internet
 *  
 *  
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    01 Nov 2020:      Ported from rpislim
 *    20 May 2021:      Backup csv files to persistent filesystem
 *    23 Apr 2024:      Split globals.h and locals.h
 *    10 May 2024:      Add pids.html
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

//
// The default (empty) URL page MUST be the first in this list
//
EXTRACT_DYN(DYN_HTML_EMPTY,   HTTP_HTML,     0, DYN_FLAG_PORT, "",                  http_DynPageEmpty          )
EXTRACT_DYN(DYN_HTML_INFO,    HTTP_HTML,     0, DYN_FLAG_PORT, "info.html",         http_DynPageDefault        )
EXTRACT_DYN(DYN_JSON_INFO,    HTTP_JSON,     0, DYN_FLAG_PORT, "info.json",         http_DynPageDefault        )
EXTRACT_DYN(DYN_JSON_PARMS,   HTTP_JSON,     0, DYN_FLAG_PORT, "parms.json",        http_DynPageParms          )
EXTRACT_DYN(DYN_HTML_PARMS,   HTTP_HTML,     0, DYN_FLAG_PORT, "parms.html",        http_DynPageParms          )
EXTRACT_DYN(DYN_HTML_LOG,     HTTP_HTML,     0, DYN_FLAG_PORT, "log.html",          http_DynPageLog            )
EXTRACT_DYN(DYN_JSON_LOG,     HTTP_JSON,     0, DYN_FLAG_PORT, "log.json",          http_DynPageLog            )
EXTRACT_DYN(DYN_HTML_EXIT,    HTTP_HTML,     0, DYN_FLAG_NONE, "exitslim.html",     http_DynPageExitSlim       )
EXTRACT_DYN(DYN_HTML_NEXIT,   HTTP_HTML,     0, DYN_FLAG_NONE, "exit.html",         http_DynPageExit           )
EXTRACT_DYN(DYN_JSON_NEXIT,   HTTP_JSON,     0, DYN_FLAG_PORT, "exit.json",         http_DynPageExit           )
EXTRACT_DYN(DYN_HTML_CSV,     HTTP_HTML,     0, DYN_FLAG_PORT, "csv.html",          http_DynPageCsvFiles       )
EXTRACT_DYN(DYN_HTML_CSVL,    HTTP_HTML,     0, DYN_FLAG_PORT, "level",             http_DynPageCsvFiles       )
EXTRACT_DYN(DYN_NONE_BOOT,    HTTP_HTML,     0, DYN_FLAG_NONE, "rebootslim",        http_DynPageRebootSlim     )
//
EXTRACT_DYN(DYN_NONE_PIDS,    HTTP_HTML,     0, DYN_FLAG_PORT, "pids",              http_DynPagePids           )
EXTRACT_DYN(DYN_HTML_PIDS,    HTTP_HTML,     0, DYN_FLAG_PORT, "pids.html",         http_DynPagePids           )
EXTRACT_DYN(DYN_HTML_PIDSR,   HTTP_HTML,     0, DYN_FLAG_PORT, "pidsref.html",      http_DynPagePids           )

