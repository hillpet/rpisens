/*  (c) Copyright:  2024 Patrn, Confidential Data 
 *
 *  Workfile:           gen_cmds.h
 *  Purpose:            Sensor command defines   
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    25 Apr 2024:      Created
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/
 
EXTRACT_SMC(SMCMD_NONE,          "NO command"            )
EXTRACT_SMC(SMCMD_IDLE_MODE,     "Sensor Idle"           )
EXTRACT_SMC(SMCMD_DLD_SENSOR,    "Sensor Mode"           )
EXTRACT_SMC(SMCMD_SET_RAM,       "Setup RAM Download"    )
EXTRACT_SMC(SMCMD_DLD_RAM,       "Download RAM"          )
EXTRACT_SMC(SMCMD_SET_ROM,       "Setup ROM Download"    )
EXTRACT_SMC(SMCMD_DLD_ROM,       "Download EEPROM"       )
EXTRACT_SMC(SMCMD_HTTP_MIAC,     "HTTP MIAC"             )
EXTRACT_SMC(SMCMD_HTTP_PUMP,     "HTTP Pump"             )
EXTRACT_SMC(SMCMD_HTTP_SENSOR,   "HTTP Sensor"           )
EXTRACT_SMC(SMCMD_UCTL_MODE,     "MicroCtl Mode"         )
EXTRACT_SMC(SMCMD_RESET,         "Sensor Reset"          )
EXTRACT_SMC(NUM_SMCMDS,          "# Modes"               )
//
EXTRACT_SMC(SMCMD_INIT,          "Sensor Init"           )
EXTRACT_SMC(SMCMD_DELAY,         "Sensor Delay"          )
EXTRACT_SMC(SMCMD_READY,         "Sensor Ready"          )
EXTRACT_SMC(SMCMD_ERROR,         "Sensor Error"          )
EXTRACT_SMC(SMCMD_EXIT,          "Sensor Exit"           )
