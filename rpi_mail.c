/*  (c) Copyright:  2024 Patrn, Confidential Data
 *
 *  Workfile:           rpi_mail.c
 *  Purpose:            Send mail functions
 *                      
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       
 *  Note:               
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    09 May 2024:      Split off from pikrelman
 *    28 May 2024:      Replace "\n" by CRLF
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <stdarg.h>
#include <signal.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>
//
#include <common.h>
#include "config.h"
#include "locals.h"
#include "globals.h"
//
#include "rpi_func.h"
#include "rpi_main.h"
#include "rpi_mailx.h"
#include "rpi_mail.h"
//
//#define USE_PRINTF
#include <printx.h>

//
// Local functions
//
static char   *rpi_GetArgGlobal           (const MARG *);
static char   *rpi_GetArgOption           (const MARG *);
//
// Mail Summary stuff
//
static const char   *pcLogfile         = RPI_PUBLIC_LOG_PATH;
static const char   *pcMailBodyFile    = RPI_WORK_DIR "KinshiMail.txt";
static const char   *pcNoAttachment    = "/home/public/www/noattachment.jpg";
static const char   *pcSubjQuote       = "\"";
static const char   *pcSubjMaxLen      = "\"Subject too long\"";
static const char   *pcSubjWan         = " ** WAN CHANGED! **";
static const char   *pcMailSubject     = "Kinshi summary ";
static const char   *pcMailSepLine50   = "==================================================" CRLF;
static const char   *pcMailTo          = "peter@patrn.nl";
static const char   *pcMailCc          = "peter.hillen@ziggo.nl";
//
// msmtp mailx parameters
//
//#define  DEBUG_OUTPUT
//
static const MARG stMailArguments[]    =
{
//    tArgs             pcArg                      pfGetArg
   {  0,                "mpack",                   rpi_GetArgOption    },    // 
   {  0,                "-s",                      rpi_GetArgOption    },    // 
   {  PAR_MAILX_SUBJ,   NULL,                      rpi_GetArgGlobal    },    // 
   {  0,                "-d",                      rpi_GetArgOption    },    // 
   {  PAR_MAILX_BODY,   NULL,                      rpi_GetArgGlobal    },    // 
   {  PAR_MAILX_ATTM,   NULL,                      rpi_GetArgGlobal    },    // 

   #ifdef   DEBUG_OUTPUT
   {  0,                "-o",                      rpi_GetArgOption    },    // 
   {  0,                "/mnt/rpicache/mail.out",  rpi_GetArgOption    },    // 
   #else    //DEBUG_OUTPUT
   {  PAR_MAILX_TO,     NULL,                      rpi_GetArgGlobal    },    // 
   {  PAR_MAILX_CC,     NULL,                      rpi_GetArgGlobal    },    //
   #endif   //DEBUG_OUTPUT
};
static const int iNumMailArguments = (sizeof(stMailArguments) / sizeof(MARG)) - 1;

//=============================================================================
// Global Data 
//=============================================================================

/*----------------------------------------------------------------------
_______________GLOBAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/

// 
// Function:   RPI_CreateMailBody
// Purpose:    Create the Mailbody containing the summarize of todays events
// 
// Parameters: 
// Returns:     
// 
bool RPI_CreateMailBody()
{
   int64    llPos;
   FILE    *ptFile;
   char     cDandT[MAX_TIME_LENZ];

   //
   // Write new Logfile entries to mail body
   //
   llPos = pstMap->G_llLogFilePos;
   if( GEN_FileCopyRecords((char *)pcMailBodyFile, (char *)pcLogfile, FALSE, &llPos) )
   {
      pstMap->G_llLogFilePos = llPos;
   }
   //
   // Mail: Add summarize results from today:
   //
   ptFile = safefopen((char *)pcMailBodyFile, "a+");
   if(ptFile)
   {
      RTC_ConvertDateTime(TIME_FORMAT_WW_DD_MMM_YYYY_HH_MM_SS, cDandT, RTC_GetSystemSecs());
      //
      // Put out summary data
      //
      GEN_FPRINTF(ptFile, CRLF);
      GEN_FPRINTF(ptFile, pcMailSepLine50);
      //
      // Put out malloc count
      //
      GEN_FPRINTF(ptFile, "SafeMalloc balance = %d" CRLF, GLOBAL_GetMallocs());
      //
      // Check if WAN changed since the last email
      //    WAN_NOT_FOUND     Not yet retrieved
      //    WAN_SAME          WAN retrieved and no change
      //    WAN_CHANGED       WAN retrieved and has changed
      //    WAN_ERROR         ERROR retrieving
      //
      if(pstMap->G_iWanAddr == WAN_NOT_FOUND)
      {
         GEN_FPRINTF(ptFile, "WAN Address Not yet detected!" CRLF);
      }
      else 
      {
         if(pstMap->G_iWanAddr & WAN_SAME)
         {
            GEN_FPRINTF(ptFile, "WAN Address still %s" CRLF, pstMap->G_pcWanAddr);
            pstMap->G_iWanAddr &= ~WAN_SAME;
         }
         if(pstMap->G_iWanAddr & WAN_CHANGED)
         {
            GEN_FPRINTF(ptFile, "WAN Address has changed to %s" CRLF, pstMap->G_pcWanAddr);
            pstMap->G_iWanAddr &= ~WAN_CHANGED;
         }
         if(pstMap->G_iWanAddr & WAN_ERROR)
         {
            GEN_FPRINTF(ptFile, "WAN Address ERROR in detection, last IP was %s" CRLF, pstMap->G_pcWanAddr);
            pstMap->G_iWanAddr &= ~WAN_ERROR;
         }
         if(pstMap->G_iWanAddr & WAN_NO_URL)
         {
            GEN_FPRINTF(ptFile, "NO WAN URL was set in persistent memory" CRLF);
            pstMap->G_iWanAddr &= ~WAN_NO_URL;
         }
      }
      //
      // Add Kinshi summary data here
      //
      GEN_FPRINTF(ptFile, "Kinshi Summary on [%s]:" CRLF, cDandT);
      GEN_FPRINTF(ptFile, pcMailSepLine50);
      GEN_FPRINTF(ptFile, "Pond Level     : %.1f mm" CRLF,     pstMap->G_flPondLevel);
      GEN_FPRINTF(ptFile, "High-Water     : %.1f mm" CRLF,     pstMap->G_flHiWater);
      GEN_FPRINTF(ptFile, "Low -Water     : %.1f mm" CRLF,     pstMap->G_flLoWater);
      GEN_FPRINTF(ptFile, "Flow rate      : %.1f mm/min" CRLF, pstMap->G_flFlowRate);
      GEN_FPRINTF(ptFile, "Sensor-Bottom  : %.1f mm" CRLF,     pstMap->G_flSensorBottom);
      GEN_FPRINTF(ptFile, "Floater Offset : %.1f mm" CRLF,     pstMap->G_flFloater);
      GEN_FPRINTF(ptFile,  CRLF);
      safefclose(ptFile);
      if(GLOBAL_DebugPrint(MM_DEBUG_MAIL, 1)) LOG_printf("RPI-CreateMailBody():Created %s OK" CRLF, pcMailBodyFile);
   }
   else
   {
      LOG_Report(errno, "EML", "RPI-CreateMailBody():Open ERROR:%s", pcMailBodyFile);
      if(GLOBAL_DebugPrint(MM_DEBUG_MAIL, 1)) LOG_printf("RPI-CreateMailBody():Open ERROR:%s" CRLF, pcMailBodyFile);
   }
   return(TRUE);
}

// 
// Function:   RPI_SendMail
// Purpose:    Mail a summarize of todays events
// 
// Parameters:  
// Returns:
// Note:       Keep track of max subject length.     
// 
bool RPI_SendMail()
{
   bool     fCc=TRUE;
   int      iLen;
   char    *pcPar;

   //
   // Send mail: Summarize Kinshi stats from today:
   //
   RPI_CreateMailBody();
   //
   // PAR_MAILX_SUBJ
   // PAR_MAILX_BODY
   // PAR_MAILX_ATTM
   // PAR_MAILX_TO
   // PAR_MAILX_CC
   //
   // mpack -s "Subject xxx" -d message_body_file.txt [-m maxsize] [-c content-type] file mail-address
   //
   // pcPar size is MAX_MAIL_LEN bytes. Make sure the subjectline fits 
   // Subject line is between double quotes !!
   //
   pcPar = GLOBAL_GetParameter(PAR_MAILX_SUBJ);
   iLen  = 1;                                         // "
   iLen += GEN_STRLEN(pcMailSubject);                 // Kinshi summary
   iLen += GEN_STRLEN(pstMap->G_pcHostname);          // (PatrnRpi11(xxx)
   iLen += GEN_STRLEN(pcSubjWan);                     // ** WAN changed **
   iLen += 1;                                         // "
   if(iLen > GLOBAL_GetParameterSize(PAR_MAILX_SUBJ))
   {
      if(GLOBAL_DebugPrint(MM_DEBUG_MAIL, 1)) LOG_printf("RPI-SendMail():Subject line too long: %d bytes" CRLF, iLen);
      GEN_STRCPY(pcPar, pcSubjMaxLen);                // Subject line was too long
   }
   else
   {
      GEN_STRCPY(pcPar, pcSubjQuote);                 // Add Subject line
      GEN_STRCAT(pcPar, pcMailSubject);
      GEN_STRCAT(pcPar, pstMap->G_pcHostname);
      //
      // Check if WAN changed, if so add it to the subject line
      //
      if(pstMap->G_iWanAddr == WAN_CHANGED)
      {
         //
         // WAN_ERROR        -1: ERROR retrieving
         // WAN_NOT_FOUND     0: Not yet retrieved
         // WAN_SAME          1: WAN retrieved and no change
         // WAN_CHANGED       2: WAN retrieved and has changed
         //
         GEN_STRCAT(pcPar, pcSubjWan);
      }
      GEN_STRCAT(pcPar, pcSubjQuote);
      if(GLOBAL_DebugPrint(MM_DEBUG_MAIL, 1)) LOG_printf("RPI-SendMail():Subject:%s" CRLF, pcPar);
   }
   if( (pcPar = GLOBAL_GetParameter(PAR_MAILX_BODY)) )   GEN_STRCPY(pcPar, pcMailBodyFile);
   //
   // file to attach
   //
   pcPar = GLOBAL_GetParameter(PAR_MAILX_ATTM);
   if(GEN_STRLEN(pcPar)) 
   {
      //
      // We have a file to attach
      //
      if(!GEN_FileExists(pcPar))
      {
         //
         // Attchment file does NOT exist: use generic file
         //
         if(GLOBAL_DebugPrint(MM_DEBUG_MAIL, 1)) LOG_printf("RPI-SendMail():Attach file %s does NOT exist" CRLF, pcPar);
         GEN_STRCPY(pcPar, pcNoAttachment);
      }
   }
   else
   {
      //
      // No attchment given: use generic file
      //
      GEN_STRCPY(pcPar, pcNoAttachment);
   }
   if(GLOBAL_DebugPrint(MM_DEBUG_MAIL, 1)) LOG_printf("RPI-SendMail():Attach file %s" CRLF, pcPar);
   //
   // Email addresses
   //
   if( (pcPar = GLOBAL_GetParameter(PAR_MAILX_TO))   )   GEN_STRCPY(pcPar, pcMailTo);
   if(GLOBAL_DebugPrint(MM_DEBUG_MAIL, 1)) LOG_printf("RPI-SendMail():TO: %s" CRLF, pcPar);
   if( (pcPar = GLOBAL_GetParameter(PAR_MAILX_CC))   )   GEN_STRCPY(pcPar, pcMailCc);
   if(GLOBAL_DebugPrint(MM_DEBUG_MAIL, 1)) LOG_printf("RPI-SendMail():CC: %s" CRLF, pcPar);
   //
   fCc = MAILX_SendMail(stMailArguments, iNumMailArguments);
   return(fCc);
}

//
// Function:   RPI_WaitMailxCompletion
// Purpose:    Wait for motion mailx completion
//             
// Parms:      SecsNow, Timeout secs
// Returns:    New absolute timeout secs or 0 if done.
// Note:       
// 
u_int32 RPI_WaitMailxCompletion(u_int32 ulSecsNow, u_int32 ulSecsTimeout)
{
   int      iCc;
   pid_t    tPid;

   if(ulSecsNow > ulSecsTimeout)
   {
      //
      // Mailx should be completed by now. If not, something went
      // wrong and we will terminate Mailx and report back.
      //
      tPid = GLOBAL_PidGet(PID_MAILX);
      if(tPid)
      {
         //
         // Check if the Mailx Helper thread is still running:
         //     0:   OKee, PID not found
         //    +1:   Hmmm, PID Still running
         //    -1:   OKee, PID Zombie or error
         //
         iCc = GEN_WaitProcessTimeout(tPid, 200);
         switch(iCc)
         {
            case 0:
               GLOBAL_PidPut(PID_MAILX, 0);
               break;

            case 1:
               LOG_Report(0, "EML", "RPI-WaitMailxCompletion():Mailx still running: force exit");
               PRINTF("RPI-WaitMailxCompletion():Mailx still running: force exit" CRLF);
               //
               GEN_KillProcess(tPid, DO_FRIENDLY, KILL_TIMEOUT_FORCED);
               GEN_WaitProcess(tPid);
               GLOBAL_PidPut(PID_MAILX, 0);
               break;

            default:
               PRINTF("RPI-WaitMailxCompletion():Mailx is zombie" CRLF);
               GEN_WaitProcess(tPid);
               break;
         }
      }
      ulSecsTimeout = 0;
   }
   return(ulSecsTimeout);
}


/*----------------------------------------------------------------------
________________LOCAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/


/*------  Local functions separator ------------------------------------
__ARGS_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

//
// Function:   rpi_GetArgGlobal
// Purpose:    Get Argument from the global list, insert value from Global parameter
//             
// Parms:      Argument entry
// Returns:    Composed pcArgument
// Note:       The result is allocated memory: Since the OS will free this memory,
//             we should NOT use safemalloc() here as to not confuse our mallc admin.
//
//             pstArg-> PAR_xxxx
//                      "-opt"
//
static char *rpi_GetArgGlobal(const MARG *pstArg)
{
   int         iLenArg;
   const char *pcPar;
   char       *pcArg=NULL;

   pcPar = (char *)GLOBAL_GetParameter(pstArg->tArgs);
   if(pcPar)
   {
      iLenArg = GEN_STRLEN(pcPar);
      if(iLenArg)
      {
         pcArg = (char *) malloc(iLenArg+8);
         //
         // Copy args and vars to destination
         //
         GEN_STRCPY(pcArg, pcPar);
         PRINTF("rpi-GetArgGlobal():Arg=%s" CRLF, pcArg);
      }
   }
   return(pcArg);
}

//
// Function:   rpi_GetArgOption
// Purpose:    Get Argument: Option value
//             
// Parms:      Argument entry
// Returns:    Composed pcArgument
// Note:       The result is allocated memory: Since the OS will free this memory,
//             we should NOT use safemalloc() here as to not confuse our mallc admin.
//
static char *rpi_GetArgOption(const MARG *pstArg)
{
   int         iLenArg;
   const char *pcOpt;
   char       *pcArg=NULL;

   pcOpt = pstArg->pcArg;
   if(pcOpt)
   {
      iLenArg = GEN_STRLEN(pcOpt);
      if(iLenArg)
      {
         pcArg = (char *) malloc(iLenArg+8);
         //
         // Copy args and vars to destination
         //
         GEN_STRCPY(pcArg, pcOpt);
         PRINTF("rpi-GetArgNull():Arg=%s" CRLF, pcArg);
      }
   }
   return(pcArg);
}

/*----------------------------------------------------------------------
______________COMMENT_FUNCTIONS(){}
------------------------------x----------------------------------------*/
#ifdef COMMENT

#endif   //COMMENT
