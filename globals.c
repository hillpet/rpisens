/*  (c) Copyright:  2023..2024  Patrn ESS, Confidential Data
 *
 *  Workfile:           globals.c
 *  Purpose:            Global variables for Raspberry pi HTTP server
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    07 Apr 2023:      Ported from rpidraw
 *    24 Apr 2024:      Move to global/local startup 
 *    02 May 2024:      SIGUSR2 to command Comms
 *    04 May 2024:      Add GLOBAL_GetStatusText
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <stdarg.h>
#include <signal.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>
//
#include <common.h>
#include "config.h"
//
#define DEFINE_GLOBALS
#include "locals.h"
#include "globals.h"
//
#include "rpi_func.h"

//#define USE_PRINTF
#include <printx.h>

//
// Global data
//
const char *pcMapFile = RPI_MAP_PATH;

//
// Local functions
//
static bool    global_CreateMap        (void);
static void    global_InitMemory       (MAPSTATE);
static bool    global_Open             (const char *, int);
static void    global_PidsInit         (void);
static int     global_RestoreMapfile   (void);
static void    global_RestoreDefaults  (bool);
static int     global_SaveMapfile      (void);
//
// Static variables
//
static int        iGlobalMapSize     = 0;
static int        iFdMap             = 0;
static size_t     tGlobalAllocated   = 0;
//
static const char   *pcShellRestore  = "cp " RPI_BACKUP_DIR RPI_BASE_NAME ".* " RPI_WORK_DIR;
static const char   *pcShellBackup   = "cp " RPI_WORK_DIR   RPI_BASE_NAME ".* " RPI_BACKUP_DIR;
static const char   *pcDirRestoreSrc = RPI_BACKUP_DIR;
static const char   *pcDirRestoreDst = RPI_WORK_DIR;
static const char   *pcAppText       = "-" RPI_APP_TEXT;
static const char   *pcErrPath       = RPI_ERROR_PATH;
//

//
// These global settings are being initialized on EVERY RESTART of the Apps.
//
static const GLOBALS stGlobalParameters[] =
{
//
//    Macro elements:               Example
//  
//       a  Enum                    PAR_VERSION
//       b  iFunction         x     WB|PAR_A|JSN_TXT|PAR____|PAR____|PAR____|PAR____|PAR____|PAR_ALL
//       c  pcJson                  "Version"
//       d  pcHtml                  "vrs="
//       e  pcOption                "-v"
//       f  iValueOffset      x     offsetof(RPIMAP, G_pcSwVersion)
//       g  iValueSize        x     MAX_PARM_LEN
//       h  iChangedOffset    x     ALWAYS_CHANGED or offsetof(RPIMAP, G_ubVersionChanged)
//       i  iChanged          x     0
//       j  pcDefault         x     "v1.00-cr103"
//       k  pfHttpFun               http_CollectXxxxx
//
//    b           e           j           i           h              f              g           k
//    iFunction   pcOption    pcDefault   iChanged    iChangedOffset iValueOffset   iValueSize  pfHttpFun
//
#define  EXTRACT_PAR(a,b,c,d,e,f,g,h,i,j,k)    {b,e,j,i,h,f,g,k},
#include "par_defs.h"   
#undef   EXTRACT_PAR
   {  0,          NULL,       NULL,       0,          0,             0,             0,          NULL  }
};

//
// Status strings
//
static const char *pcGenStatus[] = 
{
#define  EXTRACT_ST(a,b)   b,
#include "gen_stats.h"
#undef   EXTRACT_ST
   NULL
};
//
// PID list
//
static const PIDL stDefaultPidList[] =
{
// Pid-file:   ePid, pcPid, pcHelp
// PIDL:       tPid, ptSem, iFlag, iNotify, ulTs, iCount, pfnInit, pcName, pcHelp
//
#define  EXTRACT_PID(a,b,c)   {0,NULL,0,0,0,0,NULL,b,c},
#include "par_pids.h"
#undef   EXTRACT_PID
};

/*----------------------------------------------------------------------
__________________SINGLE_THREAD(){}
------------------------------x----------------------------------------*/

//
// Function:   GLOBAL_Init
// Purpose:    Init all global variables
//
// Parms:      
// Returns:    TRUE if mmap was found correctly
// Note:       These files are being called before the other threads are 
//             active and there is no MAP file or LOG file.
//             Only use printf()
//
bool GLOBAL_Init(void)
{
   bool     fCc=FALSE;
   int      iResetLength;
   u_int8  *pubReset;
   
   //
   // (Try to) copy the mmap file from SD to RAM disk
   // Create the global MMAP mapping
   //
   if(global_RestoreMapfile() != 0)
   {
      printf("GLOBAL-Init:No MAP file restored from file!" CRLF);
   }
   fCc = global_CreateMap();
   
   //================================================================================
   // Mutex is OKee: LOG-file can be openened from here on
   //================================================================================
   // Reset protected global settings every restart :
   //    G_iResetStart ... G_iResetEnd
   //
   pubReset     = (u_int8 *)((void *)pstMap + offsetof(RPIMAP, G_iResetStart));
   iResetLength = offsetof(RPIMAP, G_iResetEnd) - offsetof(RPIMAP, G_iResetStart);
   printf("GLOBAL-Init: Reset MAP %d reset-parm bytes" CRLF, iResetLength);
   GEN_MEMSET(pubReset, 0x00, iResetLength);
   // Mark start & end
   pstMap->G_iResetStart = 0x66666666;
   pstMap->G_iResetEnd   = 0x99999999;
   //
   pstMap->G_stVirtLcd.ubVirtCols = LCD_VIRTUAL_COLS;
   pstMap->G_stVirtLcd.ubVirtRows = LCD_VIRTUAL_ROWS;
   //
   // Load global settings every restart of selected vars (WB) 
   //
   global_RestoreDefaults(FALSE);
   global_PidsInit();
   return(fCc);
}

//
// Function:   global_CreateMap
// Purpose:    Read the mapping from the file
// Parms:
//
// Returns:    TRUE if mmap was OKee
// Note:       Make sure to setup the GLOBAL mutex as soon as we have a valid MAP file
//             before using functions that make use of the mutex !
//
//             Mutex init types:
//                PTHREAD_MUTEX_NORMAL
//                PTHREAD_MUTEX_ERRORCHECK
//                PTHREAD_MUTEX_RECURSIVE
//                PTHREAD_MUTEX_DEFAULT
//
//=============================================================================
//             On critical errors this function with exit() theApp !
//=============================================================================
//
static bool global_CreateMap(void)
{
   pthread_mutexattr_t tMutexAttr;
   bool  fCc=FALSE;
   int   iCc;
   //
   // Open the mmap file
   //
   if(!global_Open(pcMapFile, sizeof(RPIMAP)))
   {
      printf("global_CreateMap(): MMAP %s open error" CRLF, pcMapFile);
      exit(EXIT_CC_GEN_ERROR);
   }
   //
   if((pstMap->G_iSignature1 != DATA_VALID_SIGNATURE)
        ||
      (pstMap->G_iSignature2 != DATA_VALID_SIGNATURE)
        ||
      (pstMap->G_iVersionMajor != DATA_VERSION_MAJOR))
   {
      //
      // Bad MAP file !
      //
      global_InitMemory(MAP_CLEAR);
      global_RestoreDefaults(TRUE);
      //=======================================================================
      // Init the global mutex
      //=======================================================================
      pthread_mutexattr_init(&tMutexAttr);
      pthread_mutexattr_settype(&tMutexAttr, PTHREAD_MUTEX_ERRORCHECK);
      if( (iCc = pthread_mutex_init(&pstMap->G_tMutex, &tMutexAttr)) != 0) 
      {
         printf("global_CreateMap(): mutex init ERROR %d (%s)", iCc, strerror(iCc));
         exit(EXIT_CC_GEN_ERROR);
      }
      printf("global_CreateMap():Mutex created" CRLF);
      printf("global_CreateMap():MAP file signature or DB version mismatch, creating new Mapping:" CRLF);
      printf("global_CreateMap():New signature-1 = 0x%08x" CRLF, pstMap->G_iSignature1);
      printf("global_CreateMap():New signature-2 = 0x%08x" CRLF, pstMap->G_iSignature2);
      printf("global_CreateMap():New DB version  = v%d.%d" CRLF, pstMap->G_iVersionMajor, pstMap->G_iVersionMinor);
   }
   else
   {
      global_InitMemory(MAP_RESTART);
      //=======================================================================
      // Init the global mutex
      //=======================================================================
      if( (iCc = pthread_mutex_init(&pstMap->G_tMutex, NULL)) != 0) 
      {
         printf("global_CreateMap(): mutex init ERROR %d (%s)", iCc, strerror(iCc));
         exit(EXIT_CC_GEN_ERROR);
      }
      printf("global_CreateMap():Mutex created" CRLF);
      printf("global_CreateMap():MAP file OKee, v%d.%d" CRLF, pstMap->G_iVersionMajor, pstMap->G_iVersionMinor);
      fCc = TRUE;
   }
   return(fCc);
}

//
// Function:   global_InitMemory
// Purpose:    Init the MAP memory
//
// Parms:      Cmd
// Returns:
// Note:       CANNOT Use LOG_printf(), PRINTFx() or LOG_Report() due to the use of the G_tmutex !
//
static void global_InitMemory(MAPSTATE tState)
{
  int      iIdx;
  u_int32  ulTime;

  switch(tState)
  {
     case MAP_CLEAR:
        GEN_MEMSET(pstMap, 0, sizeof(RPIMAP));
        //
        pstMap->G_iSignature1    = DATA_VALID_SIGNATURE;
        pstMap->G_iSignature2    = DATA_VALID_SIGNATURE;
        pstMap->G_iVersionMajor  = DATA_VERSION_MAJOR;
        pstMap->G_iVersionMinor  = DATA_VERSION_MINOR;
        pstMap->G_fInit          = TRUE;
        break;

     case MAP_SYNCHR:
        break;

     case MAP_RESTART:
        pstMap->G_fInit = FALSE;
        //
        ulTime = RTC_GetSystemSecs();
        if(ulTime > (pstMap->G_ulStartTimestamp+ES_SECONDS_PER_DAY))
        {
           // it's more than a day agoo:
           iIdx = 0;
        }
        else
        {
           // Get the current quarter 0...(24*4)-1
           iIdx = RTC_GetCurrentQuarter(0);
           printf("global_InitMemory():Restart %d quarter" CRLF, iIdx);
        }
        break;

     default:
        break;
  }
}

//
// Function:   global_Open
// Purpose:    Open the main file for the mapping
//
// Parms:      Filename, size
// Returns:    TRUE if OKee
// Note:       We do NOT setup the GLOBAL mutex since we do NOT have a valid MAP file !
//             We CANNOT use functions that make use of the mutex here !
//
//
static bool global_Open(const char *pcName, int iMapSize)
{
   bool  fCc=FALSE;
    
   pstMap         = NULL;
   iGlobalMapSize = 0;

   iFdMap = open(pcName, O_RDWR|O_CREAT, 0640);
   if(iFdMap > 0)
   {
      //
      // Make sure the file has the minimum size!
      //
      lseek(iFdMap, iMapSize, SEEK_SET);
      safewrite(iFdMap, "EOF", 4);
      //
      pstMap = (RPIMAP *) mmap(NULL, iMapSize, PROT_READ|PROT_WRITE, MAP_SHARED, iFdMap, 0);
      if(pstMap == MAP_FAILED)
      {
         printf("global_Open():Mapping failed:%s" CRLF, strerror(errno));
      }
      else
      {
         iGlobalMapSize = iMapSize;
         fCc = TRUE;
      }
   }
   else printf("global_Open():ERROR open %s: %s" CRLF, pcName, strerror(errno));
   return(fCc);
}

//
// Function:   global_PidsInit
// Purpose:    Init all threads
//
// Parms:      
// Returns:    
// Note:       
//
static void global_PidsInit(void)
{
   int      ePid;

   //
   // Log PIDs
   //
   for(ePid=0; ePid<NUM_PIDT; ePid++)
   {
      pstMap->G_stPidList[ePid] = stDefaultPidList[ePid];
      printf("global_PidsInit():Setup %s" CRLF, pstMap->G_stPidList[ePid].pcHelp);
   }
}

//
// Function:   global_RestoreDefaults
// Purpose:    Restore global variables
//
// Parms:      Cold-boot or Warm-boot only
// Returns:    
// Note:       fCold TRUE:  init all vars according to their initial setting (pstMap->pcDefault) 
//                   FALSE: init all vars according to their WB flag setting (pstMap->pcDefault) 
//
static void global_RestoreDefaults(bool fCold)
{
   int            x;
   u_int8        *pubChanged;
   const GLOBALS *pstGlobals = stGlobalParameters;

   for(x=0; x<NUM_GLOBAL_DEFS; x++)
   {
      if(fCold || (pstGlobals->iFunction & WB) )
      {
         //
         // Vars marked WB will be initialized with its default setting on every start !
         //
         if(pstGlobals->iFunction & PAR_A)
         {
            char *pcValue;

            // ASCII parm
            if(pstGlobals->pcDefault) 
            {
               pcValue = (char *)GLOBAL_GetParameter(x);
               GEN_STRNCPY(pcValue, pstGlobals->pcDefault, pstGlobals->iValueSize);
            }
         }
         if(pstGlobals->iFunction & PAR_B)
         {
            int  *piValue;

            // Int-BCD parm
            if(pstGlobals->pcDefault)
            {
               piValue = (int *)GLOBAL_GetParameter(x);
              *piValue = (int)strtol(pstGlobals->pcDefault, NULL, 10);
            }
         }
         if(pstGlobals->iFunction & PAR_H)
         {
            int  *piValue;
            
            //Int-HEX parm
            if(pstGlobals->pcDefault) 
            {
               piValue = (int *)GLOBAL_GetParameter(x);
              *piValue = (int)strtol(pstGlobals->pcDefault, NULL, 16);
            }
         }
         if(pstGlobals->iFunction & PAR_F)
         {
            double  *pflValue;
            
            //Double float parm
            if(pstGlobals->pcDefault) 
            {
               pflValue = (double *)GLOBAL_GetParameter(x);
              *pflValue = strtod(pstGlobals->pcDefault, NULL);
            }
         }
         //
         // Handle changed markers
         //
         if(pstGlobals->iChangedOffset >= 0)
         {
            pubChanged  = (u_int8 *)pstMap + pstGlobals->iChangedOffset;
            *pubChanged = pstGlobals->iChanged;
         }
      }
      pstGlobals++;
   }
}

//
// Function:   global_RestoreMapfile
// Purpose:    Copy the MMAP file from SD to RAM disk
//             cp /usr/local/share/rpi/rpislim.map <WORK_DIR>/pikrellman.map
// Parms:     
// Returns:    0=Mapfile loaded correctly form persistent storage
// Note:       CANNOT Use LOG_printf(), PRINTFx() or LOG_Report() due to the use of the G_tmutex !
//
static int global_RestoreMapfile()
{
   int   iCc=0; 
   char  cSrc=0, cDst=0;

   GLOBAL_GetFileInfo((char *)pcDirRestoreSrc, &cSrc);
   GLOBAL_GetFileInfo((char *)pcDirRestoreDst, &cDst);
   //
   if( (cSrc == 'D') && (cDst == 'D') )
   {
      printf("global_RestoreMapfile(): restore %s.*" CRLF, pcShellRestore);
      iCc = system(pcShellRestore);
      if(iCc) 
      {
         printf("global_RestoreMapfile():ERROR Restore %s.*" CRLF, pcShellRestore);
      }
   }
   else
   {
      if(cSrc != 'D') printf("global_RestoreMapfile():Dir [%s] not found" CRLF, pcDirRestoreSrc);
      if(cDst != 'D') printf("global_RestoreMapfile():Dir [%s] not found" CRLF, pcDirRestoreDst);
   }
   return(iCc);
}

/*----------------------------------------------------------------------
_______________GLOBAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/

// 
// Function:   GLOBAL_CheckDelete
// Purpose:    Check the status of the delete parameter
// 
// Parameters: Full pathname of file to delete
// Returns:    TRUE if delete parameter found and file deleted
// 
bool GLOBAL_CheckDelete(char *pcPath)
{
   bool  fDelete=FALSE;
   int   iCc;

   if(GEN_STRNCMPI(pstMap->G_pcDelete, "yes", 3) == 0)
   {
      iCc = remove(pcPath);
      if(iCc == 0)
      {
         fDelete = TRUE;
         GEN_STRCPY(pstMap->G_pcDelete, "no");
      }
   }
   return(fDelete);
}

//
// Function:   GLOBAL_Close
// Purpose:    Close the main file with the mapping
//
// Parms:      TRUE to save the mapfile to disk
// Returns:    TRUE if OKee
// Note:       
//
bool GLOBAL_Close(bool fSave)
{
   if(pstMap)
   {
      PRINTF("GLOBAL-Close():Map=%p" CRLF, pstMap);
      pthread_mutex_destroy(&(pstMap->G_tMutex));
      munmap(pstMap, iGlobalMapSize); 
      safeclose(iFdMap);
      if(fSave) global_SaveMapfile();
   }
   else
   {
      printf("GLOBAL-Close():NO Map!!!" CRLF);
   }
   return(TRUE);
}

//
//  Function:  GLOBAL_ConvertDebugMask
//  Purpose:   Convert G_pcDebugMask <---> G_ulDebugMask 
//
//  Parms:     TRUE = Ascii --> u_init32
//  Returns:   Debug mask 
//
u_int32 GLOBAL_ConvertDebugMask(bool fAsciiToInt)
{
   if(fAsciiToInt)
   {
      pstMap->G_ulDebugMask = strtoul(pstMap->G_pcDebugMask, NULL, 16);
      PRINTF("GLOBAL-ConvertDebugMask():A-->I:Mask=0x%lx" CRLF, pstMap->G_ulDebugMask);
   }
   else
   {
      GEN_SNPRINTF(pstMap->G_pcDebugMask, MAX_PARM_LEN, "%lx", pstMap->G_ulDebugMask);
      PRINTF("GLOBAL-ConvertDebugMask():I-->A:Mask=%s" CRLF, pstMap->G_pcDebugMask);
   }
   return(pstMap->G_ulDebugMask);
}

//
// Function:   GLOBAL_DebugClr
// Purpose:    Reset Debug bits
//
// Parms:      Debug bit
// Returns:    TRUE if all G_iDebug bits are zero
// Note:       
//
bool GLOBAL_DebugClr(int iBits)
{
   bool  fCc;

   GLOBAL_Lock();
   pstMap->G_iDebug &= ~iBits;
   fCc = (pstMap->G_iDebug == 0);
   GLOBAL_Unlock();
   return(fCc);
}

//
// Function:   GLOBAL_DebugCopy
// Purpose:    Copy Debug data
//
// Parms:      Dest, Src, Size
// Returns:    
// Note:       
//
void GLOBAL_DebugCopy(u_int8 *pubDst, u_int8 *pubSrc, int iSize)
{
   GLOBAL_Lock();
   GEN_MEMCPY(pubDst, pubSrc, iSize);
   GLOBAL_Unlock();
}

//
// Function:   GLOBAL_DebugGet
// Purpose:    Get all Debug bits
//
// Parms:      Debug bits to retrieve
// Returns:    G_iDebug bits
// Note:       
//
int GLOBAL_DebugGet(int iBits)
{
   int   iRes;

   iRes = pstMap->G_iDebug & iBits;
   return(iRes);
}

//
// Function:   GLOBAL_DebugPrint
// Purpose:    Conditional Debug print to stdout
//
// Parms:      Verbose level, Debug level
// Returns:    TRUE if print valid
// Note:       Call as:
//                (G_iDebugLevel = Y)
//
//             //
//             // Prints to stdout if xx < G_iDebugLevel :
//             //
//             if(GLOBAL_DebugPrint(MM_DEBUG_BLA, xx)) GEN_PRINTF("aap" CRLF);
//
bool GLOBAL_DebugPrint(int iBits, int iLevel)
{
   bool  fCc=FALSE;

   if( (pstMap->G_iDebug & iBits) && (iLevel <= pstMap->G_iDebugLevel) ) fCc = TRUE;
   return(fCc);
}

//
// Function:   GLOBAL_DebugSet
// Purpose:    Set Debug bits
//
// Parms:      Debug bit
// Returns:    TRUE if G_iDebug has bits set
// Note:       
//
bool GLOBAL_DebugSet(int iBits)
{
   bool  fCc;

   GLOBAL_Lock();
   pstMap->G_iDebug |= iBits;
   fCc = (pstMap->G_iDebug != 0);
   GLOBAL_Unlock();
   return(fCc);
}

//
// Function:   GLOBAL_ExecuteNotifications
// Purpose:    Check and execute signal notifications
//
// Parms:      NFY Struct, nr, parameter
// Returns:    Nr of handled notifications
// Note:       
//
int GLOBAL_ExecuteNotifications(const NFY *pstNfy, int iNfy, void *pvParm)
{
   int   x, iNr=0;
      
   if(GLOBAL_GetSignal(pstNfy->ePid) == 0) return(0);
   //
   for(x=0; x<iNfy; x++)
   {
      if(GLOBAL_GetSignalNotification(pstNfy->ePid, pstNfy->iFlag))
      {
         //PRINTF("NFY Pid-%d Fl=0x%X %s" CRLF, GLOBAL_PidGet(pstNfy->ePid), pstNfy->iFlag, GLOBAL_PidGetName(pstNfy->ePid));
         pstNfy->pfCb(pvParm);
         iNr++;
      }
      pstNfy++;
   }
   return(iNr);
}

//
// Function:   GLOBAL_ExecuteParameter
// Purpose:    Execute the parameter callback, if any
//
// Parameters: Parameter enum
// Returns:    TRUE if OKee
//
//
bool GLOBAL_ExecuteParameter(GLOPAR tParm)
{
   bool     fCc=FALSE;
   HTTPFUN  pfCallback;

   if(tParm < NUM_GLOBAL_DEFS)
   {
      //
      // Call the parameter callback function, if any
      //
      pfCallback = stGlobalParameters[tParm].pfHttpFun;
      if(pfCallback) 
      {
         fCc = pfCallback(tParm);
      }
   }
   return(fCc);
}

//
// Function:   GLOBAL_ExpandMap
// Purpose:    Expand the current mapfile and save to new file
//
// Parms:      Expand zero-reset size, End-size
// Returns:    
// Note:       
//
void GLOBAL_ExpandMap(int iZero, int iSpare)
{
   int         i, iFd, iSize;
   u_int8     *pubSrc;
   const char  cZero[2]={0,0};

   iFd = safeopen2(RPI_MAP_NEWPATH, O_RDWR|O_CREAT|O_TRUNC, 0640);
   //
   // Copy   MMAP including reset-parms
   // Expand MMAP reset-parms (if required)
   // Copy   MMAP static parms
   // Expand MMAP spare static parms (if required)
   // Add    Signature-2 
   // Add    "EOF"

   // Copy MMAP including reset-parms
   pubSrc   = (u_int8 *)pstMap;
   iSize    = offsetof(RPIMAP, G_iResetEnd);
   safewrite(iFd, (char *)pubSrc, iSize);

   // Expand MMAP reset-parms (if required)
   if(iZero > 0)
   {
      for(i=0; i<iZero; i++) safewrite(iFd, cZero, 1);
   }
   LOG_printf("GLOBAL-Expand(): Added MMAP %d reset-parm bytes" CRLF, iZero);

   // Copy MMAP static parms
   pubSrc += iSize;
   iSize   = offsetof(RPIMAP, G_iSignature2) - iSize;
   safewrite(iFd, (char *)pubSrc, iSize);
   
   // Expand MMAP spare static parms (if required)
   if(iSpare > 0)
   {
      for(i=0; i<iSpare; i++) safewrite(iFd, cZero, 1);
   }
   LOG_printf("GLOBAL-Expand(): Added MMAP %d static-spare bytes" CRLF, iSpare);

   // Add Signature-2 
   // Add "EOF"
   safewrite(iFd, (char *)&pstMap->G_iSignature2, sizeof(int));
   safewrite(iFd, "EOF", 4);
   safeclose(iFd);
}

//
//  Function:  GLOBAL_FlushLog
//  Purpose:   Flush/Save the LOG file to persistent storage
//
//  Parms:     
//  Returns:   Shell CC (EXIT_CC_OKEE, ...)
//
int GLOBAL_FlushLog()
{
   LOG_ReportFlushFile();
   return( global_SaveMapfile() );
}

//
//  Function:  GLOBAL_GetDebugMask
//  Purpose:   Retrieve the current debug mask
//
//  Parms:     
//  Returns:   Mask
//
u_int32 GLOBAL_GetDebugMask(void)
{
   return(pstMap->G_ulDebugMask);
}

// 
// Function:   GLOBAL_GetFileInfo
// Purpose:    Retrieve info about a file in this dir
// 
// Parameters: Dir path, Char buffer for result
// Returns:    TRUE if OKee
// Note:       CANNOT Use LOG_printf(), PRINTFx() or LOG_Report() due to their use of the G_tmutex !
// 
bool GLOBAL_GetFileInfo(char *pcPath, char *pcChar)
{
   bool        fCc=TRUE;
   struct stat stStat;

   if(stat(pcPath, &stStat) == -1) return(FALSE);

   switch(stStat.st_mode & S_IFMT)
   {
      case S_IFDIR: 
         *pcChar = 'D';
         break;

      case S_IFREG:
         *pcChar = 'F';
         break;

      default:
         *pcChar = '?';
         break;
   }
   return(fCc);
}

//
//  Function:  GLOBAL_GetGlobalParameter
//  Purpose:   Retrieve the pointer to a global parameter
//
//  Parms:     Parameter mmap enum
//  Returns:   Pointer to Parameter 
//
void *GLOBAL_GetGlobalParameter(int iId)
{
   void *pvData=NULL;

   if( (iId >= 0) && (iId < NUM_GLOBAL_DEFS) )
   {
      pvData = (void *)pstMap + stGlobalParameters[iId].iValueOffset;
   }
   return(pvData);
}

//
//  Function:  GLOBAL_GetHostName
//  Purpose:   Retrieve the global host name. If not stored yet, obtain it first
//
//  Parms:     
//  Returns:   Pointer to name (Fixed name on error)
//             PatrnRpixxx-Carport
//
char *GLOBAL_GetHostName()
{
   const char *pcDefaultName = "PatrnRpiXX";
   char       *pcName;
   int         iErr;

   pcName = pstMap->G_pcHostname;
   if(GEN_STRLEN(pcName) == 0)
   {
      iErr = gethostname(pcName, MAX_URL_LEN);
      if(iErr)
      {
         //
         // gethostname() failed, supply default name
         //
         pcName = (char *)pcDefaultName;
      }
      //
      // Concatenate the usefull name
      //
      safestrcat(pcName, pcAppText, MAX_URL_LEN);
   }
   return(pcName);
}

//
//  Function:  GLOBAL_GetLog
//  Purpose:   Retrieve the LOG file struct^
//
//  Parms:     
//  Returns:   LOG struct
//
GLOG *GLOBAL_GetLog()
{
   return(&(pstMap->G_stLog));
}

//
//  Function:  GLOBAL_GetMallocs
//  Purpose:   Return the global malloc count
//  Parms:     
//
//  Returns:   Count
//
int GLOBAL_GetMallocs(void)
{
   return(pstMap->G_iMallocs);
}

// 
// Function:   GLOBAL_GetOption
// Purpose:    Retrieve a single option value from the global list
// 
// Parameters: Option text
// Returns:    Option value^ or NULL if not found
// 
// 
char *GLOBAL_GetOption(const char *pcOption)
{
   const GLOBALS *pstParm=stGlobalParameters;
   char          *pcValue;

   while(pstParm->pcOption)
   {
      if(GEN_STRLEN(pstParm->pcOption) > 0)
      {
         if(GEN_STRCMP(pstParm->pcOption, pcOption) == 0)
         {
            pcValue = (char *) ((void *)pstMap + pstParm->iValueOffset);
            return(pcValue);
         }
      }
      pstParm++;
   }
   return(NULL);
}

// 
// Function:   GLOBAL_GetParameter
// Purpose:    Retrieve a single parameter from the global list
// 
// Parameters: Parameter enum
// Returns:    Parameter^ or NULL if not found
// 
// 
void *GLOBAL_GetParameter(GLOPAR tParm)
{
   void *pvValue=NULL;

   if(tParm < NUM_GLOBAL_DEFS)
   {
      pvValue = (void *)pstMap + stGlobalParameters[tParm].iValueOffset;
   }
   return(pvValue);
}

//
// Function:   GLOBAL_GetParameterAsInt
// Purpose:    Get an integer parameter
//
// Parameters: Parameter enum, int ptr
// Returns:    TRUE if OKee
//
//
bool GLOBAL_GetParameterAsInt(GLOPAR tParm, int *piDest)
{
   bool  fCc=FALSE;

   if(tParm < NUM_GLOBAL_DEFS)
   {
      switch(GLOBAL_GetParameterType(tParm))
      {
         case PAR_A:
            {
               char *pcValue;
               // ASCII parm
               pcValue = GLOBAL_GetParameter(tParm);
               if(pcValue)
               {
                  *piDest = (int)strtol(pcValue, NULL, 0);
                  fCc = TRUE;
               }
            }
            break;
            
         case PAR_H:
         case PAR_B:
            {
               int  *piValue;
               // Int-BCD parm
               piValue = GLOBAL_GetParameter(tParm);
               if(piValue)
               {
                  *piDest = *piValue;
                  fCc = TRUE;
               }
            }
            break;
            
         case PAR_F:
            {
               double  *pflValue, flValue;
               //Double float parm
               pflValue = GLOBAL_GetParameter(tParm);
               if(pflValue)
               {
                  // Round Up/Down
                  flValue  = *pflValue;
                  flValue += 0.50;
                  *piDest  = (int)flValue;
                  fCc = TRUE;
               }
            }
            break;
      }
   }
   return(fCc);
}

// 
// Function:   GLOBAL_GetParameterSize
// Purpose:    Retrieve the size of a single parameter from the global list
// 
// Parameters: Parameter enum
// Returns:    Size
// 
// 
int GLOBAL_GetParameterSize(GLOPAR tParm)
{
   int   iSize=0;

   if(tParm < NUM_GLOBAL_DEFS)
   {
      iSize = stGlobalParameters[tParm].iValueSize;
   }
   else PRINTF("GLOBAL-GetParameterSize():Unknown parameter %d" CRLF, tParm);
   return(iSize);
}

//
// Function:   GLOBAL_GetParameterType
// Purpose:    Retrieve the type of a single parameter from the global list
//
// Parameters: Parameter enum
// Returns:    Type (PAR_A, PAR_B, ...
//
//
int GLOBAL_GetParameterType(GLOPAR tParm)
{
   int   iType=0;

   if(tParm < NUM_GLOBAL_DEFS)
   {
      iType = stGlobalParameters[tParm].iFunction & PAR_TYPE_MASK;
   }
   else PRINTF("GLOBAL-GetParameterType():Unknown parameter %d" CRLF, tParm);
   return(iType);
}

//
// Function:   GLOBAL_GetParameters
// Purpose:    Retrieve the camera parameter list
//
// Parms:      
// Returns:    The List
//
const GLOBALS *GLOBAL_GetParameters(void)
{
   return(stGlobalParameters);
}

//
// Function:   GLOBAL_GetSignal
// Purpose:    Get the SIGUSRx notification flags. 
//
// Parms:      User PID
// Returns:    Flag
// Note:       
//
//
int GLOBAL_GetSignal(int ePid)
{
   return(pstMap->G_stPidList[ePid].iNotify);
}

//
// Function:   GLOBAL_GetSignalNotification
// Purpose:    Get and clear the SIGUSRx notification flag. (Clears the flag after reading)
//
// Parms:      User PID, Flag
// Returns:    TRUE if notification set
// Note:       Do NOT use LOG_Printf(), PRINTF() etc between Lock() and Unlock() !!
//
//
bool GLOBAL_GetSignalNotification(int ePid, int iFlag)
{
   bool  fNfy=FALSE;
   int   iNotify;

   GLOBAL_Lock();
   iNotify = pstMap->G_stPidList[ePid].iNotify;
   //
   if(iNotify & iFlag) 
   {
      fNfy = TRUE;
      pstMap->G_stPidList[ePid].iNotify &= ~iFlag;
   }
   GLOBAL_Unlock();
   return(fNfy);
}

//
// Function:   GLOBAL_GetStatusText
// Purpose:    Return global status text
//
// Parms:      Status
// Returns:    Status
// Note:       
//
const char *GLOBAL_GetStatusText(int iStatus)
{
   const char *pcStatus;

   if(iStatus < NUM_GEN_STATUS)
   {
      pcStatus = pcGenStatus[iStatus];
   }
   return(pcStatus);
}

//
// Function:   GLOBAL_GetTraceCode
// Purpose:    Retrieve and update the tracecode
//
// Parms:     
// Returns:    Tracecode 
// Note:       Do NOT use LOG_Printf(), PRINTF() etc between Lock() and Unlock() !!
//
int GLOBAL_GetTraceCode()
{
   int   iTrace=0;

   iTrace = ++(pstMap->G_iTraceCode);
   return(iTrace);
}

//
//  Function:  GLOBAL_HostNotification
//  Purpose:   Signal completion
//
//  Parms:     Message flag
//  Returns:   New notification
//  Note:      
//
int GLOBAL_HostNotification(int iMsg)
{
   int   iFlag;
   
   iFlag = GLOBAL_SetSignalNotification(PID_HOST, iMsg);
   return(iFlag);
}

//
// Function:   GLOBAL_Lock
// Purpose:    Lock global mutex
//
// Parms:      
// Returns:    0=OKee, else error
// Note:       Do NOT use LOG_Printf(), PRINTF() etc between Lock() and Unlock() !!
//
int GLOBAL_Lock(void)
{
   int   iCc;

   if( (iCc = pthread_mutex_lock(&pstMap->G_tMutex)) )
   {
      GEN_PRINTF("GLOBAL-Lock(): mutex ERROR %d (%s)" CRLF, iCc, strerror(iCc));
   }
   return(iCc);
}


//
// Function:   GLOBAL_MemorySafeMalloc
// Purpose:    Allocate a block of memory, clear all data
//
// Parms:      Caller, Size
// Returns:    Pointer to the block
// Note:       The safe function keeps signature and size at the start of the memory block
//             Caller can be __PRETTY_FUNCTION__
//
//             +------------+
//             | 0xDEADBEEF |
//             +------------+
//             |Malloc Size |
//             +------------+
//    Return ->| Malloc     |
//            ...          ...
//             |  Memory    |
//             +------------+
//             | 0xDEADBEEF |
//             +------------+
//
void *GLOBAL_MemorySafeMalloc(const char *pcCaller, size_t tSize)
{
   void    *pvData = NULL;
   u_int8  *pubData;
   
   if(tSize)
   {
      tGlobalAllocated += tSize;
      //
      pvData = safemalloc(tSize+12);
      if(pvData) 
      {
         pvData = GEN_ulong_put((u_int8 *)pvData, (u_int32)0xdeadbeef);
         pvData = GEN_ulong_put((u_int8 *)pvData, (u_int32)tSize);
         //
         pubData = (u_int8 *)pvData;
         GEN_ulong_put(&pubData[tSize], (u_int32)0xdeadbeef);
      }
   }
   else
   {
      PRINTF("GLOBAL-MemorySafeMalloc():[%s] Size Error !" CRLF, pcCaller);
      LOG_Report(0, "GBL", "GLOBAL-MemorySafeMalloc():[%s] Size Error !", pcCaller);
   }
   return(pvData);
}

//
// Function:   GLOBAL_MemorySafeFree
// Purpose:    Free the block of memory
//
// Parms:      Caller, Pointer to the block
// Returns:    NULL
// Note:       The safe function keeps signature and size at the start of the memory block
//             Caller can be __PRETTY_FUNCTION__
//
void *GLOBAL_MemorySafeFree(const char *pcCaller, void *pvData)
{
   u_int32  ulSig, ulSize;
   u_int8  *pubData;
   u_int8  *pubTemp;

   pubData  = (u_int8 *)pvData;
   if(pubData)
   {
      pubData -= 8;
      // Get top signature
      ulSig  = GEN_ulong_get(&pubData[0], 0xffffffff, 0);
      if(ulSig != 0xdeadbeef)
      {
         PRINTF("GLOBAL-MemorySafeFree():[%s] Top Signature error !" CRLF, pcCaller);
         LOG_Report(0, "GBL", "GLOBAL-MemorySafeFree():[%s]Signature error !", pcCaller);
         return(NULL);
      }
      // Get allocated size
      ulSize = GEN_ulong_get(&pubData[4], 0xffffffff, 0);
      if(ulSize > tGlobalAllocated)
      {
         PRINTF("GLOBAL-MemorySafeFree():[%s] Size Error !" CRLF, pcCaller);
         LOG_Report(0, "GBL", "GLOBAL-MemorySafeFree():[%s] Size Error !", pcCaller);
         return(NULL);
      }
      // Get bottom signature
      pubTemp = (u_int8 *)pvData;
      ulSig = GEN_ulong_get(&pubTemp[ulSize], 0xffffffff, 0);
      if(ulSig != 0xdeadbeef)
      {
         PRINTF("GLOBAL-MemorySafeFree():[%s] Bottom Signature error !" CRLF, pcCaller);
         LOG_Report(0, "GBL", "GLOBAL-MemorySafeFree():[%s] Bottom Signature Error !", pcCaller);
      }
      tGlobalAllocated -= (size_t) ulSize;
      safefree(pubData);
   }
   return(NULL);
}

//
//  Function:   GLOBAL_MemoryGetTotalMalloc
//  Purpose:    Get the current total amount of allocated memory
//  Parms:      
//  Returns:    size of allocated memory
//
u_int32 GLOBAL_MemoryGetTotalMalloc(void)
{
   return((u_int32) tGlobalAllocated);
}

//
//  Function:   GLOBAL_MemoryGetMallocSize
//  Purpose:    Get the size of a block of allocated memory
//  Parms:      Pointer to the block
//  Returns:    Size
//
u_int32 GLOBAL_MemoryGetMallocSize(void *pvData)
{
   u_int32  ulSig, ulSize=0;
   u_int8  *pubTemp;

   pubTemp = (u_int8 *)pvData;
   //
   if(pubTemp)
   {
      pubTemp -= 8;
      ulSig  = GEN_ulong_get(&pubTemp[0], 0xffffffff, 0);
      if(ulSig != 0xdeadbeef)
      {
         PRINTF("GLOBAL-MemorySafeGetMallocSize():Signature error !" CRLF);
         LOG_Report(0, "GBL", "GLOBAL-MemorySafeGetMallocSize():Signature error !");
      }
      else
      {
         ulSize = GEN_ulong_get(&pubTemp[4], 0xffffffff, 0);
      }
   }
   return(ulSize);
}

//
//  Function:  GLOBAL_Notify
//  Purpose:   Notify threads using a signal
//
//  Parms:     Pid-Enum, Nfy flag, Signal
//  Returns:   Remainder notification
//  Note:      
//
int GLOBAL_Notify(int ePid, int iMsg, int iSignal)
{
   int   iFlag=0;
   pid_t tPid=GLOBAL_PidGet(ePid);

   if(tPid > 0)
   {
      //
      // Thread is active: signal
      //
      GLOBAL_Lock();
      pstMap->G_stPidList[ePid].iNotify |= iMsg;
      iFlag = pstMap->G_stPidList[ePid].iNotify;
      GLOBAL_Unlock();
      //
      // Signal the thread
      //
      kill(tPid, iSignal);
   }
   else PRINTF("GLOBAL-Notify(): BAD PID %d=%d" CRLF, ePid, tPid);
   return(iFlag);
}

// 
// Function:   GLOBAL_ParmHasChanged
// Purpose:    Check if a parm has been altered from defaults
// 
// Parameters: Parm ^
// Returns:    TRUE if parm is marked as changed
// 
bool GLOBAL_ParmHasChanged(const GLOBALS *pstArgs)
{
   bool        fChanged=FALSE;
   u_int8     *pubChanged;
   int         iChangedOffset=pstArgs->iChangedOffset;

   switch(iChangedOffset)
   {
      case ALWAYS_CHANGED:
         fChanged = TRUE;
         break;

      case NEVER_CHANGED:
         break;

      default:
         pubChanged = (u_int8 *)pstMap + iChangedOffset;
         fChanged   = *pubChanged;
         break;
   }
   return(fChanged);
}

// 
// Function    : GLOBAL_ParmSetChanged
// Description : Set the parm changed marker
// 
// Parameters  : Parm ^, set/unset
// Returns     : TRUE if parm has changed successfully
// 
// Note        : Some parms cannot be changed (NEVER_CHANGED)
//               Some are always volatile (ALWAYS_CHANGED)
// 
bool GLOBAL_ParmSetChanged(const GLOBALS *pstArgs, bool fChanged)
{
   u_int8     *pubChanged;
   int         iChangedOffset=pstArgs->iChangedOffset;

   switch(iChangedOffset)
   {
      case ALWAYS_CHANGED:
         // Do not alter the change marker
         break;

      case NEVER_CHANGED:
         // Cannot alter the change marker
         if(fChanged)  fChanged = FALSE;
         break;

      default:
         pubChanged = (u_int8 *)pstMap + iChangedOffset;
        *pubChanged = (u_int8)fChanged;
         fChanged   = TRUE;
         break;
   }
   return(fChanged);
}

//
// Function:   GLOBAL_ReadSecs
// Purpose:    Return seconds counter
//
// Parms:      
// Returns:    Running seconds counter
// Note:       
//
u_int32 GLOBAL_ReadSecs(void)
{
   //PRINTF("GLOBAL-ReadSecs: 0x%lX" CRLF, pstMap->G_ulSecondsCounter);
   return(pstMap->G_ulSecondsCounter);
}

//
// Function:   GLOBAL_SetTraceCode
// Purpose:    Set the tracecode
//
// Parms:     
// Returns:    
// Note:       Do NOT use LOG_Printf(), PRINTF() etc between Lock() and Unlock() !!
//
void GLOBAL_SetTraceCode(int iCode)
{
   pstMap->G_iTraceCode = iCode;
}

//
//  Function:  GLOBAL_SetDebugMask
//  Purpose:   Update the current debug mask
//
//  Parms:     New mask
//  Returns:    
//
void GLOBAL_SetDebugMask(u_int32 ulMask)
{
   pstMap->G_ulDebugMask = ulMask;
   PRINTF("GLOBAL-SetDebugMask():Mask=0x%lx" CRLF, pstMap->G_ulDebugMask);
}

//
//  Function:   GLOBAL_Share
//  Purpose:    Start sharing the mmap file
//
//  Parms:      
//  Returns:    True if OKee
//
bool GLOBAL_Share(void)
{
   bool     fCc=TRUE;

   pstMap = (RPIMAP *) mmap(NULL, iGlobalMapSize, PROT_READ|PROT_WRITE, MAP_SHARED, iFdMap, 0);
   if(pstMap == MAP_FAILED)
   {
      LOG_Report(0, "GBL", "Re-mapping failed");
      PRINTF("GLOBAL-Share(): remapping failed" CRLF);
      fCc = FALSE;
   }
   else
   {
      PRINTF("GLOBAL-Share(): Map=%p" CRLF, pstMap);
   }
   return(fCc);
}

//
// Function:   GLOBAL_Status
// Purpose:    Update and/or return global status
//
// Parms:      Status (or GLOBAL_STATUS_ASK)
// Returns:    Status
// Note:       pcGenStatus updated
//
int GLOBAL_Status(int iStatus)
{
   if(iStatus < NUM_GEN_STATUS)
   {
      pstMap->G_iHttpStatus = iStatus;
      GEN_STRNCPY(pstMap->G_pcStatus, pcGenStatus[iStatus], MAX_PARM_LEN);
      pstMap->G_pcStatus[MAX_PARM_LEN-1] = 0;
      PRINTF("GLOBAL-Status: %s" CRLF, pstMap->G_pcStatus);
   }
   return(pstMap->G_iHttpStatus);
}

//
// Function:   GLOBAL_Sync
// Purpose:    Sync the main file with the mapping
//
// Parms:      
// Returns:    TRUE if OKee
// Note:       
//
bool  GLOBAL_Sync(void)
{
  msync(pstMap, iGlobalMapSize, MS_SYNC);
  return(TRUE);
}

// 
// Function:   GLOBAL_SegmentationFault
// Purpose:    Store segv cause
// 
// Parameters: 
// Returns:    
// 
void GLOBAL_SegmentationFault(const char *pcFile, int iLineNr)
{
   pid_t tPid = getpid();

   GEN_STRNCPY(pstMap->G_pcSegFaultFile, pcFile, MAX_URL_LEN);
   pstMap->G_iSegFaultLine = iLineNr;
   pstMap->G_iSegFaultPid  = tPid;
   LOG_Report(0, "GBL", "GLOBAL-SegmentationFault():%s, Line %d, Pid=%d", pcFile, iLineNr, tPid);
}

//
// Function:   GLOBAL_SetParameterAsFloat
// Purpose:    Update a single parameter from the global list
//
// Parameters: Parameter enum, Data ptr
// Returns:    TRUE if OKee
//
//
bool GLOBAL_SetParameterAsFloat(GLOPAR tParm, double flData)
{
   bool  fCc=FALSE;

   if(tParm < NUM_GLOBAL_DEFS)
   {
      switch(GLOBAL_GetParameterType(tParm))
      {
         case PAR_A:
            {
               int   iSize;
               char *pcValue;
               // ASCII parm
               pcValue = (char *)GLOBAL_GetParameter(tParm);
               if(pcValue)
               {
                  iSize = GLOBAL_GetParameterSize(tParm);
                  GEN_SNPRINTF(pcValue, iSize, "%f", flData);
                  fCc = TRUE;
               }
            }
            break;
            
         case PAR_H:
         case PAR_B:
            {
               int  *piValue;
               // Int-BCD parm
               piValue = (int *)GLOBAL_GetParameter(tParm);
               if(piValue)
               {
                  *piValue = (int)flData;
                  fCc = TRUE;
               }
            }
            break;
            
         case PAR_F:
            {
               double  *pflValue;
               //Double float parm
               pflValue = (double *)GLOBAL_GetParameter(tParm);
               if(pflValue)
               {
                  *pflValue = (double) flData;
                  fCc = TRUE;
               }
            }
            break;

         default:
            break;
      }
   }
   return(fCc);
}

//
// Function:   GLOBAL_SetParameterAsInt
// Purpose:    Update a single parameter from the global list
//
// Parameters: Parameter enum, Data ptr
// Returns:    TRUE if OKee
//
//
bool GLOBAL_SetParameterAsInt(GLOPAR tParm, int iData)
{
   bool  fCc=FALSE;

   if(tParm < NUM_GLOBAL_DEFS)
   {
      switch(GLOBAL_GetParameterType(tParm))
      {
         case PAR_A:
            {
               int   iSize;
               char *pcValue;
               // ASCII parm
               pcValue = (char *)GLOBAL_GetParameter(tParm);
               if(pcValue)
               {
                  iSize = GLOBAL_GetParameterSize(tParm);
                  GEN_SNPRINTF(pcValue, iSize, "%d", iData);
                  fCc = TRUE;
               }
            }
            break;
            
         case PAR_H:
         case PAR_B:
            {
               int  *piValue;
               // Int-BCD parm
               piValue = (int *)GLOBAL_GetParameter(tParm);
               if(piValue)
               {
                  *piValue = iData;
                  fCc = TRUE;
               }
            }
            break;
            
         case PAR_F:
            {
               double  *pflValue;
               //Double float parm
               pflValue = (double *)GLOBAL_GetParameter(tParm);
               if(pflValue)
               {
                  *pflValue = (double) iData;
                  fCc = TRUE;
               }
            }
            break;

         default:
            break;
      }
   }
   return(fCc);
}

//
// Function:   GLOBAL_SetParameterAsText
// Purpose:    Update a single parameter from the global list
//
// Parameters: Parameter enum, Data ptr
// Returns:    TRUE if OKee
//
//
bool GLOBAL_SetParameterAsText(GLOPAR tParm, char *pcData)
{
   bool  fCc=FALSE;

   if(tParm < NUM_GLOBAL_DEFS)
   {
      switch(GLOBAL_GetParameterType(tParm))
      {
         case PAR_A:
            {
               int   iSize;
               char *pcValue;
               // ASCII parm
               pcValue = (char *)GLOBAL_GetParameter(tParm);
               if(pcValue)
               {
                  iSize = GLOBAL_GetParameterSize(tParm);
                  GEN_STRNCPY(pcValue, pcData, iSize);
                  fCc = TRUE;
               }
            }
            break;
            
         case PAR_B:
            {
               int  *piValue;
               // Int-BCD parm
               piValue = (int *)GLOBAL_GetParameter(tParm);
               if(piValue)
               {
                  *piValue = (int)strtol(pcData, NULL, 10);
                  fCc = TRUE;
               }
            }
            break;
            
         case PAR_H:
            {
               int  *piValue;
               // Int-BCD parm
               piValue = (int *)GLOBAL_GetParameter(tParm);
               if(piValue)
               {
                  *piValue = (int)strtol(pcData, NULL, 16);
                  fCc = TRUE;
               }
            }
            break;
            
            case PAR_F:
            {
               double  *pflValue;
               //Double float parm
               pflValue = (double *)GLOBAL_GetParameter(tParm);
               if(pflValue)
               {
                  *pflValue = strtod(pcData, NULL);
                  fCc = TRUE;
               }
            }
            break;
      }
   }
   return(fCc);
}

//
// Function:   GLOBAL_SetSignalNotification
// Purpose:    Set the SIGUSR1 notification flag and notify receiving thread
//
// Parms:      PID enum, Flag
// Returns:    New Flag
// Note:       Do NOT use LOG_Printf(), PRINTF() etc between Lock() and Unlock() !!
//
int GLOBAL_SetSignalNotification(int ePid, int iFlag)
{
   int   iNewFlag=0;
   pid_t tPid;

   if((tPid = GLOBAL_PidGet(ePid)) > 0)
   {
      //
      // Thread is active: signal
      //
      GLOBAL_Lock();
      pstMap->G_stPidList[ePid].iNotify |= iFlag;
      iNewFlag = pstMap->G_stPidList[ePid].iNotify;
      GLOBAL_Unlock();
      //
      // Signal the thread
      //
      kill(tPid, SIGUSR1);
   }
   return(iNewFlag);
}

// 
// Function:   GLOBAL_SetParameterChanged
// Purpose:    Set changed marker for this parameter from the global list
// 
// Parameters: Parameter enum
// Returns:    TRUE if OK
// 
// 
bool GLOBAL_SetParameterChanged(GLOPAR tParm)
{
   bool  fCc=FALSE;

   if(tParm < NUM_GLOBAL_DEFS)
   {
      fCc = GLOBAL_ParmSetChanged(&stGlobalParameters[tParm], TRUE);
   }
   return(fCc);
}

//
// Function:   GLOBAL_Unlock
// Purpose:    Unlock global mutex
//
// Parms:      
// Returns:    0=OKee, else error
// Note:       Do NOT use LOG_Printf(), PRINTF() etc between Lock() and Unlock() !!
//
int GLOBAL_Unlock(void)
{
   int   iCc;
   
   if( (iCc = pthread_mutex_unlock(&pstMap->G_tMutex)) )
   {
      GEN_PRINTF("GLOBAL-Unlock(): mutex ERROR %d (%s)" CRLF, iCc, strerror(iCc));
   }
   return(iCc);
}

//
// Function:   GLOBAL_UpdateSecs
// Purpose:    Count seconds
//
// Parms:      
// Returns:    
// Note:       
//
void GLOBAL_UpdateSecs(void)
{
   pstMap->G_ulSecondsCounter++;
   //PRINTF("GLOBAL-UpdateSecs: 0x%lX" CRLF, pstMap->G_ulSecondsCounter);
}


/*----------------------------------------------------------------------
__________________PID_FUNCTIONS(){}
------------------------------x----------------------------------------*/

//
// Function:   GLOBAL_PidCheckGuards
// Purpose:    Check if all threads are still active
//             
// Parms:     
// Returns:    TRUE if OKee
// Note:       G_stPidList[i]->pid_t       tPid   : The PID
//                             int         iFlag  : The guard marker
//                             const char *pcName : The PID short-name
//                             const char *pcHelp : The PID long-name
//             Do NOT use LOG_Printf(), PRINTF() etc between Lock() and Unlock() !!
//
bool GLOBAL_PidCheckGuards(void)
{
   bool     fCc=TRUE;
   int      iFlag, iGuard;
   int      ePid;
   PIDL    *pstPidl;
   
   iGuard = pstMap->G_iGuards;
   //
   for(ePid=0; ePid<NUM_PIDT; ePid++) 
   {
      pstPidl = &(pstMap->G_stPidList[ePid]);
      if( (pstPidl->tPid > 0) && (pstMap->G_stPidList[ePid].iFlag != 0) )
      {
         //
         // This thread seems to be running and has an Init-marker:
         // Verify that the flag is active again
         //
         iFlag = iGuard & pstMap->G_stPidList[ePid].iFlag;
         if(iFlag) 
         {
            PRINTF("GLOBAL-PidCheckGuards:%s OKee" CRLF, pstPidl->pcHelp);
         }
         else
         {
            //
            // Thread did NOT respond in time: send final wakeup call
            //
            GLOBAL_SetSignalNotification(ePid, GLOBAL_GRD_ALL_RUN);
            LOG_Report(0, "GBL", "GLOBAL-PidCheckGuards():%s NOT set", pstPidl->pcHelp);
            PRINTF("GLOBAL-PidCheckGuards:%s NOT set" CRLF, pstPidl->pcHelp);
            fCc = FALSE;
         }
      }
      else
      {
         PRINTF("GLOBAL-PidCheckGuards:(G=0x%04X, F=0x%04X) : %5d-%s not guarded" CRLF, 
                     iGuard,
                     pstMap->G_stPidList[ePid].iFlag,
                     pstPidl->tPid, 
                     pstPidl->pcHelp);
      }
   }
   return(fCc);
}

//
// Function:   GLOBAL_PidClearGuards
// Purpose:    Clear all guard markers
//
// Parms:      
// Returns:    
// Note:       Do NOT use LOG_Printf(), PRINTF() etc between Lock() and Unlock() !!
//
void GLOBAL_PidClearGuards(void)
{
   GLOBAL_Lock();
   pstMap->G_iGuards = 0;
   GLOBAL_Unlock();
}

//
// Function:   GLOBAL_PidCheckThreads
// Purpose:    Check if all threads are still active
//             
// Parms:      Secs now
// Returns:    TRUE if all threads OKee
// Note:       The last thread active timestamp MUST be after last midnight
//
//            00:00                          |Last response TS
//             -+----------------------------+-----------------+------
//                                                            Now
//
bool GLOBAL_PidCheckThreads(u_int32 ulSecsNow)
{
   bool     fCc=TRUE;
   int      ePid;
   u_int32  ulMidnightSecs;
   char     cBuffer[32];
   PIDL    *pstPidl;
   PFUNIN   pfnInit;
   
   ulMidnightSecs = RTC_GetMidnight(ulSecsNow);
   for(ePid=0; ePid<NUM_PIDT; ePid++) 
   {
      if(GLOBAL_PidGet(ePid))
      {
         pstPidl = &(pstMap->G_stPidList[ePid]);
         if(pstPidl->ulTs < ulMidnightSecs)
         {
            //
            // This thread did not respond to the last midnight event.
            // Kill and restart (if it has a re-init function).
            //
            RTC_ConvertDateTime(TIME_FORMAT_WW_DD_MMM_YYYY_HH_MM_SS, cBuffer, pstPidl->ulTs);
            //
            pfnInit = pstPidl->pfnInit;
            if(pfnInit) 
            {
               LOG_Report(0, "GBL", "GLOBAL-PidCheckThreads():%s last run at %s: request restart", pstPidl->pcName, cBuffer);
               GLOBAL_PidTerminate(ePid, 1000);
               pfnInit();
               fCc = FALSE;
            }
            else LOG_Report(0, "GBL", "GLOBAL-PidCheckThreads():%s last run at %s: NO restart available", pstPidl->pcName, cBuffer);

         }
      }
   }
   return(fCc);
}

//
//  Function:  GLOBAL_PidGet
//  Purpose:   Retrieve thread pid
//
//  Parms:     PID enum
//  Returns:   tPid
//
pid_t GLOBAL_PidGet(PIDT ePid)
{
   pid_t tPid=0;

   if(pstMap && (ePid != -1) && (ePid < NUM_PIDT) )
   {
      tPid = pstMap->G_stPidList[ePid].tPid;
      //PRINTF("GLOBAL-PidGet: Pid %s=%d" CRLF, GLOBAL_PidGetName(ePid), tPid);
   }
   return(tPid);
}

//
// Function:   GLOBAL_PidGetGuard
// Purpose:    Get the guard marker for this thread
//             
// Parms:      Thread ID
// Returns:    TRUE if marker has been set 
// Note:       G_stPidList[i]->pid_t       tPid   : The PID
//                             int         iFlag  : The guard marker
//                             const char *pcName : The PID short-name
//                             const char *pcHelp : The PID long-name
//             Do NOT use LOG_Printf(), PRINTF() etc between Lock() and Unlock() !!
//
bool GLOBAL_PidGetGuard(PIDT ePid)
{
   bool  fMarker=TRUE;
   int   iFlag;

   if(pstMap && (ePid < NUM_PIDT) )
   {
      GLOBAL_Lock();
      iFlag = pstMap->G_stPidList[ePid].iFlag;
      //
      // Get the flag
      //
      if( (pstMap->G_iGuards & iFlag) == 0)
      {
         fMarker = FALSE;
      }
      GLOBAL_Unlock();
   }
   return(fMarker);
}

//
//  Function:  GLOBAL_PidGetName
//  Purpose:   Retrieve thread pid name
//
//  Parms:     PID enum
//  Returns:   name
//
const char *GLOBAL_PidGetName(PIDT ePid)
{
   const char *pcName=NULL;

   if(pstMap && (ePid != -1) && (ePid < NUM_PIDT) )
   {
      pcName = pstMap->G_stPidList[ePid].pcName;
   }
   return(pcName);
}

//
//  Function:  GLOBAL_PidGetHelp
//  Purpose:   Retrieve thread pid help
//
//  Parms:     PID enum
//  Returns:   Help
//
const char *GLOBAL_PidGetHelp(PIDT ePid)
{
   const char *pcHelp=NULL;

   if(pstMap && (ePid != -1) && (ePid < NUM_PIDT) )
   {
      pcHelp = pstMap->G_stPidList[ePid].pcHelp;
   }
   return(pcHelp);
}

//
// Function:   GLOBAL_PidInit
// Purpose:    Run the thread Init function
//
// Parms:      PID enum
// Returns:    iCc
//
int GLOBAL_PidInit(PIDT ePid)
{
   int      iCc=EXIT_CC_GEN_ERROR;
   PFUNIN   pfnInit;

   if(pstMap && (ePid < NUM_PIDT) )
   {
      pfnInit = pstMap->G_stPidList[ePid].pfnInit;
      if(pfnInit) iCc = pfnInit();
   }
   return(iCc);
}

//
//  Function:  GLOBAL_PidPut
//  Purpose:   Store thread pid
//
//  Parms:     PID enum, Pid
//  Returns:   tPid
//
bool GLOBAL_PidPut(PIDT ePid, pid_t tPid)
{
   bool fCc = FALSE;

   if(pstMap && (ePid != -1) && (ePid < NUM_PIDT) )
   {
      pstMap->G_stPidList[ePid].tPid = tPid;
      PRINTF("GLOBAL-PidPut: Pid %s=%d" CRLF, GLOBAL_PidGetName(ePid), tPid);
      fCc = TRUE;
   }
   return(fCc);
}

//
// Function:   GLOBAL_PidsLog
// Purpose:    Log all threads
//
// Parms:      
// Returns:    Nr of threads running
// Note:       
//
int GLOBAL_PidsLog()
{
   int      ePid, iRunning=0;
   pid_t    tPid;
   PIDL    *pstPidl;

   //
   // Log PIDs
   //
   for(ePid=0; ePid<NUM_PIDT; ePid++) 
   {
      pstPidl = &pstMap->G_stPidList[ePid];
      tPid    = pstPidl->tPid;
      if(tPid > 0) iRunning++;
      //
      if(GLOBAL_DebugGet(MM_DEBUG_PRINT)) LOG_printf("GLOBAL-PidsLog():%s=%5d (%s)" CRLF, pstPidl->pcName, tPid, pstPidl->pcHelp);
      LOG_Report(0, "GBL", "%s = %5d (%s)", pstPidl->pcName, tPid, pstPidl->pcHelp);
   }
   return(iRunning);
}

//
// Function:   GLOBAL_PidTerminate
// Purpose:    Terminate a single thread
//
// Parms:      ePid, Timeout in mSecs
// Returns:    iCc
// Note:       Caller should NOT kill itself !
//
int GLOBAL_PidTerminate(int ePid, int iMsecTimeout)
{
   int   iCc=EXIT_CC_GEN_ERROR;
   pid_t tPid;
   PIDL *pstPidl;

   if(pstMap && (ePid < NUM_PIDT) )
   {
      pstPidl = &(pstMap->G_stPidList[ePid]);
      tPid    = pstPidl->tPid;
      //
      // First persuade thread to terminate peacefully
      //
      if( (tPid > 0) && (tPid != getpid()) )
      {
         iCc = GEN_KillProcess(tPid, DO_FRIENDLY, 0);
         switch(iCc)
         {
            default:
               // OKee
               iCc = EXIT_CC_OKEE;
               LOG_Report(0, "GBL", "GLOBAL-PidTerminate():Terminated %s (%s)", pstPidl->pcName, pstPidl->pcHelp);
               break;

            case -1:
               // Zombie
               LOG_Report(0, "GBL", "GLOBAL-PidTerminate():Zombied %s (%s)", pstPidl->pcName, pstPidl->pcHelp);
               iCc = EXIT_CC_OKEE;
               break;

            case +1:
               if( GEN_WaitProcessTimeout(tPid, iMsecTimeout) == 1)
               {
                  iCc = GEN_KillProcess(tPid, NO_FRIENDLY, 500);
                  LOG_Report(0, "GBL", "GLOBAL-PidTerminate():%s (%s) did not exit: force it.", pstPidl->pcName, pstPidl->pcHelp);
               }
               else
               {
                  iCc = EXIT_CC_OKEE;
                  LOG_Report(0, "GBL", "GLOBAL-PidTerminate():%s (%s) exited", pstPidl->pcName, pstPidl->pcHelp);
               }
               break;
         }
         pstMap->G_stPidList[ePid].tPid = 0;
      }
   }
   return(iCc);
}

//
// Function:   GLOBAL_PidsTerminate
// Purpose:    Terminate all threads
//
// Parms:      Timeout in mSecs
// Returns:    Nr of threads were still running
// Note:       Caller should NOT kill itself !
//
int GLOBAL_PidsTerminate(int iMsecTimeout)
{
   int      iCc, ePid, iRunning=0;
   pid_t    tPid;

   if(!LOG_TraceOpenFile((char *)pcErrPath)) 
   {
      LOG_Report(0, "GBL", "GLOBAL-PidsTerminate: Logfile [%s] NOT opened", pcErrPath);
      PRINTF("GLOBAL-PidsTerminate: Logfile [%s] NOT opened" CRLF, pcErrPath);
   }
   //
   // First persuade all threads (if any) to terminate peacefully
   //
   LOG_Trace("GLOBAL-PidsTerminate():Friendly" CRLF);
   for(ePid=0; ePid<NUM_PIDT; ePid++)
   {
      tPid = pstMap->G_stPidList[ePid].tPid;
      if( (tPid > 0) && (tPid != getpid()) )
      {
         iCc = GEN_KillProcess(tPid, DO_FRIENDLY, iMsecTimeout);
         // Returns:     0 PID killed
         //             +1 PID Still running
         //             -1 PID Zombie or error
         switch(iCc)
         {
            case 0:
            default:
               PRINTF("GLOBAL-PidsTerminate():%s (%d):Terminated" CRLF, GLOBAL_PidGetName(ePid), tPid);
               LOG_Trace("GLOBAL-PidsTerminate():%s (%d):Terminated" CRLF, GLOBAL_PidGetName(ePid), tPid);
               pstMap->G_stPidList[ePid].tPid = 0;
               break;

            case +1:
               PRINTF("GLOBAL-PidsTerminate():%s (%d):Still running" CRLF, GLOBAL_PidGetName(ePid), tPid);
               LOG_Trace("GLOBAL-PidsTerminate():%s (%d):Still running" CRLF, GLOBAL_PidGetName(ePid), tPid);
               break;

            case -1:
               PRINTF("GLOBAL-PidsTerminate():%s (%d):Zombie" CRLF, GLOBAL_PidGetName(ePid), tPid);
               LOG_Trace("GLOBAL-PidsTerminate():%s (%d):Zombie" CRLF, GLOBAL_PidGetName(ePid), tPid);
               pstMap->G_stPidList[ePid].tPid = 0;
               break;
         }
      }
   }
   //
   // Force all threads (if any) to terminate if not voluntarily
   //
   LOG_Trace("GLOBAL-PidsTerminate():NOT Friendly" CRLF);
   for(ePid=0; ePid<NUM_PIDT; ePid++)
   {
      tPid = pstMap->G_stPidList[ePid].tPid;
      if( (tPid > 0) && (tPid != getpid()) )
      {
         if( GEN_WaitProcessTimeout(tPid, iMsecTimeout) == 1)
         {
            iRunning++;
            LOG_Trace("GLOBAL-PidsTerminate:%s (%d) NOT terminated yet" CRLF, GLOBAL_PidGetName(ePid), tPid);
            PRINTF("GLOBAL-PidsTerminate:%s-%s NOT terminated yet" CRLF, GLOBAL_PidGetName(ePid), tPid);
            iCc = GEN_KillProcess(tPid, NO_FRIENDLY, 500);
            // Returns:     0 PID killed
            //             +1 PID Still running
            //             -1 PID Zombie or error
            switch(iCc)
            {
               case 0:
               default:
                  LOG_Trace("GLOBAL-PidsTerminate():%s (%d) Killed" CRLF, GLOBAL_PidGetName(ePid), tPid);
                  break;
               case 1:
                  LOG_Trace("GLOBAL-PidsTerminate():%s (%d) Running" CRLF, GLOBAL_PidGetName(ePid), tPid);
                  break;

               case -1:
                  LOG_Trace("GLOBAL-PidsTerminate():%s (%d) Zombie" CRLF, GLOBAL_PidGetName(ePid), tPid);
                  break;
            }
         }
         else
         {
            PRINTF("GLOBAL-PidsTerminate():%s (%d) Terminated OK" CRLF, GLOBAL_PidGetName(ePid), tPid);
            LOG_Trace("GLOBAL-PidsTerminate():%s (%d) Terminated OK" CRLF, GLOBAL_PidGetName(ePid), tPid);
            pstMap->G_stPidList[ePid].tPid = 0;
         }
      }
   }
   LOG_TraceFlushFile(); 
   LOG_TraceCloseFile(); 
   return(iRunning);
}

//
//  Function:  GLOBAL_PutMallocs
//  Purpose:   Dump the MMAP variables
//  Parms:     Owner
//
//  Returns:
//
void GLOBAL_PutMallocs(int iMalloc)
{
   pstMap->G_iMallocs += iMalloc;
}

//
// Function:   GLOBAL_PidSaveGuard
// Purpose:    Save the required guard marker
//             GLOBAL_xxx_ALL_INI for Guard checks
//                             0  if no Guard checks.
// Parms:      PID enum, marker
// Returns:    
//
void GLOBAL_PidSaveGuard(PIDT ePid, int iMarker)
{
   if(pstMap && (ePid < NUM_PIDT) )
   {
      pstMap->G_stPidList[ePid].iFlag = iMarker;
   }
}

//
// Function:   GLOBAL_PidSetGuard
// Purpose:    Set the correct guard marker
//
// Parms:      PID enum
// Returns:    
// Note:       This function should be called regularly (at least within GUARD_SECS
//             secs) in order to SET their GLOBAL_xxx_INI flag in G_iGuard.
//             If they fail (Current time is later than the previous check) the
//             event will be counted. At the end of the day, the counts of all
//             active threads should be lower that a threshold. If not the service 
//             will be restarted by the WatchDog, who is triggered by PID_HOST 
//             through a SIGUSR1 (Restart) or SIGUSR2 (Reboot).
//
//             G_iGuards
//             G_stPidList[i]->pid_t       tPid   : The PID
//                             int         iFlag  : The guard marker
//                             const char *pcName : The PID short-name
//                             const char *pcHelp : The PID long-name
//             Do NOT use LOG_Printf(), PRINTF() etc between Lock() and Unlock() !!
//
//
bool GLOBAL_PidSetGuard(PIDT ePid)
{
   bool  fChanged=FALSE;
   int   iFlag, iGuard;

   if(pstMap && (ePid < NUM_PIDT) )
   {
      GLOBAL_Lock();
      iGuard = pstMap->G_iGuards;
      iFlag  = pstMap->G_stPidList[ePid].iFlag;
      //
      // Set the flag only if necessary
      //
      if( iFlag && ((iGuard & iFlag) == 0) )
      {
         iGuard |= iFlag;
         pstMap->G_iGuards = iGuard;
         fChanged = TRUE;
      }
      GLOBAL_Unlock();
   }
   PRINTF("GLOBAL-PidSetGuard():%5d-0x%08X(%s)" CRLF, pstMap->G_stPidList[ePid].tPid, iGuard, pstMap->G_stPidList[ePid].pcHelp);
   return(fChanged);
}

//
// Function:   GLOBAL_PidSetInit
// Purpose:    Set the thread Init function
//
// Parms:      PID enum, Init function
// Returns:    
//
void GLOBAL_PidSetInit(PIDT ePid, PFUNIN pfnInit)
{
   if(pstMap && (ePid < NUM_PIDT) )
   {
      pstMap->G_stPidList[ePid].pfnInit = pfnInit;
      pstMap->G_stPidList[ePid].ulTs    = RTC_GetSystemSecs();
   }
}

//
//  Function:  GLOBAL_PidTimestamp
//  Purpose:   Update thread timestamp
//
//  Parms:     Pid-Enum, Timestamp (or 0 for current time)
//  Returns:   
//  Note:      
//
void GLOBAL_PidTimestamp(PIDT ePid, u_int32 ulTs)
{
   PIDL *pstPid;

   if(pstMap && (ePid >= 0) && (ePid < NUM_PIDT) )
   {
      pstPid = &pstMap->G_stPidList[ePid];
      if(ulTs == 0) ulTs = RTC_GetSystemSecs();
      pstPid->ulTs = ulTs;
      pstPid->iCount++;
   }
   else LOG_Report(0, "GBL", "GLOBAL-PidTimestamp():BAD info (Map=%p, ePid=%d)", pstMap, ePid);
}

// 
// Function:   GLOBAL_Signal
// Purpose:    Signal a PID_xx
// 
// Parameters: PIDT PID_xxx, signal
// Returns:    
// Note:       
// 
void GLOBAL_Signal(PIDT ePid, int iSignal)
{
   pid_t tPid;

   tPid = GLOBAL_PidGet(ePid);
   if(tPid > 0) kill(tPid, iSignal);
}

// 
// Function:   GLOBAL_SignalPid
// Purpose:    Signal a PID
// 
// Parameters: Pid, signal
// Returns:    
// Note:       
// 
void GLOBAL_SignalPid(pid_t tPid, int iSignal)
{
   if(tPid > 0) kill(tPid, iSignal);
}

//
// Function:   GLOBAL_SignalWait
// Purpose:    Wait for the signal
//
// Parms:      Pid, Signal, timeout(0=indefinitely)
// Returns:     0: Signal received
//             +1: Timeout
//             -1: Error
// Note:       Used to execute all running threads synchronized
//
int GLOBAL_SignalWait(int ePid, int iSignal, int iTimeout)
{
   bool  fWait=TRUE;
   int   iOpt;

   while(fWait)
   {
      iOpt = GLOBAL_SemaphoreWait(ePid, iTimeout);
      switch(iOpt)
      {
         case 0:
            if( GLOBAL_GetSignalNotification(ePid, iSignal)) 
            {
               PRINTF("GLOBAL-SignalWait():Signal" CRLF);
               fWait = FALSE;
            }
            break;

         case -1:
         case +1:
            fWait = FALSE;
            break;

         default:
            break;
      }
   }
   return(iOpt);
}

/*----------------------------------------------------------------------
____________SEMAPHORE_FUNCTIONS(){}
------------------------------x----------------------------------------*/

//
// Function:   GLOBAL_SemaphoreDelete
// Purpose:    Delete the Semaphore
//
// Parms:      PID_xxxx
// Returns:    TRUE if OKee
// Note:       
//
bool GLOBAL_SemaphoreDelete(int ePid)
{
   bool fCc=FALSE;
   
   if(pstMap->G_stPidList[ePid].tPid)
   {
      if( sem_destroy(pstMap->G_stPidList[ePid].ptSem) == -1) 
      {
         LOG_Report(errno, "GBL", "GLOBAL-SemaphoreDelete():sem_destroy");
      }
      else fCc = TRUE;
   }
   return(fCc);
}

//
// Function:   GLOBAL_SemaphoreInit
// Purpose:    Init the Semaphore
//
// Parms:      PID_xxxx
// Returns:    0=OK, -1=ERROR (errno)
// Note:       Parm1=0: Used between threads in the same process
//                   1: Used between different processes
//             Parm2=0: Initial value=0 : Sem already locked.
//                   1: Initial value=1 : Sem unlocked.
//
bool GLOBAL_SemaphoreInit(int ePid)
{
   bool fCc=FALSE;
   
   if(pstMap->G_stPidList[ePid].tPid)
   {
      //
      // Make sure the G_stPidList[x].ptSem has a valid semaphore
      //
      pstMap->G_stPidList[ePid].ptSem = &(pstMap->G_tSemList[ePid]);
      if(sem_init(pstMap->G_stPidList[ePid].ptSem, 1, 0) == -1) 
      {
         LOG_Report(errno, "GBL", "GLOBAL-SemaphoreInit():sem_init ERROR ");
      }
      else fCc = TRUE;
   }
   return(fCc);
}

//
// Function:   GLOBAL_SemaphorePost
// Purpose:    Unlock the Semaphore
//
// Parms:      PID_xxxx
// Returns:    TRUE if OKee
// Note:       sem_post() increments (unlocks) the semaphore pointed to by sem. If the semaphore's 
//             value consequently becomes greater than zero, then another process or thread blocked
//             in a sem_wait(3) call will be woken up and proceed to lock the semaphore. 
//
bool GLOBAL_SemaphorePost(int ePid)
{
   bool     fCc=FALSE;
   int      iSemVal;
   sem_t   *ptSem;

   if(pstMap->G_stPidList[ePid].tPid)
   {
      if( (ptSem = pstMap->G_stPidList[ePid].ptSem) )
      {
         sem_getvalue(ptSem, &iSemVal);
         if(iSemVal > 0)
         {
            //
            // Sem already locked!
            //
            //pwjh LOG_Report(0, "GBL", "GLOBAL-SemaphorePost(): semaphore = %d", iSemVal);
            fCc = TRUE;
         }
         else
         {
            //
            // sem++ (UNLOCK)   
            //
            if(sem_post(ptSem) == -1) 
            {
               LOG_Report(errno, "GBL", "GLOBAL-SemaphorePost(): sem_post error");
            }
            else fCc = TRUE;
         }
      }
   }
   else LOG_Report(0, "GBL", "GLOBAL-SemaphorePost():No PID");
   return(fCc);
}

//
// Function:   GLOBAL_SemaphoreWait
// Purpose:    Wait till Semaphore has been unlocked, then LOCK and proceed
//
// Parms:      PID_xxxx, Timeout (or -1 if no timeout)
// Returns:     0 if sem unlocked
//             +1 if sem timeout
//             -1 on error (errno)
// Note:       sem_wait() decrements (locks) the semaphore pointed to by sem. 
//             If the semaphore's value is greater than zero, then the decrement proceeds, 
//             and the function returns, immediately. If the semaphore currently has the 
//             value zero, then the call blocks until either it becomes possible to perform
//             the decrement (i.e., the semaphore value rises above zero), 
//             or a signal handler interrupts the call. 
//
//             sem_timedwait() is the same as sem_wait(), except that abs_timeout specifies a 
//             limit on the amount of time that the call should block if the decrement cannot 
//             be immediately performed. The abs_timeout argument points to a structure that
//             specifies an absolute timeout in seconds and nanoseconds since the Epoch, 
//             1970-01-01 00:00:00 +0000 (UTC). This structure is defined as follows:
//
//             struct timespec 
//             {
//                time_t tv_sec;      /* Seconds */
//                long   tv_nsec;     /* Nanoseconds [0 .. 999999999] */
//             };
//             If the timeout has already expired by the time of the call, and the semaphore 
//             could not be locked immediately, then sem_timedwait() fails with a timeout error 
//             (errno set to ETIMEDOUT). If the operation can be performed immediately, then 
//             sem_timedwait() never fails with a timeout error, regardless of the value of 
//             abs_timeout. Furthermore, the validity of abs_timeout is not checked in this case.
//
//
int GLOBAL_SemaphoreWait(int ePid, int iMsecs)
{
   int               iCc=0;
   bool              fWait=TRUE;
   long              lTime;
   struct timespec   stTime;

   if(iMsecs > 0)
   {
      clock_gettime(CLOCK_REALTIME, &stTime);
      stTime.tv_sec += iMsecs / 1000;
      lTime          = stTime.tv_nsec;
      lTime         += ONEMILLION * (long) (iMsecs % 1000);
      //
      // Check secs overflow
      //
      if(lTime >= ONEBILLION) 
      {
         lTime -= ONEBILLION;
         stTime.tv_sec++;
      }
      stTime.tv_nsec = lTime;
      //PRINTF("GEN_WaitSemaphore():CLOCK time=%ld - %ld" CRLF, stTime.tv_sec, stTime.tv_nsec);
      do
      {
         iCc = sem_timedwait(pstMap->G_stPidList[ePid].ptSem, &stTime);
         if(iCc == -1)
         {
            switch(errno)
            {
               case EINTR:
                  // Interrupted: continue waiting 
                  break;

               case ETIMEDOUT: 
                  fWait = FALSE;
                  iCc   = 1;
                  break;

               default:
               case EINVAL:
                  fWait = FALSE;
                  //PRINTF("sup_WaitSemaphore():INVALID time=%ld - %ld" CRLF, stTime.tv_sec, stTime.tv_nsec);
                  LOG_Report(errno, "GBL", "GLOBAL-SemaphorePost(): sem_post error");
                  break;
            }
         }
         else fWait = FALSE;
      }
      while(fWait);
   }
   else
   {
      //
      // if    sem == 0 : wait
      // else  sem-- (LOCK)   
      //
      while( ((iCc = sem_wait(pstMap->G_stPidList[ePid].ptSem)) < 0) && (errno == EINTR) ) continue;
   }
   //
   // iCc= 0: Normal completion: Sem=UNLOCKED
   // iCc= 1: Timeout:           Sem=LOCKED
   // iCc=-1: Error:             Sem=LOCKED
   //
   return(iCc);
}


/*----------------------------------------------------------------------
_______________CONFIG_FUNCTIONS(){}
------------------------------x----------------------------------------*/

//
//  Function:  GLOBAL_DumpVars
//  Purpose:   Dump the MMAP variables
//  Parms:     Owner
//
//  Returns:
//
void GLOBAL_DumpVars(char *pcSrc)
{
   LOG_printf("%s(): Mapfile    : [    ] %p"     CRLF, pcSrc, pstMap);
   LOG_printf("%s(): Version    : [%4d] v%d.%d"  CRLF, pcSrc, offsetof(RPIMAP, G_iVersionMajor), pstMap->G_iVersionMajor, pstMap->G_iVersionMinor);
   LOG_printf("%s(): Signature1 : [%4d] 0x%x"    CRLF, pcSrc, offsetof(RPIMAP, G_iSignature1),   pstMap->G_iSignature1);
   //
   LOG_printf("%s(): IP Wan     : [%4d] <%s>"    CRLF, pcSrc, offsetof(RPIMAP, G_pcWanIp),       pstMap->G_pcWanIp);
   LOG_printf("%s(): IP         : [%4d] <%s>"    CRLF, pcSrc, offsetof(RPIMAP, G_pcGenericIp),   pstMap->G_pcGenericIp);
   //
   LOG_printf("%s(): Status     : [%4d] <%s>"    CRLF, pcSrc, offsetof(RPIMAP, G_pcStatus),      pstMap->G_pcStatus);
   LOG_printf("%s(): Outputfile : [%4d] <%s>"    CRLF, pcSrc, offsetof(RPIMAP, G_pcLastFile),    pstMap->G_pcLastFile);
   LOG_printf("%s(): Signature2 : [%4d] 0x%x"    CRLF, pcSrc, offsetof(RPIMAP, G_iSignature1),   pstMap->G_iSignature2);
}

/*----------------------------------------------------------------------
________________LOCAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/

//
// Function:   global_SaveMapfile
// Purpose:    Copy the MMAP file from RAM disk to SD 
//             cp /usr/local/share/rpi/spicam.map <WORK_DIR>/pikrellman.map
// Parms:     
// Returns:    Shell CC
// Note:       CANNOT Use LOG_printf(), PRINTFx() or LOG_Report() due to the use of the G_tmutex !
//
static int global_SaveMapfile()
{
   int   iCc=EXIT_CC_GEN_ERROR;
   char  cSrc, cDst;

   GLOBAL_GetFileInfo((char *)pcDirRestoreSrc, &cSrc);
   GLOBAL_GetFileInfo((char *)pcDirRestoreDst, &cDst);
   //
   if( (cSrc == 'D') && (cDst == 'D') )
   {
      iCc = system(pcShellBackup);
   }
   return(iCc);
}

/*----------------------------------------------------------------------
__________________COMMENTED_OUT(){}
------------------------------x----------------------------------------*/

#ifdef COMMENT


#endif   //COMMENT

