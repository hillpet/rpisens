/*  (c) Copyright:  2020..2024  Patrn, Confidential Data
 *
 *  Workfile:           config.h
 *  Purpose:            Global configurations
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PIC 16f628A Ultrasoon sensor
 * 
 *  Author:             Peter Hillen
 *  Changes:
 *    01 Nov 2020:      Ported from rpislim
 *    28 Nov 2020:      IO_PATRN GPIO
 *    01 May 2021:      Add CVS records to day file; Add CLI startup mode
 *    02 May 2021:      Fix KINS_MODE_POWEROFF bug
 *    20 May 2021:      Backup csv files to persistent filesystem
 *    21 May 2021:      Add HTTP Delete (for csv files)
 *    01 Jul 2921:      Add LOGs to determine spontaneoud exits
 *    29 Mar 2022:      Add Variadic macro's for PRINTF()
 *    29 Mar 2022:      Use non-blocking Read
 *    02 Jun 2022:      Add Kinshi Mode55 select (O,P,D,R)
 *    25 Jun 2022:      Move to new globals.*
 *    11 Jan 2023:      Add G_iDelete as extension to G_pcDelete
 *    10 Apr 2024:      Move HTTP_SendTextFile() to common
 *    24 Apr 2024:      Move to global/local startup 
 *    01 May 2024:      DB 7.0: Move uCtl RAM/EEPROM content to persistent storage
 *    09 May 2024:      Add midnight mail
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _CONFIG_H_
#define _CONFIG_H_

//#define FEATURE_RUN_USERSPACE        // Run TheApp in userspace
  #define FEATURE_SHOW_USER            // Show user credantials on RUN_USERSPACE
//#define FEATURE_NOFILE_TODAY         // Show todays values if date is not on file
//#define FEATURE_PI_JUICE             // PiJuice battery backup support
//#define FEATURE_NET_REPORT_PAGE      // Report full HTML page on NET errors
  #define FEATURE_NET_DUMP_RECORD      // Dump contents of network buffers
//#define FEATURE_ECHO_HTTP_IN         // Echo all incoming HTTP data
//#define FEATURE_ECHO_HTTP_OUT        // Echo all outging  HTTP data
  #define FEATURE_AUTO_REFRESH_METER   // Auto refresh meter.html through browser
//#define FEATURE_TRUNCATE_LOG_MIDNIGHT // Truncate logfile each night

//
// Verbose flags
// +-------+-------+-------+-------+
// | Level |     24 Flags          |
// +-------+-------+-------+-------+
//
#define VERBOSE_LEVEL_MASK    0xFF000000     // Verbose level mask
#define VERBOSE_LEVEL_SHIFT   24             // Verbose level shift
//
// Verbode Mode flags
#define VERBOSE_MODE_IDLE     0x00000001     // Force Startup in IDLE mode
#define VERBOSE_MODE_RESET    0x00000002     // Force Startup Microcontroller restart
#define VERBOSE_MODE_SENSOR   0x00000004     // Force Startup Smart Sensor mode
#define VERBOSE_MODE_MEMORY   0x00000008     // Trigger Sensor memory read-once
//                            0x00000010     // 
//                            0x00000020     // 
//                            0x00000040     // 
#define VERBOSE_MODE_SIM      0x00000080     // Simulation mode
// Verbose LOG flags
#define VERBOSE_LOG_SENSOR1   0x00000100     // LOG Sensor data
#define VERBOSE_LOG_SENSOR2   0x00000200     // LOG Sensor data details
#define VERBOSE_LOG_SENSOR3   0x00000400     // LOG Sensor data details
#define VERBOSE_LOG_SENSOR4   0x00000800     // LOG Sensor data raw sensor bytes
#define VERBOSE_LOG_MIAC      0x00001000     // Miac parsing
#define VERBOSE_LOG_CALIBRATE 0x00002000     // LOG Dx for calibration
//
typedef enum RCUKEYS
{
#define  EXTRACT_KEY(a,b)  a,
#include "key_defs.h"
#undef   EXTRACT_KEY
   //
   NUM_RCU_KEYS
}  RCUKEYS;

//=============================================================================
#ifdef   FEATURE_IO_NONE      // IO : NO IO board used
//=============================================================================
#define  RPI_IO_BOARD         "RpiSens uses NO IO !"
//
#define  LED_R
#define  LED_G
#define  LED_Y
//
#define  SETUP_GPIO()         0
#define  INP_GPIO(x)        
#define  OUT_GPIO(x)     
#define  BUTTON(x)            FALSE
#define  LED(x, y)            
#endif

//=============================================================================
#ifdef   FEATURE_IO_PATRN     // IO : Use Patrn.nl board with wiringPi lib
//=============================================================================
#include <wiringPi.h>
//
#define  RPI_IO_BOARD         "RpiSens uses Discrete LEDs"
//
#define  LED_R
#define  LED_Y                2        // GPIO-3 Pin 15
#define  LED_G                3        // GPIO-2 Pin 13
//
#define  BUTTON_OFF
//
#define  SETUP_GPIO()         wiringPiSetup()
#define  INP_GPIO(x)          pinMode(x, INPUT)
#define  OUT_GPIO(x)          pinMode(x, INPUT);pinMode(x, OUTPUT)
#define  BUTTON(x)            FALSE
#define  LED(x, y)            digitalWrite(x, y)
#endif

//=============================================================================
#ifdef   FEATURE_PIG_PATRN    // IO : Use Patrn.nl board with pigpio lib
//=============================================================================
#include <pigpio.h>
//
#define  RPI_IO_BOARD         "RpiSens uses Discrete LEDs"
//
#define  LED_R
#define  LED_Y                2        // GPIO-3 Pin 15
#define  LED_G                3        // GPIO-2 Pin 13
//
#define  BUTTON_OFF
//
#define  SETUP_GPIO()         gpioInitialise()
#define  INP_GPIO(x)          gpioSetMode(x, PI_INPUT);
#define  OUT_GPIO(x)          gpioSetMode(x, PI_OUTPUT);
#define  BUTTON(x)            FALSE
#define  LED(x, y)            gpioWrite(x, y)
#endif

//
// The build process determines RELEASE or DEBUG builds through the
// DEBUG_ASSERT compile-time switch.
//
#define THE_APP                     "Kinshi Smart-Sensor"
#define RPI_APP_TEXT                "RpiSense"
#define RPI_BASE_NAME               "rpisens"
#define BUILD                       "072"
//
#define DATA_VALID_SIGNATURE        0xDEADBEEF           // Valid signature
#define DATA_VERSION_MAJOR          8                    // DB changes to MMAP
#define DATA_VERSION_MINOR          0                    // DB additions to EXTRA_POOL
//
//
#ifdef  DEBUG_ASSERT
#define VERSION                     "2.05-cr" BUILD
#define ASSERT(x)                   assert(x)
//
#else   //DEBUG_ASSERT
#define VERSION                     "2.05." BUILD
#define ASSERT(x)
//
#endif  //DEBUG_ASSERT
//
#define MIN_POWER_LEVEL             10                   // Powerlevel % for shutdown
//
// Target RAM disk for high load file access
//
#define RPI_LOG_EXT                 ".log"
#define RPI_MAP_EXT                 ".map"
//
#define RPI_LOG_FILE                RPI_BASE_NAME RPI_LOG_EXT
#define RPI_MAP_FILE                RPI_BASE_NAME RPI_MAP_EXT
//
#define RPI_ERR_FILE                RPI_BASE_NAME ".err"
#define RPI_SHELL_FILE              RPI_BASE_NAME ".txt"
#define RPI_ALL_FILES               RPI_BASE_NAME ".*"
//
#define RPI_WORK_DIR                "/mnt/rpicache/"
#define RPI_RAMFS_DIR               "/mnt/rpicache"
#define RPI_BACKUP_DIR              "/all/rpi/"
#define RPI_PUBLIC_WWW              "/home/public/www/"
//
#define RPI_PUBLIC_LOG_PATH         RPI_WORK_DIR RPI_LOG_FILE
#define RPI_ERROR_PATH              RPI_WORK_DIR RPI_ERR_FILE
#define RPI_MAP_PATH                RPI_WORK_DIR RPI_MAP_FILE
#define RPI_MAP_NEWPATH             RPI_WORK_DIR RPI_BASE_NAME "new.map"
#define RPI_TRACE_FILE              RPI_WORK_DIR "trace.txt"
#define RPI_DL_DIR                  ""
#define RPI_CSV_FILE                "%s" RPI_DL_DIR "SmartS_%s.csv"
#define RPI_FLT_FILE                "%s" RPI_DL_DIR "SmartS_%s.bin"
//
#define RPI_PERS_LOG_PATH           RPI_BACKUP_DIR RPI_LOG_FILE
#define RPI_SYMBOL_FILE             RPI_BACKUP_DIR "Filtersoon.lst"
//
#define RPI_PUBLIC_DEFAULT          "index.html"
#define RPI_LOCALHOST               "www.patrn.nl"
#define RPI_WATCHDOG                "rpidog"

#endif /* _CONFIG_H_ */

