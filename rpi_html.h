/*  (c) Copyright:  2020..2024  Patrn, Confidential Data
 *
 *  Workfile:           rpi_html.h
 *  Purpose:            Generic HTML functions
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    01 Nov 2020:      Ported from rpislim
 *    23 Apr 2024:      Split globals.h and locals.h
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _RPI_HTML_H_
#define _RPI_HTML_H_

//
// Define HTML Element type
//
#define  HE_NONE        0x0000         // No specials
#define  HE_COMMA       0x0001         // , for multi=element Separator
#define  HE_COLN        0x0002         // : for multi=element Separator
#define  HE_TEXT        0x0004         // "Text"
#define  HE_VLS         0x0008         // '|' 
#define  HE_SPACE1      0x0010         // ' '
#define  HE_SPACE2      0x0020         // '  '
#define  HE_VLE         0x0040         // '|' 
#define  HE_CRLF        0x0080         // CRLF
#define  HE_BR          0x0100         // </br>
#define  HE_SPACE4      0x0200         // '    '

//
// Global prototypes
//
bool     HTML_RespondObject         (NETCL *, RPIDATA *);
RPIDATA *HTML_TerminateObject       (RPIDATA *);
RPIDATA *HTML_InsertData            (RPIDATA *, char *, int);
RPIDATA *HTML_ReleaseObject         (RPIDATA *);
int      HTML_GetObjectSize         (RPIDATA *);

#endif   //_RPI_HTML_H_
