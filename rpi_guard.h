/*  (c) Copyright:  2022..2024  Patrn, Confidential Data
 *
 *  Workfile:           rpi_guard.h
 *  Purpose:            Headerfile for pikrellman running state monitor thread
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PIC 16f628A Ultrasoon sensor
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    25 Jun 2022:      Created
 *    23 Apr 2024:      Split globals.h and locals.h
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _RPI_GUARD_H_
#define _RPI_GUARD_H_
 
#define  GUARD_COUNT                      3
#define  GUARD_TIMEOUT                    1000
//
#define  GUARD_SECS                       (((5*60)*1000)/GUARD_TIMEOUT)
#define  GUARD_SECS_RETRY                 (2*60)
#define  SET_GUARD_SECS                   (1*60)
//
// Global functions
//
int GRD_Init               (void);

#endif /* _RPI_GUARD_H_ */
