/*  (c) Copyright:  2020..2024  Patrn, Confidential Data
 *
 *  Workfile:           rpi_slim.c
 *  Purpose:            Handling dynamic HTTP pages for the Kinshi Smart Ultrasoon sensor
 *                      Ultrasoon sensor :  HC-SR04
 *                      Settings         :  9600 baud, 8 bits, no parity
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *  Note:               
 *
 *  Author:             Peter Hillen
 *  Changes:       
 *    01 Nov 2020:      Ported from rpislim
 *    29 Nov 2020:      Turn OFF auto refreash life data
 *    01 Dec 2020:      Add DistRaw to meter; Auto refresh HTTP featured
 *    29 Mar 2022:      Add Variadic macro's for PRINTF()
 *    17 Aug 2022:      Add Miac
 *    22 Apr 2024:      Split Meter html page (settings and water meter)
 *    24 Apr 2024:      Move to global/local startup 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <sched.h>
#include <errno.h>
#include <time.h>
#include <termios.h>
#include <unistd.h>
//
#include <common.h>
#include "config.h"
#include "locals.h"
#include "globals.h"
//
#include "gen_http.h"
#include "rpi_page.h"
#include "rpi_html.h"
#include "rpi_json.h"
#include "rpi_func.h"
#include "rpi_comm.h"
#include "rpi_slim.h"

//#define USE_PRINTF
#include <printx.h>

//
// External HTTP data
//
extern const char *pcHttpResponseHeader;
extern const char *pcWebPageStart;
extern const char *pcWebPageStartRefresh;
extern const char *pcWebPageCenter;
extern const char *pcWebPageLeft;
extern const char *pcWebPageStopBad;
extern const char *pcWebPageDefault;
extern const char *pcWebPageFontStart;
extern const char *pcWebPageFontEnd;
//
extern const char *pcWebPageNotImplemented;
extern const char *pcWebPageLineBreak;
extern const char *pcWebPageEnd;
//
// Misc tex strings/legends
//
static const char  *pcWebPageHdr          = "<h3>%s</h3>";
static const char  *pcWebPageLink         = "&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"%s\">%s</a>";
static const char  *pcLinkTotals          = "[Dag Totaal]";
static const char  *pcLinkDetails         = "[Dag Detail]";
static const char  *pcLinkPrevDay         = "[Vorige Dag]";
static const char  *pcLinkNextDay         = "[Volgende Dag]";
static const char  *pcLinkData            = "[Meterstand]";
static const char  *pcLinkExport          = "[Export]";
static const char  *pcLinkMonth           = "[Maand Totaal]";
static const char  *pcLinkPrevMonth       = "[Vorige Maand]";
static const char  *pcLinkNextMonth       = "[Volgende Maand]";
static const char  *pcLinkScaleUp         = "[Schaal +]";
static const char  *pcLinkScaleDn         = "[Schaal -]";
static const char  *pcCourierNew          = "Courier New";
//
static const char  *pcMiacRom             = "Smart Sensor Miac EEPROM";
static const char  *pcMiacRam             = "Smart Sensor Miac RAM";
//
static const char  *pcWebPageTitleSlim    = "PATRN Smart Sensor monitor";
//
// Add meta refresh to http header
//
static const char  *pcWebRefreshSlim      = "6;url=http://192.168.178." IPADDR "/meter";
static const char  *pcWebPageTitleMonth   = "PATRN Smart Sensor monitor - Month";
static const char  *pcWebPageTitleDay     = "PATRN Smart Sensor monitor - Day";
static const char  *pcSmartLegendQtr      = "**************************************************************************************************"; 
static const char  *pcSmartLegendHrs1     = "*0...0...0...0...0...0...0...0...0...0...1...1...1...1...1...1...1...1...1...1...2...2...2...2...*"; 
static const char  *pcSmartLegendHrs2     = "*0...1...2...3...4...5...6...7...8...9...0...1...2...3...4...5...6...7...8...9...0...1...2...3...*"; 
//
static const char  *pcSmartLegendDay1     = "*********************************";
static const char  *pcSmartLegendDay2     = "*0........1.........2.........3.*";
static const char  *pcSmartLegendDay3     = "*1...5....0....5....0....5....0.*";
//
static const char *pcSmartHtmlStuff[NUM_EMS]  =
{
  "mm/sd (Waterhoogte)",            // EMS_LEV  Waterlevel
  "C/sd (Temperatuur)",             // EMS_TMP  Temperature
  "mm/sd (Afstand)",                // EMS_DIS  Ultrasoon samples
  "%/sd (% Pomp AAN)",              // EMS_PMP  % Pump ON
   "%/sd (% X10 Bypass)",           // EMS_X10  X10 Bypass
};
//
// JSON IDs
// The fieldnames MUST correctpond to the ones used by the Android App, so keep them intact !!
//
static const char *pcSmartJsonField[NUM_EMS]  =
{
   "Waterlevel",                    // EMS_LEV  Waterlevel
   "Temperature",                   // EMS_TMP  Temperature
   "SmartSoon",                     // EMS_DIS  Ultrasoon samples
   "PumpOn",                        // EMS_PMP  % Pump ON
   "X10Bypass",                     // EMS_X10  X10 Bypass
};

//
// EMS Totals
//
static const char *pcEmsTotals[NUM_EMS]  =
{
   "Water Total",                   // EMS_LEV  Waterlevel
   "Temp Total",                    // EMS_TMP  Temperature
   "US Total",                      // EMS_DIS  Ultrasoon samples
   "Pump On",                       // EMS_PMP  % Pump ON
   "X10Bypass",                     // EMS_X10  X10 Bypass
};
//
// EMS units
//
static const char *pcSmartJsonScale[NUM_EMS]  =
{
   "mm/sd",                         // EMS_LEV  Waterlevel
   "C/sd",                          // EMS_TMP  Temperature
   "mm/sd",                         // EMS_DIS  Ultrasoon samples
   "%/sd",                          // EMS_PMP  % Pump ON
   "%/sd",                          // EMS_X10  X10 Bypass
};
//
// Scales for all stuff
//
static const double flScales[NUM_SCALES] =
{ 80000.0, 40000.0, 20000.0, 10000.0, 5000.0, 2000.0, 1000.0,  500.0, 200.0, 100.0, 50.0, 20.0, 10.0, 5.0, 2.0, 1.0 };
  
//
// Local prototypes
//
static void       slim_InitDynamicPages            (int);
static bool       slim_DynPageDaySmart             (NETCL *, int);
static bool       slim_DynPageDay                  (NETCL *, int);
static bool       slim_DynPageDaySmartPrevHtml     (NETCL *, int);
static bool       slim_DynPageDaySmartNextHtml     (NETCL *, int);
static bool       slim_DynPageDayPrevHtml          (NETCL *, int);
static bool       slim_DynPageDayNextHtml          (NETCL *, int);
static bool       slim_DynPageDayScaleUpHtml       (NETCL *, int);
static bool       slim_DynPageDayScaleDnHtml       (NETCL *, int);
static bool       slim_DynPageLiveData             (NETCL *, int);
static bool       slim_DynPageMonth                (NETCL *, int);
static bool       slim_DynPageMonthPrevHtml        (NETCL *, int);
static bool       slim_DynPageMonthNextHtml        (NETCL *, int);
static bool       slim_DynPageMonthScaleUpHtml     (NETCL *, int);
static bool       slim_DynPageMonthScaleDnHtml     (NETCL *, int);
static bool       slim_DynPageSlimExport           (NETCL *, int);
static bool       slim_DynPageMiacRom              (NETCL *, int);
static bool       slim_DynPageMiacRam              (NETCL *, int);
static bool       slim_DynPageMiac                 (NETCL *, int);
static bool       slim_DynPageSensor               (NETCL *, int);
//
static bool       slim_DynBuildPageDayHtml         (NETCL *);
static bool       slim_DynBuildPageDaySmartHtml    (NETCL *);
static bool       slim_DynBuildLiveSmartHtml       (NETCL *);
static bool       slim_DynBuildPageMonthHtml       (NETCL *);
//
static bool       slim_DynBuildPageDaySmartJson    (NETCL *);
static bool       slim_DynBuildPageDefaultJson     (NETCL *);
static bool       slim_DynBuildLiveSmartJson       (NETCL *);
static bool       slim_DynBuildMiacRomHtml         (NETCL *);
static bool       slim_DynBuildMiacRamHtml         (NETCL *);
//
static int        slim_AutoScaleSmartData          (double *, int, int);
static void       slim_BuildRowHtmlText            (NETCL *, char *, char *, char *);
static void       slim_BuildRowHtmlMiac            (NETCL *, int, int);
static void       slim_BuildRowHtmlFloat           (NETCL *, char *, double, char *);
static RPIDATA   *slim_BuildSmartDay               (NETCL *, RPIDATA *, EMS, SMDATA  *);
static RPIDATA   *slim_BuildJsonArray              (NETCL *, RPIDATA *, EMS, SMDATA  *, int);
static RPIDATA   *slim_BuildJsonDetailedDay        (NETCL *, RPIDATA *, EMS, SMDATA  *, int);
static bool       slim_BuildMiac                   (NETCL *, int);
static RPIDATA   *slim_BuildSmartData              (RPIDATA *, char *, int, int, double *, int);
static void       slim_CopySmartData               (KINSP);
static void       slim_GetPeriodicData             (EMS, SMDATA *, double *, int);
static bool       slim_IsToday                     (u_int32);
static u_int32    slim_RetrieveDisplayData         (u_int32, KINSP);
static bool       slim_ShowFilesHtml               (NETCL *, char *, char *, const char *);
static u_int32    slim_UpdateGlobalSecs            (void);
static void       slim_UpdateYearMonthDay          (void);
//
//
// Enums and dynamic LUTs (Global DYN pages)
//
enum
{
   #define  EXTRACT_DYN(a,b,c,d,e,f)   a,
   #include "pages.h"
//
// Enums and dynamic LUTs (Local DYN pages)
//
   #include "slimp.h"
   #undef EXTRACT_DYN
   NUM_SMARTS_DYNS
};
//
// Local DYN pages
//
static const NETDYN stDynamicPages[] =
{
//    tUrl,    tType,   iTimeout,   iFlags,  pcUrl,   pfDynCb;
   #define  EXTRACT_DYN(a,b,c,d,e,f)   {a,b,c,d,e,f},
   #include "slimp.h"
   #undef EXTRACT_DYN
   {  -1,      0,       0,          0,       NULL,    NULL  }
};


/*------  Local functions separator ------------------------------------
__GLOBAL_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//  
// Function:   SLIM_InitDynamicPages
// Purpose:    Init the Smart Sensor dynamic pages
// 
// Parameters: Port
// Returns:    
// Note:       
//  
void SLIM_InitDynamicPages(int iPort)
{
   pstMap->G_ulSmartSecs      = RTC_GetSystemSecs();  //    Today
   pstMap->G_iSmartScaleMonth = 0;                    //    Auto scale Month plot
   pstMap->G_iSmartScaleDay   = 0;                    //    Auto scale Day plot
   //
   //PRINTF("SLIM_InitDynamicPages():SecsNow=%d" CRLF, (int)pstMap->G_ulSmartSecs);
   slim_UpdateYearMonthDay();
   slim_InitDynamicPages(iPort);
}

/*------  Local functions separator ------------------------------------
__DYN_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//
// Function:   slim_DynPageLiveData
// Purpose:    Handle the Smart Sensor live data display: start TODAY
//             meter
//             meter.html
//             meter.json
// Parms:
// Returns:    
// Note:       Smart Sensor data:  pstMap->G_pc...[]
//
static bool slim_DynPageLiveData(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;

   pstMap->G_ulSmartSecs      = RTC_GetSystemSecs();  //    Today
   pstMap->G_iSmartScaleMonth = 0;                    //    Auto scale Month plot
   pstMap->G_iSmartScaleDay   = 0;                    //    Auto scale Day plot
   //
   // Update G_pc YMD to reflect todays date
   //
   slim_UpdateYearMonthDay();
   //
   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
         break;

      case HTTP_HTML:
         // 
         // Put out the HTML header, start tag and the rest
         //         
         PRINTF("slim-DynPageData():HTML" CRLF);
         fCc = slim_DynBuildLiveSmartHtml(pstCl);
         break;

      case HTTP_JSON:
         PRINTF("slim-DynPageData():JSON" CRLF);
         GEN_STRCPY(pstMap->G_pcCommand, "Actual Data");
         fCc = slim_DynBuildLiveSmartJson(pstCl);
         break;
   }
   return(fCc);
}

//
// Function:   slim_DynPageDay
// Purpose:    Handle the dynamic display of Smart Sensor month results: Start at current G_ settings (YMD)
//             slimdag.html
//             slimdag.json
// Parms:
// Returns:    
// Note:       Page layout :
//
//             " ************************************"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                X                 |" 
//             " |                X                 |"
//             " |                XX                |"
//             " |                XXX               |"
//             " ************************************"
//             " 0   1   2   3   4   5    ..       24"
//
static bool slim_DynPageDay(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;

   //
   // Make sure G_ulSmartSecs reflects the G_pc YMD setting
   //
   slim_UpdateGlobalSecs();
   //
   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
         PRINTF("http_DynPageDay():not implemented yet" CRLF);
         break;

      case HTTP_HTML:
         PRINTF("http_DynPageDay()" CRLF);
         slim_RetrieveDisplayData(0, KINS_DAY);
         fCc = slim_DynBuildPageDayHtml(pstCl);
         break;

      case HTTP_JSON:
         GEN_STRCPY(pstMap->G_pcCommand, "smartday");
         PRINTF("http_DynPageDay():JSON not implemented yet" CRLF);
         fCc = slim_DynBuildPageDefaultJson(pstCl);
         break;
   }
   return(fCc);
}

//
// Function:   slim_DynPageDaySmart
// Purpose:    Handle the dynamic display of Smart Sensor day results: Start at current G_ settings (YMD)
//             slim
//             slim.html
//             slim.json
// Parms:      Network, index to dyn page
// Returns:    
// Note:       Used by the slim, slim.json and slim.html page
//             Page layout html:
//
//             " ************************************"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                X                 |" 
//             " |                X                 |"
//             " |                XX                |"
//             " |                XXX               |"
//             " ************************************"
//             " 0   1   2   3   4   5    ..       24"
//
static bool slim_DynPageDaySmart(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;

   //
   // Make sure G_ulSmartSecs reflects the G_pc YMD setting
   //
   slim_UpdateGlobalSecs();
   //
   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
         PRINTF("slim-DynPageDaySmart():not implemented yet" CRLF);
         break;

      case HTTP_HTML:
         PRINTF("slim-DynPageDaySmart()" CRLF);
         slim_RetrieveDisplayData(0, KINS_DAY);
         fCc = slim_DynBuildPageDayHtml(pstCl);
         break;

      case HTTP_JSON:
         PRINTF("slim-DynPageDaySmart()" CRLF);
         slim_RetrieveDisplayData(0, KINS_DAY);
         GEN_STRCPY(pstMap->G_pcCommand, "slim");
         fCc = slim_DynBuildPageDaySmartJson(pstCl);
         break;
   }
   return(fCc);
}

//
// Function:   slim_DynPageDaySmartNextHtml
// Purpose:    Handle the dynamic display of Smart Sensor Next Day results
//
// Parms:
// Returns:
// Note:       Used by the slim.html --> slimnext.html page
//
static bool slim_DynPageDaySmartNextHtml(NETCL *pstCl, int iIdx)
{
   u_int32  ulSecs;
   
   PRINTF("slim-DynPageDaySmartNextHtml()" CRLF);
   //
   // One day forward
   //
   ulSecs = pstMap->G_ulSmartSecs + (24*60*60);
   pstMap->G_ulSmartSecs = slim_RetrieveDisplayData(ulSecs, KINS_DAY);
   slim_UpdateYearMonthDay();
   //
   // Display day page
   //
   return(slim_DynBuildPageDaySmartHtml(pstCl));
}

//
// Function:   slim_DynPageDaySmartPrevHtml
// Purpose:    Handle the dynamic display of Smart Sensor Previous Day results
//
// Parms:
// Returns:
// Note:       Used by the slim.html --> slimprev.html page
//
static bool slim_DynPageDaySmartPrevHtml(NETCL *pstCl, int iIdx)
{
   u_int32  ulSecs;
   
   PRINTF("slim-DynPageDaySmartPrevHtml()" CRLF);
   ulSecs = pstMap->G_ulSmartSecs;
   //
   // One day back
   //
   ulSecs = pstMap->G_ulSmartSecs - (24*60*60);
   pstMap->G_ulSmartSecs = slim_RetrieveDisplayData(ulSecs, KINS_DAY);
   slim_UpdateYearMonthDay();
   //
   // Display day page
   //
   return(slim_DynBuildPageDaySmartHtml(pstCl));
}

//
// Function:   slim_DynPageDayNextHtml
// Purpose:    Handle the dynamic display of Smart Sensor Next Day results
//
// Parms:
// Returns:
// Note:
//
static bool slim_DynPageDayNextHtml(NETCL *pstCl, int iIdx)
{
   u_int32  ulSecs;
   
   PRINTF("slim-DynPageDayNextHtml()" CRLF);
   //
   // One day forward
   //
   ulSecs = pstMap->G_ulSmartSecs + (24*60*60);
   pstMap->G_ulSmartSecs = slim_RetrieveDisplayData(ulSecs, KINS_DAY);
   slim_UpdateYearMonthDay();
   //
   // Display day page
   //
   return(slim_DynBuildPageDayHtml(pstCl));
}

//
// Function:   slim_DynPageDayPrevHtml
// Purpose:    Handle the dynamic display of Smart Sensor Previous Day results
//
// Parms:
// Returns:
// Note:
//
static bool slim_DynPageDayPrevHtml(NETCL *pstCl, int iIdx)
{
   u_int32  ulSecs;
   
   PRINTF("slim-DynPageDayPrevHtml()" CRLF);
   ulSecs = pstMap->G_ulSmartSecs;
   //
   // One day back
   //
   ulSecs = pstMap->G_ulSmartSecs - (24*60*60);
   pstMap->G_ulSmartSecs = slim_RetrieveDisplayData(ulSecs, KINS_DAY);
   slim_UpdateYearMonthDay();
   //
   // Display day page
   //
   return(slim_DynBuildPageDayHtml(pstCl));
}

//
// Function:   slim_DynPageDayScaleUpHtml
// Purpose:    Handle the dynamic display of Smart Sensor scale
//
// Parms:
// Returns:
// Note:
//
static bool slim_DynPageDayScaleUpHtml(NETCL *pstCl, int iIdx)
{
   PRINTF("slim-DynPageDayScaleUpHtml()" CRLF);
   //
   // Display day page
   //
   if(pstMap->G_iSmartScaleDay < NUM_SCALES-1) pstMap->G_iSmartScaleDay++;
   return(slim_DynBuildPageDayHtml(pstCl));
}

//
// Function:   slim_DynPageDayScaleDnHtml
// Purpose:    Handle the dynamic display of Smart Sensor scale
//
// Parms:
// Returns:
// Note:
//
static bool slim_DynPageDayScaleDnHtml(NETCL *pstCl, int iIdx)
{
   PRINTF("slim-DynPageDayScaleDnHtml()" CRLF);
   //
   if(pstMap->G_iSmartScaleDay) pstMap->G_iSmartScaleDay--;
   return(slim_DynBuildPageDayHtml(pstCl));
}

//
// Function:   slim_DynPageMonth
// Purpose:    Handle the dynamic display of Smart Sensor month results
//             slimmaand.html
//             slimmaand.json
// Parms:
// Returns:
// Note:       Page layout :
//
//             " ************************************"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                X                 |" 
//             " |                X                 |"
//             " |                XX                |"
//             " |                XXX               |"
//             " ************************************"
//             "  1   5   10   15   20   25  ..    31"
//
static bool slim_DynPageMonth(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;

   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
         PRINTF("http_DynPageMonth():not implemented yet" CRLF);
         break;

      case HTTP_HTML:
         PRINTF("http_DynPageMonth()" CRLF);
         slim_RetrieveDisplayData(0, KINS_MONTH);
         fCc = slim_DynBuildPageMonthHtml(pstCl);
         break;

      case HTTP_JSON:
         GEN_STRCPY(pstMap->G_pcCommand, "smartmonth");
         PRINTF("http_DynPageMonth():JSON not implemented yet" CRLF);
         fCc = slim_DynBuildPageDefaultJson(pstCl);
         break;
   }
   return(fCc);
}

//
// Function:   slim_DynPageMonthNextHtml
// Purpose:    Handle the dynamic display of Smart Sensor Next Month results
//
// Parms:      Client, URL index
// Returns:
// Note:
//
static bool slim_DynPageMonthNextHtml(NETCL *pstCl, int iIdx)
{
   int      iDays;
   u_int32  ulSecs;
   
   PRINTF("slim-DynPageMonthNextHtml()" CRLF);
   ulSecs = pstMap->G_ulSmartSecs;
   //
   // One month forward
   //
   iDays = RTC_GetDaysPerMonth(ulSecs);
   PRINTF("slim-DynPageMonthNextHtml():This month has %d days" CRLF, iDays);
   //
   ulSecs += (iDays*24*60*60);
   pstMap->G_ulSmartSecs = slim_RetrieveDisplayData(ulSecs, KINS_MONTH);
   slim_UpdateYearMonthDay();
   //
   // Display month page
   //
   return(slim_DynBuildPageMonthHtml(pstCl));
}

//
// Function:   slim_DynPageMonthPrevHtml
// Purpose:    Handle the dynamic display of Smart Sensor Previous Month results
//
// Parms:
// Returns:
// Note:
//
static bool slim_DynPageMonthPrevHtml(NETCL *pstCl, int iIdx)
{
   int      iDays;
   u_int32  ulSecs;
   
   PRINTF("slim-DynPageMonthPrevHtml()" CRLF);
   ulSecs = pstMap->G_ulSmartSecs;
   //
   // One month back
   //
   iDays = RTC_GetDaysPerMonth(ulSecs);
   PRINTF("slim-DynPageMonthNextHtml():This month has %d days" CRLF, iDays);
   //
   ulSecs -= (iDays*24*60*60);
   pstMap->G_ulSmartSecs = slim_RetrieveDisplayData(ulSecs, KINS_MONTH);
   slim_UpdateYearMonthDay();
   //
   // Display day page
   //
   PRINTF("slim-DynPageMonthPrevHtml():Show month now..." CRLF);
   return(slim_DynBuildPageMonthHtml(pstCl));
}

//
// Function:   slim_DynPageMonthScaleUpHtml
// Purpose:    Handle the dynamic display of Smart Sensor scale
//
// Parms:
// Returns:
// Note:
//
static bool slim_DynPageMonthScaleUpHtml(NETCL *pstCl, int iIdx)
{
   
   PRINTF("slim-DynPageMonthScaleUpHtml()" CRLF);
   //
   // Display month page
   //
   if(pstMap->G_iSmartScaleMonth < NUM_SCALES-1) pstMap->G_iSmartScaleMonth++;
   return(slim_DynBuildPageMonthHtml(pstCl));
}

//
// Function:   slim_DynPageMonthScaleDnHtml
// Purpose:    Handle the dynamic display of Smart Sensor scale
//
// Parms:
// Returns:
// Note:
//
static bool slim_DynPageMonthScaleDnHtml(NETCL *pstCl, int iIdx)
{
   PRINTF("slim-DynPageMonthScaleDnHtml()" CRLF);
   //
   // Display month page
   //
   if(pstMap->G_iSmartScaleMonth) pstMap->G_iSmartScaleMonth--;
   return(slim_DynBuildPageMonthHtml(pstCl));
}

//
// Function:   slim_DynPageSlimExport
// Purpose:    Export the Smart-E live data to csv file
//             slimexport.html
// Parms:
// Returns:    
// Note:       Page layout :
//
static bool slim_DynPageSlimExport(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;

   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
         PRINTF("slim-DynPageSlimExport():not implemented yet" CRLF);
         break;

      case HTTP_HTML:
         PRINTF("slim-DynPageSlimExport()" CRLF);
         fCc = slim_ShowFilesHtml(pstCl, RPI_DL_DIR, pstMap->G_pcFilter, "Data files");
         break;

      case HTTP_JSON:
         GEN_STRCPY(pstMap->G_pcCommand, "export");
         PRINTF("slim-DynPageSlimExport():JSON not implemented yet" CRLF);
         fCc = slim_DynBuildPageDefaultJson(pstCl);
         break;
   }
   return(fCc);
}

//
// Function:   slim_DynPageMiacRom
// Purpose:    Memory Inspect and Change EEPROM
//             miac
// Parms:
// Returns:    
// Note:       Page layout :
//
static bool slim_DynPageMiacRom(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;

   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
      case HTTP_JSON:
         PRINTF("slim-DynPageMiacRom():Json" CRLF);
         GEN_STRCPY(pstMap->G_pcCommand, "miac-eeprom");
         fCc = RPI_BuildJsonMessageArgs(pstCl, PAR_MIA);
         break;

      case HTTP_HTML:
         PRINTF("slim-DynPageMiacRom()" CRLF);
         fCc = slim_DynBuildMiacRomHtml(pstCl);
         break;
   }
   return(fCc);
}

//
// Function:   slim_DynPageMiacRam
// Purpose:    Memory Inspect and Change RAM
//             miac
// Parms:
// Returns:    
// Note:       Page layout :
//
static bool slim_DynPageMiacRam(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;

   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
      case HTTP_JSON:
         PRINTF("slim-DynPageMiacRam():Json" CRLF);
         GEN_STRCPY(pstMap->G_pcCommand, "miac-ram");
         fCc = RPI_BuildJsonMessageArgs(pstCl, PAR_MIA);
         break;

      case HTTP_HTML:
         PRINTF("slim-DynPageMiacRam()" CRLF);
         fCc = slim_DynBuildMiacRamHtml(pstCl);
         break;
   }
   return(fCc);
}

// 
// Function:   slim_DynPageMiac
// Purpose:    Dynamically created parameters page
// 
// Parameters: Client, DynPage index
// Returns:    1 if OKee
// Note:       
//
static bool slim_DynPageMiac(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;

   PRINTF("slim-DynPageMiac()" CRLF);
   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
         PRINTF("slim-DynPageMiac():No type !" CRLF);
         break;

      case HTTP_HTML:
         PRINTF("slim-DynPageMiac():Html" CRLF);
         //
         // Put out the HTML header, start tag and the rest
         //
         HTTP_BuildGeneric(pstCl, pcHttpResponseHeader);
         //
         HTTP_BuildGeneric(pstCl, pcWebPageStart, pcWebPageTitleSlim);
         HTTP_BuildGeneric(pstCl, pcWebPageCenter);
         HTTP_BuildGeneric(pstCl, pcWebPageLineBreak);
         HTTP_BuildGeneric(pstCl, pcWebPageDefault);
         HTTP_BuildGeneric(pstCl, pcWebPageLineBreak);
         HTTP_BuildGeneric(pstCl, pcWebPageEnd);
         fCc = TRUE;
         break;

      case HTTP_JSON:
         PRINTF("slim-DynPageMiac():Json" CRLF);
         GEN_STRCPY(pstMap->G_pcCommand, "miac");
         fCc = RPI_BuildJsonMessageArgs(pstCl, PAR_MIA);
         break;
   }
   return(fCc);
}

// 
// Function:   slim_DynPageSensor
// Purpose:    Dynamically created parameters page
// 
// Parameters: Client, DynPage index
// Returns:    1 if OKee
// Note:       
//
static bool slim_DynPageSensor(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;

   PRINTF("slim-DynPageSensor()" CRLF);
   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
         PRINTF("slim-DynPageSensor():No type !" CRLF);
         break;

      case HTTP_HTML:
         PRINTF("slim-DynPageSensor():Html" CRLF);
         //
         // Put out the HTML header, start tag and the rest
         //
         HTTP_BuildGeneric(pstCl, pcHttpResponseHeader);
         //
         HTTP_BuildGeneric(pstCl, pcWebPageStart, pcWebPageTitleSlim);
         HTTP_BuildGeneric(pstCl, pcWebPageCenter);
         HTTP_BuildGeneric(pstCl, pcWebPageLineBreak);
         HTTP_BuildGeneric(pstCl, pcWebPageDefault);
         HTTP_BuildGeneric(pstCl, pcWebPageLineBreak);
         HTTP_BuildGeneric(pstCl, pcWebPageEnd);
         fCc = TRUE;
         break;

      case HTTP_JSON:
         PRINTF("slim-DynPageSensor():Json" CRLF);
         GEN_STRCPY(pstMap->G_pcCommand, "sensor");
         fCc = RPI_BuildJsonMessageArgs(pstCl, PAR_PRM);
         break;
   }
   pstMap->G_stCmd.iCommand = SMCMD_HTTP_SENSOR;
   GLOBAL_Signal(PID_COMM, SIGUSR1);
   return(fCc);
}



/*------  Local functions separator ------------------------------------
__HTML_BUILD_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//
// Function:   slim_DynBuildPageDayHtml
// Purpose:    Build the dynamic display of Smart Sensor day
//
// Parms:
// Returns:    
// Note:       Page layout :
//
//             " ************************************"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                X                 |" 
//             " |                X                 |"
//             " |                XX                |"
//             " |                XXX               |"
//             " ************************************"
//             " 0   1   2   3   4   5    ..       24"
//
static bool slim_DynBuildPageDayHtml(NETCL *pstCl)
{
   int      tEms, iLen;
   char    *pcLink, *pcDate;
   RPIDATA *pstObj=HTML_InsertData(NULL, NULL, 0);

   PRINTF("slim-DynBuildPageDayHtml()" CRLF);
   //
   HTTP_BuildGeneric(pstCl, pcHttpResponseHeader);
   HTTP_BuildGeneric(pstCl, pcWebPageStart, pcWebPageTitleDay);
   HTTP_BuildLineBreaks(pstCl, 2);
   //
   // Show the date/time of this graph
   //
   iLen     = RTC_ConvertDateTimeSize(TIME_FORMAT_WW_DD_MMM_YYYY) + 32;
   pcDate   = safemalloc(iLen);
   //
   RTC_ConvertDateTime(TIME_FORMAT_WW_DD_MMM_YYYY, pcDate, pstMap->G_ulSmartSecs);
   //
   HTTP_BuildGeneric(pstCl, pcWebPageCenter);
   HTTP_BuildGeneric(pstCl, pcWebPageHdr, pcDate);
   HTTP_BuildLineBreaks(pstCl, 1);
   //
   HTTP_BuildGeneric(pstCl, pcWebPageFontStart, pcCourierNew);
   //
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_DAYUP)))  HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkScaleUp);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_DAYDN)))  HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkScaleDn);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_DAYPRV))) HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkPrevDay);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_DAYNXT))) HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkNextDay);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_MON)))    HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkMonth);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_LIVE)))   HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkData);
   //
   HTTP_BuildLineBreaks(pstCl, 3);
   //
   for(tEms=0; tEms<NUM_EMS; tEms++)
   {
      pstObj = slim_BuildSmartDay(pstCl, pstObj, tEms, pstMap->G_stDispDay);
   }
   HTTP_BuildLineBreaks(pstCl, 3);
   //
   HTTP_BuildGeneric(pstCl, pcWebPageFontEnd);
   HTTP_BuildEnd(pstCl);
   HTML_ReleaseObject(pstObj);
   safefree(pcDate);
   return(TRUE);
}

//
// Function:   slim_DynBuildPageDaySmartHtml
// Purpose:    Build the dynamic display of Smart Sensor day
//
// Parms:
// Returns:    
// Note:       Page layout :
//
//             " ************************************"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                X                 |" 
//             " |                X                 |"
//             " |                XX                |"
//             " |                XXX               |"
//             " ************************************"
//             " 0   1   2   3   4   5    ..       24"
//
static bool slim_DynBuildPageDaySmartHtml(NETCL *pstCl)
{
   int      iLen;
   char    *pcLink, *pcDate;
   RPIDATA *pstObj=HTML_InsertData(NULL, NULL, 0);

   PRINTF("slim-DynBuildPageDaySmartHtml()" CRLF);
   //
   HTTP_BuildGeneric(pstCl, pcHttpResponseHeader);
   HTTP_BuildGeneric(pstCl, pcWebPageStart, pcWebPageTitleDay);
   HTTP_BuildLineBreaks(pstCl, 2);
   //
   // Show the date/time of this graph
   //
   iLen   = RTC_ConvertDateTimeSize(TIME_FORMAT_WW_DD_MMM_YYYY) + 32;
   pcDate = safemalloc(iLen);
   //
   RTC_ConvertDateTime(TIME_FORMAT_WW_DD_MMM_YYYY, pcDate, pstMap->G_ulSmartSecs);
   //
   HTTP_BuildGeneric(pstCl, pcWebPageCenter);
   HTTP_BuildGeneric(pstCl, pcWebPageHdr, pcDate);
   HTTP_BuildLineBreaks(pstCl, 1);
   //
   HTTP_BuildGeneric(pstCl, pcWebPageFontStart, pcCourierNew);
   //
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_PRVSLM))) HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkPrevDay);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_NXTSLM))) HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkNextDay);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_MON)))    HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkMonth);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_LIVE)))   HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkData);
   //
   HTTP_BuildLineBreaks(pstCl, 3);
   //
   pstObj  = slim_BuildSmartDay(pstCl, pstObj, EMS_DIS, pstMap->G_stDispDay);
   //
   HTTP_BuildLineBreaks(pstCl, 3);
   HTTP_BuildGeneric(pstCl, pcWebPageFontEnd);
   HTTP_BuildEnd(pstCl);
   HTML_ReleaseObject(pstObj);
   safefree(pcDate);
   return(TRUE);
}

//
// Function:   slim_DynBuildLiveSmartHtml
// Purpose:    Build the live data to be send back through the socket
//
// Parms:      Socket descriptor
// Returns:    TRUE if OKee
//
static bool slim_DynBuildLiveSmartHtml(NETCL *pstCl)
{
   bool     fCc;
   char    *pcLink;
   // 
   // Put out the HTML header, start tag and the rest
   //         
   HTTP_BuildGeneric(pstCl, pcHttpResponseHeader);

#ifdef FEATURE_AUTO_REFRESH_METER
   HTTP_BuildGeneric(pstCl, pcWebPageStartRefresh, pcWebPageTitleSlim, pcWebRefreshSlim);
#else //FEATURE_AUTO_REFRESH_METER
   HTTP_BuildGeneric(pstCl, pcWebPageStart, pcWebPageTitleSlim);
#endif   //FEATURE_AUTO_REFRESH_METER

   HTTP_BuildGeneric(pstCl, pcWebPageCenter);
   HTTP_BuildLineBreaks(pstCl, 2);
   //
   HTTP_BuildGeneric(pstCl, pcWebPageFontStart, pcCourierNew);
   //
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_DAY)))    HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkDetails);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_MON)))    HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkMonth);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_DAYSLM))) HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkTotals);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_EXPORT))) HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkExport);
   //
   HTTP_BuildGeneric(pstCl, pcWebPageFontEnd);
   //
   HTTP_BuildLineBreaks(pstCl, 3);
   //
   // Table 1: Waterlevel settings (line width, width, Nr columns)
   //
   HTTP_BuildTableStart(pstCl, "Smart Sensor Settings", 1, 50, 3);
   //
   HTTP_BuildTableRowStart(pstCl, 20);
   HTTP_BuildTableColumnText(pstCl, "Meter",   10);
   HTTP_BuildTableColumnText(pstCl, "Stand",   15);
   HTTP_BuildTableColumnText(pstCl, "Eenheid", 25);
   HTTP_BuildTableRowEnd(pstCl);
   //
   slim_BuildRowHtmlText( pstCl, "Versie",                    pstMap->G_pcSwVersion,     "S/W RPi");
   slim_BuildRowHtmlText( pstCl, "Sensor",                    pstMap->G_pcEquipment,     "S/W Sensor");
   slim_BuildRowHtmlText( pstCl, "Temperatuur",               pstMap->G_pcTemperature,   "C");           // EMS_TMP
   slim_BuildRowHtmlText( pstCl, "Sample timestamp",          pstMap->G_pcTimeInSecs,    "HMS");
   slim_BuildRowHtmlText( pstCl, "Samples genegeerd",         pstMap->G_pcDropped,       "Onderbroken meting");
   slim_BuildRowHtmlText( pstCl, "Pomp Mode",                 pstMap->G_pcPumpMode,      "00=Auto;01=Aan;05=Uit;15=Tmr;55=RPi-Uit");
   slim_BuildRowHtmlText( pstCl, "Systeem Status",            pstMap->G_pcSysFlags,      "Flags");
   slim_BuildRowHtmlFloat(pstCl, "S  (Sensor-Bodem)",         pstMap->G_flSensorBottom,  "mm");
   slim_BuildRowHtmlFloat(pstCl, "Dm (Sensor-Vlotter)",       pstMap->G_flDm,            "mm");
   slim_BuildRowHtmlFloat(pstCl, "F  (Vlotter Hoogte)",       pstMap->G_flFloater,       "mm");
   HTTP_BuildTableEnd(pstCl);
   //
   HTTP_BuildLineBreaks(pstCl, 2);
   //
   // Table 2: Waterlevel actuals (line width, width, Nr columns)
   //
   HTTP_BuildTableStart(pstCl, "Smart Sensor Values", 1, 50, 3);
   //
   HTTP_BuildTableRowStart(pstCl, 20);
   HTTP_BuildTableColumnText(pstCl, "Meter",   10);
   HTTP_BuildTableColumnText(pstCl, "Stand",   15);
   HTTP_BuildTableColumnText(pstCl, "Eenheid", 25);
   HTTP_BuildTableRowEnd(pstCl);
   //
   slim_BuildRowHtmlFloat(pstCl, "Water Hi-Level",            pstMap->G_flHiWater,       "mm");
   slim_BuildRowHtmlText( pstCl, "Water Actueel",             pstMap->G_pcWaterLevel,    "mm");          // EMS_DIS
   slim_BuildRowHtmlFloat(pstCl, "Water Lo-Level",            pstMap->G_flLoWater,       "mm");
   slim_BuildRowHtmlText( pstCl, "Pomp Status",               pstMap->G_pcPumpState,     "Aan/Uit");
   slim_BuildRowHtmlText( pstCl, "Pomp Aan %",                pstMap->G_pcPumpOn,        "% per dag");   // EMS_PMP
   HTTP_BuildTableEnd(pstCl);
   //
   fCc = HTTP_BuildGeneric(pstCl, pcWebPageEnd);
   return(fCc);
}

//
// Function:   slim_DynBuildPageMonthHtml
// Purpose:    Build the dynamic display of Smart Sensor month results
//
// Parms:
// Returns:
// Note:       Page layout :
//
//             " ************************************"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                                  |"
//             " |                X                 |" 
//             " |                X                 |"
//             " |                XX                |"
//             " |                XXX               |"
//             " ************************************"
//             "  1   5   10   15   20   25  ..    31"
//
static bool slim_DynBuildPageMonthHtml(NETCL *pstCl)
{
   int      tEms, iFs;
   double  *pflVar;
   char    *pcBuffer, *pcLink, *pcTemp;
   RPIDATA *pstObj=HTML_InsertData(NULL, NULL, 0);

   PRINTF("slim-DynBuildPageMonthHtml()" CRLF);
   pcBuffer = safemalloc(MAX_DAYSZ * MAX_LINES);
   pflVar   = safemalloc(MAX_DAYS  * sizeof(double));
   //
   HTTP_BuildGeneric(pstCl, pcHttpResponseHeader);
   HTTP_BuildGeneric(pstCl, pcWebPageStart, pcWebPageTitleMonth);
   HTTP_BuildLineBreaks(pstCl, 2);
   //
   // Show the date/time of this graph
   //
   pcTemp = safemalloc(256);
   //
   RTC_ConvertDateTime(TIME_FORMAT_AMM_YYYY, pcTemp, pstMap->G_ulSmartSecs);
   //
   HTTP_BuildGeneric(pstCl, pcWebPageCenter);
   HTTP_BuildGeneric(pstCl, pcWebPageHdr, pcTemp);
   HTTP_BuildLineBreaks(pstCl, 1);
   //
   HTTP_BuildGeneric(pstCl, pcWebPageFontStart, pcCourierNew);
   //
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_MONUP)))  HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkScaleUp);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_MONDN)))  HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkScaleDn);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_PRVMON))) HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkPrevMonth);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_NXTMON))) HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkNextMonth);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_DAY)))    HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkDetails);
   if((pcLink = GEN_FindDynamicPageName(DYN_SME_HTML_LIVE)))   HTTP_BuildGeneric(pstCl, pcWebPageLink, pcLink, pcLinkData);
   //
   HTTP_BuildLineBreaks(pstCl, 3);
   //
   for(tEms=0; tEms<NUM_EMS; tEms++)
   {
      //
      // Copy periodic vars from data struct
      //
      slim_GetPeriodicData(tEms, pstMap->G_stDispMonth, pflVar, MAX_DAYS); 
      iFs    = pstMap->G_iSmartScaleMonth;
      if(iFs >= NUM_SCALES)
      {
         PRINTF("slim-DynBuildPageMonthHtml():Scale index %d too large (%s)" CRLF, iFs, pcSmartHtmlStuff[tEms]);
         iFs = pstMap->G_iSmartScaleMonth = 0;
      }
      //
      // HTTP_BuildGeneric() is NOT good at formatted float (or double) conversions ! It will do basic formatting
      // ("%f"  -->%f) and ("%F" -->%5.3f) on double precision floating point numbers only.
      //
      //    So, i.o. HTTP_BuildGeneric(..., "%5.1f", flXxxx) convert float to string and use
      //             HTTP_BuildGeneric(..., "%s", pcXxxx) 
      //
      // Example pcTemp:
      // "Totaal: 14.0 kWh    (Auto)VS=500.00 Wh T1"
      //    or
      // "Totaal: 14.0 kWh    (Auto)500.00 Wh/sd T1"
      //
      if(iFs) 
      {
         GEN_SNPRINTF(pcTemp, 255, "&nbsp;&nbsp;&nbsp;&nbsp; %5.2f %s",
                        flScales[iFs]/MAX_LINES, pcSmartHtmlStuff[tEms]);
      }
      else    
      {
         iFs = slim_AutoScaleSmartData(pflVar, MAX_DAYS, iFs);
         GEN_SNPRINTF(pcTemp, 255, "&nbsp;&nbsp;&nbsp;&nbsp; (Auto)%5.2f %s",
                        flScales[iFs]/MAX_LINES, pcSmartHtmlStuff[tEms]);
      }
      //
      HTTP_BuildGeneric(pstCl, "%s", pcTemp);
      //
      pstObj = HTML_InsertData(pstObj, (char *)pcSmartLegendDay1, HE_SPACE4|HE_BR);
      pstObj = slim_BuildSmartData(pstObj, pcBuffer, MAX_DAYSZ, MAX_LINES, pflVar, iFs);
      //
      pstObj = HTML_InsertData(pstObj, (char *)pcSmartLegendDay1, HE_SPACE4|HE_BR);
      pstObj = HTML_InsertData(pstObj, (char *)pcSmartLegendDay2, HE_SPACE4|HE_BR);
      pstObj = HTML_InsertData(pstObj, (char *)pcSmartLegendDay3, HE_SPACE4|HE_BR);
      //
      // Put out HTML graph
      //
      HTTP_BuildLineBreaks(pstCl, 1);
      HTML_RespondObject(pstCl, pstObj);
      HTTP_BuildLineBreaks(pstCl, 3);
   }
   //
   HTTP_BuildGeneric(pstCl, pcWebPageFontEnd);
   HTTP_BuildEnd(pstCl);
   HTML_ReleaseObject(pstObj);
   safefree(pflVar);
   safefree(pcTemp);
   safefree(pcBuffer);
   return(TRUE);
}

/*------  Local functions separator ------------------------------------
__JSON_BUILD_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//
// Function:   slim_DynBuildPageDaySmartJson
// Purpose:    Build the JSON response object to be send back through the socket
//
// Parms:      Socket descriptor
// Returns:    TRUE if OKee
// Note:       The slim.json page is used by the SmartE Android App to pass info
//             to the user for 3 menu entries:
//             - USN       : Ultrasoon Sensor
//             - ON + OFF  : % Pump On and Off
//
static bool slim_DynBuildPageDaySmartJson(NETCL *pstCl)
{
   bool        fCc;
   RPIDATA    *pstObj=JSON_InsertParameter(NULL, NULL, NULL, 0);

   PRINTF("slim-DynBuildPageDaySmartJson()" CRLF);
   //
   //
   // Build JSON object
   //
   pstObj = JSON_InsertParameter(pstObj, NULL,                       NULL,                         JE_CURLB|JE_CRLF);
   pstObj = JSON_InsertParameter(pstObj, "Command",                  pstMap->G_pcCommand,          JE_TEXT|JE_COMMA|JE_CRLF);
   pstObj = JSON_InsertParameter(pstObj, "Time",                     pstMap->G_pcTimeInSecs,       JE_TEXT|JE_COMMA|JE_CRLF);   
   pstObj = JSON_InsertParameter(pstObj, "Sensor",                   pstMap->G_pcEquipment,        JE_TEXT|JE_COMMA|JE_CRLF);   
   pstObj = JSON_InsertParameter(pstObj, "Version",                  pstMap->G_pcSwVersion,        JE_TEXT|JE_COMMA|JE_CRLF);
   pstObj = JSON_InsertParameter(pstObj, "Year",                     pstMap->G_pcYear,             JE_TEXT|JE_COMMA|JE_CRLF);
   pstObj = JSON_InsertParameter(pstObj, "Month",                    pstMap->G_pcMonth,            JE_TEXT|JE_COMMA|JE_CRLF);
   pstObj = JSON_InsertParameter(pstObj, "Day",                      pstMap->G_pcDay,              JE_TEXT|JE_COMMA|JE_CRLF);
   //
   pstObj = JSON_InsertParameter(pstObj, pcEmsTotals[EMS_DIS],       pstMap->G_pcWaterLevel,       JE_TEXT|JE_COMMA|JE_CRLF);
   pstObj = JSON_InsertParameter(pstObj, pcEmsTotals[EMS_PMP],       pstMap->G_pcPumpOn,           JE_TEXT|JE_COMMA|JE_CRLF);
   //
   // Select Auto scale mode (0)
   //
   pstObj = slim_BuildJsonArray(pstCl, pstObj, EMS_DIS, pstMap->G_stDispDay, 0);
   pstObj = slim_BuildJsonArray(pstCl, pstObj, EMS_PMP, pstMap->G_stDispDay, 0);
   //
   pstObj  = JSON_TerminateObject(pstObj);
   fCc     = JSON_RespondObject(pstCl, pstObj);
   //
   // Done with this function: exit
   //
   JSON_ReleaseObject(pstObj);
   return(fCc);
}

//
// Function:   slim_DynBuildLiveSmartJson
// Purpose:    Build the live data JSON response object to be send back through the socket
//
// Parms:      Socket descriptor
// Returns:    TRUE if OKee
//
static bool slim_DynBuildLiveSmartJson(NETCL *pstCl)
{
   bool        fCc;
   RPIDATA    *pstObj=NULL;

   PRINTF("slim-slim_DynBuildLiveSmartJson()" CRLF);
   //
   // Build JSON object
   //
   pstObj = JSON_InsertParameter(pstObj, NULL,                       NULL,                         JE_CURLB|JE_CRLF);
   pstObj = JSON_InsertParameter(pstObj, "Command",                  pstMap->G_pcCommand,          JE_TEXT|JE_COMMA|JE_CRLF);
   pstObj = JSON_InsertParameter(pstObj, "Time",                     pstMap->G_pcTimeInSecs,       JE_TEXT|JE_COMMA|JE_CRLF);   
   pstObj = JSON_InsertParameter(pstObj, "Sensor",                   pstMap->G_pcEquipment,        JE_TEXT|JE_COMMA|JE_CRLF);   
   pstObj = JSON_InsertParameter(pstObj, "Version",                  pstMap->G_pcSwVersion,        JE_TEXT|JE_COMMA|JE_CRLF);
   pstObj = JSON_InsertParameter(pstObj, "Samples",                  pstMap->G_pcSmartSamples,     JE_TEXT|JE_COMMA|JE_CRLF);
   //
   pstObj = JSON_InsertParameter(pstObj, pcEmsTotals[EMS_DIS],       pstMap->G_pcWaterLevel,       JE_TEXT|JE_COMMA|JE_CRLF);
   pstObj = JSON_InsertParameter(pstObj, pcEmsTotals[EMS_TMP],       pstMap->G_pcTemperature,      JE_TEXT|JE_COMMA|JE_CRLF);
   pstObj = JSON_InsertParameter(pstObj, pcEmsTotals[EMS_PMP],       pstMap->G_pcPumpOn,           JE_TEXT|JE_COMMA|JE_CRLF);
   pstObj = JSON_InsertParameter(pstObj, pcEmsTotals[EMS_X10],       pstMap->G_pcX10Bypass,        JE_TEXT|JE_COMMA|JE_CRLF);
   //
   pstObj = JSON_TerminateObject(pstObj);
   fCc    = JSON_RespondObject(pstCl, pstObj);
   JSON_ReleaseObject(pstObj);
   return(fCc);
}


//
// Function:   slim_DynBuildPageDefaultJson
// Purpose:    Build the default JSON response object to be send back through the socket
//
// Parms:      Socket descriptor
// Returns:    TRUE if OKee
//
static bool slim_DynBuildPageDefaultJson(NETCL *pstCl)
{
   bool     fCc;
   RPIDATA *pstObj=JSON_InsertParameter(NULL, NULL, NULL, 0);

   PRINTF("slim-slim_DynBuildPageDefaultJson()" CRLF);
   //
   // Build default JSON object
   //
   pstObj = JSON_InsertParameter(pstObj, NULL,        NULL,                         JE_CURLB|JE_CRLF);
   pstObj = JSON_InsertParameter(pstObj, "Command",   pstMap->G_pcCommand,          JE_TEXT|JE_COMMA|JE_CRLF);
   pstObj = JSON_InsertParameter(pstObj, "Time",      pstMap->G_pcTimeInSecs,       JE_TEXT|JE_COMMA|JE_CRLF);   
   pstObj = JSON_InsertParameter(pstObj, "Sensor",    pstMap->G_pcEquipment,        JE_TEXT|JE_COMMA|JE_CRLF);   
   pstObj = JSON_InsertParameter(pstObj, "Version",   pstMap->G_pcSwVersion,        JE_TEXT|JE_COMMA|JE_CRLF);
   pstObj = JSON_InsertParameter(pstObj, "Notice",    "Not Implemented",            JE_TEXT|JE_COMMA|JE_CRLF);
   //
   pstObj = JSON_TerminateObject(pstObj);
   fCc    = JSON_RespondObject(pstCl, pstObj);
   JSON_ReleaseObject(pstObj);
   return(fCc);
}

//
// Function:   slim_DynBuildMiacRomHtml
// Purpose:    Build the MIAC EEPROM data to be send back through the socket
//             8 bytes/line makes (KINS_ROM_LEN / 8) lines
// Parms:      Socket descriptor
// Returns:    TRUE if OKee
//
static bool slim_DynBuildMiacRomHtml(NETCL *pstCl)
{
   return(slim_BuildMiac(pstCl, MIAC_MODE_ROM));
}

//
// Function:   slim_DynBuildMiacRamHtml
// Purpose:    Build the MIAC RAM data to be send back through the socket
//             8 bytes/line makes (KINS_RAM_LEN / 8) lines
// Parms:      Socket descriptor
// Returns:    TRUE if OKee
//
static bool slim_DynBuildMiacRamHtml(NETCL *pstCl)
{
   return(slim_BuildMiac(pstCl, MIAC_MODE_RAM));
}



/*------  Local functions separator ------------------------------------
__LOCAL_FUNCTIONS(){};
----------------------------------------------------------------------------*/

// 
// Function:   slim_AutoScaleSmartData
// Purpose:    Auto scale this plot, if requiered
// 
// Parameters: Values ptr, Number of values, ScaleIdx
// Returns:    Scale
// Note:       
//
static int slim_AutoScaleSmartData(double *pflVal, int iNr, int iFs)
{
   bool     fOvfl=TRUE;
   int      x;

   //
   // If AUTO scale: find best scale
   //
   if(iFs == 0)
   {
      iFs = NUM_SCALES;
      while(iFs && fOvfl)
      {
         iFs--;
         //
         // Check if any sample overflows the graph, if so try next scale
         //
         fOvfl = FALSE;
         for(x=0; x<iNr; x++)
         {
            if(pflVal[x] > flScales[iFs]) 
            {
               fOvfl = TRUE;
               break;
            }
         }
      }
   }
   return(iFs);
}

//
// Function:   slim_BuildRowHtmlFloat
// Purpose:    Build a singe row with double floating point
// 
// Parameters: Client, Text 1, Float, Text2
// Returns:    
// Note:       
//
static void slim_BuildRowHtmlFloat(NETCL *pstCl, char *pcText1, double flValue, char *pcText2)
{
   HTTP_BuildTableRowStart(pstCl, 10);
   HTTP_BuildTableColumnText(pstCl, pcText1, 10);
   HTTP_BuildTableColumnFloat(pstCl, flValue, 10);
   HTTP_BuildTableColumnText(pstCl, pcText2, 10);
   HTTP_BuildTableRowEnd(pstCl);
}

//
// Function:   slim_BuildRowHtmlMiac
// Purpose:    Build a singe MIAC row with data
// 
// Parameters: Client, Mode, Addr
// Returns:    
// Note:       
//
static void slim_BuildRowHtmlMiac(NETCL *pstCl, int iMode, int iAddr)
{
   int      i, iCurAddr;
   char     cAddr[8];
   char     cData[16];
   u_int8  *pubData;

   switch(iMode)
   {
      default:
      case MIAC_MODE_ROM:
         pubData  = &(pstMap->G_ubRomImage[iAddr]);
         iCurAddr = pstMap->G_iRomAddr;
         break;

      case MIAC_MODE_RAM:
         pubData  = &(pstMap->G_ubRamImage[iAddr]);
         iCurAddr = pstMap->G_iRamAddr;
         break;
   }
   HTTP_BuildTableRowStart(pstCl, 10);
   GEN_SNPRINTF(cAddr, 7, "0x%03x", iAddr);
   HTTP_BuildTableColumnText(pstCl, cAddr, 10);
   for(i=0; i<8; i++)
   {
      if(iAddr == iCurAddr) GEN_SNPRINTF(cData, 15, "<b>0x%02x</b>", pubData[i]);
      else                  GEN_SNPRINTF(cData, 15, "0x%02x",        pubData[i]); 
      HTTP_BuildTableColumnText(pstCl, cData, 5);
      iAddr++;
   }
   HTTP_BuildTableRowEnd(pstCl);
}

//
// Function:   slim_BuildRowHtmlText
// Purpose:    Build a singe row with text
// 
// Parameters: Client, text 1, text2, text3
// Returns:    
// Note:       
//
static void slim_BuildRowHtmlText(NETCL *pstCl, char *pcText1, char *pcText2, char *pcText3)
{
   HTTP_BuildTableRowStart(pstCl, 10);
   HTTP_BuildTableColumnText(pstCl, pcText1, 10);
   HTTP_BuildTableColumnText(pstCl, pcText2, 10);
   HTTP_BuildTableColumnText(pstCl, pcText3, 10);
   HTTP_BuildTableRowEnd(pstCl);
}

// 
// Function:   slim_BuildSmartDay
// Purpose:    Build HTML or JSON data for a single day
// 
// Parameters: NetCl, Object, Data struct, EMS type
// Returns:    Object
// Note:       Object->tType knows HTML or JSON
//
static RPIDATA *slim_BuildSmartDay(NETCL *pstCl, RPIDATA *pstObj, EMS tEms, SMDATA *pstDay)
{
   int      iFs;
   char    *pcBuffer, *pcTmp1, *pcTmp2;
   double  *pflVar;

   pcTmp1   = safemalloc(256);
   pcTmp2   = safemalloc(256);
   pcBuffer = safemalloc(MAX_QRTSZ * MAX_LINES);
   pflVar   = safemalloc(MAX_QRTS  * sizeof(double));
   //
   // Copy periodic vars from data struct
   //
   slim_GetPeriodicData(tEms, pstDay, pflVar, MAX_QRTS); 
   //
   iFs = pstMap->G_iSmartScaleDay;
   if(iFs >= NUM_SCALES)
   {
      PRINTF("slim-BuildSmartDay():Scale index %d too large (%s)" CRLF, iFs, pcSmartHtmlStuff[tEms]);
      iFs = pstMap->G_iSmartScaleDay = 0;
   }
   PRINTF("slim-BuildSmartDay():Scale[%d]=%5.1f %s" CRLF, iFs, flScales[iFs], pcSmartHtmlStuff[tEms]);
   //
   // HTTP_BuildGeneric() is NOT good at formatted float (or double) conversions ! It will do basic formatting
   // ("%f"  -->%f) and ("%F" -->%5.3f) on double precision floating point numbers only.
   //
   //    So, i.o. HTTP_BuildGeneric(..., "%5.1f", flXxxx) convert float to string ansd use
   //             HTTP_BuildGeneric(..., "%s", pcXxxx) 
   //
   if(iFs) 
   {
      GEN_SNPRINTF(pcTmp1, 255, "&nbsp;&nbsp;&nbsp;&nbsp; %5.2f %s",
                     flScales[iFs]/MAX_LINES, pcSmartHtmlStuff[tEms]);
   }
   else    
   {
      iFs = slim_AutoScaleSmartData(pflVar, MAX_QRTS, iFs);
      GEN_SNPRINTF(pcTmp1, 255, "&nbsp;&nbsp;&nbsp;&nbsp; (Auto)%5.2f %s",
                     flScales[iFs]/MAX_LINES, pcSmartHtmlStuff[tEms]);
   }
   switch(pstObj->tType)
   {
      default:
      case HTTP_HTML:
         HTTP_BuildGeneric(pstCl, "%s", pcTmp1);
         //
         pstObj = HTML_InsertData(pstObj, (char *)pcSmartLegendQtr,  HE_SPACE4|HE_BR);
         pstObj = slim_BuildSmartData(pstObj, pcBuffer, MAX_QRTSZ,   MAX_LINES, pflVar, iFs);
         pstObj = HTML_InsertData(pstObj, (char *)pcSmartLegendQtr,  HE_SPACE4|HE_BR);
         pstObj = HTML_InsertData(pstObj, (char *)pcSmartLegendHrs1, HE_SPACE4|HE_BR);
         pstObj = HTML_InsertData(pstObj, (char *)pcSmartLegendHrs2, HE_SPACE4|HE_BR);
         //
         // Put out HTML graph
         //
         HTTP_BuildLineBreaks(pstCl, 1);
         HTML_RespondObject(pstCl, pstObj);
         HTTP_BuildLineBreaks(pstCl, 3);
         break;

      case HTTP_JSON:
         GEN_SNPRINTF(pcTmp1, 255, "Scale %s %s", pcSmartJsonField[tEms], pcSmartJsonScale[tEms]);
         GEN_SNPRINTF(pcTmp2, 255, "%5.1f",       flScales[iFs]/MAX_LINES);
         //
         pstObj = JSON_InsertParameter(pstObj, pcTmp1,                  pcTmp2,        JE_TEXT|JE_COMMA|JE_CRLF);
         pstObj = JSON_InsertParameter(pstObj, pcSmartJsonField[tEms],  NULL,          JE_SQRB|JE_CRLF);
         //
         pstObj = slim_BuildSmartData(pstObj, pcBuffer, MAX_QRTSZ, MAX_LINES, pflVar, iFs);
         break;
   }
   safefree(pflVar);
   safefree(pcBuffer);
   safefree(pcTmp2);
   safefree(pcTmp1);
   return(pstObj);
}

//
// Function:   slim_BuildJsonArray
// Purpose:    Build the JSON array with detailed JSON objects
//
// Parms:      Net client, JSON Object, Ems, Data, ScaleIdx
// Returns:    New JSON Object
// Note:       
//
static RPIDATA *slim_BuildJsonArray(NETCL *pstCl, RPIDATA *pstObj, EMS tEms, SMDATA *pstDay, int iFs)
{
   pstObj  = slim_BuildJsonDetailedDay(pstCl, pstObj, tEms, pstDay, iFs);
   pstObj  = JSON_TerminateArray(pstObj);
   return(pstObj);
}

// 
// Function:   slim_BuildJsonDetailedDay
// Purpose:    Build JSON data for a single day
// 
// Parameters: NetCl, Object, EMS type, Data, ScaleIdx
// Returns:    Object
// Note:       
//
static RPIDATA *slim_BuildJsonDetailedDay(NETCL *pstCl, RPIDATA *pstObj, EMS tEms, SMDATA *pstDay, int iFs)
{
   int      i;
   char    *pcFlVal;
   char    *pcFlTxt;
   double  *pflVar;
   
   pcFlTxt = safemalloc(64);
   pcFlVal = safemalloc(64);
   pflVar  = safemalloc(MAX_QRTS  * sizeof(double));
   //
   // Copy periodic vars from data struct
   //
   slim_GetPeriodicData(tEms, pstDay, pflVar, MAX_QRTS); 
   //
   // If Auto scale: pick the correct one, else leave it alone
   //
   if(iFs == 0)
   {
      iFs = slim_AutoScaleSmartData(pflVar, MAX_QRTS, 0);
   }
   GEN_SNPRINTF(pcFlTxt, 63, "%sFullScale", pcSmartJsonField[tEms]);
   GEN_SNPRINTF(pcFlVal, 63, "%5.3f",       flScales[iFs]);
   pstObj = JSON_InsertParameter(pstObj, pcFlTxt, pcFlVal, JE_TEXT|JE_COMMA|JE_CRLF);
   pstObj = JSON_InsertParameter(pstObj, pcSmartJsonField[tEms], NULL, JE_SQRB|JE_CRLF);
   //
   // Insert the array with all the floating values of the quarters in this day
   //
   for(i=0; i<MAX_QRTS; i++)
   {
      GEN_SNPRINTF(pcFlVal, 63, "%5.3f", pflVar[i]);
      pstObj = JSON_InsertParameter(pstObj, NULL, pcFlVal, JE_TEXT|JE_COMMA|JE_CRLF);
   }
   safefree(pflVar);
   safefree(pcFlVal);
   safefree(pcFlTxt);
   return(pstObj);
}

//
// Function:   slim_BuildMiac
// Purpose:    Build the MIAC RAM data to be send back through the socket
//             8 bytes/line makes (KINS_RAM_LEN / 8) lines
// Parms:      Socket descriptor, mode
// Returns:    TRUE if OKee
//
static bool slim_BuildMiac(NETCL *pstCl, int iMode)
{
   bool        fCc;
   int         i, iSize, iAddr=0;
   const char *pcMiac;

   switch(iMode)
   {
      default:
      case MIAC_MODE_ROM:
         iSize   = KINS_ROM_LEN;
         pcMiac  = pcMiacRom;
         break;

      case MIAC_MODE_RAM:
         iSize = KINS_RAM_LEN;
         pcMiac  = pcMiacRam;
         break;
   }
   // 
   // Put out the HTML header, start tag and the rest
   //         
   HTTP_BuildGeneric(pstCl, pcHttpResponseHeader);
   HTTP_BuildGeneric(pstCl, pcWebPageCenter);
   HTTP_BuildLineBreaks(pstCl, 2);
   //
   HTTP_BuildGeneric(pstCl, pcWebPageFontStart, pcCourierNew);
   HTTP_BuildLineBreaks(pstCl, 1);
   //
   // Table: line width, width, Nr columns
   //
   HTTP_BuildTableStart(pstCl, pcMiac, 1, 50, 9);
   //
   HTTP_BuildTableRowStart(pstCl, 20);
   HTTP_BuildTableColumnText(pstCl, "Addr",    10);
   HTTP_BuildTableColumnText(pstCl, " 0 ",      5);
   HTTP_BuildTableColumnText(pstCl, " 1 ",      5);
   HTTP_BuildTableColumnText(pstCl, " 2 ",      5);
   HTTP_BuildTableColumnText(pstCl, " 3 ",      5);
   HTTP_BuildTableColumnText(pstCl, " 4 ",      5);
   HTTP_BuildTableColumnText(pstCl, " 5 ",      5);
   HTTP_BuildTableColumnText(pstCl, " 6 ",      5);
   HTTP_BuildTableColumnText(pstCl, " 7 ",      5);
   HTTP_BuildTableRowEnd(pstCl);
   //
   for(i=0; i<iSize/8; i++)
   {
      slim_BuildRowHtmlMiac( pstCl, iMode, iAddr);
      iAddr += 8;
   }
   HTTP_BuildTableEnd(pstCl);
   //
   HTTP_BuildGeneric(pstCl, pcWebPageFontEnd);
   fCc = HTTP_BuildGeneric(pstCl, pcWebPageEnd);
   return(fCc);
}

// 
// Function:   slim_BuildSmartData
// Purpose:    Build HTML or JSON image
// 
// Parameters: Object, Buffer, Buffer size, num lines, double ptr, ScaleIdx
// Returns:    Object
// Note:       Object->tType knows HTML or JSON
//
static RPIDATA *slim_BuildSmartData(RPIDATA *pstObj, char *pcBuffer, int iSize, int iMaxLines, double *pflVal, int iFs)
{
   int      x, iIdx, iLine;
   char     cIcon;
   char     cLedgeY[4];
   double   flRes, flFs, flVal, flTh;

   //
   // iFs has the correct value for this plot
   //
   flFs  = flScales[iFs];
   flRes = flFs / iMaxLines;
   PRINTF("slim-BuildSmartData():scale=%5.1f" CRLF, flRes);
   //
   // Buffer hold records of symbols + '\0': Preserve the ASCIIZ terminator !
   //
   for(x=0; x<iSize-1; x++)
   {
      iIdx  = x;
      flVal = pflVal[x];
      if(flVal > flFs) cIcon = 'O';
      else             cIcon = 'X';
      //
      flTh = (double)0.0;
      for(iLine=0; iLine<iMaxLines; iLine++)
      {
         if(flVal > flTh) pcBuffer[iIdx] = cIcon;
         else             pcBuffer[iIdx] = '.';
         flTh += flRes;
         iIdx += iSize;
      }
   }
   //
   // Put out buffer line by line (buffer is mirrored)
   //
   switch(pstObj->tType)
   {
      default:
      case HTTP_HTML:
         do
         {
            GEN_SNPRINTF(cLedgeY, 3, "%02d", iLine);
            iLine--;
            pstObj = HTML_InsertData(pstObj, cLedgeY, 0);
            pstObj = HTML_InsertData(pstObj, &pcBuffer[iLine*iSize], HE_SPACE2|HE_VLS|HE_VLE|HE_BR);
         }
         while(iLine);
         break;

      case HTTP_JSON:
         do
         {
            iLine--;
            pstObj = JSON_InsertParameter(pstObj, NULL, &pcBuffer[iLine*iSize], JE_TEXT|JE_COMMA|JE_CRLF);
         }
         while(iLine);
         break;
   }
   return(pstObj);
}

//
// Function:   slim_CopySmartData
// Purpose:    Copy Flt.Point data from live source to display buffer
//
// Parms:      Period (day, month)
// Returns:    
// Note:       Copy to G_stDispDay/Month
//
static void slim_CopySmartData(KINSP tPer)
{
   switch(tPer)
   {
      default:
         break;

      case KINS_DAY:
         GEN_MEMCPY((u_int8 *)pstMap->G_stDispDay, (u_int8 *)pstMap->G_stThisDay, sizeof(pstMap->G_stDispDay));
         break;

      case KINS_MONTH:
         GEN_MEMCPY((u_int8 *)pstMap->G_stDispMonth, (u_int8 *)pstMap->G_stThisMonth, sizeof(pstMap->G_stDispMonth));
         break;
   }
   pstMap->G_ulSmartSecs = RTC_GetSystemSecs();
   slim_UpdateYearMonthDay();
}

// 
// Function:   slim_GetPeriodicData
// Purpose:    Get all periodic fp data from data struct
// 
// Parameters: EMS, Data ptr, Dest ptr, Nr of elements
// Returns:    
// Note:       
//
static void slim_GetPeriodicData(EMS tEms, SMDATA *pstDst, double *pflVar, int iNr)
{
   int      i;
   double   flVar;

   for(i=0; i<iNr; i++, pstDst++) 
   {
      switch(tEms)
      {
         case EMS_LEV: flVar = pstDst->flWaterLevel;    break;
         case EMS_TMP: flVar = pstDst->flTemperature;   break;
         case EMS_DIS: flVar = pstDst->flDistance;      break;
         case EMS_PMP: flVar = pstDst->flPumpOn;        break;
         case EMS_X10: flVar = pstDst->flX10Bypass;     break;
         default:      flVar = 0.0;                     break;
      }
      pflVar[i] = flVar;
   }
}

// 
// Function:   slim_InitDynamicPages
// Purpose:    Register dynamic webpages
// 
// Parameters: Port
// Returns:    
// Note:       
//
static void slim_InitDynamicPages(int iPort)
{
   const NETDYN  *pstDyn=stDynamicPages;

   PRINTF("slim-InitDynamicPages(): port=%d" CRLF, iPort);
   //
   while(pstDyn->pcUrl)
   {
      //PRINTF("slim-InitDynamicPages():Url=%s" CRLF, pstDyn->pcUrl);
      if(pstDyn->iFlags & DYN_FLAG_PORT)
      {
         //
         // Register for this specific port
         //
         GEN_RegisterDynamicPage(pstDyn->tUrl, -1, pstDyn->tType, pstDyn->iTimeout, iPort, pstDyn->pcUrl, (PFVOIDPI)pstDyn->pfDynCb);
      }
      else
      {
         //
         // Register for all ports
         //
         GEN_RegisterDynamicPage(pstDyn->tUrl, -1, pstDyn->tType, pstDyn->iTimeout,     0, pstDyn->pcUrl, (PFVOIDPI)pstDyn->pfDynCb);
      }
      pstDyn++;
   }
}

//
// Function:   slim_IsToday
// Purpose:    Check if the secs is today
//
// Parms:      ulSecs
// Returns:    TRUE if today
// Note:       
//
static bool slim_IsToday(u_int32 ulSecs)
{
   bool     fCc=FALSE;
   u_int32  ulMidnight;
   u_int32  ulTomorrow;

   ulMidnight = RTC_GetMidnight(0);
   ulTomorrow = ulMidnight + (24*60*60);
   //
   if( (ulSecs >= ulMidnight) && (ulSecs < ulTomorrow) ) fCc = TRUE;
   return(fCc);
}

//
// Function:   slim_RetrieveDisplayData
// Purpose:    Retrieve the correct data to display (From file, Live data or none)
//
// Parms:      ulSecs or 0, Period
// Returns:    ulSecs
// Note:       
//
static u_int32 slim_RetrieveDisplayData(u_int32 ulSecs, KINSP tPer)
{
   if(ulSecs == 0) ulSecs = pstMap->G_ulSmartSecs;
   //
   switch(tPer)
   {
      case KINS_DAY:
         if(slim_IsToday(ulSecs)) slim_CopySmartData(KINS_DAY);
         else                     RPI_RestoreDay(ulSecs, (u_int8 *)pstMap->G_stDispDay, sizeof(pstMap->G_stDispDay));
         break;

      case KINS_MONTH:
         if(slim_IsToday(ulSecs)) slim_CopySmartData(KINS_MONTH);
         else                     RPI_RestoreMonth(ulSecs, (u_int8 *)pstMap->G_stDispMonth, sizeof(pstMap->G_stDispMonth));
         break;

      default:
         break;
   }
   return(ulSecs);
}

//
// Function:   slim_ShowFilesHtml
// Purpose:    Show files in directory from disk 
// 
// Parameters: Client, dir name, filter, header
// Returns:    TRUE if OKee
// Note:       
//
static bool slim_ShowFilesHtml(NETCL *pstCl, char *pcDir, char *pcFilter, const char *pcHeader)
{
   bool     fCc=FALSE;
   int      x, iNum=0;
   char     cForD;
   char    *pcFileName;
   char    *pcFullPath;
   char    *pcFilePath;
   char    *pcTemp;
   void    *pvData;

   pcFileName = (char *) safemalloc(MAX_PATH_LEN);
   pcFilePath = (char *) safemalloc(MAX_PATH_LEN);
   pcFullPath = (char *) safemalloc(MAX_PATH_LEN);
   pcTemp     = (char *) safemalloc(MAX_PATH_LEN);
   //
   GEN_SPRINTF(pcFullPath, "%s%s", pstMap->G_pcRamDir, pcDir);
   LOG_Report(0, "DYN", "slim-ShowFilesHtml():Path=[%s] Filter=%s", pcFullPath, pcFilter);
   PRINTF("slim-ShowFilesHtml():Path=[%s] Filter=%s" CRLF, pcFullPath, pcFilter);
   //
   pvData = FINFO_GetDirEntries(pcFullPath, pcFilter, &iNum);
   if(iNum > 0)
   {
      HTTP_BuildRespondHeader(pstCl);
      HTTP_BuildStart(pstCl);
      //
      // Build table
      //
      HTTP_BuildTableStart(pstCl, pcHeader, 1, 70, 3);
      //
      for (x=0; x<iNum; x++)
      {
         FINFO_GetFilename(pvData, x, pcFileName, MAX_PATH_LEN);
         GEN_SPRINTF(pcFilePath, "%s%s", pcFullPath, pcFileName);
         FINFO_GetFileInfo(pcFilePath, FI_FILEDIR, &cForD, 1);
         PRINTF("slim-ShowFilesHtml(): Filter(%s)-File(%c):[%s]" CRLF, pcFilter, cForD, pcFileName);
         if( cForD == 'F')
         {
            GEN_SPRINTF(pcTemp, "%s%s", pcFullPath, pcFileName);
            HTTP_BuildTableRowStart(pstCl, 6);
            HTTP_BuildTableColumnLink(pstCl, pcFileName, 25, pcTemp);
            FINFO_GetFileInfo(pcFilePath, FI_SIZE, pcTemp, MAX_PATH_LEN);
            HTTP_BuildTableColumnText(pstCl, pcTemp, 15);
            FINFO_GetFileInfo(pcFilePath, FI_MDATE, pcTemp, MAX_PATH_LEN);
            HTTP_BuildTableColumnText(pstCl, pcTemp, 30);
            HTTP_BuildTableRowEnd(pstCl);
         }
      }
      FINFO_ReleaseDirEntries(pvData, iNum);
      HTTP_BuildTableEnd(pstCl);
      HTTP_BuildLineBreaks(pstCl, 1);
      HTTP_BuildEnd(pstCl);
      fCc = TRUE;
   }
   else
   {
      PRINTF("slim-ShowFilesHtml(): No file/dir entries" CRLF);
      HTTP_BuildRespondHeader(pstCl);
      HTTP_BuildStart(pstCl);
      HTTP_BuildGeneric(pstCl, pcWebPageStopBad);
      HTTP_BuildLineBreaks(pstCl, 1);
      HTTP_BuildEnd(pstCl);
   }
   safefree(pcFileName);
   safefree(pcFilePath);
   safefree(pcFullPath);
   safefree(pcTemp);
   return(fCc);
}

//
// Function:   slim_UpdateYearMonthDay
// Purpose:    Update current global G_pcYear, G_pcMonth, G_pcDay from G_ulSmartSecs
//
// Parms:      
// Returns:    
// Note:       
//
static void slim_UpdateYearMonthDay()
{
   u_int32 ulSecs;

   ulSecs = pstMap->G_ulSmartSecs;
   GEN_SNPRINTF(pstMap->G_pcYear,  MAX_PARM_LEN, "%d", RTC_GetYear(ulSecs));
   GEN_SNPRINTF(pstMap->G_pcMonth, MAX_PARM_LEN, "%d", RTC_GetMonth(ulSecs));
   GEN_SNPRINTF(pstMap->G_pcDay,   MAX_PARM_LEN, "%d", RTC_GetDay(ulSecs));
}

//
// Function:   slim_UpdateGlobalSecs
// Purpose:    Update global G_ulSecs from GL YMD
//
// Parms:      
// Returns:    Secs from G_ YMD
// Note:       The call will return the system second (since 1970) on the YMD given, but
//             at the H:M:S of the time now.
//
static u_int32 slim_UpdateGlobalSecs()
{
   int      iYear, iMonth, iDay;
   u_int32  ulSecs, ulSecsNow;

   iYear  = atoi(pstMap->G_pcYear);
   iMonth = atoi(pstMap->G_pcMonth);
   iDay   = atoi(pstMap->G_pcDay);
   //
   ulSecsNow = RTC_GetSystemSecs();
   ulSecs    = RTC_GetSecs(iYear, iMonth, iDay);
   ulSecs   += (RTC_GetHour(ulSecsNow) * 3600);
   ulSecs   += (RTC_GetMinute(ulSecsNow) * 60);
   ulSecs   +=  RTC_GetSecond(ulSecsNow);
   //
   pstMap->G_ulSmartSecs = ulSecs;
   PRINTF("slim_UpdateGlobalSecs():YMD = %04d-%02d-%02d (%d Secs)" CRLF, iYear, iMonth, iDay, (int) ulSecs);
   return(ulSecs);
}


