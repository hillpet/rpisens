/*  (c) Copyright:  2020..2024  Patrn, Confidential Data
 *
 *  Workfile:           rpi_comm.h
 *  Purpose:            Headerfile for rpi_comm.c
 *                      Ultrasoon sensor :  HC-SR04
 *                      Settings         :  9600 baud, 8 bits, no parity
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *  Note:               Raspberry Pi has several ways to access the serial port. For each type this differs:
 *                      
 *                      RPI-1: /dev/ttyAMA0 is the first UART
 *                      RPI-2: /dev/ttyAMA0 is the first UART
 *                      RPI-3: /dev/ttyAMA0 is the Bluetooth device (chosen because of the high bandwidth)
 *                             /dev/ttyS0   is the first UART
 *                      RPI-4: /dev/ttyAMA0 is the Bluetooth device (chosen because of the high bandwidth)
 *                             /dev/ttyS0   is the first UART
 *
 *                      To overcome incompatibilities, raspbian (from 20160510) has two aliases for this:
 *
 *                            /dev/serial0 is ttyS0   (UART0)
 *                            /dev/serial1 is ttyAMA0 (Bluetooth)
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    01 Nov 2020:      Ported from rpislim
 *    23 Apr 2024:      Split globals.h and locals.h
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _RPI_COMM_H_
#define _RPI_COMM_H_

int      COMM_Init            (void);
u_int16  COMM_CalculateCrc    (const u_int8 *, int);
//
#define  SMARTS_COMM_NON_BLOCKING
#define  SMARTS_CYCLE_SECS    10
#define  SMARTS_TIMEOUT       ((SMARTS_CYCLE_SECS+2)*1000)
//
typedef enum  _smart_commands_
{
#define  EXTRACT_SMC(a,b)   a,
#include "gen_cmds.h"
#undef   EXTRACT_SMC
}  SMCMD;
//
enum
{
   STATE_READ_DATA      = 0,
   STATE_READ_CRC,
   //
   NUM_STATES
};
//
enum
{
   SMART_SENSOR_IDLE=0,                // Idle: Idle
   SMART_SENSOR_XFER,                  // Xfer: Transfer RAM/EEPROM data
   SMART_SENSOR_RECS,                  // Recs: Read sensor distance records
   SMART_SENSOR_SIM,                   // SIM : Update values
   SMART_SENSOR_CRC                    // SIM : Update CRC
};

#endif /* _RPI_COMM_H_ */
